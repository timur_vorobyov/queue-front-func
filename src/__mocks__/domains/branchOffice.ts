import { BranchOffice } from "src/types/Admin/SuperAdmin/BranchOffice/branchOffice";
import { getTotalPages, paginationFilter, searchFilter } from "../utils";
import { mockedTowns } from "./town";
import { mockedVisitPurposes } from "./visitPurpose";

export const mockedBranchOffices: BranchOffice[] = [
  {
    id: 1,
    name: "Алиф",
    town: {
      id: mockedTowns[1].id,
      name: mockedTowns[1].name,
    },
    initial: "A",
    timer: 5000,
    visitPurposes: [
      {
        id: mockedVisitPurposes[1].id,
        name: mockedVisitPurposes[1].name,
      },
    ],
  },
  {
    id: 2,
    name: "МХБ",
    town: {
      id: mockedTowns[0].id,
      name: mockedTowns[0].name,
    },
    initial: "B",
    timer: 5000,
    visitPurposes: [
      {
        id: mockedVisitPurposes[0].id,
        name: mockedVisitPurposes[0].name,
      },
    ],
  },
  {
    id: 3,
    name: "Artel",
    town: {
      id: mockedTowns[3].id,
      name: mockedTowns[3].name,
    },
    initial: "F",
    timer: 5000,
    visitPurposes: [
      {
        id: mockedVisitPurposes[0].id,
        name: mockedVisitPurposes[0].name,
      },
    ],
  },
  {
    id: 4,
    name: "Авранг",
    town: {
      id: mockedTowns[4].id,
      name: mockedTowns[4].name,
    },
    initial: "I",
    timer: 5000,
    visitPurposes: [
      {
        id: mockedVisitPurposes[0].id,
        name: mockedVisitPurposes[0].name,
      },
    ],
  },
  {
    id: 5,
    name: "Kogan",
    town: {
      id: mockedTowns[5].id,
      name: mockedTowns[5].name,
    },
    initial: "Pайзап",
    timer: 5000,
    visitPurposes: [
      {
        id: mockedVisitPurposes[0].id,
        name: mockedVisitPurposes[0].name,
      },
    ],
  },
  {
    id: 6,
    name: "Волна-2",
    town: {
      id: mockedTowns[5].id,
      name: mockedTowns[5].name,
    },
    initial: "PD",
    timer: 5000,
    visitPurposes: [
      {
        id: mockedVisitPurposes[0].id,
        name: mockedVisitPurposes[0].name,
      },
    ],
  },
  {
    id: 7,
    name: "Волна",
    town: {
      id: mockedTowns[5].id,
      name: mockedTowns[5].name,
    },
    initial: "ЩС",
    timer: 5000,
    visitPurposes: [
      {
        id: mockedVisitPurposes[0].id,
        name: mockedVisitPurposes[0].name,
      },
    ],
  },
  {
    id: 8,
    name: "Сомон",
    town: {
      id: mockedTowns[0].id,
      name: mockedTowns[0].name,
    },
    initial: "К",
    timer: 5000,
    visitPurposes: [
      {
        id: mockedVisitPurposes[0].id,
        name: mockedVisitPurposes[0].name,
      },
    ],
  },
  {
    id: 9,
    name: "Нидур",
    town: {
      id: mockedTowns[0].id,
      name: mockedTowns[0].name,
    },
    initial: "С",
    timer: 5000,
    visitPurposes: [
      {
        id: mockedVisitPurposes[0].id,
        name: mockedVisitPurposes[0].name,
      },
    ],
  },
  {
    id: 10,
    name: "Зисор",
    town: {
      id: mockedTowns[0].id,
      name: mockedTowns[0].name,
    },
    initial: "L",
    timer: 5000,
    visitPurposes: [
      {
        id: mockedVisitPurposes[0].id,
        name: mockedVisitPurposes[0].name,
      },
    ],
  },
  {
    id: 11,
    name: "Бат",
    town: {
      id: mockedTowns[0].id,
      name: mockedTowns[0].name,
    },
    initial: "O",
    timer: 5000,
    visitPurposes: [
      {
        id: mockedVisitPurposes[0].id,
        name: mockedVisitPurposes[0].name,
      },
    ],
  },
];

export const getMockedPaginatedBranchOfficesList = (
  townId: number,
  searchExpression: string,
  limit: number,
  page: number
) => {
  let filteredMockedBranchOffices = mockedBranchOffices;
  if (townId) {
    filteredMockedBranchOffices = filteredMockedBranchOffices.filter(
      (mockedBranchOffice) => mockedBranchOffice.town.id === townId
    );
  }
  filteredMockedBranchOffices = searchFilter(
    filteredMockedBranchOffices,
    searchExpression
  );
  filteredMockedBranchOffices = paginationFilter(
    filteredMockedBranchOffices,
    limit,
    page
  );
  const totalRecords = mockedBranchOffices.length;

  return {
    paginationData: {
      currentPage: page,
      totalPages: getTotalPages(totalRecords, limit),
      totalRecords,
    },
    branchOffices: filteredMockedBranchOffices,
  };
};
