import { Permission } from "src/types/Admin/SuperAdmin/Permission/permission";
import { getTotalPages, paginationFilter, searchFilter } from "../utils";

export const mockedPermissions: Permission[] = [
  {
    id: 1,
    name: "admin access",
  },
  {
    id: 2,
    name: "crm developer",
  },
  {
    id: 3,
    name: "crm clients",
  },
  {
    id: 4,
    name: "crm partners",
  },
  {
    id: 5,
    name: "crm documents",
  },
  {
    id: 6,
    name: "crm reception",
  },
  {
    id: 7,
    name: "news online",
  },
  {
    id: 8,
    name: "crm audit",
  },
  {
    id: 9,
    name: "service payment",
  },
  {
    id: 10,
    name: "crm hr",
  },
  {
    id: 11,
    name: "crm consultants",
  },
];

export const getMockedPaginatedPermissionsList = (
  searchExpression: string,
  limit: number,
  page: number
) => {
  let filteredMockedPermissions = mockedPermissions;

  filteredMockedPermissions = searchFilter(
    filteredMockedPermissions,
    searchExpression
  );
  filteredMockedPermissions = paginationFilter(
    filteredMockedPermissions,
    limit,
    page
  );
  const totalRecords = mockedPermissions.length;

  return {
    paginationData: {
      currentPage: page,
      totalPages: getTotalPages(totalRecords, limit),
      totalRecords,
    },
    permissions: filteredMockedPermissions,
  };
};
