import { Role } from "src/types/Admin/SuperAdmin/Role/role";
import { getTotalPages, paginationFilter, searchFilter } from "../utils";
import { mockedPermissions } from "./permission";

export const mockedRoles: Role[] = [
  {
    id: 1,
    name: "admin",
    permissions: [mockedPermissions[0]],
  },
  {
    id: 2,
    name: "hall admin",
    permissions: [mockedPermissions[1]],
  },
  {
    id: 3,
    name: "super admin",
    permissions: [mockedPermissions[2]],
  },
  {
    id: 4,
    name: "user",
    permissions: [mockedPermissions[5]],
  },
  {
    id: 5,
    name: "manager",
    permissions: [mockedPermissions[4]],
  },
  {
    id: 6,
    name: "customer",
    permissions: [mockedPermissions[3]],
  },
];

export const getMockedPaginatedRolesList = (
  searchExpression: string,
  limit: number,
  page: number
) => {
  let filteredMockedRoles = mockedRoles;

  filteredMockedRoles = searchFilter(filteredMockedRoles, searchExpression);
  filteredMockedRoles = paginationFilter(filteredMockedRoles, limit, page);
  const totalRecords = mockedPermissions.length;

  return {
    paginationData: {
      currentPage: page,
      totalPages: getTotalPages(totalRecords, limit),
      totalRecords,
    },
    roles: filteredMockedRoles,
  };
};
