import { Service } from "src/types/Admin/SuperAdmin/Service/service";
import { getTotalPages, paginationFilter, searchFilter } from "../utils";

export const mockedServices: Service[] = [
  {
    id: 1,
    name: "Подписание",
    isActive: true,
  },
  {
    id: 2,
    name: "Новая заявка",
    isActive: true,
  },
  {
    id: 3,
    name: "Заявка на карту",
    isActive: false,
  },
  {
    id: 4,
    name: "Получение карты",
    isActive: false,
  },
  {
    id: 5,
    name: "Кассовые операции",
    isActive: true,
  },
  {
    id: 6,
    name: "Денежные переводы/SWIFT",
    isActive: false,
  },
  {
    id: 7,
    name: "Пополнение карты/депозита",
    isActive: true,
  },
  {
    id: 8,
    name: "Конвертация",
    isActive: true,
  },
  {
    id: 9,
    name: "Снятие и пополнение счетов",
    isActive: true,
  },
  {
    id: 10,
    name: "Снятие с КМ",
    isActive: true,
  },
  {
    id: 11,
    name: "Снятие с карты Visa",
    isActive: true,
  },
];

export const getMockedPaginatedServicesList = (
  searchExpression: string,
  limit: number,
  page: number
) => {
  let filteredMockedServices = mockedServices;

  filteredMockedServices = searchFilter(
    filteredMockedServices,
    searchExpression
  );
  filteredMockedServices = paginationFilter(
    filteredMockedServices,
    limit,
    page
  );
  const totalRecords = mockedServices.length;

  return {
    paginationData: {
      currentPage: page,
      totalPages: getTotalPages(totalRecords, limit),
      totalRecords,
    },
    services: filteredMockedServices,
  };
};
