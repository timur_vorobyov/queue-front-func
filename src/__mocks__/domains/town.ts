import { Town } from "src/types/Admin/SuperAdmin/town";

export const mockedTowns: Town[] = [
  {
    id: 1,
    name: "Душанбе",
  },
  {
    id: 2,
    name: "Худжанд",
  },
  {
    id: 3,
    name: "Вахдат",
  },
  {
    id: 4,
    name: "Бохтар",
  },
  {
    id: 5,
    name: "Пенджикент",
  },
  {
    id: 6,
    name: "Истаравшан",
  },
  {
    id: 7,
    name: "Турсунзода",
  },
];
