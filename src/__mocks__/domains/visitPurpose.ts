import { VisitPurpose } from "src/types/Admin/visitPurpose";

export const mockedVisitPurposes: VisitPurpose[] = [
  {
    id: 1,
    name: "Касса",
  },
  {
    id: 2,
    name: "Рассрочка",
  },
];
