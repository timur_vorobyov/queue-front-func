import { rest } from "msw";
import { PaginationData } from "src/components/Pagination/Pagination";
import { routes } from "src/config/routes";
import { BranchOffice } from "src/types/Admin/SuperAdmin/branchOffice";
import { getMockedPaginatedBranchOfficesList } from "../domains/branchOffice";

export const GET_PAGINATED_BRANCH_OFFICES_LIST_RESPONSE = (
  townId: number,
  searchExpression: string,
  limit: number,
  page: number
) => {
  const {
    branchOffices,
    paginationData,
  }: {
    branchOffices: BranchOffice[];
    paginationData: PaginationData;
  } = getMockedPaginatedBranchOfficesList(
    townId,
    searchExpression,
    limit,
    page
  );

  return [
    rest.get(
      routes.api.superAdmin.getPaginatedBranchOfficesList,
      (req, res, ctx) => {
        const townId = req.url.searchParams.get("townId");
        const searchExpression = req.url.searchParams.get("searchExpression");
        const limit = req.url.searchParams.get("limit");
        const page = req.url.searchParams.get("page");

        if (
          townId === null ||
          searchExpression === null ||
          limit === null ||
          page === null
        ) {
          return res(
            ctx.status(403),
            ctx.json({
              errorMessage: "One of the params is missing",
            })
          );
        }
        return res(
          ctx.status(200),
          ctx.json({
            pagination: {
              currentPage: paginationData.currentPage,
              totalPages: paginationData.totalPages,
              totalRecords: paginationData.totalRecords,
            },
            branchOfficesPureList: branchOffices,
          })
        );
      }
    ),
  ];
};

export const GET_SECOND_PAGE_PAGINATED_BRANCH_OFFICES_LIST_RESPONSE = (
  townId: number,
  searchExpression: string,
  limit: number,
  page: number
) => {
  const {
    branchOffices,
    paginationData,
  }: {
    branchOffices: BranchOffice[];
    paginationData: PaginationData;
  } = getMockedPaginatedBranchOfficesList(
    townId,
    searchExpression,
    limit,
    page
  );

  return [
    rest.get(
      routes.api.superAdmin.getPaginatedBranchOfficesList,
      (req, res, ctx) => {
        const pageQueryParam = req.url.searchParams.get("page");

        if (pageQueryParam !== page.toString()) {
          return res(
            ctx.status(403),
            ctx.json({
              errorMessage: "The pagination is broken",
            })
          );
        }

        return res(
          ctx.status(200),
          ctx.json({
            pagination: {
              currentPage: paginationData.currentPage,
              totalPages: paginationData.totalPages,
              totalRecords: paginationData.totalRecords,
            },
            branchOfficesPureList: branchOffices,
          })
        );
      }
    ),
  ];
};

export const GET_BRANCH_OFFICES_WITH_INCRAESED_LIMIT_LIST_RESPONSE = (
  townId: number,
  searchExpression: string,
  limit: number,
  page: number
) => {
  const {
    branchOffices,
    paginationData,
  }: {
    branchOffices: BranchOffice[];
    paginationData: PaginationData;
  } = getMockedPaginatedBranchOfficesList(
    townId,
    searchExpression,
    limit,
    page
  );

  return [
    rest.get(
      routes.api.superAdmin.getPaginatedBranchOfficesList,
      (req, res, ctx) => {
        const pageQueryParam = req.url.searchParams.get("page");
        const limitQueryParam = req.url.searchParams.get("limit");

        if (
          pageQueryParam !== page.toString() ||
          limitQueryParam !== limit.toString()
        ) {
          return res(
            ctx.status(403),
            ctx.json({
              errorMessage: "The limit is broken",
            })
          );
        }
        return res(
          ctx.status(200),
          ctx.json({
            pagination: {
              currentPage: paginationData.currentPage,
              totalPages: paginationData.totalPages,
              totalRecords: paginationData.totalRecords,
            },
            branchOfficesPureList: branchOffices,
          })
        );
      }
    ),
  ];
};

export const GET_BRANCH_OFFICES_FILTERED_BY_SEARCHER_LIST_RESPONSE = (
  townId: number,
  searchExpression: string,
  limit: number,
  page: number
) => {
  const {
    branchOffices,
    paginationData,
  }: {
    branchOffices: BranchOffice[];
    paginationData: PaginationData;
  } = getMockedPaginatedBranchOfficesList(
    townId,
    searchExpression,
    limit,
    page
  );

  return [
    rest.get(
      routes.api.superAdmin.getPaginatedBranchOfficesList,
      (req, res, ctx) => {
        const searchExpressionQueryParam = req.url.searchParams.get(
          "searchExpression"
        );

        if (searchExpressionQueryParam !== searchExpression) {
          return res(
            ctx.status(403),
            ctx.json({
              errorMessage: "The searcher is broken",
            })
          );
        }
        return res(
          ctx.status(200),
          ctx.json({
            pagination: {
              currentPage: paginationData.currentPage,
              totalPages: paginationData.totalPages,
              totalRecords: paginationData.totalRecords,
            },
            branchOfficesPureList: branchOffices,
          })
        );
      }
    ),
  ];
};

export const GET_BRANCH_OFFICES_FILTERED_BY_TOWN_LIST_RESPONSE = (
  townId: number,
  searchExpression: string,
  limit: number,
  page: number
) => {
  const {
    branchOffices,
    paginationData,
  }: {
    branchOffices: BranchOffice[];
    paginationData: PaginationData;
  } = getMockedPaginatedBranchOfficesList(
    townId,
    searchExpression,
    limit,
    page
  );

  return [
    rest.get(
      routes.api.superAdmin.getPaginatedBranchOfficesList,
      (req, res, ctx) => {
        const towmQueryParam = req.url.searchParams.get("townId");

        if (towmQueryParam !== townId.toString()) {
          return res(
            ctx.status(403),
            ctx.json({
              errorMessage: "The town filter is broken",
            })
          );
        }
        return res(
          ctx.status(200),
          ctx.json({
            pagination: {
              currentPage: paginationData.currentPage,
              totalPages: paginationData.totalPages,
              totalRecords: paginationData.totalRecords,
            },
            branchOfficesPureList: branchOffices,
          })
        );
      }
    ),
  ];
};

export const DELETE_BRANCH_OFFICE_RESPONSE = [
  rest.delete(
    routes.api.superAdmin.deleteBranchOffice + ":id",
    (req, res, ctx) => {
      const { id } = req.params;

      if (id !== "1") {
        return res(
          ctx.status(403),
          ctx.json({
            errorMessage: "The remove logic is broken",
          })
        );
      }
      return res(
        ctx.status(200),
        ctx.json({ message: "BRANCH_OFFICE_IS_REMOVED" })
      );
    }
  ),
];

export const STORE_BRANCH_OFFICE_RESPONSE = [
  rest.post(routes.api.superAdmin.storeBranchOffice, (req, res, ctx) => {
    const { townId, name, initial, timer, visitPurposesIds } = req.body as {
      townId: number;
      name: string;
      initial: string;
      timer: number;
      visitPurposesIds: number[];
    };
    if (
      !townId ||
      !name ||
      !initial ||
      !timer ||
      visitPurposesIds.length === 0
    ) {
      return res(
        ctx.status(403),
        ctx.json({
          errorMessage: "One of the params is missing",
        })
      );
    }

    return res(
      ctx.status(200),
      ctx.json({ message: "BRANCH_OFFICE_IS_SAVED" })
    );
  }),
];

export const STORE_BRANCH_OFFICE_WITH_THE_SAME_INITIAL_RESPONSE = [
  rest.post(routes.api.superAdmin.storeBranchOffice, (req, res, ctx) => {
    const { townId, name, initial, timer, visitPurposesIds } = req.body as {
      townId: number;
      name: string;
      initial: string;
      timer: number;
      visitPurposesIds: number[];
    };

    if (
      !townId ||
      !name ||
      !initial ||
      !timer ||
      visitPurposesIds.length === 0
    ) {
      return res(
        ctx.status(403),
        ctx.json({
          errorMessage: "One of the params is missing",
        })
      );
    }

    return res(ctx.status(200), ctx.json({ message: "INITIAL_ERROR" }));
  }),
];

export const UPDATE_BRANCH_OFFICE_RESPONSE = [
  rest.put(
    routes.api.superAdmin.updateBranchOffice + ":id",
    (req, res, ctx) => {
      const { id } = req.params;
      const { townId, name, initial, timer, visitPurposesIds } = req.body as {
        townId: number;
        name: string;
        initial: string;
        timer: number;
        visitPurposesIds: number[];
      };
      if (
        !id ||
        !townId ||
        !name ||
        !initial ||
        !timer ||
        visitPurposesIds.length === 0
      ) {
        return res(
          ctx.status(403),
          ctx.json({
            errorMessage: "One of the params is missing",
          })
        );
      }

      return res(
        ctx.status(200),
        ctx.json({ message: "BRANCH_OFFICE_IS_UPDATED" })
      );
    }
  ),
];

export const UPDATE_BRANCH_OFFICE_WITH_THE_SAME_INITIAL_RESPONSE = [
  rest.put(
    routes.api.superAdmin.updateBranchOffice + ":id",
    (req, res, ctx) => {
      const { id } = req.params;
      const { townId, name, initial, timer, visitPurposesIds } = req.body as {
        townId: number;
        name: string;
        initial: string;
        timer: number;
        visitPurposesIds: number[];
      };
      if (
        !id ||
        !townId ||
        !name ||
        !initial ||
        !timer ||
        !visitPurposesIds.includes(1)
      ) {
        return res(
          ctx.status(403),
          ctx.json({
            errorMessage: "One of the params is missing",
          })
        );
      }

      return res(ctx.status(200), ctx.json({ message: "INITIAL_ERROR" }));
    }
  ),
];
