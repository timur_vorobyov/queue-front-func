import { rest } from "msw";
import { PaginationData } from "src/components/Pagination/Pagination";
import { routes } from "src/config/routes";
import { Permission } from "src/types/Admin/SuperAdmin/Permission/permission";
import {
  getMockedPaginatedPermissionsList,
  mockedPermissions,
} from "../domains/permission";

export const GET_PERMISSIONS_LIST_RESPONSE = [
  rest.get(routes.api.getPermissionsList, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(mockedPermissions));
  }),
];

export const GET_PAGINATED_PERMISSIONS_LIST_RESPONSE = (
  searchExpression: string,
  limit: number,
  page: number
) => {
  const {
    permissions,
    paginationData,
  }: {
    permissions: Permission[];
    paginationData: PaginationData;
  } = getMockedPaginatedPermissionsList(searchExpression, limit, page);

  return [
    rest.get(
      routes.api.superAdmin.getPaginatedPermissionsList,
      (req, res, ctx) => {
        const searchExpression = req.url.searchParams.get("searchExpression");
        const limit = req.url.searchParams.get("limit");
        const page = req.url.searchParams.get("page");

        if (searchExpression === null || limit === null || page === null) {
          return res(
            ctx.status(403),
            ctx.json({
              errorMessage: "One of the params is missing",
            })
          );
        }
        return res(
          ctx.status(200),
          ctx.json({
            pagination: {
              currentPage: paginationData.currentPage,
              totalPages: paginationData.totalPages,
              totalRecords: paginationData.totalRecords,
            },
            permissionsPureList: permissions,
          })
        );
      }
    ),
  ];
};

export const DELETE_PERMISSION_RESPONSE = [
  rest.delete(
    routes.api.superAdmin.deletePermission + ":id",
    (req, res, ctx) => {
      const { id } = req.params;

      if (id !== "1") {
        return res(
          ctx.status(403),
          ctx.json({
            errorMessage: "The remove logic is broken",
          })
        );
      }
      return res(
        ctx.status(200),
        ctx.json({ message: "PERMISSION_IS_REMOVED" })
      );
    }
  ),
];

export const STORE_PERMISSION_RESPONSE = [
  rest.post(routes.api.superAdmin.storePermission, (req, res, ctx) => {
    const { name } = req.body as {
      name: string;
    };
    if (!name) {
      return res(
        ctx.status(403),
        ctx.json({
          errorMessage: "One of the params is missing",
        })
      );
    }

    return res(ctx.status(200), ctx.json({ message: "PERMISSION_IS_SAVED" }));
  }),
];

export const UPDATE_PERMISSION_RESPONSE = [
  rest.put(routes.api.superAdmin.updatePermission + ":id", (req, res, ctx) => {
    const { id } = req.params;

    if (!id) {
      return res(
        ctx.status(403),
        ctx.json({
          errorMessage: "One of the params is missing",
        })
      );
    }

    return res(ctx.status(200), ctx.json({ message: "PERMISSION_IS_UPDATED" }));
  }),
];
