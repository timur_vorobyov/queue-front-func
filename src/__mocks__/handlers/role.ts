import { PaginationData } from "src/components/Pagination/Pagination";
import { Role } from "src/types/Admin/SuperAdmin/Role/role";
import { rest } from "msw";
import { routes } from "src/config/routes";
import { getMockedPaginatedRolesList } from "../domains/role";

export const GET_PAGINATED_ROLES_LIST_RESPONSE = (
  searchExpression: string,
  limit: number,
  page: number
) => {
  const {
    roles,
    paginationData,
  }: {
    roles: Role[];
    paginationData: PaginationData;
  } = getMockedPaginatedRolesList(searchExpression, limit, page);

  return [
    rest.get(routes.api.superAdmin.getPaginatedRolesList, (req, res, ctx) => {
      const searchExpression = req.url.searchParams.get("searchExpression");
      const limit = req.url.searchParams.get("limit");
      const page = req.url.searchParams.get("page");

      if (searchExpression === null || limit === null || page === null) {
        return res(
          ctx.status(403),
          ctx.json({
            errorMessage: "One of the params is missing",
          })
        );
      }
      return res(
        ctx.status(200),
        ctx.json({
          pagination: {
            currentPage: paginationData.currentPage,
            totalPages: paginationData.totalPages,
            totalRecords: paginationData.totalRecords,
          },
          rolesPureList: roles,
        })
      );
    }),
  ];
};

export const DELETE_ROLE_RESPONSE = [
  rest.delete(routes.api.superAdmin.deleteRole + ":id", (req, res, ctx) => {
    const { id } = req.params;

    if (id !== "1") {
      return res(
        ctx.status(403),
        ctx.json({
          errorMessage: "The remove logic is broken",
        })
      );
    }
    return res(ctx.status(200), ctx.json({ message: "ROLE_IS_REMOVED" }));
  }),
];

export const STORE_ROLE_RESPONSE = [
  rest.post(routes.api.superAdmin.storeRole, (req, res, ctx) => {
    const { name, permissionIds } = req.body as {
      name: string;
      permissionIds: number[];
    };
    if (!name || permissionIds.length === 0) {
      return res(
        ctx.status(403),
        ctx.json({
          errorMessage: "One of the params is missing",
        })
      );
    }

    return res(ctx.status(200), ctx.json({ message: "ROLE_IS_SAVED" }));
  }),
];

export const UPDATE_ROLE_RESPONSE = [
  rest.put(routes.api.superAdmin.updateRole + ":id", (req, res, ctx) => {
    const { id } = req.params;
    const { name, permissionIds } = req.body as {
      name: string;
      permissionIds: number[];
    };

    if (!id || !name || permissionIds.length === 0) {
      return res(
        ctx.status(403),
        ctx.json({
          errorMessage: "One of the params is missing",
        })
      );
    }

    return res(ctx.status(200), ctx.json({ message: "ROLE_IS_UPDATED" }));
  }),
];
