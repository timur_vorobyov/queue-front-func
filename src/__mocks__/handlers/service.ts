import { PaginationData } from "src/components/Pagination/Pagination";
import { rest } from "msw";
import { routes } from "src/config/routes";
import { Service } from "src/types/Admin/SuperAdmin/Service/service";
import { getMockedPaginatedServicesList } from "../domains/service";

export const GET_PAGINATED_SERVICES_LIST_RESPONSE = (
  searchExpression: string,
  limit: number,
  page: number
) => {
  const {
    services,
    paginationData,
  }: {
    services: Service[];
    paginationData: PaginationData;
  } = getMockedPaginatedServicesList(searchExpression, limit, page);

  return [
    rest.get(
      routes.api.superAdmin.getPaginatedServicesList,
      (req, res, ctx) => {
        const searchExpression = req.url.searchParams.get("searchExpression");
        const limit = req.url.searchParams.get("limit");
        const page = req.url.searchParams.get("page");

        if (searchExpression === null || limit === null || page === null) {
          return res(
            ctx.status(403),
            ctx.json({
              errorMessage: "One of the params is missing",
            })
          );
        }
        return res(
          ctx.status(200),
          ctx.json({
            pagination: {
              currentPage: paginationData.currentPage,
              totalPages: paginationData.totalPages,
              totalRecords: paginationData.totalRecords,
            },
            servicesPureList: services,
          })
        );
      }
    ),
  ];
};

export const DELETE_SERVICE_RESPONSE = [
  rest.delete(routes.api.superAdmin.deleteService + ":id", (req, res, ctx) => {
    const { id } = req.params;

    if (id !== "1") {
      return res(
        ctx.status(403),
        ctx.json({
          errorMessage: "The remove logic is broken",
        })
      );
    }
    return res(ctx.status(200), ctx.json({ message: "SERVICE_IS_REMOVED" }));
  }),
];

export const STORE_SERVICE_RESPONSE = [
  rest.post(routes.api.superAdmin.storeService, (req, res, ctx) => {
    const { name, isActive } = req.body as {
      name: string;
      isActive: boolean;
    };
    if (!name || isActive) {
      return res(
        ctx.status(403),
        ctx.json({
          errorMessage: "One of the params is missing",
        })
      );
    }

    return res(ctx.status(200), ctx.json({ message: "SERVICE_IS_SAVED" }));
  }),
];

export const UPDATE_SERVICE_RESPONSE = [
  rest.put(routes.api.superAdmin.updateService + ":id", (req, res, ctx) => {
    const { id } = req.params;
    const { name, isActive } = req.body as {
      name: string;
      isActive: boolean;
    };

    if (!id || !name || isActive) {
      return res(
        ctx.status(403),
        ctx.json({
          errorMessage: "One of the params is missing",
        })
      );
    }

    return res(ctx.status(200), ctx.json({ message: "SERVICE_IS_UPDATED" }));
  }),
];
