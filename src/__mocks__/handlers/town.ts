import { rest } from "msw";
import { routes } from "src/config/routes";
import { mockedTowns } from "../domains/town";

export const GET_TOWNS_LIST_RESPONSE = [
  rest.get(routes.api.getTownsList, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(mockedTowns));
  }),
];
