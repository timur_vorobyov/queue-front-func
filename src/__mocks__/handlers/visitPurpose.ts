import { rest } from "msw";
import { routes } from "src/config/routes";
import { mockedVisitPurposes } from "../domains/visitPurpose";

export const GET_VISIT_PURPOSES_LIST_RESPONSE = [
  rest.get(routes.api.getVisitPurposesList, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(mockedVisitPurposes));
  }),
];
