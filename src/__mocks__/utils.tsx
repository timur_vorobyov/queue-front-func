import { render } from "@testing-library/react";
import React from "react";
import { BrowserRouter } from "react-router-dom";

export const searchFilter = (entity: any[], searchExpression: string) =>
  entity.filter((obj: { name: string }) => obj.name.includes(searchExpression));

export const paginationFilter = (entity: any[], limit: number, page: number) =>
  page !== 1 ? entity.slice(limit, page * limit) : entity.slice(0, limit);

export const getTotalPages = (length: number, limit: number) =>
  Math.ceil(length / limit);

export const renderWithRouter = (children: React.ReactNode) =>
  render(
    /* @ts-ignore */
    <BrowserRouter>{children}</BrowserRouter>
  );
