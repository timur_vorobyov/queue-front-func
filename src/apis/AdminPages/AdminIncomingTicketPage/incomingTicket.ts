import { routes } from "src/config/routes";
import { TICKET_STATUES } from "src/consts/ticketConsts";
import apiClient from "src/helpers/httpInterceptor";
import { Ticket } from "src/store/types/screenPageTicket";

export const getIncomingTickets = async (
  branchOfficeId: string,
  visitPurposeId: number
): Promise<Ticket[] | null> => {
  const response = await apiClient.get(routes.api.getIncomingTicketsList, {
    params: {
      branchOfficeId: branchOfficeId,
      visitPurposeId: visitPurposeId,
      statusName: TICKET_STATUES.NEW,
    },
  });
  return response.data;
};

export const removeTickets = async (
  incomingTicketIds: number[]
): Promise<string> => {
  const response = await apiClient.delete(routes.api.admin.removeTickets, {
    params: {
      ticketIds: incomingTicketIds,
    },
  });

  return response.data.message;
};
