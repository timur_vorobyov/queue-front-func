import apiClient from "src/helpers/httpInterceptor";
import { routes } from "src/config/routes";
import { PaginationData } from "src/components/Pagination/Pagination";
import {
  BranchOffice,
  BranchOfficeInModal,
} from "src/types/Admin/SuperAdmin/BranchOffice/branchOffice";

export const storeBranchOffice = async ({
  townId,
  name,
  initial,
  timer,
  visitPurposesIds,
}: BranchOfficeInModal): Promise<string> => {
  const response = await apiClient.post(
    routes.api.superAdmin.storeBranchOffice,
    {
      townId,
      name,
      initial,
      timer,
      visitPurposesIds,
    }
  );

  return response.data.message;
};

export const updateBranchOffice = async ({
  townId,
  name,
  initial,
  timer,
  visitPurposesIds,
  id,
}: BranchOfficeInModal): Promise<string> => {
  const response = await apiClient.put(
    routes.api.superAdmin.updateBranchOffice + encodeURIComponent(id),
    {
      townId,
      name,
      initial,
      timer,
      visitPurposesIds,
    }
  );
  return response.data.message;
};

export const deleteBranchOffice = async (id: number): Promise<string> => {
  const response = await apiClient.delete(
    routes.api.superAdmin.deleteBranchOffice + encodeURIComponent(id)
  );
  return response.data.message;
};

export const getPaginatedBranchOfficesList = async (
  townId: number,
  searchExpression: string,
  limit: number,
  page = 1
): Promise<{
  paginationData: PaginationData;
  branchOffices: BranchOffice[];
}> => {
  const response = await apiClient.get(
    routes.api.superAdmin.getPaginatedBranchOfficesList,
    {
      params: {
        townId,
        searchExpression,
        limit,
        page,
      },
    }
  );
  return {
    paginationData: response.data.pagination,
    branchOffices: formatBranchOfficesList(response.data.branchOfficesPureList),
  };
};

const formatBranchOfficesList = (branchOfficesList: []): BranchOffice[] => {
  const formattedBranchOffices = branchOfficesList.map(
    (bracnhOffice: BranchOffice) => ({
      id: bracnhOffice.id,
      name: bracnhOffice.name,
      town: bracnhOffice.town,
      initial: bracnhOffice.initial,
      timer: bracnhOffice.timer,
      visitPurposes: bracnhOffice.visitPurposes,
    })
  );

  return formattedBranchOffices;
};

/*
export const assosiateBranches = async (data: {
  crmBranchName: string | null;
  displayName: string | null;
  branchOfficeId: number;
}): Promise<boolean> => {
  const response = await apiClient.post(routes.api.crm.associateBranches, {
    crmBranchName: data.crmBranchName,
    displayName: data.displayName,
    id: data.branchOfficeId,
  });

  if (response.data.message == "BRANCHES_ARE_ASSOCIATED") {
    return true;
  }

  return false;
};

const getCrmBranches = (): Promise<CrmBranches> => {
  const response = awaiasync t apiClient.post(routes.api.crm.branches);
  return toCrmBranchesFrontFormat(response.data);
};

const toCrmBranchesFrontFormat = (crmBranchesList: []): CrmBranches => {
  const formattedCrmBranches = crmBranchesList.map(
    (crmBranch: CrmBranchBackendFromat) => ({
      crmBranchLabel: crmBranch.display_name,
      crmBranchName: crmBranch.name,
    })
  );

  return formattedCrmBranches;
};
*/
