import { PaginationData } from "src/components/Pagination/Pagination";
import { routes } from "src/config/routes";
import apiClient from "src/helpers/httpInterceptor";
import { PermissionInModal } from "src/types/Admin/SuperAdmin/Permission/permission";
import { Permission } from "src/types/Admin/SuperAdmin/Role/permission";

export const storePermission = async ({
  name,
}: PermissionInModal): Promise<string> => {
  const response = await apiClient.post(routes.api.superAdmin.storePermission, {
    name,
  });

  if (response.data.statusCode) {
    return response.data.statusCode;
  }

  return response.data.message;
};

export const updatePermission = async ({
  id,
  name,
}: PermissionInModal): Promise<string> => {
  const response = await apiClient.put(
    routes.api.superAdmin.updatePermission + encodeURIComponent(id),
    {
      name,
    }
  );
  return response.data.message;
};

export const deletePermission = async (id: number): Promise<string> => {
  const response = await apiClient.delete(
    routes.api.superAdmin.deletePermission + encodeURIComponent(id)
  );
  return response.data.message;
};

export const getPaginatedPermissionsList = async (
  searchExpression: string,
  limit: number,
  page = 1
): Promise<{
  paginationData: PaginationData;
  permissions: Permission[];
}> => {
  const response = await apiClient.get(
    routes.api.superAdmin.getPaginatedPermissionsList,
    {
      params: {
        searchExpression,
        limit,
        page,
      },
    }
  );

  return {
    paginationData: response.data.pagination,
    permissions: formatPermissionList(response.data.permissionsPureList),
  };
};

const formatPermissionList = (permissionsList: []): Permission[] => {
  const formatedPermissions = permissionsList.map((permission: Permission) => ({
    id: permission.id,
    name: permission.name,
  }));

  return formatedPermissions;
};
