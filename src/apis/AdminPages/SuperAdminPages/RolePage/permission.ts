import apiClient from "src/helpers/httpInterceptor";
import { routes } from "src/config/routes";
import { Permission } from "src/types/Admin/SuperAdmin/Role/permission";

export const getPermissionsList = async (): Promise<Permission[]> => {
  const response = await apiClient.get(routes.api.getPermissionsList);
  return formatPermissionsList(response.data);
};

const formatPermissionsList = (permissionsList: []): Permission[] => {
  const formatedPermissions = permissionsList.map((permission: Permission) => ({
    id: permission.id,
    name: permission.name,
  }));

  return formatedPermissions;
};
