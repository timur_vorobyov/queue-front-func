import apiClient from "src/helpers/httpInterceptor";
import { routes } from "src/config/routes";
import { PaginationData } from "src/components/Pagination/Pagination";
import { Role, RoleInModal } from "src/types/Admin/SuperAdmin/Role/role";

export const storeRole = async ({
  name,
  permissionIds,
}: RoleInModal): Promise<string> => {
  const response = await apiClient.post(routes.api.superAdmin.storeRole, {
    name,
    permissionIds,
  });

  if (response.data.statusCode) {
    return response.data.statusCode;
  }

  return response.data.message;
};

export const updateRole = async ({
  id,
  name,
  permissionIds,
}: RoleInModal): Promise<string> => {
  const response = await apiClient.put(
    routes.api.superAdmin.updateRole + encodeURIComponent(id),
    {
      name,
      permissionIds: permissionIds,
    }
  );

  return response.data.message;
};

export const deleteRole = async (id: number): Promise<string> => {
  const response = await apiClient.delete(
    routes.api.superAdmin.deleteRole + encodeURIComponent(id)
  );
  return response.data.message;
};

export const getPaginatedRolesList = async (
  searchExpression: string,
  limit: number,
  page = 1
): Promise<{
  paginationData: PaginationData;
  roles: Role[];
}> => {
  const response = await apiClient.get(
    routes.api.superAdmin.getPaginatedRolesList,
    {
      params: {
        searchExpression,
        limit,
        page,
      },
    }
  );
  return {
    paginationData: response.data.pagination,
    roles: formatRolesList(response.data.rolesPureList),
  };
};

const formatRolesList = (rolesList: []): Role[] => {
  const formatedRoles = rolesList.map((role: Role) => ({
    id: role.id,
    name: role.name,
    permissions: role.permissions,
  }));

  return formatedRoles;
};
