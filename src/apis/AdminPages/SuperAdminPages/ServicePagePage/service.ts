import apiClient from "src/helpers/httpInterceptor";
import { routes } from "src/config/routes";
import { PaginationData } from "src/components/Pagination/Pagination";
import {
  Service,
  ServiceInModal,
} from "src/types/Admin/SuperAdmin/Service/service";

export const storeService = async ({
  name,
  isActive,
}: ServiceInModal): Promise<string> => {
  const response = await apiClient.post(routes.api.superAdmin.storeService, {
    name,
    isActive,
  });

  if (response.data.statusCode) {
    return response.data.statusCode;
  }

  return response.data.message;
};

export const updateService = async ({
  id,
  name,
  isActive,
}: ServiceInModal): Promise<string> => {
  const response = await apiClient.put(
    routes.api.superAdmin.updateService + encodeURIComponent(id),
    {
      name,
      isActive,
    }
  );

  return response.data.message;
};

export const deleteService = async (id: number): Promise<string> => {
  const response = await apiClient.delete(
    routes.api.superAdmin.deleteService + encodeURIComponent(id)
  );
  return response.data.message;
};

export const getPaginatedServicesList = async (
  searchExpression: string,
  limit: number,
  page = 1
): Promise<{
  paginationData: PaginationData;
  services: Service[];
}> => {
  const response = await apiClient.get(
    routes.api.superAdmin.getPaginatedServicesList,
    {
      params: {
        searchExpression,
        limit,
        page,
      },
    }
  );

  return {
    paginationData: response.data.pagination,
    services: formatServicesList(response.data.servicesPureList),
  };
};

const formatServicesList = (servicesList: []): Service[] => {
  const formatedServices = servicesList.map((service: Service) => ({
    id: service.id,
    name: service.name,
    isActive: service.isActive,
  }));

  return formatedServices;
};
