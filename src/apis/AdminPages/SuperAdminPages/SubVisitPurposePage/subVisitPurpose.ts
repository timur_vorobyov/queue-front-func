import apiClient from "src/helpers/httpInterceptor";
import { routes } from "src/config/routes";
import {
  SubVisitPurpose,
  SubVisitPurposeInModal,
} from "src/types/Admin/SuperAdmin/SubVisitPurpose/subVisitPurpose";
import { PaginationData } from "src/components/Pagination/Pagination";

type SubVisitPurposeDbType = {
  id: number;
  name: string;
  visitPurpose: {
    id: number;
    name: string;
  };
};

export const storeSubVisitPurpose = async ({
  visitPurposeId,
  name,
}: SubVisitPurposeInModal): Promise<string> => {
  const response = await apiClient.post(
    routes.api.superAdmin.storeSubVisitPurpose,
    {
      name,
      visitPurposeId,
    }
  );

  return response.data.message;
};

export const updateSubVisitPurpose = async ({
  visitPurposeId,
  name,
  id,
}: SubVisitPurposeInModal): Promise<string> => {
  const response = await apiClient.put(
    routes.api.superAdmin.updateSubVisitPurpose + encodeURIComponent(id),
    {
      name,
      visitPurposeId,
    }
  );

  return response.data.message;
};

export const deleteSubVisitPurpose = async (id: number) => {
  const response = await apiClient.delete(
    routes.api.superAdmin.deleteSubVisitPurpose + encodeURIComponent(id)
  );

  return response.data.message;
};

export const getPaginatedSubVisitPurposesList = async (
  searchExpression: string,
  limit: number,
  page = 1
): Promise<{
  paginationData: PaginationData;
  subVisitPurposes: SubVisitPurpose[];
}> => {
  const response = await apiClient.get(
    routes.api.superAdmin.getPaginatedSubVisitPurposesList,
    {
      params: {
        searchExpression,
        limit,
        page,
      },
    }
  );
  return {
    paginationData: response.data.pagination,
    subVisitPurposes: formatSubVisitPurposesList(
      response.data.subVisitPurposesPureList
    ),
  };
};

const formatSubVisitPurposesList = (
  subVisitPurposesList: SubVisitPurposeDbType[]
): SubVisitPurpose[] => {
  return subVisitPurposesList.map(
    (subVisitPurpose: SubVisitPurposeDbType): SubVisitPurpose => {
      return {
        id: subVisitPurpose.id,
        name: subVisitPurpose.name,
        visitPurpose: {
          id: subVisitPurpose.visitPurpose.id,
          name: subVisitPurpose.visitPurpose.name,
        },
      };
    }
  );
};
