import apiClient from "src/helpers/httpInterceptor";
import { routes } from "src/config/routes";
import { PaginationData } from "src/components/Pagination/Pagination";
import { Town, TownInModal } from "src/types/Admin/SuperAdmin/Town/town";

export const storeTown = async ({ name }: TownInModal): Promise<string> => {
  const response = await apiClient.post(routes.api.superAdmin.storeTown, {
    name,
  });

  return response.data.message;
};

export const updateTown = async ({
  name,
  id,
}: TownInModal): Promise<string> => {
  const response = await apiClient.put(
    routes.api.superAdmin.updateTown + encodeURIComponent(id),
    {
      name,
    }
  );
  return response.data.message;
};

export const deleteTown = async (id: number): Promise<string> => {
  const response = await apiClient.delete(
    routes.api.superAdmin.deleteTown + encodeURIComponent(id)
  );
  return response.data.message;
};

export const getPaginatedTownsList = async (
  searchExpression: string,
  limit: number,
  page = 1
): Promise<{
  paginationData: PaginationData;
  towns: Town[];
}> => {
  const response = await apiClient.get(
    routes.api.superAdmin.getPaginatedTownsList,
    {
      params: {
        searchExpression,
        limit,
        page,
      },
    }
  );
  return {
    paginationData: response.data.pagination,
    towns: formatTownsList(response.data.townsPureList),
  };
};

const formatTownsList = (townsList: []): Town[] => {
  const formattedTowns = townsList.map((town: Town) => ({
    id: town.id,
    name: town.name,
  }));

  return formattedTowns;
};
