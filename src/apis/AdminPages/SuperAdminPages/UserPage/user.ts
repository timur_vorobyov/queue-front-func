import apiClient from "src/helpers/httpInterceptor";
import { routes } from "src/config/routes";
import { PaginationData } from "src/components/Pagination/Pagination";
import { User, UserInModal } from "src/types/Admin/SuperAdmin/User/user";

export const storeUser = async ({
  firstName,
  lastName,
  branchOfficeId,
  visitPurposesIds,
  password,
  email,
  isActive,
  isAllowedHaveSameDeskNumber,
  roleId,
}: UserInModal): Promise<string> => {
  const response = await apiClient.post(routes.api.superAdmin.storeUser, {
    branchOfficeId,
    visitPurposesIds,
    firstName,
    lastName,
    password,
    email,
    isActive,
    isAllowedHaveSameDeskNumber,
    roleId,
  });

  return response.data.message;
};

export const updateUser = async ({
  id,
  firstName,
  lastName,
  branchOfficeId,
  visitPurposesIds,
  password,
  email,
  isActive,
  isAllowedHaveSameDeskNumber,
  roleId,
}: UserInModal): Promise<string> => {
  const response = await apiClient.put(
    routes.api.superAdmin.updateUser + encodeURIComponent(id),
    {
      branchOfficeId,
      visitPurposesIds,
      firstName,
      lastName,
      password,
      email,
      isActive,
      isAllowedHaveSameDeskNumber,
      roleId,
    }
  );

  return response.data.message;
};

export const deleteUser = async (id: number): Promise<string> => {
  const response = await apiClient.delete(
    routes.api.superAdmin.deleteUser + encodeURIComponent(id)
  );
  return response.data.message;
};

export const getPaginatedUsersList = async (
  branchOfficeId: string,
  searchExpression: string,
  limit: number,
  page: number
): Promise<{
  paginationData: PaginationData;
  users: User[];
}> => {
  const response = await apiClient.get(
    routes.api.superAdmin.getPaginatedUsersList,
    {
      params: {
        branchOfficeId,
        searchExpression,
        limit,
        page,
      },
    }
  );

  return {
    paginationData: response.data.pagination,
    users: formatUsersList(response.data.usersPureList),
  };
};

const formatUsersList = (usersList: []): User[] => {
  const formattedUsers = usersList.map((user: User) => ({
    id: user.id,
    branchOffice: {
      id: user.branchOffice.id,
      name: user.branchOffice.name,
      townId: user.branchOffice.townId,
    },
    visitPurposesIds: user.visitPurposesIds,
    email: user.email,
    deskNumber: user.deskNumber,
    createdAt: user.createdAt,
    isActive: user.isActive,
    firstName: user.firstName,
    lastName: user.lastName,
    role: {
      id: user.role ? user.role.id : 0,
      name: user.role ? user.role.name : "",
    },
    isAllowedHaveSameDeskNumber: user.isAllowedHaveSameDeskNumber,
  }));

  return formattedUsers;
};
