import { PaginationData } from "src/components/Pagination/Pagination";
import { routes } from "src/config/routes";
import apiClient from "src/helpers/httpInterceptor";
import { Service } from "src/types/Admin/SuperAdmin/VisitPurpose/service";
import {
  VisitPurpose,
  VisitPurposeInModal,
} from "src/types/Admin/SuperAdmin/VisitPurpose/visitPurpose";
import { VisitPurposeType } from "src/types/Admin/SuperAdmin/VisitPurpose/visitPurposeType";

export const storeVisitPurpose = async ({
  name,
  servicesIds,
  series,
  tjMessage,
  ruMessage,
  isPrioratised,
  visitPurposeTypeId,
  isCashRegister,
}: VisitPurposeInModal): Promise<string> => {
  const response = await apiClient.post(
    routes.api.superAdmin.storeVisitPurpose,
    {
      name,
      servicesIds,
      series,
      tjMessage,
      ruMessage,
      isPrioratised,
      visitPurposeTypeId,
      isCashRegister,
    }
  );

  if (response.data.message == "SERIES_ERROR") {
    return response.data.message;
  }

  if (response.data.statusCode) {
    return response.data.statusCode;
  }

  return response.data.message;
};

export const updateVisitPurpose = async ({
  id,
  name,
  servicesIds,
  series,
  tjMessage,
  ruMessage,
  isPrioratised,
  visitPurposeTypeId,
  isCashRegister,
}: VisitPurposeInModal): Promise<string> => {
  const response = await apiClient.put(
    routes.api.superAdmin.updateVisitPurpose + encodeURIComponent(id),
    {
      name,
      servicesIds,
      series,
      tjMessage,
      ruMessage,
      isPrioratised,
      visitPurposeTypeId,
      isCashRegister,
    }
  );
  return response.data.message;
};

export const deleteVisitPurpose = async (id: number): Promise<string> => {
  const respone = await apiClient.delete(
    routes.api.superAdmin.deleteVisitPurpose + encodeURIComponent(id)
  );
  return respone.data.message;
};

export const getPaginatedVisitPurposesList = async (
  branchOfficeId: string,
  searchExpression = "",
  limit = 5,
  page = 1
): Promise<{
  paginationData: PaginationData;
  visitPurposes: VisitPurpose[];
}> => {
  const response = await apiClient.get(
    routes.api.superAdmin.getPaginatedVisitPurposesList,
    {
      params: {
        searchExpression,
        limit,
        page,
        branchOfficeId,
      },
    }
  );

  return {
    paginationData: response.data.pagination,
    visitPurposes: formatVisitPurposesList(response.data.visitPurposesPureList),
  };
};

const formatVisitPurposesList = (visitPurposesList: []): VisitPurpose[] => {
  const formatedVisitPurposes = visitPurposesList.map(
    (visitPurpose: VisitPurpose) => ({
      id: visitPurpose.id,
      name: visitPurpose.name,
      series: visitPurpose.series,
      services: visitPurpose.services,
      tjMessage: visitPurpose.tjMessage,
      ruMessage: visitPurpose.ruMessage,
      isPrioratised: visitPurpose.isPrioratised,
      visitPurposeType: visitPurpose.visitPurposeType,
      isCashRegister: visitPurpose.isCashRegister,
    })
  );

  return formatedVisitPurposes;
};

export const getServicesList = async (): Promise<Service[]> => {
  const response = await apiClient.get(routes.api.getServicesList);
  return formatServicesList(response.data);
};

const formatServicesList = (servicesList: []): Service[] => {
  const formattedServices = servicesList.map((service: Service) => ({
    id: service.id,
    name: service.name,
    isActive: service.isActive,
  }));

  return formattedServices;
};

export const getVisitPurposeTypesList = async (): Promise<
  VisitPurposeType[]
> => {
  const response = await apiClient.get(routes.api.getVisitPurposeTypesList);
  return toVisitPurposeTypeFrontFormat(response.data);
};

const toVisitPurposeTypeFrontFormat = (
  visitPurposeTypesPureList: []
): VisitPurposeType[] => {
  const formattedServices = visitPurposeTypesPureList.map(
    (visitPurposeType: VisitPurposeType) => ({
      id: visitPurposeType.id,
      name: visitPurposeType.name,
    })
  );

  return formattedServices;
};
