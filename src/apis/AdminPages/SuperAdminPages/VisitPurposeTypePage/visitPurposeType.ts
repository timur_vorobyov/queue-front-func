import apiClient from "src/helpers/httpInterceptor";
import { routes } from "src/config/routes";
import {
  VisitPurposeType,
  VisitPurposeTypeInModal,
} from "src/types/Admin/SuperAdmin/ViisitPurposeType/visitPurposeType";
import { PaginationData } from "src/components/Pagination/Pagination";

type VisitPurposeTypeDbType = {
  id: number;
  name: string;
};

export const storeVisitPurposeType = async ({
  name,
}: VisitPurposeTypeInModal): Promise<string> => {
  const response = await apiClient.post(
    routes.api.superAdmin.storeVisitPurposeType,
    {
      name,
    }
  );

  return response.data.message;
};

export const updateVisitPurposeType = async ({
  name,
  id,
}: VisitPurposeTypeInModal): Promise<string> => {
  const response = await apiClient.put(
    routes.api.superAdmin.updateVisitPurposeType + encodeURIComponent(id),
    {
      name,
    }
  );

  return response.data.message;
};

export const deleteVisitPurposeType = async (visitPurposeId: number) => {
  const response = await apiClient.delete(
    routes.api.superAdmin.deleteSubVisitPurposeType +
      encodeURIComponent(visitPurposeId)
  );

  return response.data.message;
};

export const getPaginatedVisitPurposeTypesList = async (
  searchExpression: string,
  limit: number,
  page = 1
): Promise<{
  paginationData: PaginationData;
  visitPurposeTypes: VisitPurposeType[];
}> => {
  const response = await apiClient.get(
    routes.api.superAdmin.getPaginatedVisitPurposeTypesList,
    {
      params: {
        searchExpression,
        limit,
        page,
      },
    }
  );
  return {
    paginationData: response.data.pagination,
    visitPurposeTypes: formatVisitPurposeTypesList(
      response.data.visitPurposeTypesPureList
    ),
  };
};

const formatVisitPurposeTypesList = (
  visitPurposeTypesList: VisitPurposeTypeDbType[]
): VisitPurposeType[] => {
  return visitPurposeTypesList.map(
    (visitPurposeType: VisitPurposeTypeDbType): VisitPurposeType => {
      return {
        id: visitPurposeType.id,
        name: visitPurposeType.name,
      };
    }
  );
};
