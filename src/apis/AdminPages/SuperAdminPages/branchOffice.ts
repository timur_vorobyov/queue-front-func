import apiClient from "src/helpers/httpInterceptor";
import { routes } from "src/config/routes";
import { BranchOffice } from "src/types/Admin/SuperAdmin/branchOffice";

export const getBranchOfficesList = async (
  townId = 0
): Promise<BranchOffice[]> => {
  const response = await apiClient.get(routes.api.getBranchOfficesList, {
    params: {
      townId,
    },
  });
  return formatBranchOfficesList(response.data);
};

const formatBranchOfficesList = (branchOfficesList: []): BranchOffice[] => {
  const formattedBranchOffices = branchOfficesList.map(
    (bracnhOffice: BranchOffice) => ({
      id: bracnhOffice.id,
      name: bracnhOffice.name,
    })
  );

  return formattedBranchOffices;
};
