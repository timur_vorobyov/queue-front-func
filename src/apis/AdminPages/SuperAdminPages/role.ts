import apiClient from "src/helpers/httpInterceptor";
import { routes } from "src/config/routes";
import { Role } from "src/types/Admin/SuperAdmin/User/role";

export const getRolesList = async (townId = 0): Promise<Role[]> => {
  const response = await apiClient.get(routes.api.getRolesList, {
    params: {
      townId,
    },
  });
  return formatRolesList(response.data);
};

const formatRolesList = (rolesList: []): Role[] => {
  const formattedRoles = rolesList.map((role: Role) => ({
    id: role.id,
    name: role.name,
  }));

  return formattedRoles;
};
