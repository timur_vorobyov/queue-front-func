import apiClient from "src/helpers/httpInterceptor";
import { routes } from "src/config/routes";
import { Town } from "src/types/Admin/SuperAdmin/town";

export const getTownsList = async (): Promise<Town[]> => {
  const response = await apiClient.get(routes.api.getTownsList);
  return formatTownsList(response.data);
};

const formatTownsList = (townsList: []): Town[] => {
  const formatedTowns = townsList.map((town: Town) => ({
    id: town.id,
    name: town.name,
  }));

  return formatedTowns;
};
