import apiClient from "src/helpers/httpInterceptor";
import { routes } from "src/config/routes";
import { VisitPurpose } from "src/types/Admin/visitPurpose";

export const getVisitPurposesList = async (
  branchOfficeId = 0
): Promise<VisitPurpose[]> => {
  const response = await apiClient.get(routes.api.getVisitPurposesList, {
    params: {
      branchOfficeId,
    },
  });
  return formatVisitPurposesList(response.data);
};

const formatVisitPurposesList = (visitPurposesList = []): VisitPurpose[] => {
  const formattedVisitPurposes = visitPurposesList.map(
    (visitPurpose: VisitPurpose) => ({
      id: visitPurpose.id,
      name: visitPurpose.name,
    })
  );

  return formattedVisitPurposes;
};
