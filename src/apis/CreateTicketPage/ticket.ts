import { getBranchOfficeId } from "src/helpers/auth-data";
import apiClient from "src/helpers/httpInterceptor";
import { routes } from "src/config/routes";

export const createTicket = async (
  visitPurposeId: number,
  customerPhoneNumber: string,
  subVisitPurposeId: number
): Promise<string> => {
  const response = await apiClient.post(routes.api.createTicket, {
    visitPurposeId,
    customerPhoneNumber,
    branchOfficeId: getBranchOfficeId(),
    subVisitPurposeId: subVisitPurposeId,
  });

  return toTicketFrontFormat(response.data.ticket);
};

const toTicketFrontFormat = (ticket: { ticketFullNumber: string }): string => {
  return ticket.ticketFullNumber;
};
