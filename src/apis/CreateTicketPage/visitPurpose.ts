import apiClient from "src/helpers/httpInterceptor";
import { routes } from "src/config/routes";
import {
  SubVisitPurposeDbType,
  VisitPurpose,
  VisitPurposeDbType,
} from "src/types/UserPageTypes/CreateTicketPageTypes/visitPurpose";

export const getVisitPurposeList = async (
  branchOfficeId: string
): Promise<VisitPurpose[]> => {
  const response = await apiClient.get(
    routes.api.visitPurposesList + encodeURIComponent(branchOfficeId)
  );

  return toVisitPurposeArray(response.data);
};

const toVisitPurposeArray = (visitPurposeList: []): VisitPurpose[] => {
  const formatedVisitPurposes = visitPurposeList.map(
    (visitPurpose: VisitPurposeDbType) => ({
      id: visitPurpose.id,
      name: visitPurpose.name,
      subVisitPurposes: visitPurpose.subVisitPurposes.map(
        (subVisitPurpose: SubVisitPurposeDbType) => ({
          id: subVisitPurpose.id,
          name: subVisitPurpose.name,
          visitPurposeId: subVisitPurpose.visit_purpose_id,
        })
      ),
    })
  );

  return formatedVisitPurposes;
};
