import { routes } from "src/config/routes";
import { townId } from "src/helpers/auth-data";
import apiClient from "src/helpers/httpInterceptor";
import { BranchOffice } from "src/types/UserPageTypes/ProfilePageTypes/branchOffice";

export const getBranchOfficeList = async (): Promise<BranchOffice[]> => {
  const response = await apiClient.get(
    routes.api.branchOfficesList + encodeURIComponent(townId())
  );

  return toBranchOfficeArray(response.data);
};

const toBranchOfficeArray = (branchOfficeList: []): BranchOffice[] => {
  const formatedBranchOffices = branchOfficeList.map(
    (branchOffice: BranchOffice) => ({
      id: branchOffice.id,
      name: branchOffice.name,
    })
  );

  return formatedBranchOffices;
};
