import { routes } from "src/config/routes";
import { userId } from "src/helpers/auth-data";
import apiClient from "src/helpers/httpInterceptor";

export const updateUserSettings = async (
  userIdWithSameDeskNumber: number | null,
  deskNumber: string,
  branchOfficeId: string,
  visitPurposesIds: number[]
): Promise<string> => {
  const response = await apiClient.post(routes.api.updateUserWorkData, {
    userId: userId(),
    userIdWithSameDeskNumber: userIdWithSameDeskNumber,
    deskNumber: deskNumber,
    branchOfficeId: branchOfficeId,
    visitPurposesIds: visitPurposesIds,
  });

  return response.data.message;
};

export const checkDeskNumberExistance = async (
  deskNumber: string,
  branchOfficeId: string,
  visitPurposesIds: number[]
): Promise<{
  userIdWithSameDeskNumber: number | null;
  userFirstName: string;
  message: string;
}> => {
  const response = await apiClient.post(routes.api.deskNumberCheck, {
    userId: userId(),
    deskNumber: deskNumber,
    branchOfficeId: branchOfficeId,
    visitPurposesIds: visitPurposesIds,
  });
  return response.data;
};
