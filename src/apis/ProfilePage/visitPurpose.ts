import { routes } from "src/config/routes";
import apiClient from "src/helpers/httpInterceptor";
import { VisitPurpose } from "src/types/UserPageTypes/QueuePageTypes/visitPurpose";

export const getVisitPurposeList = async (
  branchOfficeId: number
): Promise<VisitPurpose[]> => {
  const response = await apiClient.get(
    routes.api.visitPurposesList + encodeURIComponent(branchOfficeId)
  );
  return toVisitPurposeArray(response.data);
};

const toVisitPurposeArray = (visitPurposeList: []): VisitPurpose[] => {
  const formatedVisitPurposes = visitPurposeList.map(
    (visitPurpose: VisitPurpose) => ({
      id: visitPurpose.id,
      name: visitPurpose.name,
    })
  );

  return formatedVisitPurposes;
};
