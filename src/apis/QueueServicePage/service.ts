import { routes } from "src/config/routes";
import apiClient from "src/helpers/httpInterceptor";
import { Service } from "src/types/UserPageTypes/QueuePageTypes/service";

export const getServicesList = async (
  visitPurposeId: number
): Promise<Service[] | []> => {
  if (visitPurposeId != 0) {
    const response = await apiClient.post(routes.api.servicesList, {
      visitPurposeId: visitPurposeId,
    });
    return toServiceFrontFormat(response.data);
  }

  return [];
};

const toServiceFrontFormat = (serviceList: []): Service[] => {
  const formatedServices = serviceList.map((service: Service) => ({
    id: service.id,
    name: service.name,
  }));

  return formatedServices;
};
