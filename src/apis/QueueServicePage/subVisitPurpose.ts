import { routes } from "src/config/routes";
import apiClient from "src/helpers/httpInterceptor";
import { SubVisitPurpose } from "src/types/UserPageTypes/QueuePageTypes/subVisitPurpose";

export const getSubVisitPurposesList = async (
  visitPurposeId: number
): Promise<SubVisitPurpose[] | []> => {
  if (visitPurposeId != 0) {
    const response = await apiClient.get(routes.api.subVisitPurposesList, {
      params: {
        visitPurposeId: visitPurposeId,
      },
    });

    return toSubVisitPurposesListFrontFormat(response.data);
  }

  return [];
};

const toSubVisitPurposesListFrontFormat = (
  subVisitPurposesList: []
): SubVisitPurpose[] => {
  const formatedServices = subVisitPurposesList.map(
    (service: SubVisitPurpose) => ({
      id: service.id,
      name: service.name,
    })
  );

  return formatedServices;
};
