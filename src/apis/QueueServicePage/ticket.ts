import { routes } from "src/config/routes";
import { TICKET_STATUES } from "src/consts/ticketConsts";
import {
  getBranchOfficeId,
  userId,
  getVisitPurposesIds,
} from "src/helpers/auth-data";
import apiClient from "src/helpers/httpInterceptor";
import { Ticket } from "src/store/types/ticket";

export const getIncomingTickets = async (): Promise<Ticket[] | null> => {
  if (getBranchOfficeId() != null && getVisitPurposesIds().length != 0) {
    const response = await apiClient.get(routes.api.fullTicketsList, {
      params: {
        branchOfficeId: getBranchOfficeId(),
        statusName: TICKET_STATUES.NEW,
        visitPurposesIds: getVisitPurposesIds(),
      },
    });
    return response.data;
  }

  return null;
};

export const getAcceptedTicket = async (): Promise<Ticket[] | null> => {
  if (getBranchOfficeId() != null && getVisitPurposesIds().length != 0) {
    const response = await apiClient.get(routes.api.ticket, {
      params: {
        branchOfficeId: getBranchOfficeId(),
        statusName: TICKET_STATUES.INVITED,
        visitPurposesIds: getVisitPurposesIds(),
        userId: userId(),
      },
    });

    return response.data;
  }

  return null;
};

export const getServingTicket = async (): Promise<Ticket | null> => {
  if (getBranchOfficeId() != null && getVisitPurposesIds().length != 0) {
    const response = await apiClient.get(routes.api.ticket, {
      params: {
        branchOfficeId: getBranchOfficeId(),
        statusName: TICKET_STATUES.SERVING,
        visitPurposesIds: getVisitPurposesIds(),
        userId: userId(),
      },
    });

    return response.data;
  }

  return null;
};

export const acceptTicket = async (ticketId: number): Promise<string> => {
  const response = await apiClient.put(routes.api.acceptTicket, {
    userId: userId(),
    ticketId: ticketId,
  });

  return response.data.message;
};

export const serveTicket = async (ticketId: number): Promise<void> => {
  await apiClient.put(routes.api.serveTicket, {
    userId: userId(),
    ticketId: ticketId,
  });
};

export const declineTicket = async (ticketId: number): Promise<void> => {
  await apiClient.put(routes.api.declineTicket, {
    userId: userId(),
    ticketId: ticketId,
  });
};

export const completeService = async (
  ticketId: number,
  serviceId: number,
  visitPurposeId: number,
  subVisitPurposeId: number
): Promise<void> => {
  await apiClient.put(routes.api.completeService, {
    visitPurposeId: visitPurposeId,
    userId: userId(),
    ticketId: ticketId,
    serviceId: serviceId,
    subVisitPurposeId: subVisitPurposeId,
  });

  await updateStatisticsData();
};

export const updateStatisticsData = async (): Promise<void> => {
  await apiClient.post(routes.api.updateStatisticsData, {
    userId: userId(),
    visitPurposesIds: getVisitPurposesIds(),
    branchOfficeId: getBranchOfficeId(),
  });
};
