import { routes } from "src/config/routes";
import {
  getBranchOfficeId,
  getVisitPurposesIds,
  userId,
} from "src/helpers/auth-data";
import apiClient from "src/helpers/httpInterceptor";
import { UserStatus } from "src/types/UserPageTypes/QueuePageTypes/user";

export const updateUserStatus = async (statusId: number): Promise<string> => {
  const response = await apiClient.post(routes.api.updateUserStatus, {
    userId: userId(),
    statusId: statusId,
  });
  const message = response.data.message;

  if (message == "STATUS_IS_UPDATED") {
    localStorage.setItem(
      "queue-user-data",
      JSON.stringify(response.data.userData)
    );
  }

  return message;
};

export const getUserStatusesList = async (): Promise<UserStatus[]> => {
  const response = await apiClient.get(routes.api.getUserStatusesList);
  return toUserStatusFrontFormat(response.data);
};

const toUserStatusFrontFormat = (userStatusesList: []): UserStatus[] => {
  const formattedVisitPurposes = userStatusesList.map(
    (userStatus: UserStatus) => ({
      id: userStatus.id,
      name: userStatus.name,
    })
  );

  return formattedVisitPurposes;
};

export const getCustomersAmountUserServed = async (): Promise<string> => {
  const response = await apiClient.get(
    routes.api.getCustomersAmountUserServed,
    {
      params: {
        userId: userId(),
      },
    }
  );
  return response.data.customersAmountUserServed;
};

export const getUserAverageServiceTime = async (): Promise<string | null> => {
  if (getVisitPurposesIds().length != 0) {
    const response = await apiClient.get(routes.api.getUserAverageServiceTime, {
      params: {
        userId: userId(),
        visitPurposesIds: getVisitPurposesIds(),
      },
    });
    return response.data.userAverageServiceTime;
  }

  return null;
};

export const getUsersAverageServiceTime = async (): Promise<string | null> => {
  const response = await apiClient.get(routes.api.getUsersAverageServiceTime, {
    params: {
      branchOfficeId: getBranchOfficeId(),
    },
  });
  return response.data.usersAverageServiceTime;
};

export const getUsersMaxServiceTime = async (): Promise<string | null> => {
  const response = await apiClient.get(routes.api.getUsersMaxServiceTime, {
    params: {
      branchOfficeId: getBranchOfficeId(),
    },
  });

  return response.data.usersMaxServiceTime;
};
