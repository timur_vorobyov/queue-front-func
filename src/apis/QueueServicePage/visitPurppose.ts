import { VisitPurpose } from "src/types/UserPageTypes/QueuePageTypes/visitPurpose";
import { routes } from "src/config/routes";
import { getBranchOfficeId } from "src/helpers/auth-data";
import apiClient from "src/helpers/httpInterceptor";

export const getVisitPurposeList = async (): Promise<VisitPurpose[] | []> => {
  if (getBranchOfficeId() != 0) {
    const response = await apiClient.get(
      routes.api.visitPurposesList + encodeURIComponent(getBranchOfficeId())
    );
    return toVisitPurposeFrontFormat(response.data);
  }

  return [];
};

const toVisitPurposeFrontFormat = (visitPurposesList: []): VisitPurpose[] => {
  const formatedVisitPurposesList = visitPurposesList.map(
    (visitPurpose: VisitPurpose) => ({
      id: visitPurpose.id,
      name: visitPurpose.name,
    })
  );

  return formatedVisitPurposesList;
};
