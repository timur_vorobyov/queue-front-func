import { routes } from "src/config/routes";
import { TICKET_STATUES } from "src/consts/ticketConsts";
import apiClient from "src/helpers/httpInterceptor";
import { Ticket } from "src/store/types/screenPageTicket";

export const getAcceptedTickets = async (
  branchOfficeId: string,
  visitPurposesIds: string[]
): Promise<Ticket[] | null> => {
  const response = await apiClient.get(routes.api.shortTicketsList, {
    params: {
      branchOfficeId: branchOfficeId,
      statusName: TICKET_STATUES.INVITED,
      visitPurposesIds: visitPurposesIds,
    },
  });
  return response.data;
};

export const getIncomingTickets = async (
  branchOfficeId: string,
  visitPurposesIds: string[]
): Promise<Ticket[] | null> => {
  const response = await apiClient.get(routes.api.shortTicketsList, {
    params: {
      branchOfficeId: branchOfficeId,
      statusName: TICKET_STATUES.NEW,
      visitPurposesIds: visitPurposesIds,
    },
  });
  return response.data;
};
