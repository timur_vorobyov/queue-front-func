import React from "react";
import { Redirect, Route } from "react-router-dom";
import { routes } from "src/config/routes";
import { roleName, userIsLoggedIn } from "src/helpers/auth-data";

export const AdminProtectedRoute = ({ component: Component, ...rest }: any) => {
  if (userIsLoggedIn() == true) {
    if (roleName() == "super admin" || roleName() == "hall admin") {
      return <Route {...rest} component={() => <Component />} />;
    } else {
      return <Redirect to={routes.queueService} />;
    }
  } else {
    return <Redirect to={routes.login} />;
  }
};
