import React from "react";
import { Redirect, Route } from "react-router-dom";
import { routes } from "src/config/routes";
import { userIsLoggedIn } from "src/helpers/auth-data";

export const AuthProtectedRoute = ({ component: Component, ...rest }: any) => {
  if (userIsLoggedIn() == true) {
    return <Route {...rest} component={() => <Component />} />;
  } else {
    return <Redirect to={routes.login} />;
  }
};
