import React from "react";
import { Spinner } from "react-bootstrap";
import "./Loader.css";

export default function Loader() {
  return (
    <div className="Loader__overlay" data-testid="loader">
      <Spinner animation="border" role="status" />
    </div>
  );
}
