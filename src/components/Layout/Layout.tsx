import { Container } from "react-bootstrap";
import NavBar from "./NavBar";
import React, { useEffect, useState } from "react";
import classes from "./Layout.css";
import cn from "clsx";
import SideBar from "src/components/Layout/SideBar";
import { routes } from "src/config/routes";
import { userId, userIsLoggedIn, userStatusName } from "src/helpers/auth-data";
import apiClient from "src/helpers/httpInterceptor";
import SubscriptionsManager from "./SubscriptionsManager";
import PagesWithtNavAndSideBars from "src/pages/routers/PagesWithNavAndSideBars/PagesWithNavAndSideBars";

const Layout = () => {
  const [
    isAcceptTicketButtonDisabled,
    setIsAcceptTicketButtonDisabled,
  ] = useState(userStatusName() == "work time" ? false : true);

  const setUserData = async (): Promise<void> => {
    const userData = await apiClient.get(
      routes.api.getUserData + encodeURIComponent(userId())
    );

    if (userData.data == "User not found") {
      localStorage.removeItem("queue-user-data");
      localStorage.removeItem("queue-access-token");
      return window.location.replace(routes.login);
    }

    await localStorage.setItem(
      "queue-user-data",
      JSON.stringify(userData.data)
    );
  };

  const setCustomersAmountUserServed = (isButtonDisabled: boolean) => {
    setIsAcceptTicketButtonDisabled(isButtonDisabled);
  };

  useEffect(() => {
    setUserData();
  }, []);

  return (
    <div className={cn(classes.container)}>
      {userIsLoggedIn() == true ? <SubscriptionsManager /> : null}
      <div className={cn(classes.container)}>
        <SideBar />
        <div className={cn(classes.mainContent)}>
          <div className={cn(classes.blueSpace)}>
            <NavBar onStatusChanged={setCustomersAmountUserServed} />
          </div>
          <Container className={cn(classes.bootstrapContainer)}>
            <div className={cn(classes.content)}>
              <PagesWithtNavAndSideBars
                isAcceptTicketButtonDisabled={isAcceptTicketButtonDisabled}
              />
            </div>
          </Container>
        </div>
      </div>
    </div>
  );
};
export default Layout;
