import { PureComponent } from "react";
import cn from "clsx";
import classes from "./SideBar.css";
import React from "react";
import Typography from "../../Typography";
import {
  branchOfficeFullName,
  roleName,
  userFullName,
} from "src/helpers/auth-data";
import {
  SidebarUserData,
  SidebarSuperAdminData,
  SidebarHallAdminData,
} from "./SidebarData";
import SubMenu from "./SubMenu";

class SideBar extends PureComponent {
  render(): React.ReactNode {
    return (
      <main className={cn(classes.container)}>
        <div className={cn(classes.header)}>
          <div className={cn(classes.userData)}>
            <span className={cn(classes.sidebarHeader)}>{userFullName()}</span>
          </div>
          <div className={cn(classes.userData)}>
            <Typography color="light-muted" variant="span">
              {branchOfficeFullName()}
            </Typography>
          </div>
        </div>
        <div className={cn(classes.header)}>
          <span className={cn(classes.sidebarHeader)}>Основное</span>
        </div>
        {SidebarUserData.map((item: any, index: number) => {
          return <SubMenu item={item} key={index} />;
        })}
        {roleName() == "super admin" ? (
          <>
            <div className={cn(classes.header)}>
              <span className={cn(classes.sidebarHeader)}>Супер админ</span>
            </div>
            {SidebarSuperAdminData.map((item: any, index: number) => {
              return <SubMenu item={item} key={index} />;
            })}
          </>
        ) : null}
        {roleName() == "hall admin" ? (
          <>
            <div className={cn(classes.header)}>
              <span className={cn(classes.sidebarHeader)}>Админ зала</span>
            </div>
            {SidebarHallAdminData.map((item: any, index: number) => {
              return <SubMenu item={item} key={index} />;
            })}
          </>
        ) : null}
      </main>
    );
  }
}

export default SideBar;
