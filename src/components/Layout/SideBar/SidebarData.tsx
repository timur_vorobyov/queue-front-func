import { routes } from "src/config/routes";
import DownArrow from "./images/DownArrow";
import Home from "./images/Home";
import RightArrow from "./images/RightArrow";

export const SidebarUserData = [
  {
    title: "Мой профиль",
    icon: Home,
    iconClosed: RightArrow,
    iconOpened: DownArrow,
    subNavs: [
      {
        title: "Настройки",
        path: routes.profile,
        icon: RightArrow,
        locationPathname: "/profile/",
      },
      {
        title: "Принять клиента",
        path: routes.queueService,
        icon: RightArrow,
        locationPathname: "/",
      },
    ],
  },
];

export const SidebarSuperAdminData = [
  {
    title: "Пользователи",
    iconClosed: RightArrow,
    iconOpened: DownArrow,
    subNavs: [
      {
        title: "Пользователи",
        path: routes.superAdmin.users,
        icon: RightArrow,
        locationPathname: "/super-admin/users/",
      },
      {
        title: "Роли",
        path: routes.superAdmin.roles,
        icon: RightArrow,
        locationPathname: "/super-admin/roles/",
      },
      {
        title: "Пермишины",
        path: routes.superAdmin.permissions,
        icon: RightArrow,
        locationPathname: "/super-admin/permissions/",
      },
    ],
  },
  {
    title: "Локации",
    iconClosed: RightArrow,
    iconOpened: DownArrow,
    subNavs: [
      {
        title: "Филиалы",
        path: routes.superAdmin.branchOffices,
        icon: RightArrow,
        locationPathname: "/super-admin/branch-offices/",
      },
      {
        title: "Города",
        path: routes.superAdmin.towns,
        icon: RightArrow,
        locationPathname: "/super-admin/towns/",
      },
      {
        title: "Цели визита",
        path: routes.superAdmin.visitPurposes,
        icon: RightArrow,
        locationPathname: "/super-admin/visit-purposes/",
      },
      ,
      {
        title: "Типы цели визита",
        path: routes.superAdmin.visitPurposesTypes,
        icon: RightArrow,
        locationPathname: routes.superAdmin.visitPurposesTypes,
      },
      {
        title: "Подцели визита",
        path: routes.superAdmin.subVisitPurposes,
        icon: RightArrow,
        locationPathname: routes.superAdmin.subVisitPurposes,
      },
      {
        title: "Сервисы",
        path: routes.superAdmin.services,
        icon: RightArrow,
        locationPathname: "/super-admin/services/",
      },
    ],
  },
  {
    title: "Статистика",
    iconClosed: RightArrow,
    iconOpened: DownArrow,
    subNavs: [
      {
        title: "Отчёт по тикетам",
        path: routes.superAdmin.reports.tickets,
        icon: RightArrow,
        locationPathname: "/super-admin/reports/tickets",
      },
      {
        title: "Отчёт по юзерам",
        path: routes.superAdmin.reports.users,
        icon: RightArrow,
        locationPathname: "/super-admin/reports/users",
      },
    ],
  },
  {
    title: "Тикеты",
    icon: Home,
    iconClosed: RightArrow,
    iconOpened: DownArrow,
    subNavs: [
      {
        title: "Корзина",
        path: routes.admin.declinedTickets,
        icon: RightArrow,
        locationPathname: "/admin/declined-tickets/",
      },
      {
        title: "В ожидании",
        path: routes.admin.incomingTickets,
        icon: RightArrow,
        locationPathname: "/admin/incoming-tickets/",
      },
    ],
  },
];

export const SidebarHallAdminData = [
  {
    title: "Пользователи",
    icon: Home,
    iconClosed: RightArrow,
    iconOpened: DownArrow,
    subNavs: [
      {
        title: "Пользователи",
        path: routes.hallAdmin.users,
        icon: RightArrow,
        locationPathname: "/hall-admin/profile/",
      },
    ],
  },
  {
    title: "Тикеты",
    icon: Home,
    iconClosed: RightArrow,
    iconOpened: DownArrow,
    subNavs: [
      {
        title: "Корзина",
        path: routes.admin.declinedTickets,
        icon: RightArrow,
        locationPathname: "/admin/declined-tickets/",
      },
      {
        title: "В ожидании",
        path: routes.admin.incomingTickets,
        icon: RightArrow,
        locationPathname: "/admin/incoming-tickets/",
      },
    ],
  },
];
