import React, { useState } from "react";
import classes from "./SideBar.css";
import cn from "clsx";
import { Link, useLocation } from "react-router-dom";

const SubMenu = ({ item }: any) => {
  const [subnav, setSubnav] = useState(false);
  const showSubnav = () => setSubnav(!subnav);
  const location = useLocation();

  return (
    <>
      <ul className={cn(classes.menuList)}>
        <li className={cn(classes.sidebarListItem)}>
          <a className={cn(classes.sidebarLink)} onClick={showSubnav}>
            <span>{item.title}</span>
            {subnav ? item.iconOpened() : item.iconClosed()}
          </a>
        </li>
        {subnav
          ? item.subNavs.map((subNav: any, index: number) => {
              return (
                <li className={cn(classes.sidebarListItem)} key={index}>
                  <Link
                    className={cn(
                      classes.sidebarLink,
                      location.pathname === subNav.locationPathname
                        ? classes.activeSidebarLink
                        : ""
                    )}
                    to={subNav.path}
                  >
                    <span>{subNav.icon()}</span>
                    <span>{subNav.title}</span>
                  </Link>
                </li>
              );
            })
          : null}
      </ul>
    </>
  );
};

export default SubMenu;
