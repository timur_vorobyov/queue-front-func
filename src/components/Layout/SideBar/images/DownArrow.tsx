import React from "react";

const DownArrow = () => {
  return (
    <svg
      viewBox="0 0 64 64"
      aria-labelledby="title"
      aria-describedby="desc"
      role="img"
      width="25px"
      height="25px"
    >
      <path
        data-name="layer1"
        fill="none"
        stroke="#535763"
        strokeMiterlimit="10"
        strokeWidth="4"
        d="M20 26l11.994 14L44 26"
        strokeLinejoin="round"
        strokeLinecap="round"
      ></path>
    </svg>
  );
};

export default DownArrow;
