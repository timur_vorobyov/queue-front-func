import React from "react";

const RightArrow = () => {
  return (
    <svg
      viewBox="0 0 64 64"
      aria-labelledby="title"
      aria-describedby="desc"
      role="img"
      width="25px"
      height="25px"
    >
      <path
        data-name="layer1"
        fill="none"
        stroke="#535763"
        strokeMiterlimit="10"
        strokeWidth="4"
        d="M26 20.006L40 32 26 44.006"
        strokeLinejoin="round"
        strokeLinecap="round"
      ></path>
    </svg>
  );
};

export default RightArrow;
