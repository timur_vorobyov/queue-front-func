import { useEffect } from "react";
import {
  getBranchOfficeId,
  userId,
  getVisitPurposesIds,
} from "src/helpers/auth-data";
import { useActions } from "src/utils/hooks/useActions";
import echo from "laravel-echo";
import { createEchoInstance } from "src/helpers/echo";
import useMultipleApiCalls from "src/utils/hooks/use-async-multuple-call";
import {
  getIncomingTickets,
  getAcceptedTicket,
  getServingTicket,
} from "src/apis/QueueServicePage/ticket";
import { Ticket } from "src/store/types/ticket";
import { useLocation } from "react-router-dom";

const SubscriptionsManager = () => {
  const {
    setIncomingTickets,
    setAcceptedTicket,
    setInitialData,
    setRecoveredTicket,
    setServingTicket,
  } = useActions();

  const [
    [incomingTicketsList, acceptedTicket, servingTicket],
    setApiCallData,
  ] = useMultipleApiCalls([
    getIncomingTickets,
    getAcceptedTicket,
    getServingTicket,
  ]);
  let echo: echo;
  const location = useLocation();

  useEffect(() => {
    setApiCallData();
  }, [location]);

  useEffect(() => {
    if (incomingTicketsList || acceptedTicket || servingTicket) {
      setInitialData({
        incomingTickets: incomingTicketsList,
        acceptedTicket: acceptedTicket,
        servingTicket: servingTicket,
      });
    }
  }, [incomingTicketsList, acceptedTicket, servingTicket]);

  useEffect(() => {
    connectEcho();
    listenTicketsEvent();
    return () => disconnectEcho();
  }, []);

  const connectEcho = (): void => {
    echo = createEchoInstance();
  };

  const disconnectEcho = (): void => {
    echo.disconnect();
  };

  const listenTicketsEvent = () => {
    getVisitPurposesIds().map((visitPurposeId: number) => {
      echo
        .channel(
          "ticket." +
            getBranchOfficeId() +
            "." +
            visitPurposeId +
            ".pending.recover"
        )
        .listen(".ticket-pending", (data: { ticketPending: Ticket }): void => {
          setRecoveredTicket(data.ticketPending);
        });

      echo
        .channel(
          "ticket." +
            getBranchOfficeId() +
            "." +
            visitPurposeId +
            ".pending.add"
        )
        .listen(".ticket-pending", (data: { ticketPending: Ticket }): void => {
          setIncomingTickets(data.ticketPending);
        });

      echo
        .channel(
          "ticket." +
            getBranchOfficeId() +
            "." +
            visitPurposeId +
            ".pending.delete"
        )
        .listen(".ticket-pending", (data: { ticketPending: Ticket }): void => {
          setIncomingTickets(data.ticketPending);
        });

      echo
        .channel(
          "ticket." +
            userId() +
            "." +
            getBranchOfficeId() +
            "." +
            visitPurposeId +
            ".accepted.add"
        )
        .listen(
          ".ticket-accepted",
          (data: { acceptedTicket: Ticket }): void => {
            setAcceptedTicket(data.acceptedTicket);
          }
        );

      echo
        .channel(
          "ticket." +
            userId() +
            "." +
            getBranchOfficeId() +
            "." +
            visitPurposeId +
            ".accepted.delete"
        )
        .listen(".ticket-accepted", (): void => {
          setAcceptedTicket({} as Ticket);
        });
    });
    echo
      .channel("ticket." + userId() + ".serving.add")
      .listen(".ticket-serving", (data: { ticketServing: Ticket }): void => {
        setServingTicket(data.ticketServing);
      });
    echo
      .channel("ticket." + userId() + ".serving.delete")
      .listen(".ticket-serving", (): void => {
        setServingTicket({} as Ticket);
      });
    echo
      .channel("user-data." + userId())
      .listen(".user-data", (data: { userData: any }): void => {
        localStorage.setItem("queue-user-data", JSON.stringify(data.userData));
      });
  };

  return null;
};

export default SubscriptionsManager;
