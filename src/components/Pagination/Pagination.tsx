import React, { PureComponent } from "react";
import classes from "./Pagination.css";
import cn from "clsx";
import { Link } from "react-router-dom";

export type PaginationData = {
  currentPage: number;
  totalPages: number;
  totalRecords: number;
};

const leftPage = "left";
const rigthPage = "right";

const range = (from: number, to: number, step = 1): Array<unknown> => {
  let i = from;
  const range = [];

  while (i <= to) {
    range.push(i);
    i += step;
  }

  return range;
};

interface PaginationProps {
  totalRecords: number;
  totalPages: number;
  onPageChanged: Function;
  currentPage: number;
}

interface PaginationStates {
  currentPage: number;
}

class Pagination extends PureComponent<PaginationProps, PaginationStates> {
  private pageNeighbours: number;

  constructor(props: PaginationProps) {
    super(props);
    this.pageNeighbours = 2;
  }

  gotoPage = (page: number) => {
    const currentPage = Math.max(0, Math.min(page, this.props.totalPages));
    this.setState({ currentPage });
    this.props.onPageChanged(page);
  };

  handleClick =
    (page: number) => (event: React.MouseEvent<HTMLElement, MouseEvent>) => {
      event.preventDefault();
      this.gotoPage(page);
    };

  handleMoveLeft = (event: React.MouseEvent<HTMLElement, MouseEvent>) => {
    event.preventDefault();
    this.gotoPage(this.props.currentPage - this.pageNeighbours * 2 - 1);
  };

  handleMoveRight = (event: React.MouseEvent<HTMLElement, MouseEvent>) => {
    event.preventDefault();
    this.gotoPage(this.props.currentPage + this.pageNeighbours * 2 + 1);
  };

  fetchPageNumbers = () => {
    const totalPages = this.props.totalPages;
    const currentPage = this.props.currentPage;
    const pageNeighbours = this.pageNeighbours;

    /**
     * totalNumbers: the total page numbers to show on the control
     * totalBlocks: totalNumbers + 2 to cover for the left(<) and right(>) controls
     */
    const totalNumbers = this.pageNeighbours * 2 + 3;
    const totalBlocks = totalNumbers + 2;

    if (totalPages > totalBlocks) {
      const startPage = Math.max(2, currentPage - pageNeighbours);
      const endPage = Math.min(totalPages - 1, currentPage + pageNeighbours);
      let pages = range(startPage, endPage);

      /**
       * hasLeftSpill: has hidden pages to the left
       * hasRightSpill: has hidden pages to the right
       * spillOffset: number of hidden pages either to the left or to the right
       */
      const hasLeftSpill = startPage > 2;
      const hasRightSpill = totalPages - endPage > 1;
      const spillOffset = totalNumbers - (pages.length + 1);

      switch (true) {
        // handle: (1) < {5 6} [7] {8 9} (10)
        case hasLeftSpill && !hasRightSpill: {
          const extraPages = range(startPage - spillOffset, startPage - 1);
          pages = [leftPage, ...extraPages, ...pages];
          break;
        }

        // handle: (1) {2 3} [4] {5 6} > (10)
        case !hasLeftSpill && hasRightSpill: {
          const extraPages = range(endPage + 1, endPage + spillOffset);
          pages = [...pages, ...extraPages, rigthPage];
          break;
        }

        // handle: (1) < {4 5} [6] {7 8} > (10)
        case hasLeftSpill && hasRightSpill:
        default: {
          pages = [leftPage, ...pages, rigthPage];
          break;
        }
      }

      return [1, ...pages, totalPages];
    }

    return range(1, totalPages);
  };

  render() {
    if (!this.props.totalRecords || this.props.totalPages === 1) return null;

    const currentPage = this.props.currentPage;
    const pages = this.fetchPageNumbers();

    return (
      <>
        <ul className={cn("pagination", classes.pagination)}>
          {pages.map((page: any, index: number) => {
            if (page === leftPage)
              return (
                <li key={index} className="page-item">
                  <Link
                    className="page-link"
                    to="#"
                    aria-label="Previous"
                    onClick={this.handleMoveLeft}
                  >
                    <span aria-hidden="true">&laquo;</span>
                    <span className="sr-only">Previous</span>
                  </Link>
                </li>
              );

            if (page === rigthPage)
              return (
                <li key={index} className="page-item">
                  <Link
                    className="page-link"
                    to="#"
                    aria-label="Next"
                    onClick={this.handleMoveRight}
                  >
                    <span aria-hidden="true">&raquo;</span>
                    <span className="sr-only">Next</span>
                  </Link>
                </li>
              );

            return (
              <li
                key={index}
                className={`page-item${currentPage === page ? " active" : ""}`}
              >
                <Link
                  className="page-link"
                  to="#"
                  onClick={this.handleClick(page)}
                >
                  {page}
                </Link>
              </li>
            );
          })}
        </ul>
      </>
    );
  }
}

export default Pagination;
