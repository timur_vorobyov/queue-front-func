export const routes = {
  /**Person */
  queueService: "/",
  login: "/login/",
  profile: "/profile/",
  registerTicket: "/register-ticket",
  ticketVisitPurpose: "/register-ticket/visit-purpose/",
  ticketCustomerPhoneNumber: "/register-ticket/customer-phone-number/",
  showTicket: "/register-ticket/show-ticket/",
  screens: "/screens/",
  screen: "/screen",
  tv: "/screen/tv",
  tvNew: "/screen/tv-new",
  tvFx: "/screen/tv/fx",
  led: "/screen/led",
  ledWhite: "/screen/led-white",
  notFound: "/not-found/",

  /**Admin */
  admin: {
    declinedTickets: "/admin/declined-tickets/",
    incomingTickets: "/admin/incoming-tickets/",
  },

  /**Hall Admin */
  hallAdmin: {
    users: "/hall-admin/users/",
  },

  /**Super Admin */
  superAdmin: {
    towns: "/super-admin/towns/",
    branchOffices: "/super-admin/branch-offices/",
    users: "/super-admin/users/",
    roles: "/super-admin/roles/",
    permissions: "/super-admin/permissions/",
    visitPurposes: "/super-admin/visit-purposes/",
    subVisitPurposes: "/super-admin/sub-visit-purposes/",
    visitPurposesTypes: "/super-admin/visit-purposes-types/",
    services: "/super-admin/services/",
    reports: {
      tickets: "/super-admin/reports/tickets",
      users: "/super-admin/reports/users",
    },
  },

  /**API */

  api: {
    townsList: "towns-list/",
    branchOfficesList: "branch-offices-list/",
    servicesList: "services-list/",
    visitPurposesList: "visit-purposes-list/",
    subVisitPurposesList: "sub-visit-purposes-list/",
    updateUserWorkData: "update-work-data/",
    deskNumberCheck: "desk-number-check/",
    createTicket: "tickets/",
    acceptTicket: "accept-ticket/",
    serveTicket: "serve-ticket/",
    declineTicket: "decline-ticket/",
    completeService: "complete-service/",
    fullTicketsList: "full-tickets-list",
    ticket: "ticket",
    shortTicketsList: "short-tickets-list",
    getCustomersAmountUserServed: "get-customers-amount-user-served/",
    getUserAverageServiceTime: "get-user-average-service-time/",
    getUsersAverageServiceTime: "get-users-average-service-time/",
    getUsersMaxServiceTime: "get-users-max-service-time/",
    getUserData: "get-user-data/",
    updateStatisticsData: "update-statistics-data/",
    updateUserStatus: "update-user-status/",

    /**Pure Models List */
    getUsersList: "/users/",
    getServicesList: "/services/",
    getBranchOfficesList: "/branch-offices/",
    getTownsList: "/towns/",
    getVisitPurposesList: "/visit-purposes/",
    getUserStatusesList: "/user-statuses/",
    getVisitPurposeTypesList: "/visit-purpose-types/",
    getRolesList: "/roles/",
    getPermissionsList: "/permissions/",
    getIncomingTicketsList: "/incoming-tickets/",

    /**Super Admin */
    superAdmin: {
      /**Branch Office */
      storeBranchOffice: "/super-admin/branch-offices/",
      deleteBranchOffice: "/super-admin/branch-office/",
      updateBranchOffice: "/super-admin/branch-offices/",
      getPaginatedBranchOfficesList: "/super-admin/paginated-branch-offices/",

      /**Town */
      storeTown: "/super-admin/towns/",
      deleteTown: "/super-admin/town/",
      updateTown: "/super-admin/towns/",
      getPaginatedTownsList: "/super-admin/paginated-towns/",

      /**Permission */
      storePermission: "/super-admin/permissions/",
      deletePermission: "/super-admin/permission/",
      updatePermission: "/super-admin/permissions/",
      getPaginatedPermissionsList: "/super-admin/paginated-permissions/",

      /**Role */
      storeRole: "/super-admin/roles/",
      deleteRole: "/super-admin/role/",
      updateRole: "/super-admin/roles/",
      getPaginatedRolesList: "/super-admin/paginated-roles/",

      /**User */
      storeUser: "/super-admin/users/",
      getPaginatedUsersList: "/super-admin/paginated-users/",
      updateUser: "/super-admin/users/",
      deleteUser: "/super-admin/user/",
      getUsersVisitPurposesList: "/super-admin/get-visit-purposes-list/",
      getBranchOfficeListFilteredByTownId:
        "/super-admin/get-branch-office-list/",

      /**Visit Purpose  */
      storeVisitPurpose: "/super-admin/visit-purposes/",
      getPaginatedVisitPurposesList: "/super-admin/paginated-visit-purposes",
      updateVisitPurpose: "/super-admin/visit-purposes/",
      deleteVisitPurpose: "/super-admin/visit-purpose/",

      /**Sub Visit Purpose */
      storeSubVisitPurpose: "/super-admin/sub-visit-purposes/",
      getPaginatedSubVisitPurposesList:
        "/super-admin/paginated-sub-visit-purposes",
      updateSubVisitPurpose: "/super-admin/sub-visit-purposes/",
      deleteSubVisitPurpose: "/super-admin/sub-visit-purpose/",

      /**Visit Purpose Type */
      storeVisitPurposeType: "/super-admin/visit-purpose-types/",
      getPaginatedVisitPurposeTypesList:
        "/super-admin/paginated-visit-purpose-types",
      updateVisitPurposeType: "/super-admin/visit-purpose-types/",
      deleteSubVisitPurposeType: "/super-admin/visit-purpose-types/",

      /**Service  */
      storeService: "/super-admin/services/",
      getPaginatedServicesList: "/super-admin/paginated-services/",
      updateService: "/super-admin/services/",
      deleteService: "/super-admin/service/",

      /**Report */
      report: {
        getPaginatedTicketsList: "/super-admin/reports/tickets",
        getPaginatedUserActionTimesList: "/super-admin/reports/users",
      },

      /**Ticket */
      ticketsExport: "super-admin/tickets/export",
      usersExport: "super-admin/users/export",
    },

    /**Hall Admin */
    hallAdmin: {
      /**User */
      updateUser: "/hall-admin/users/",
      getPaginatedUsersList: "/hall-admin/users/",
    },

    /**Admin */
    admin: {
      /** Ticket */
      getDeclinedTicketsList: "/admin/declined-tickets/",
      recoverTicket: "/admin/recover-ticket/",
      removeTickets: "/admin/remove-tickets/",
    },
    crm: {
      //get all branches from crm
      branches: "/crm/branches/",
      //associate crm branch with queue branch
      associateBranches: "/super-admin/associate/branches",
    },
  },
} as const;

export type Route = typeof routes[keyof typeof routes];
