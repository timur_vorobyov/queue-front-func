import { useState, createContext } from "react";
import React from "react";

export const CreateTicketPagesContext = createContext({
  visitPurposeId: 0,
  setVisitPurposeId: (_value: number) => {
    return;
  },
  subVisitPurposeId: 0,
  setSubVisitPurposeId: (_value: number) => {
    return;
  },
  customerPhoneNumber: "",
  setCustomerPhoneNumber: (_value: string) => {
    return;
  },
  isWithoutCustomerPhoneNumber: false,
  setIsWithoutCustomerPhoneNumber: (_value: boolean) => {
    return;
  },
  isRedirectedToHomePage: false,
  setIsRedirectedToHomePage: (_value: boolean) => {
    return;
  },
});

const CreateTicketPagesContextProvider = (props: {
  children: JSX.Element;
}): JSX.Element => {
  const [visitPurposeId, setVisitPurposeId] = useState(0);
  const [subVisitPurposeId, setSubVisitPurposeId] = useState(0);
  const [customerPhoneNumber, setCustomerPhoneNumber] = useState("");
  const [isWithoutCustomerPhoneNumber, setIsWithoutCustomerPhoneNumber] =
    useState(false);
  const [isRedirectedToHomePage, setIsRedirectedToHomePage] = useState(false);

  return (
    <CreateTicketPagesContext.Provider
      value={{
        visitPurposeId,
        setVisitPurposeId,
        subVisitPurposeId,
        setSubVisitPurposeId,
        customerPhoneNumber,
        setCustomerPhoneNumber,
        isWithoutCustomerPhoneNumber,
        setIsWithoutCustomerPhoneNumber,
        isRedirectedToHomePage,
        setIsRedirectedToHomePage,
      }}
    >
      {props.children}
    </CreateTicketPagesContext.Provider>
  );
};

export default CreateTicketPagesContextProvider;
