import { LOCALES } from "src/i18n";
import { useState, createContext } from "react";
import React from "react";

export const I18nContext = createContext({
  locale: "ru",
  setLocale: (_value: string) => {
    return;
  },
});

const I18nContextProvider = (props: { children: JSX.Element }): JSX.Element => {
  const localStorageLocale = localStorage.getItem("locale");
  let defaultLocale;
  if (!localStorageLocale) {
    defaultLocale = LOCALES.RUSSIAN;
  } else {
    defaultLocale = localStorageLocale;
  }

  const [locale, setLocale] = useState(defaultLocale);

  return (
    <I18nContext.Provider value={{ locale, setLocale }}>
      {props.children}
    </I18nContext.Provider>
  );
};

export default I18nContextProvider;
