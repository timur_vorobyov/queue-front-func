export const getUserData = () => {
  const userJsonData = localStorage.getItem("queue-user-data");
  const userData = userJsonData ? JSON.parse(userJsonData) : "";
  return userData;
};

export const setUserData = (userData: any) => {
  localStorage.setItem("queue-user-data", JSON.stringify(userData));
};

export const userId = () => {
  const userData = getUserData();
  return userData.id;
};
export const userFullName = () => {
  const userData = getUserData();
  return userData.fullName;
};
export const getBranchOfficeId = () => {
  const userData = getUserData();
  return userData.branchOfficeId;
};
export const branchOfficeFullName = () => {
  const userData = getUserData();
  return userData.branchOfficeFullName;
};
export const getDeskNumber = () => {
  const userData = getUserData();
  return userData.deskNumber;
};
export const getVisitPurposesIds = () => {
  const userData = getUserData();
  return userData.visitPurposesIds;
};
export const visitPurposesNames = () => {
  const userData = getUserData();
  return userData.visitPurposesNames;
};
export const roleName = () => {
  const userData = getUserData();
  return userData.roleName;
};
export const townId = () => {
  const userData = getUserData();
  return userData.townId;
};
export const timer = () => {
  const userData = getUserData();
  return userData.timer;
};
export const getAccessToken = () => {
  const token = localStorage.getItem("queue-access-token");
  const accessToken = token != "null" && token != " " ? token : "";
  return accessToken;
};

export const userStatusId = () => {
  const userData = getUserData();
  return userData.userStatusId;
};

export const userStatusName = () => {
  const userData = getUserData();
  return userData.userStatusName;
};

export const userIsLoggedIn = () => {
  return getAccessToken() ? true : false;
};
