import Echo from "laravel-echo";
// @ts-ignore
import socketio from "socket.io-client";

export const createEchoInstance = () => {
  const options = {
    broadcaster: "socket.io",
    host: process.env.SOCKET_API,
    transports: ["websocket", "polling", "flashsocket"],
    client: socketio,
    auth: {
      headers: {
        Authorization: "Bearer " + localStorage.getItem("queue-access-token"),
      },
    },
  };

  const echo = new Echo(options);

  return echo;
};
