import axios from "axios";
import { getAccessToken } from "./auth-data";

const apiUrl = process.env.BACKEND_URL;

export default axios.create({
  baseURL: apiUrl,
  headers: {
    Authorization: "Bearer " + getAccessToken(),
    "Content-Type": "application/json",
    Accept: "application/json",
  },
});
