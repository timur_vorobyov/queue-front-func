import tj from "./tj";
import ru from "./ru";

export default {
  ...tj,
  ...ru,
};
