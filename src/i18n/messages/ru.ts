import { LOCALES } from "../locales";

export default {
  [LOCALES.RUSSIAN]: {
    welcome: "Добро пожаловать",
    pushToGetOrder: "Нажмите, чтобы получить очередь",
    getOrder: "Получить очередь",
    inputPhoneNumber: "Введите номер вашего мобильного телефона",
    youWillReceiveTheMessage: "Вы получите SMS с номером билета",
    back: "Назад",
    send: "Отправить",
    phoneNumberNotExist: "Нет мобильного телефона",
    thank: "Спасибо",
    messageWasSentOnYourPhone:
      "Номер вашей очереди отправлен вам в виде СМС на телефон",
    checkYourPhone: "Проверьте ваш телефон, пожалуйста",
    yourPhoneNumberIs: "Ваш номер очереди",
    notForget: "Не забудьте",
    yourVisitPurpose: "Цель вашего визита",
    yourSubVisitPurpose: "Подцель вашего визита",
    backToHome: "Вернуться на главную",
    enterYourNumberInFull: "Пожалуйста полностью введите ваш номер",
  },
};
