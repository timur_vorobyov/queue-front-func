import { LOCALES } from "../locales";

export default {
  [LOCALES.TAJIK]: {
    welcome: "Хуш омадед",
    pushToGetOrder: "Барои навбат гирифтан пахш намоед",
    getOrder: "Навбат гирифтан",
    inputPhoneNumber: "Рақами мобилии худро ворид кунед",
    youWillReceiveTheMessage: "Ба Шумо СМС бо рақами навбат меояд",
    back: "Ба кафо",
    send: "Ирсол",
    phoneNumberNotExist: "Телефони мобили надорам",
    thank: "Ташаккур",
    messageWasSentOnYourPhone:
      "Рақами навбати Шумо ба телефонатон ҳамчун СМС фиристода шуд",
    checkYourPhone: "Марҳамат телефонатонро тафтиш кунед",
    yourPhoneNumberIs: "Раками навбати шумо",
    notForget: "Аз хотир набароред",
    yourVisitPurpose: "Мақсади ташрифи Шумо",
    yourSubVisitPurpose: "Зермаксад ташрифи Шумо",
    backToHome: "Ба асосӣ",
    enterYourNumberInFull: "Лутфан рақамҳоятонро пурра ворид кунед",
  },
};
