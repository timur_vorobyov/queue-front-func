import { I18nContext } from "src/contexts/i18nContextProvider/I18nContextProvider";
import React from "react";
import { Fragment, useContext } from "react";
import { IntlProvider } from "react-intl";
import messages from "./messages";
import { LOCALES } from "./locales";

type Provider = {
  children: JSX.Element;
};

const Provider = ({ children }: Provider) => {
  const { locale } = useContext(I18nContext);

  return (
    <IntlProvider
      defaultLocale={LOCALES.RUSSIAN}
      locale={locale}
      textComponent={Fragment}
      messages={messages[locale]}
    >
      {children}
    </IntlProvider>
  );
};

export default Provider;
