import React from "react";
import { render } from "react-dom";
import AppWrapper from "src/wrappers/AppWrapper";
import "!style-loader!css-loader!bootstrap/dist/css/bootstrap.css";
import "!style-loader!css-loader!react-notifications/lib/notifications.css";
import "!style-loader!css-loader!react-datepicker/dist/react-datepicker.css";
import PagesWithoutSideBars from "./pages/routers/PagesWithoutNavAndSideBars/PagesWithoutNavAndSideBars";
import { Provider } from "react-redux";
import { store } from "src/store";
import Layout from "./components/Layout/Layout";
import I18nContextProvider from "./contexts/i18nContextProvider";
import { I18nProvider } from "./i18n";

render(
  <AppWrapper>
    <Provider store={store}>
      <I18nContextProvider>
        <I18nProvider>
          <PagesWithoutSideBars>
            <Layout />
          </PagesWithoutSideBars>
        </I18nProvider>
      </I18nContextProvider>
    </Provider>
  </AppWrapper>,
  document.getElementById("root") as HTMLElement
);
