import React from "react";
import { Button, Modal } from "react-bootstrap";
import commonClasses from "./incomingTicketDeleteModel.css";
import cn from "clsx";
import Typography from "src/components/Typography";
import { createNotification } from "src/helpers/crud-notifications";
import { ModalDeletePropertiesState } from "../../types";
import { removeTickets } from "src/apis/AdminPages/AdminIncomingTicketPage/incomingTicket";
interface IncomingTicketDeleteModalProps {
  modalDeletePropertiesState: ModalDeletePropertiesState;
  recallIncomingTicketApi: () => void;
  incomingTicketsIds: number[];
}

const IncomingTicketDeleteModal = ({
  modalDeletePropertiesState,
  recallIncomingTicketApi,
  incomingTicketsIds,
}: IncomingTicketDeleteModalProps) => {
  const handleConfirmationDeleteVisitPurposeModalClick = async () => {
    const response = await removeTickets(incomingTicketsIds);

    if (response == "INCOMING_TICKETS_ARE_REMOVED") {
      createNotification("success", "Билеты удалены");
      modalDeletePropertiesState.setIsDeleteModalShown(false);
      recallIncomingTicketApi();
    }
  };

  return (
    <Modal
      className={cn(commonClasses.modalDiolog)}
      show={modalDeletePropertiesState.isDeleteModalShown}
      onHide={() => modalDeletePropertiesState.setIsDeleteModalShown(false)}
    >
      <Modal.Header closeButton>
        <Modal.Title>
          <Typography variant="span">
            Вы уверены что хотите удалить билеты?
          </Typography>
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className={cn(commonClasses.modalDeleteActionButtons)}>
        <Button
          className={cn(commonClasses.modalDeleteActionButton)}
          variant="secondary"
          onClick={() => handleConfirmationDeleteVisitPurposeModalClick()}
        >
          <Typography color="white" variant="span">
            Да
          </Typography>
        </Button>
        <Button
          className={cn(commonClasses.modalDeleteActionButton)}
          variant="primary"
          onClick={() =>
            modalDeletePropertiesState.setIsDeleteModalShown(false)
          }
        >
          <Typography color="white" variant="span">
            Нет
          </Typography>
        </Button>
      </Modal.Body>
    </Modal>
  );
};

export default IncomingTicketDeleteModal;
