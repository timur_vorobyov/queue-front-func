import React, { useEffect, useState } from "react";
import { getBranchOfficesList } from "src/apis/AdminPages/SuperAdminPages/branchOffice";
import { getTownsList } from "src/apis/AdminPages/SuperAdminPages/town";
import { BranchOffice } from "src/types/Admin/branchOffice";
import { Town } from "src/types/Admin/town";
import useApiCall from "src/utils/hooks/use-async-call";
// @ts-ignore
import { NotificationContainer } from "react-notifications";
import cn from "clsx";
import { Button, Form } from "react-bootstrap";
import Typography from "src/components/Typography";
import { getIncomingTickets } from "src/apis/AdminPages/AdminIncomingTicketPage/incomingTicket";
import classes from "./IncomingTicketPage.css";
import IncomingTicketCard from "./IncomingTicketCard";
import { Ticket } from "src/store/types/ticket";
import IncomingTicketDeleteModal from "./IncomingTicketDeleteModal";
import { getVisitPurposesList } from "src/apis/AdminPages/SuperAdminPages/visitPurpose";
import { VisitPurpose } from "src/types/Admin/visitPurpose";

const IncomingTicketPage = () => {
  const [branchOfficeId, setBranchOfficeId] = useState<number>(0);
  const [townId, setTownId] = useState<number>(0);
  const [townsList, setTownsList] = useState([]);
  const [towns, callTownApi] = useApiCall(getTownsList);
  const [branchOfficesList, setBranchOfficesList] = useState([]);
  const [branchOffices, callBranchOfficeApi] = useApiCall(getBranchOfficesList);
  const [incomingTicketsList, setIncomingTicketsList] = useState([]);
  const [data, callIncomigTicketApi] = useApiCall(getIncomingTickets);
  const [isDeleteModalShown, setIsDeleteModalShown] = useState<boolean>(false);
  const [isVisitPurposeChosen, setIsVisitPurposeChosen] = useState(false);
  const [isRemoveActionAllowed, setIsRemoveActionAllowed] = useState(false);
  const [visitPurposesList, setVisitPurposesList] = useState([]);
  const [visitPurposes, callVisitPurposesApi] = useApiCall(
    getVisitPurposesList
  );
  const [visitPurposeId, setVisitPurposeId] = useState<number>(0);

  useEffect(() => {
    callIncomigTicketApi([branchOfficeId, visitPurposeId]);
    callTownApi();
  }, []);

  useEffect(() => {
    if (data) {
      setIncomingTicketsList(data);
    }
  }, [data]);

  useEffect(() => {
    if (towns.length) {
      setTownsList(towns);
    }
  }, [towns]);

  useEffect(() => {
    if (branchOffices.length) {
      setBranchOfficesList(branchOffices);
    }
  }, [branchOffices]);

  useEffect(() => {
    if (visitPurposes.length) {
      setVisitPurposesList(visitPurposes);
    }
  }, [visitPurposes]);

  const recallIncomingTicketApi = () => {
    callIncomigTicketApi([branchOfficeId, visitPurposeId]);
  };

  const getBranchOfficeOptions = (): JSX.Element[] =>
    branchOfficesList.map((branchOffice: BranchOffice, index: number) => {
      return (
        <option key={index} value={branchOffice.id}>
          {branchOffice.name}
        </option>
      );
    });

  const getTownOptions = (): JSX.Element[] =>
    townsList.map((town: Town, index: number) => {
      return (
        <option key={index} value={town.id}>
          {town.name}
        </option>
      );
    });

  const getVisitPurposeOptions = (): JSX.Element[] =>
    visitPurposesList.map((visitPurpose: VisitPurpose, index: number) => {
      return (
        <option key={index} value={visitPurpose.id}>
          {visitPurpose.name}
        </option>
      );
    });

  const setTownState = async (id: number) => {
    setTownId(id);
    callBranchOfficeApi([id]);
  };

  const setBranchOfficeState = async (id: number) => {
    setBranchOfficeId(id);
    callVisitPurposesApi([id]);
  };

  const setVisitPurposeState = async (id: number) => {
    setVisitPurposeId(id);
    setIsVisitPurposeChosen(true);
  };

  const filter = () => {
    callIncomigTicketApi([branchOfficeId, visitPurposeId]);
    setIsRemoveActionAllowed(true);
  };
  return (
    <main className={cn(classes.container)}>
      <NotificationContainer />
      <div className={cn(classes.containerHeader)}>
        <div className={cn(classes.filters)}>
          <Form.Group>
            <Form.Control
              as="select"
              value={townId}
              onChange={(event: React.ChangeEvent<HTMLSelectElement>) =>
                setTownState(Number(event.target.value))
              }
              custom
            >
              <option value="0">Все города</option>
              {getTownOptions()}
            </Form.Control>
          </Form.Group>
          <Form.Group className={cn(classes.filter)}>
            <Form.Control
              as="select"
              value={branchOfficeId}
              onChange={(event: React.ChangeEvent<HTMLSelectElement>) =>
                setBranchOfficeState(Number(event.target.value))
              }
              custom
            >
              <option value="0">Все филиалы</option>
              {getBranchOfficeOptions()}
            </Form.Control>
          </Form.Group>
          <Form.Group className={cn(classes.filter)}>
            <Form.Control
              as="select"
              value={visitPurposeId}
              onChange={(event: React.ChangeEvent<HTMLSelectElement>) =>
                setVisitPurposeState(Number(event.target.value))
              }
              custom
            >
              <option value="0">Все цели визита</option>
              {getVisitPurposeOptions()}
            </Form.Control>
          </Form.Group>
          <Button
            disabled={!isVisitPurposeChosen}
            className={cn(classes.filter)}
            variant="primary"
            onClick={() => filter()}
          >
            <Typography color="white" variant="span">
              Фильтр
            </Typography>
          </Button>
          <Button
            disabled={!isRemoveActionAllowed}
            className={cn(classes.filter)}
            variant="danger"
            onClick={() => setIsDeleteModalShown(true)}
          >
            <Typography color="white" variant="span">
              Удалить все
            </Typography>
          </Button>
        </div>
      </div>
      <div className={cn(classes.incomingTickets)}>
        {incomingTicketsList.map((incomingTicket: Ticket) => {
          return (
            <IncomingTicketCard
              key={incomingTicket.id}
              incomingTicket={incomingTicket}
            />
          );
        })}
      </div>
      <IncomingTicketDeleteModal
        modalDeletePropertiesState={{
          isDeleteModalShown: isDeleteModalShown,
          setIsDeleteModalShown: setIsDeleteModalShown,
        }}
        incomingTicketsIds={incomingTicketsList.map((incomingTicket: Ticket) =>
          Number(incomingTicket.id)
        )}
        recallIncomingTicketApi={recallIncomingTicketApi}
      />
    </main>
  );
};

export default IncomingTicketPage;
