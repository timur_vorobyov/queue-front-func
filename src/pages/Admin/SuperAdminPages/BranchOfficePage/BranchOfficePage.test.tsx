import {
  cleanup,
  waitForElementToBeRemoved,
  screen,
  fireEvent,
} from "@testing-library/react";
import React from "react";
import { server } from "src/__mocks__/automatic/mockHttp";
import {
  DELETE_BRANCH_OFFICE_RESPONSE,
  GET_BRANCH_OFFICES_FILTERED_BY_SEARCHER_LIST_RESPONSE,
  GET_BRANCH_OFFICES_FILTERED_BY_TOWN_LIST_RESPONSE,
  GET_BRANCH_OFFICES_WITH_INCRAESED_LIMIT_LIST_RESPONSE,
  GET_PAGINATED_BRANCH_OFFICES_LIST_RESPONSE,
  GET_SECOND_PAGE_PAGINATED_BRANCH_OFFICES_LIST_RESPONSE,
  STORE_BRANCH_OFFICE_RESPONSE,
  STORE_BRANCH_OFFICE_WITH_THE_SAME_INITIAL_RESPONSE,
  UPDATE_BRANCH_OFFICE_RESPONSE,
  UPDATE_BRANCH_OFFICE_WITH_THE_SAME_INITIAL_RESPONSE,
} from "src/__mocks__/handlers/branchOffice";
import { GET_TOWNS_LIST_RESPONSE } from "src/__mocks__/handlers/town";
import { GET_VISIT_PURPOSES_LIST_RESPONSE } from "src/__mocks__/handlers/visitPurpose";
import BranchOfficePage from "./BranchOfficePage";
import "@testing-library/jest-dom";
import { renderWithRouter } from "src/__mocks__/utils";
import { mockedTowns } from "src/__mocks__/domains/town";
import { mockedBranchOffices } from "src/__mocks__/domains/branchOffice";

afterEach(cleanup);

describe("BranchOfficePage component", () => {
  it("should show data in the table", async () => {
    const limit = 5;
    const page = 1;
    const searchExpression = "";
    const townId = 0;

    server.use(...GET_TOWNS_LIST_RESPONSE);
    server.use(...GET_VISIT_PURPOSES_LIST_RESPONSE);
    server.use(
      ...GET_PAGINATED_BRANCH_OFFICES_LIST_RESPONSE(
        townId,
        searchExpression,
        limit,
        page
      )
    );
    renderWithRouter(<BranchOfficePage />);

    await waitForElementToBeRemoved(() => screen.getByLabelText("Loading"));
    expect(screen.getByText("Алиф")).toBeInTheDocument();
  });

  it("should paginate to second page", async () => {
    const limit = 5;
    const page = 1;
    const searchExpression = "";
    const townId = 0;

    server.use(...GET_TOWNS_LIST_RESPONSE);
    server.use(...GET_VISIT_PURPOSES_LIST_RESPONSE);
    server.use(
      ...GET_PAGINATED_BRANCH_OFFICES_LIST_RESPONSE(
        townId,
        searchExpression,
        limit,
        page
      )
    );
    renderWithRouter(<BranchOfficePage />);

    await waitForElementToBeRemoved(() => screen.getByLabelText("Loading"));
    expect(screen.getByText("Алиф")).toBeInTheDocument();

    const paginationSecondPageButton = screen.getByText("2");
    server.use(
      ...GET_SECOND_PAGE_PAGINATED_BRANCH_OFFICES_LIST_RESPONSE(
        townId,
        searchExpression,
        limit,
        2
      )
    );
    fireEvent.click(paginationSecondPageButton);
    await waitForElementToBeRemoved(() => screen.getByLabelText("Loading"));
    expect(screen.queryByText("Алиф")).not.toBeInTheDocument();
    expect(screen.queryByText("Волна")).toBeInTheDocument();
  });

  it("should show 10 items on the first page", async () => {
    const limit = 5;
    const page = 1;
    const searchExpression = "";
    const townId = 0;

    server.use(...GET_TOWNS_LIST_RESPONSE);
    server.use(...GET_VISIT_PURPOSES_LIST_RESPONSE);
    server.use(
      ...GET_PAGINATED_BRANCH_OFFICES_LIST_RESPONSE(
        townId,
        searchExpression,
        limit,
        page
      )
    );
    renderWithRouter(<BranchOfficePage />);

    await waitForElementToBeRemoved(() => screen.getByLabelText("Loading"));
    expect(screen.queryByText("Алиф")).toBeInTheDocument();

    const pageLimitSelector = screen.getByTestId("page-limit-selector");
    fireEvent.change(pageLimitSelector, { target: { value: "10" } });
    const filterButton = screen.getByTestId("filter");
    server.use(
      ...GET_BRANCH_OFFICES_WITH_INCRAESED_LIMIT_LIST_RESPONSE(
        townId,
        searchExpression,
        10,
        page
      )
    );
    fireEvent.click(filterButton);
    await waitForElementToBeRemoved(() => screen.getByLabelText("Loading"));
    expect(screen.queryByText("Алиф")).toBeInTheDocument();
    expect(screen.queryByText("Сомон")).toBeInTheDocument();
    expect(screen.getByText("2")).toBeInTheDocument();
  });

  it("should show 2 items which includes Волна inside its name", async () => {
    const limit = 5;
    const page = 1;
    const searchExpression = "";
    const townId = 0;

    server.use(...GET_TOWNS_LIST_RESPONSE);
    server.use(...GET_VISIT_PURPOSES_LIST_RESPONSE);
    server.use(
      ...GET_PAGINATED_BRANCH_OFFICES_LIST_RESPONSE(
        townId,
        searchExpression,
        limit,
        page
      )
    );
    renderWithRouter(<BranchOfficePage />);

    await waitForElementToBeRemoved(() => screen.getByLabelText("Loading"));
    expect(screen.queryByText("Алиф")).toBeInTheDocument();

    const branchOfficeName = "Волна";
    const searchInput = screen.getByTestId("search-selector");
    fireEvent.change(searchInput, { target: { value: branchOfficeName } });
    const filterButton = screen.getByTestId("filter");
    server.use(
      ...GET_BRANCH_OFFICES_FILTERED_BY_SEARCHER_LIST_RESPONSE(
        townId,
        branchOfficeName,
        limit,
        page
      )
    );
    fireEvent.click(filterButton);
    await waitForElementToBeRemoved(() => screen.getByLabelText("Loading"));
    expect(screen.queryByText("Волна")).toBeInTheDocument();
    expect(screen.queryByText("Волна-2")).toBeInTheDocument();
    expect(screen.queryByText("Алиф")).not.toBeInTheDocument();
  });

  it("should show branch offices of Душанбе", async () => {
    const limit = 5;
    const page = 1;
    const searchExpression = "";
    const townId = 0;

    server.use(...GET_TOWNS_LIST_RESPONSE);
    server.use(...GET_VISIT_PURPOSES_LIST_RESPONSE);
    server.use(
      ...GET_PAGINATED_BRANCH_OFFICES_LIST_RESPONSE(
        townId,
        searchExpression,
        limit,
        page
      )
    );
    renderWithRouter(<BranchOfficePage />);

    await waitForElementToBeRemoved(() => screen.getByLabelText("Loading"));
    expect(screen.queryByText("Алиф")).toBeInTheDocument();

    const townSelector = screen.getByTestId("town-selector");
    fireEvent.change(townSelector, { target: { value: mockedTowns[0].id } });
    const filterButton = screen.getByTestId("filter");
    server.use(
      ...GET_BRANCH_OFFICES_FILTERED_BY_TOWN_LIST_RESPONSE(
        mockedTowns[0].id,
        searchExpression,
        limit,
        page
      )
    );
    fireEvent.click(filterButton);
    await waitForElementToBeRemoved(() => screen.getByLabelText("Loading"));
    expect(screen.queryByText("МХБ")).toBeInTheDocument();
    expect(screen.queryByText("Бат")).toBeInTheDocument();
    expect(screen.queryByText("Зисор")).toBeInTheDocument();
    expect(screen.queryByText("Нидур")).toBeInTheDocument();
    expect(screen.queryByText("Сомон")).toBeInTheDocument();
    expect(screen.queryByText("Алиф")).not.toBeInTheDocument();
  });

  it("should open remove pop up when user click on cross icon", async () => {
    const limit = 5;
    const page = 1;
    const searchExpression = "";
    const townId = 0;

    server.use(...GET_TOWNS_LIST_RESPONSE);
    server.use(...GET_VISIT_PURPOSES_LIST_RESPONSE);
    server.use(
      ...GET_PAGINATED_BRANCH_OFFICES_LIST_RESPONSE(
        townId,
        searchExpression,
        limit,
        page
      )
    );
    renderWithRouter(<BranchOfficePage />);

    await waitForElementToBeRemoved(() => screen.getByLabelText("Loading"));
    expect(screen.queryByText("Алиф")).toBeInTheDocument();

    const removeButton = screen.getByTestId(
      `remove-icon-${mockedBranchOffices[0].id}`
    );
    fireEvent.click(removeButton);
    expect(
      screen.queryByText("Вы уверены что хотите удалить выбранный пункт?")
    ).toBeInTheDocument();
  });

  it("should remove the chosen branch office when a user click on the cross icon", async () => {
    const limit = 5;
    const page = 1;
    const searchExpression = "";
    const townId = 0;

    server.use(...GET_TOWNS_LIST_RESPONSE);
    server.use(...GET_VISIT_PURPOSES_LIST_RESPONSE);
    server.use(
      ...GET_PAGINATED_BRANCH_OFFICES_LIST_RESPONSE(
        townId,
        searchExpression,
        limit,
        page
      )
    );
    renderWithRouter(<BranchOfficePage />);
    await waitForElementToBeRemoved(() => screen.getByLabelText("Loading"));
    expect(screen.queryByText("Алиф")).toBeInTheDocument();
    const removeIconButton = screen.getByTestId(
      `remove-icon-${mockedBranchOffices[0].id}`
    );
    fireEvent.click(removeIconButton);
    const removeButton = screen.getByText("Да");
    server.use(...DELETE_BRANCH_OFFICE_RESPONSE);
    fireEvent.click(removeButton);
    await waitForElementToBeRemoved(() =>
      screen.getByText("Вы уверены что хотите удалить выбранный пункт?")
    );
    expect(screen.queryByText("Филиал удален")).toBeInTheDocument();
    /** the logic of removing, recalls the
      branchOffices list as a result data from the
      previous rendering get not up to date. So if you try to find branchOfficeName from previous rendering
      jest will not find it
      */
    expect(screen.queryByText("Авранг")).toBeInTheDocument();
  });

  it("should create a new branch office", async () => {
    const limit = 5;
    const page = 1;
    const searchExpression = "";
    const townId = 0;

    server.use(...GET_TOWNS_LIST_RESPONSE);
    server.use(...GET_VISIT_PURPOSES_LIST_RESPONSE);
    server.use(
      ...GET_PAGINATED_BRANCH_OFFICES_LIST_RESPONSE(
        townId,
        searchExpression,
        limit,
        page
      )
    );
    renderWithRouter(<BranchOfficePage />);
    await waitForElementToBeRemoved(() => screen.getByLabelText("Loading"));
    const addButton = screen.getByTestId("add-branch-office");
    fireEvent.click(addButton);
    expect(screen.queryByText("Добавить филиал")).toBeInTheDocument();

    const branchOfficeNameInput = screen.getByTestId("branch-office-name");
    fireEvent.change(branchOfficeNameInput, { target: { value: "Баку" } });
    const timerInput = screen.getByTestId("timer");
    fireEvent.change(timerInput, { target: { value: "5000" } });
    const initialInput = screen.getByTestId("initial");
    fireEvent.change(initialInput, { target: { value: "A" } });
    const townIdSelector = screen.getByTestId("town-id");
    fireEvent.change(townIdSelector, { target: { value: "2" } });
    /**for react-select data get transfered only this way*/
    const visitPurposeIdsInput = screen.getByLabelText("visit-purpose-ids");
    fireEvent.change(visitPurposeIdsInput, { target: { value: "Касса" } });
    const options = screen.getAllByText("Касса");
    fireEvent.click(options[5]);
    /**for react-select */
    const submitButton = screen.getByText("Сохранить");
    server.use(...STORE_BRANCH_OFFICE_RESPONSE);
    fireEvent.click(submitButton);
    await waitForElementToBeRemoved(() => screen.getByText("Добавить филиал"));
    expect(screen.queryByText("Филиал сохранён")).toBeInTheDocument();
  });

  it("should output an error if the branch office with the same initial already exists when creating a new branch office", async () => {
    const limit = 5;
    const page = 1;
    const searchExpression = "";
    const townId = 0;

    server.use(...GET_TOWNS_LIST_RESPONSE);
    server.use(...GET_VISIT_PURPOSES_LIST_RESPONSE);
    server.use(
      ...GET_PAGINATED_BRANCH_OFFICES_LIST_RESPONSE(
        townId,
        searchExpression,
        limit,
        page
      )
    );
    renderWithRouter(<BranchOfficePage />);
    await waitForElementToBeRemoved(() => screen.getByLabelText("Loading"));
    const addButton = screen.getByTestId("add-branch-office");
    fireEvent.click(addButton);
    expect(screen.queryByText("Добавить филиал")).toBeInTheDocument();

    const branchOfficeNameInput = screen.getByTestId("branch-office-name");
    fireEvent.change(branchOfficeNameInput, { target: { value: "Баку" } });
    const timerInput = screen.getByTestId("timer");
    fireEvent.change(timerInput, { target: { value: "5000" } });
    const initialInput = screen.getByTestId("initial");
    fireEvent.change(initialInput, { target: { value: "A" } });
    const townIdSelector = screen.getByTestId("town-id");
    fireEvent.change(townIdSelector, { target: { value: "2" } });
    const visitPurposeIdsInput = screen.getByLabelText("visit-purpose-ids");
    fireEvent.change(visitPurposeIdsInput, { target: { value: "Касса" } });
    const options = screen.getAllByText("Касса");
    fireEvent.click(options[5]);
    const submitButton = screen.getByText("Сохранить");
    server.use(...STORE_BRANCH_OFFICE_WITH_THE_SAME_INITIAL_RESPONSE);
    fireEvent.click(submitButton);
    await waitForElementToBeRemoved(() => screen.getByText("Добавить филиал"));
    const error = screen.queryByText(
      "Инициал который вы пытаетесь сохранить уже используется"
    );
    expect(error).toBeInTheDocument();
    if (error) {
      fireEvent.click(error);
    }
  });

  it("should update a branch office", async () => {
    const limit = 5;
    const page = 1;
    const searchExpression = "";
    const townId = 0;

    server.use(...GET_TOWNS_LIST_RESPONSE);
    server.use(...GET_VISIT_PURPOSES_LIST_RESPONSE);
    server.use(
      ...GET_PAGINATED_BRANCH_OFFICES_LIST_RESPONSE(
        townId,
        searchExpression,
        limit,
        page
      )
    );
    renderWithRouter(<BranchOfficePage />);
    await waitForElementToBeRemoved(() => screen.getByLabelText("Loading"));
    expect(screen.queryByText("Алиф")).toBeInTheDocument();
    const updateIconButton = screen.getByTestId(`update-icon-1`);
    fireEvent.click(updateIconButton);
    expect(screen.queryByText("Изменить филиал")).toBeInTheDocument();
    const branchOfficeNameInput = screen.getByTestId("branch-office-name");
    const timerInput = screen.getByTestId("timer");
    const initialInput = screen.getByTestId("initial");
    const townIdSelector = screen.getByTestId("town-id");
    const visitPurposeIdsInputValue = screen.getAllByText("Рассрочка")[1];
    expect(townIdSelector).toHaveValue("2");
    expect(branchOfficeNameInput).toHaveValue("Алиф");
    expect(timerInput).toHaveValue("5000");
    expect(initialInput).toHaveValue("A");
    expect(visitPurposeIdsInputValue).toBeInTheDocument();

    fireEvent.change(branchOfficeNameInput, { target: { value: "Баку" } });
    fireEvent.change(timerInput, { target: { value: "6000" } });
    fireEvent.change(initialInput, { target: { value: "B" } });
    fireEvent.change(townIdSelector, { target: { value: "1" } });
    const visitPurposeIdsInput = screen.getByLabelText("visit-purpose-ids");
    fireEvent.change(visitPurposeIdsInput, { target: { value: "Касса" } });
    const options = screen.getAllByText("Касса");
    fireEvent.click(options[5]);
    const submitButton = screen.getByText("Обновить");
    server.use(...UPDATE_BRANCH_OFFICE_RESPONSE);
    fireEvent.click(submitButton);
    await waitForElementToBeRemoved(() => screen.getByText("Изменить филиал"));
    expect(screen.queryByText("Филиал обновлен")).toBeInTheDocument();
  });

  it("should output an error if the branch office with the same initial already exists when updating a new branch office", async () => {
    const limit = 5;
    const page = 1;
    const searchExpression = "";
    const townId = 0;

    server.use(...GET_TOWNS_LIST_RESPONSE);
    server.use(...GET_VISIT_PURPOSES_LIST_RESPONSE);
    server.use(
      ...GET_PAGINATED_BRANCH_OFFICES_LIST_RESPONSE(
        townId,
        searchExpression,
        limit,
        page
      )
    );
    renderWithRouter(<BranchOfficePage />);
    await waitForElementToBeRemoved(() => screen.getByLabelText("Loading"));
    expect(screen.queryByText("Алиф")).toBeInTheDocument();
    const updateIconButton = screen.getByTestId(`update-icon-1`);
    fireEvent.click(updateIconButton);
    expect(screen.queryByText("Изменить филиал")).toBeInTheDocument();
    const branchOfficeNameInput = screen.getByTestId("branch-office-name");
    const timerInput = screen.getByTestId("timer");
    const initialInput = screen.getByTestId("initial");
    const townIdSelector = screen.getByTestId("town-id");
    const visitPurposeIdsInputValue = screen.getAllByText("Рассрочка")[1];
    expect(townIdSelector).toHaveValue("2");
    expect(branchOfficeNameInput).toHaveValue("Алиф");
    expect(timerInput).toHaveValue("5000");
    expect(initialInput).toHaveValue("A");
    expect(visitPurposeIdsInputValue).toBeInTheDocument();

    fireEvent.change(branchOfficeNameInput, { target: { value: "Баку" } });
    fireEvent.change(timerInput, { target: { value: "6000" } });
    fireEvent.change(initialInput, { target: { value: "B" } });
    fireEvent.change(townIdSelector, { target: { value: "1" } });
    const visitPurposeIdsInput = screen.getByLabelText("visit-purpose-ids");
    fireEvent.change(visitPurposeIdsInput, { target: { value: "Касса" } });
    const options = screen.getAllByText("Касса");
    fireEvent.click(options[5]);
    const submitButton = screen.getByText("Обновить");
    server.use(...UPDATE_BRANCH_OFFICE_WITH_THE_SAME_INITIAL_RESPONSE);
    fireEvent.click(submitButton);
    await waitForElementToBeRemoved(() => screen.getByText("Изменить филиал"));
    expect(
      screen.queryByText(
        "Инициал который вы пытаетесь сохранить уже используется"
      )
    ).toBeInTheDocument();
  });
});
