import React, { useEffect, useState } from "react";
import classes from "./BranchOfficePage.css";
import cn from "clsx";
// @ts-ignore
import { NotificationContainer } from "react-notifications";
import { MODAL_TYPE } from "../consts";
import useApiCall from "src/utils/hooks/use-async-call";
import Pagination from "src/components/Pagination/Pagination";
import Typography from "src/components/Typography";
import AdminPagesWrapper from "src/wrappers/AdminPagesWrapper";
import { AdminPagesWrapperProps } from "src/wrappers/AdminPagesWrapper/AdminPagesWrapper";
import { getPageOptions } from "../utils";
import { BranchOfficeInModal } from "src/types/Admin/SuperAdmin/BranchOffice/branchOffice";
import { Button, Form } from "react-bootstrap";
import { getPaginatedBranchOfficesList } from "src/apis/AdminPages/SuperAdminPages/BranchOfficePage/branchOffice";
import { getTownsList } from "src/apis/AdminPages/SuperAdminPages/town";
import BranchofficeTable from "./BranchOfficeTable";
import BranchOfficeDeleteModal from "./BranchOfficeDeleteModal";
import BranchofficeModal from "./BranchofficeModal";
import { Town } from "src/services/ScreensPagesService/ScreensPagesService";

const BranchOfficePage = ({
  modalPropertiesState,
  pageLimitState,
  paginationDataState,
  searchExpressionState,
  modalDeletePropertiesState,
}: AdminPagesWrapperProps) => {
  const [townId, setTownId] = useState<number>(0);
  const [towns, callTownApi] = useApiCall(getTownsList);
  const [townsList, setTownsList] = useState<Town[]>([]);
  const [data, callBranchOfficeApi, isLoading] = useApiCall(
    getPaginatedBranchOfficesList
  );
  const [branchOfficesList, setBranchOfficesList] = useState([]);

  const [branchOfficeInModal, setBranchOfficeInModal] = useState<
    BranchOfficeInModal
  >({
    id: 0,
    name: "",
    visitPurposesIds: [],
    townId: 0,
    initial: "",
    timer: 5000,
  });

  useEffect(() => {
    callBranchOfficeApi([
      townId,
      searchExpressionState.searchExpression,
      pageLimitState.pageLimit,
    ]);
    callTownApi();
  }, []);

  useEffect(() => {
    if (towns) {
      setTownsList(towns);
    }
  }, [towns]);

  useEffect(() => {
    const { branchOffices, paginationData } = data;
    if (branchOffices) {
      setBranchOfficesList(branchOffices);
      paginationDataState.setPaginationData(paginationData);
    }
  }, [data]);

  const onPageChanged = (page: number): void => {
    callBranchOfficeApi([
      townId,
      searchExpressionState.searchExpression,
      pageLimitState.pageLimit,
      page,
    ]);
  };

  const recallBranchOfficeApi = () => {
    callBranchOfficeApi([
      townId,
      searchExpressionState.searchExpression,
      pageLimitState.pageLimit,
    ]);
  };

  const getTownOptions = (): JSX.Element[] =>
    townsList.map((town: Town, index: number) => {
      return (
        <option key={index} value={town.id}>
          {town.name}
        </option>
      );
    });

  return (
    <main className={cn(classes.container)}>
      <NotificationContainer />
      <div className={cn(classes.containerHeader)}>
        <div className={cn(classes.filters)}>
          <Form.Group>
            <Form.Control
              data-testid="page-limit-selector"
              as="select"
              value={pageLimitState.pageLimit}
              onChange={(event: React.ChangeEvent<HTMLSelectElement>) =>
                pageLimitState.setPageLimit(Number(event.target.value))
              }
              custom
            >
              {getPageOptions()}
            </Form.Control>
          </Form.Group>
          <Form.Group>
            <Form.Control
              as="select"
              data-testid="town-selector"
              value={townId}
              onChange={(event: React.ChangeEvent<HTMLSelectElement>) =>
                setTownId(Number(event.target.value))
              }
              custom
            >
              <option value="0">Все города</option>
              {getTownOptions()}
            </Form.Control>
          </Form.Group>
          <Form.Group>
            <Form.Control
              data-testid="search-selector"
              type="text"
              placeholder="Поиск по имени"
              value={searchExpressionState.searchExpression}
              onChange={(event: React.ChangeEvent<HTMLSelectElement>) =>
                searchExpressionState.setSearchExpression(event.target.value)
              }
            />
          </Form.Group>
          <Button
            data-testid="filter"
            variant="primary"
            onClick={() =>
              callBranchOfficeApi([
                townId,
                searchExpressionState.searchExpression,
                pageLimitState.pageLimit,
              ])
            }
          >
            <Typography color="white" variant="span">
              Фильтр
            </Typography>
          </Button>
          <Button
            data-testid="add-branch-office"
            variant="primary"
            onClick={() => {
              modalPropertiesState.setModalProperties({
                action: MODAL_TYPE.CREATE.NAME,
                isModalShown: true,
              });
              setBranchOfficeInModal({
                id: 0,
                name: "",
                visitPurposesIds: [],
                townId: 0,
                initial: "",
                timer: 5000,
              });
            }}
          >
            <Typography color="white" variant="span">
              Добавить
            </Typography>
          </Button>
        </div>
      </div>
      {isLoading ? (
        <span aria-label="Loading" />
      ) : (
        <BranchofficeTable
          setModalProperties={modalPropertiesState.setModalProperties}
          setBranchOfficeInModal={setBranchOfficeInModal}
          setIsDeleteModalShown={
            modalDeletePropertiesState.setIsDeleteModalShown
          }
          branchOfficesList={branchOfficesList}
        />
      )}
      <BranchofficeModal
        modalPropertiesState={modalPropertiesState}
        branchOfficeInModalState={{
          branchOfficeInModal: branchOfficeInModal,
          setBranchOfficeInModal: setBranchOfficeInModal,
        }}
        recallBranchOfficeApi={recallBranchOfficeApi}
        townsList={townsList}
      />
      <BranchOfficeDeleteModal
        modalDeletePropertiesState={modalDeletePropertiesState}
        id={branchOfficeInModal.id}
        recallBranchOfficeApi={recallBranchOfficeApi}
      />
      {paginationDataState.paginationData !== undefined &&
        paginationDataState.paginationData.totalPages != 0 && (
          <Pagination
            totalPages={paginationDataState.paginationData.totalPages}
            totalRecords={paginationDataState.paginationData.totalRecords}
            onPageChanged={onPageChanged}
            currentPage={paginationDataState.paginationData.currentPage}
          />
        )}
    </main>
  );
};

export default () => AdminPagesWrapper(BranchOfficePage);
