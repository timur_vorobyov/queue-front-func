import React from "react";
import {
  BranchOffice,
  BranchOfficeInModal,
} from "src/types/Admin/SuperAdmin/BranchOffice/branchOffice";
import { ModalProperties } from "../../types";
import { Table } from "react-bootstrap";
import commonClasses from "../../SuperAdminPages.css";
import cn from "clsx";
import Typography from "src/components/Typography";
import { MODAL_TYPE } from "../../consts";
import Delete from "../../images/Delete";
import Edit from "../../images/Edit";
import classes from "./BranchOfficeTable.css";
import { VisitPurpose } from "src/types/Admin/visitPurpose";

const getVisitPurposesIds = (selectedVisitPurposes: VisitPurpose[]) => {
  const options = selectedVisitPurposes;
  const selectedOptions = [];

  for (let i = 0; i < options.length; i++) {
    selectedOptions.push(options[i].id);
  }

  return selectedOptions;
};

interface BranchofficeTableProps {
  setModalProperties: (modalProperties: ModalProperties) => void;
  setBranchOfficeInModal: (branchOfficeInModal: BranchOfficeInModal) => void;
  setIsDeleteModalShown: (isDeleteModalShown: boolean) => void;
  branchOfficesList: BranchOffice[];
}

const BranchofficeTable = ({
  setModalProperties,
  setBranchOfficeInModal,
  setIsDeleteModalShown,
  branchOfficesList,
}: BranchofficeTableProps) => {
  const getVisitPurposesRows = (visitPurposes: { name: string }[]) => {
    const arrayLength = visitPurposes.length;
    return visitPurposes.map(
      (visitPurpose: { name: string }, index: number) => {
        if (arrayLength == index + 1) {
          return visitPurpose.name;
        }

        return visitPurpose.name + ", ";
      }
    );
  };

  const getBranchOfficesList = () => {
    return branchOfficesList.map((branchOffice: BranchOffice) => {
      return (
        <tr key={branchOffice.id} className={cn(classes.tableRaw)}>
          <td>{branchOffice.name}</td>
          <td>{getVisitPurposesRows(branchOffice.visitPurposes)}</td>
          <td>{branchOffice.town.name}</td>
          <td>{branchOffice.initial}</td>
          <td>{branchOffice.timer}</td>
          <td>
            <button
              data-testid={`remove-icon-${branchOffice.id}`}
              className={cn(commonClasses.buttonAction)}
              type="button"
              onClick={() => {
                setIsDeleteModalShown(true);
                setBranchOfficeInModal({
                  id: branchOffice.id,
                  name: "",
                  townId: 0,
                  initial: "",
                  timer: 5000,
                  visitPurposesIds: [],
                });
              }}
            >
              {Delete()}
            </button>
            <button
              data-testid={`update-icon-${branchOffice.id}`}
              className={cn(commonClasses.buttonAction)}
              type="button"
              onClick={() => {
                setModalProperties({
                  isModalShown: true,
                  action: MODAL_TYPE.UPDATE.NAME,
                });
                setBranchOfficeInModal({
                  id: branchOffice.id,
                  name: branchOffice.name,
                  townId: branchOffice.town.id,
                  initial: branchOffice.initial,
                  timer: branchOffice.timer,
                  visitPurposesIds: getVisitPurposesIds(
                    branchOffice.visitPurposes
                  ),
                });
              }}
            >
              {Edit()}
            </button>
          </td>
        </tr>
      );
    });
  };

  return (
    <Table className={cn(classes.table)}>
      <thead className={cn(classes.tableHeader)}>
        <tr>
          <th>
            <Typography variant="span">Название</Typography>
          </th>
          <th>
            <Typography variant="span">Цели визита</Typography>
          </th>
          <th>
            <Typography variant="span">Город</Typography>
          </th>
          <th>
            <Typography variant="span">Инициал</Typography>
          </th>
          <th>
            <Typography variant="span">Время браузерного оповещения</Typography>
          </th>
          <th>
            <Typography variant="span">Действие</Typography>
          </th>
        </tr>
      </thead>
      <tbody>{getBranchOfficesList()}</tbody>
    </Table>
  );
};

export default BranchofficeTable;
