import React, { useEffect, useState } from "react";
import commonClasses from "../../SuperAdminPages.css";
import cn from "clsx";
import Typography from "src/components/Typography";
import { MODAL_TYPE } from "../../consts";
import { createNotification } from "src/helpers/crud-notifications";
import { Button, Form, Modal } from "react-bootstrap";
// @ts-ignore
import Select from "react-select";
import useApiCall from "src/utils/hooks/use-async-call";
import { ModalPropertiesState } from "../../types";
import { BranchOfficeInModal } from "src/types/Admin/SuperAdmin/BranchOffice/branchOffice";
import { getVisitPurposesList } from "src/apis/AdminPages/SuperAdminPages/visitPurpose";
import {
  storeBranchOffice,
  updateBranchOffice,
} from "src/apis/AdminPages/SuperAdminPages/BranchOfficePage/branchOffice";
import { Town } from "src/types/Admin/SuperAdmin/town";
import { VisitPurpose } from "src/types/Admin/visitPurpose";

interface VisitPurposeModalProps {
  modalPropertiesState: ModalPropertiesState;
  branchOfficeInModalState: {
    branchOfficeInModal: BranchOfficeInModal;
    setBranchOfficeInModal: (branchOfficeInModal: BranchOfficeInModal) => void;
  };
  recallBranchOfficeApi: () => void;
  townsList: Town[];
}

const BranchofficeModal = ({
  modalPropertiesState,
  branchOfficeInModalState,
  recallBranchOfficeApi,
  townsList,
}: VisitPurposeModalProps) => {
  const [visitPurposes, callVisitPurposeApi] = useApiCall(getVisitPurposesList);
  const [visitPurposesList, setVisitPurposesList] = useState([]);

  useEffect(() => {
    callVisitPurposeApi();
  }, []);

  useEffect(() => {
    if (visitPurposes) {
      setVisitPurposesList(visitPurposes);
    }
  }, [visitPurposes]);

  const getTownOptions = (): JSX.Element[] => {
    return townsList.map((town: Town) => {
      return (
        <option key={town.id} value={town.id}>
          {town.name}
        </option>
      );
    });
  };

  /*
  const getCrmBranchOptions = () => {
    const options = crmBranchesList.map((crmBranch: CrmBranch) => {
      return {
        value: crmBranch.crmBranchName,
        label: crmBranch.crmBranchLabel,
      };
    });
    return options;
  };
  const handleCrmBranchChange = async (selectedCrmBranch: {
    value: string | null;
    label: string | null;
  }): Promise<void> => {
    const data = {
      crmBranchName: selectedCrmBranch.value,
      displayName: selectedCrmBranch.label,
      branchOfficeId: branchOfficeId,
    };

    const isBranchesAssociated = await assosiateBranches(data);

    if (isBranchesAssociated) {
      createNotification("success", "Филиал синхронизирован с филмалом CRM");
    } else {
      createNotification(
        "error ",
        "Филиал не удалось синхронизировать с филмалом CRM"
      );
    }
  };

  const setCrmBranch = (): void => {
    if (crmBranch) {
      diassosiateBranch();
      setState({ crmBranch: false });
    } else {
      setState({ crmBranch: true });
    }
  };

  const diassosiateBranch = () => {
    const selectedCrmBranch = {
      value: null,
      label: null,
    };
    handleCrmBranchChange(selectedCrmBranch);
  };
*/

  const getVisitPurposeOptions = () => {
    const options = visitPurposesList.map((visitPurpose: VisitPurpose) => {
      return { value: visitPurpose.id, label: visitPurpose.name };
    });
    return options;
  };

  const handleVistiPurposesIdsChange = (
    selectedVisitPurposes: [{ value: number }]
  ): void => {
    const options = selectedVisitPurposes;
    const selectedOptions = [];

    for (let i = 0; i < options.length; i++) {
      selectedOptions.push(options[i].value);
    }

    branchOfficeInModalState.setBranchOfficeInModal({
      ...branchOfficeInModalState.branchOfficeInModal,
      visitPurposesIds: selectedOptions,
    });
  };

  const handleCreateBranchOfficeClick = async () => {
    const response = await storeBranchOffice(
      branchOfficeInModalState.branchOfficeInModal
    );

    if (response == "INITIAL_ERROR") {
      createNotification(
        "error",
        "Инициал который вы пытаетесь сохранить уже используется"
      );
      modalPropertiesState.setModalProperties({
        ...modalPropertiesState.modalProperties,
        isModalShown: false,
      });
    }

    if (response == "BRANCH_OFFICE_IS_SAVED") {
      createNotification("success", "Филиал сохранён");
      recallBranchOfficeApi();
      modalPropertiesState.setModalProperties({
        ...modalPropertiesState.modalProperties,
        isModalShown: false,
      });
    }
  };

  const handleUpdateBranchOfficeClick = async () => {
    const response = await updateBranchOffice(
      branchOfficeInModalState.branchOfficeInModal
    );

    if (response == "INITIAL_ERROR") {
      createNotification(
        "error",
        "Инициал который вы пытаетесь сохранить уже используется"
      );
      modalPropertiesState.setModalProperties({
        ...modalPropertiesState.modalProperties,
        isModalShown: false,
      });
    }

    if (response == "BRANCH_OFFICE_IS_UPDATED") {
      createNotification("success", "Филиал обновлен");
      recallBranchOfficeApi();
      modalPropertiesState.setModalProperties({
        ...modalPropertiesState.modalProperties,
        isModalShown: false,
      });
    }
  };

  return (
    <Modal
      className={cn(commonClasses.modalDiolog)}
      show={modalPropertiesState.modalProperties.isModalShown}
      onHide={() =>
        modalPropertiesState.setModalProperties({
          ...modalPropertiesState.modalProperties,
          isModalShown: false,
        })
      }
    >
      <Modal.Header closeButton>
        <Modal.Title>
          <Typography variant="span">
            {modalPropertiesState.modalProperties.action ==
            MODAL_TYPE.CREATE.NAME
              ? "Добавить филиал"
              : "Изменить филиал"}
          </Typography>
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div>
          <Form.Group>
            <Typography variant="label">Имя города</Typography>
            <Form.Control
              data-testid="town-id"
              as="select"
              value={branchOfficeInModalState.branchOfficeInModal.townId}
              onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
                branchOfficeInModalState.setBranchOfficeInModal({
                  ...branchOfficeInModalState.branchOfficeInModal,
                  townId: Number(event.target.value),
                })
              }
              custom
            >
              <option value="0">Пожалуйста выберите город</option>
              {getTownOptions()}
            </Form.Control>
          </Form.Group>
          <Form.Group>
            <Typography variant="label">Имя филиала</Typography>
            <Form.Control
              data-testid="branch-office-name"
              type="text"
              value={branchOfficeInModalState.branchOfficeInModal.name}
              onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
                branchOfficeInModalState.setBranchOfficeInModal({
                  ...branchOfficeInModalState.branchOfficeInModal,
                  name: event.target.value,
                })
              }
            />
          </Form.Group>
          <Form.Group>
            <Typography variant="label">Инициал филиала</Typography>
            <Form.Control
              data-testid="initial"
              type="text"
              value={branchOfficeInModalState.branchOfficeInModal.initial}
              onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
                branchOfficeInModalState.setBranchOfficeInModal({
                  ...branchOfficeInModalState.branchOfficeInModal,
                  initial: event.target.value,
                })
              }
            />
          </Form.Group>
          <Form.Group>
            <Typography variant="label">Цели визита</Typography>
            <Select
              aria-label="visit-purpose-ids"
              defaultValue={visitPurposesList.map(
                (visitPurpose: VisitPurpose) => {
                  if (
                    branchOfficeInModalState.branchOfficeInModal.visitPurposesIds.includes(
                      visitPurpose.id
                    )
                  ) {
                    return { label: visitPurpose.name, value: visitPurpose.id };
                  }

                  return null;
                }
              )}
              options={getVisitPurposeOptions()}
              isMulti
              onChange={handleVistiPurposesIdsChange}
            />
          </Form.Group>
          <Form.Group>
            <Typography variant="label">Таймер</Typography>
            <Form.Control
              data-testid="timer"
              type="text"
              value={branchOfficeInModalState.branchOfficeInModal.timer}
              onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
                branchOfficeInModalState.setBranchOfficeInModal({
                  ...branchOfficeInModalState.branchOfficeInModal,
                  timer: Number(event.target.value),
                })
              }
            />
          </Form.Group>
          {/*
          <Form.Group >
            <Typography variant="label">Связать филиал с СРМ</Typography>
            <Select
              options={getCrmBranchOptions()}
              onChange={handleCrmBranchChange}
            />
          </Form.Group>
          {modalPropertiesState.modalProperties.action ==
          MODAL_TYPE.CREATE.NAME ? (
            <> </>
          ) : (
            <Form.Check
              className={cn(classes.crmBranchSwitch)}
              type="switch"
              id="custom-switch"
              label="Связать филиалы?"
              checked={crmBranch}
              onChange={setCrmBranch}
            />
          )}*/}
        </div>
      </Modal.Body>
      <Modal.Footer>
        <Button
          variant="secondary"
          onClick={
            modalPropertiesState.modalProperties.action ==
            MODAL_TYPE.CREATE.NAME
              ? () => handleCreateBranchOfficeClick()
              : () => handleUpdateBranchOfficeClick()
          }
        >
          <Typography color="white" variant="span">
            {modalPropertiesState.modalProperties.action ==
            MODAL_TYPE.CREATE.NAME
              ? MODAL_TYPE.CREATE.ACTION
              : MODAL_TYPE.UPDATE.ACTION}
          </Typography>
        </Button>
        <Button
          variant="primary"
          onClick={() =>
            modalPropertiesState.setModalProperties({
              ...modalPropertiesState.modalProperties,
              isModalShown: false,
            })
          }
        >
          <Typography color="white" variant="span">
            Закрыть
          </Typography>
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default BranchofficeModal;
