import React from "react";
import commonClasses from "../../SuperAdminPages.css";
import cn from "clsx";
import Typography from "src/components/Typography";
import { MODAL_TYPE } from "../../consts";
import { createNotification } from "src/helpers/crud-notifications";
import { Button, Form, Modal } from "react-bootstrap";
import { ModalPropertiesState } from "../../types";
import {
  storePermission,
  updatePermission,
} from "src/apis/AdminPages/SuperAdminPages/PermissionPage/permission";
// @ts-ignore
import Select from "react-select";
import { PermissionInModal } from "src/types/Admin/SuperAdmin/Permission/permission";

interface PermissionModalProps {
  modalPropertiesState: ModalPropertiesState;
  permissionInModalState: {
    permissionInModal: PermissionInModal;
    setPermissionInModal: (permissionInModalState: PermissionInModal) => void;
  };
  recallPermissionApi: () => void;
}

const PermissionModal = ({
  modalPropertiesState,
  permissionInModalState,
  recallPermissionApi,
}: PermissionModalProps) => {
  const handleCreatePermissionClick = async () => {
    const response = await storePermission(
      permissionInModalState.permissionInModal
    );

    if (response == "PERMISSION_IS_SAVED") {
      createNotification("success", "Разрешение сохранено");
      recallPermissionApi();
      modalPropertiesState.setModalProperties({
        ...modalPropertiesState.modalProperties,
        isModalShown: false,
      });
    }
  };

  const handleUpdatePermissionClick = async () => {
    const response = await updatePermission(
      permissionInModalState.permissionInModal
    );

    if (response == "PERMISSION_IS_UPDATED") {
      createNotification("success", "Разрешение обновлено");
      recallPermissionApi();
      modalPropertiesState.setModalProperties({
        ...modalPropertiesState.modalProperties,
        isModalShown: false,
      });
    }
  };

  return (
    <Modal
      className={cn(commonClasses.modalDiolog)}
      show={modalPropertiesState.modalProperties.isModalShown}
      onHide={() =>
        modalPropertiesState.setModalProperties({
          ...modalPropertiesState.modalProperties,
          isModalShown: false,
        })
      }
    >
      <Modal.Header closeButton>
        <Modal.Title>
          <Typography variant="span">
            {modalPropertiesState.modalProperties.action ==
            MODAL_TYPE.CREATE.NAME
              ? "Добавить разрешение"
              : "Изменить разрешение"}
          </Typography>
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div>
          <Form.Group>
            <Typography variant="label">Наименование</Typography>
            <Form.Control
              type="text"
              data-testid="permission-name"
              value={permissionInModalState.permissionInModal.name}
              onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
                permissionInModalState.setPermissionInModal({
                  ...permissionInModalState.permissionInModal,
                  name: event.target.value,
                })
              }
            />
          </Form.Group>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <Button
          variant="secondary"
          onClick={
            modalPropertiesState.modalProperties.action ==
            MODAL_TYPE.CREATE.NAME
              ? () => handleCreatePermissionClick()
              : () => handleUpdatePermissionClick()
          }
        >
          <Typography color="white" variant="span">
            {modalPropertiesState.modalProperties.action ==
            MODAL_TYPE.CREATE.NAME
              ? MODAL_TYPE.CREATE.ACTION
              : MODAL_TYPE.UPDATE.ACTION}
          </Typography>
        </Button>
        <Button
          variant="primary"
          onClick={() =>
            modalPropertiesState.setModalProperties({
              ...modalPropertiesState.modalProperties,
              isModalShown: false,
            })
          }
        >
          <Typography color="white" variant="span">
            Закрыть
          </Typography>
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default PermissionModal;
