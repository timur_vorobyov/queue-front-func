import React from "react";
import { Button, Modal } from "react-bootstrap";
import commonClasses from "../../SuperAdminPages.css";
import cn from "clsx";
import Typography from "src/components/Typography";
import { createNotification } from "src/helpers/crud-notifications";
import { deletePermission } from "src/apis/AdminPages/SuperAdminPages/PermissionPage/permission";
import { ModalDeletePropertiesState } from "src/pages/Admin/types";

interface PermissionDeleteModalProps {
  modalDeletePropertiesState: ModalDeletePropertiesState;
  id: number;
  recallPermissionApi: () => void;
}

const PermissionDeleteModal = ({
  modalDeletePropertiesState,
  id,
  recallPermissionApi,
}: PermissionDeleteModalProps) => {
  const handleConfirmationDeletePermissionModalClick = async () => {
    const response = await deletePermission(id);

    if (response == "PERMISSION_IS_REMOVED") {
      createNotification("success", "Разрешение удалено");
      modalDeletePropertiesState.setIsDeleteModalShown(false);
      recallPermissionApi();
    }
  };

  return (
    <Modal
      className={cn(commonClasses.modalDiolog)}
      show={modalDeletePropertiesState.isDeleteModalShown}
      onHide={() => modalDeletePropertiesState.setIsDeleteModalShown(false)}
    >
      <Modal.Header closeButton>
        <Modal.Title>
          <Typography variant="span">
            Вы уверены что хотите удалить выбранный пункт?
          </Typography>
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className={cn(commonClasses.modalDeleteActionButtons)}>
        <Button
          className={cn(commonClasses.modalDeleteActionButton)}
          variant="secondary"
          onClick={() => handleConfirmationDeletePermissionModalClick()}
        >
          <Typography color="white" variant="span">
            Да
          </Typography>
        </Button>
        <Button
          className={cn(commonClasses.modalDeleteActionButton)}
          variant="primary"
          onClick={() =>
            modalDeletePropertiesState.setIsDeleteModalShown(false)
          }
        >
          <Typography color="white" variant="span">
            Нет
          </Typography>
        </Button>
      </Modal.Body>
    </Modal>
  );
};

export default PermissionDeleteModal;
