import {
  cleanup,
  waitForElementToBeRemoved,
  screen,
  fireEvent,
} from "@testing-library/react";
import React from "react";
import { server } from "src/__mocks__/automatic/mockHttp";
import PermissionPage from "./PermissionPage";
import "@testing-library/jest-dom";
import { renderWithRouter } from "src/__mocks__/utils";
import {
  DELETE_PERMISSION_RESPONSE,
  GET_PAGINATED_PERMISSIONS_LIST_RESPONSE,
  STORE_PERMISSION_RESPONSE,
  UPDATE_PERMISSION_RESPONSE,
} from "src/__mocks__/handlers/permission";
import { mockedPermissions } from "src/__mocks__/domains/permission";

afterEach(cleanup);

describe("PermissionPage component", () => {
  it("should show data in the table", async () => {
    const limit = 5;
    const page = 1;
    const searchExpression = "";

    server.use(
      ...GET_PAGINATED_PERMISSIONS_LIST_RESPONSE(searchExpression, limit, page)
    );
    renderWithRouter(<PermissionPage />);

    await waitForElementToBeRemoved(() => screen.getByLabelText("Loading"));
    expect(screen.getByText("crm developer")).toBeInTheDocument();
  });

  it("should paginate to second page", async () => {
    const limit = 5;
    const page = 1;
    const searchExpression = "";

    server.use(
      ...GET_PAGINATED_PERMISSIONS_LIST_RESPONSE(searchExpression, limit, page)
    );
    renderWithRouter(<PermissionPage />);

    await waitForElementToBeRemoved(() => screen.getByLabelText("Loading"));
    expect(screen.getByText("crm developer")).toBeInTheDocument();

    const paginationSecondPageButton = screen.getByText("2");
    server.use(
      ...GET_PAGINATED_PERMISSIONS_LIST_RESPONSE(searchExpression, limit, 2)
    );
    fireEvent.click(paginationSecondPageButton);
    await waitForElementToBeRemoved(() => screen.getByLabelText("Loading"));
    expect(screen.getByText("news online")).toBeInTheDocument();
  });

  it("should show 10 items on the first page", async () => {
    const limit = 5;
    const page = 1;
    const searchExpression = "";

    server.use(
      ...GET_PAGINATED_PERMISSIONS_LIST_RESPONSE(searchExpression, limit, page)
    );
    renderWithRouter(<PermissionPage />);

    await waitForElementToBeRemoved(() => screen.getByLabelText("Loading"));
    expect(screen.getByText("crm developer")).toBeInTheDocument();

    const pageLimitSelector = screen.getByTestId("page-limit-selector");
    fireEvent.change(pageLimitSelector, { target: { value: "10" } });
    const filterButton = screen.getByTestId("filter");
    server.use(
      ...GET_PAGINATED_PERMISSIONS_LIST_RESPONSE(searchExpression, 10, page)
    );
    fireEvent.click(filterButton);
    await waitForElementToBeRemoved(() => screen.getByLabelText("Loading"));
    expect(screen.queryByText("crm developer")).toBeInTheDocument();
    expect(screen.queryByText("news online")).toBeInTheDocument();
  });

  it("should show an item with name 'crm partners'", async () => {
    const limit = 5;
    const page = 1;
    const searchExpression = "";

    server.use(
      ...GET_PAGINATED_PERMISSIONS_LIST_RESPONSE(searchExpression, limit, page)
    );
    renderWithRouter(<PermissionPage />);

    await waitForElementToBeRemoved(() => screen.getByLabelText("Loading"));
    expect(screen.getByText("crm developer")).toBeInTheDocument();

    const permissionName = "crm partners";
    const searchInput = screen.getByTestId("search-selector");
    fireEvent.change(searchInput, { target: { value: permissionName } });
    const filterButton = screen.getByTestId("filter");
    server.use(
      ...GET_PAGINATED_PERMISSIONS_LIST_RESPONSE(permissionName, limit, page)
    );
    fireEvent.click(filterButton);
    await waitForElementToBeRemoved(() => screen.getByLabelText("Loading"));
    expect(screen.queryByText("crm partners")).toBeInTheDocument();
    expect(screen.queryByText("crm developer")).not.toBeInTheDocument();
  });

  it("should open remove pop up when 'crm developer' click on cross icon", async () => {
    const limit = 5;
    const page = 1;
    const searchExpression = "";

    server.use(
      ...GET_PAGINATED_PERMISSIONS_LIST_RESPONSE(searchExpression, limit, page)
    );
    renderWithRouter(<PermissionPage />);

    await waitForElementToBeRemoved(() => screen.getByLabelText("Loading"));
    expect(screen.getByText("crm developer")).toBeInTheDocument();

    const removeButton = screen.getByTestId(
      `remove-icon-${mockedPermissions[0].id}`
    );
    fireEvent.click(removeButton);
    expect(
      screen.queryByText("Вы уверены что хотите удалить выбранный пункт?")
    ).toBeInTheDocument();
  });

  it("should remove the chosen branch office when a 'crm developer' click on the cross icon", async () => {
    const limit = 5;
    const page = 1;
    const searchExpression = "";

    server.use(
      ...GET_PAGINATED_PERMISSIONS_LIST_RESPONSE(searchExpression, limit, page)
    );
    renderWithRouter(<PermissionPage />);

    await waitForElementToBeRemoved(() => screen.getByLabelText("Loading"));
    expect(screen.getByText("crm developer")).toBeInTheDocument();

    const removeIconButton = screen.getByTestId(
      `remove-icon-${mockedPermissions[0].id}`
    );
    fireEvent.click(removeIconButton);
    const removeButton = screen.getByText("Да");
    server.use(...DELETE_PERMISSION_RESPONSE);
    fireEvent.click(removeButton);
    await waitForElementToBeRemoved(() =>
      screen.getByText("Вы уверены что хотите удалить выбранный пункт?")
    );
    expect(screen.queryByText("Разрешение удалено")).toBeInTheDocument();
    /** the logic of removing, recalls the
      permissions list as a result data from the
      previous rendering get not up to date. So if you try to find permissionName from previous rendering
      jest will not find it
      */
  });

  it("should create a new permission", async () => {
    const limit = 5;
    const page = 1;
    const searchExpression = "";

    server.use(
      ...GET_PAGINATED_PERMISSIONS_LIST_RESPONSE(searchExpression, limit, page)
    );
    renderWithRouter(<PermissionPage />);

    await waitForElementToBeRemoved(() => screen.getByLabelText("Loading"));
    const addButton = screen.getByTestId("add-permission");
    fireEvent.click(addButton);
    expect(screen.queryByText("Добавить разрешение")).toBeInTheDocument();
    const permissionNameInput = screen.getByTestId("permission-name");
    fireEvent.change(permissionNameInput, {
      target: { value: "hall manager" },
    });
    const submitButton = screen.getByText("Сохранить");
    server.use(...STORE_PERMISSION_RESPONSE);
    fireEvent.click(submitButton);
    await waitForElementToBeRemoved(() =>
      screen.getByText("Добавить разрешение")
    );
    expect(screen.queryByText("Разрешение сохранено")).toBeInTheDocument();
  });

  it("should update a permission", async () => {
    const limit = 5;
    const page = 1;
    const searchExpression = "";

    server.use(
      ...GET_PAGINATED_PERMISSIONS_LIST_RESPONSE(searchExpression, limit, page)
    );
    renderWithRouter(<PermissionPage />);
    await waitForElementToBeRemoved(() => screen.getByLabelText("Loading"));

    const updateIconButton = screen.getByTestId(`update-icon-1`);
    fireEvent.click(updateIconButton);
    expect(screen.queryByText("Изменить разрешение")).toBeInTheDocument();
    const permissionNameInput = screen.getByTestId("permission-name");
    expect(permissionNameInput).toHaveValue("admin access");
    fireEvent.change(permissionNameInput, {
      target: { value: "crm developer" },
    });
    const submitButton = screen.getByText("Обновить");
    server.use(...UPDATE_PERMISSION_RESPONSE);
    fireEvent.click(submitButton);
    await waitForElementToBeRemoved(() =>
      screen.getByText("Изменить разрешение")
    );
    expect(screen.queryByText("Разрешение обновлено")).toBeInTheDocument();
  });
});
