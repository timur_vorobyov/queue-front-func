import React, { useEffect, useState } from "react";
import classes from "./PermissionPage.css";
import cn from "clsx";
import Typography from "src/components/Typography";
import { Button, Form } from "react-bootstrap";
// @ts-ignore
import { NotificationContainer } from "react-notifications";
import { MODAL_TYPE } from "../consts";
import AdminPagesWrapper from "src/wrappers/AdminPagesWrapper";
import { AdminPagesWrapperProps } from "src/wrappers/AdminPagesWrapper/AdminPagesWrapper";
import useApiCall from "src/utils/hooks/use-async-call";
import { getPageOptions } from "../utils";
import Pagination from "src/components/Pagination/Pagination";
import PermissionTable from "./PermissionTable";
import PermissionDeleteModal from "./PermissionDeleteModal";
import PermissionModal from "./PeermissionModal/PeermissionModal";
import { getPaginatedPermissionsList } from "src/apis/AdminPages/SuperAdminPages/PermissionPage/permission";
import { PermissionInModal } from "src/types/Admin/SuperAdmin/Permission/permission";

const PermissionPage = ({
  modalPropertiesState,
  pageLimitState,
  paginationDataState,
  searchExpressionState,
  modalDeletePropertiesState,
}: AdminPagesWrapperProps) => {
  const [permissionsList, setPermissionsList] = useState([]);
  const [data, callPermissionApi, isLoading] = useApiCall(
    getPaginatedPermissionsList
  );
  const [permissionInModal, setPermissionInModal] = useState<PermissionInModal>(
    {
      id: 0,
      name: "",
    }
  );

  useEffect(() => {
    callPermissionApi([
      searchExpressionState.searchExpression,
      pageLimitState.pageLimit,
    ]);
  }, []);

  useEffect(() => {
    const { permissions, paginationData } = data;
    if (permissions) {
      setPermissionsList(permissions);
      paginationDataState.setPaginationData(paginationData);
    }
  }, [data]);

  const recallPermissionApi = () => {
    callPermissionApi([
      searchExpressionState.searchExpression,
      pageLimitState.pageLimit,
    ]);
  };

  const onPageChanged = (page: number): void => {
    callPermissionApi([
      searchExpressionState.searchExpression,
      pageLimitState.pageLimit,
      page,
    ]);
  };

  return (
    <main className={cn(classes.container)}>
      <NotificationContainer />
      <div className={cn(classes.containerHeader)}>
        <div className={cn(classes.filters)}>
          <Form.Group>
            <Form.Control
              data-testid="page-limit-selector"
              as="select"
              value={pageLimitState.pageLimit}
              onChange={(event: React.ChangeEvent<HTMLSelectElement>) =>
                pageLimitState.setPageLimit(Number(event.target.value))
              }
              custom
            >
              {getPageOptions()}
            </Form.Control>
          </Form.Group>
          <Form.Group>
            <Form.Control
              data-testid="search-selector"
              type="text"
              placeholder="Поиск по имени"
              value={searchExpressionState.searchExpression}
              onChange={(event: React.ChangeEvent<HTMLSelectElement>) =>
                searchExpressionState.setSearchExpression(event.target.value)
              }
            />
          </Form.Group>
          <Button
            variant="primary"
            data-testid="filter"
            onClick={() => {
              callPermissionApi([
                searchExpressionState.searchExpression,
                pageLimitState.pageLimit,
              ]);
            }}
          >
            <Typography color="white" variant="span">
              Фильтр
            </Typography>
          </Button>
          <Button
            data-testid="add-permission"
            variant="primary"
            onClick={() => {
              modalPropertiesState.setModalProperties({
                action: MODAL_TYPE.CREATE.NAME,
                isModalShown: true,
              });
              setPermissionInModal({
                name: "",
                id: 0,
              });
            }}
          >
            <Typography color="white" variant="span">
              Добавить
            </Typography>
          </Button>
        </div>
      </div>
      {isLoading ? (
        <span aria-label="Loading" />
      ) : (
        <PermissionTable
          setModalProperties={modalPropertiesState.setModalProperties}
          setPermissionInModal={setPermissionInModal}
          setIsDeleteModalShown={
            modalDeletePropertiesState.setIsDeleteModalShown
          }
          permissionsList={permissionsList}
        />
      )}
      <PermissionDeleteModal
        modalDeletePropertiesState={modalDeletePropertiesState}
        id={permissionInModal.id}
        recallPermissionApi={recallPermissionApi}
      />
      <PermissionModal
        modalPropertiesState={modalPropertiesState}
        permissionInModalState={{
          permissionInModal: permissionInModal,
          setPermissionInModal: setPermissionInModal,
        }}
        recallPermissionApi={recallPermissionApi}
      />
      {paginationDataState.paginationData !== undefined &&
        paginationDataState.paginationData.totalPages != 0 && (
          <Pagination
            totalPages={paginationDataState.paginationData.totalPages}
            totalRecords={paginationDataState.paginationData.totalRecords}
            onPageChanged={onPageChanged}
            currentPage={paginationDataState.paginationData.currentPage}
          />
        )}
    </main>
  );
};

export default () => AdminPagesWrapper(PermissionPage);
