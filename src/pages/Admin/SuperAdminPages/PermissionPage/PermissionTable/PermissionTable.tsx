import React from "react";
import { Table } from "react-bootstrap";
import commonClasses from "../../SuperAdminPages.css";
import cn from "clsx";
import Typography from "src/components/Typography";
import { MODAL_TYPE } from "../../consts";
import Delete from "../../images/Delete";
import Edit from "../../images/Edit";
import classes from "./PermissionTable.css";
import { ModalProperties } from "../../types";
import {
  Permission,
  PermissionInModal,
} from "src/types/Admin/SuperAdmin/Permission/permission";

interface PermissionTableProps {
  setModalProperties: (modalProperties: ModalProperties) => void;
  setPermissionInModal: (permissionInModal: PermissionInModal) => void;
  setIsDeleteModalShown: (isDeleteModalShown: boolean) => void;
  permissionsList: Permission[];
}

const PermissionTable = ({
  setModalProperties,
  setPermissionInModal,
  setIsDeleteModalShown,
  permissionsList,
}: PermissionTableProps) => {
  const getPermissionsList = () => {
    return permissionsList.map((permission: Permission) => {
      return (
        <tr key={permission.id} className={cn(classes.tableRaw)}>
          <td>{permission.name}</td>
          <td>
            <button
              data-testid={`remove-icon-${permission.id}`}
              className={cn(commonClasses.buttonAction)}
              type="button"
              onClick={() => {
                setIsDeleteModalShown(true);
                setPermissionInModal({
                  id: permission.id,
                  name: "",
                });
              }}
            >
              {Delete()}
            </button>
            <button
              data-testid={`update-icon-${permission.id}`}
              className={cn(commonClasses.buttonAction)}
              type="button"
              onClick={() => {
                setModalProperties({
                  isModalShown: true,
                  action: MODAL_TYPE.UPDATE.NAME,
                });
                setPermissionInModal({
                  name: permission.name,
                  id: permission.id,
                });
              }}
            >
              {Edit()}
            </button>
          </td>
        </tr>
      );
    });
  };

  return (
    <Table className={cn(classes.table)} striped size="lg">
      <thead className={cn(classes.tableHeader)}>
        <tr>
          <th>
            <Typography variant="span">Название</Typography>
          </th>
          <th>
            <Typography variant="span">Действие</Typography>
          </th>
        </tr>
      </thead>
      <tbody>{getPermissionsList()}</tbody>
    </Table>
  );
};

export default PermissionTable;
