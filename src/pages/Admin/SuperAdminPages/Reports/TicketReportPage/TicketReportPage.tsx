import { PureComponent } from "react";
import cn from "clsx";
import classes from "./TicketReportPage.css";
import React from "react";
import TicketReportPageAdapter, {
  BranchOffice,
  BranchOffices,
  Ticket,
  Tickets,
  User,
  Users,
  VisitPurpose,
  VisitPurposes,
} from "src/services/SuperAdminPagesServices/ReportPageServices/TicketReportPageService/TicketReportPageAdapter";
import Typography from "src/components/Typography";
import { Button, Form, Table } from "react-bootstrap";
// @ts-ignore
import { NotificationContainer } from "react-notifications";
import Pagination, {
  PaginationData,
} from "src/components/Pagination/Pagination";
// @ts-ignore
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

interface TicketReportPageStates {
  ticketsList: Tickets;
  usersList: Users;
  branchOfficesList: BranchOffices;
  visitPurposesList: VisitPurposes;
  userId: string;
  visitPurposeId: string;
  branchOfficeId: string;
  searchExpression: string;
  currentPage: number;
  pageLimit: number;
  paginationData: PaginationData;
  dateFrom: Date;
  dateTo: Date;
}

class TicketReportPage extends PureComponent<{}, TicketReportPageStates> {
  private adapter: TicketReportPageAdapter;
  private pagesCountLimmit: Array<number>;
  private headers: Array<string>;

  constructor(props: {}) {
    super(props);
    this.state = {
      ticketsList: [
        {
          id: 0,
          ticketFullNumber: "",
          visitPurposeName: "",
          serviceName: "",
          customerPhoneNumber: "",
          crmUserId: 0,
          date: "",
          time: "",
          customerPendingTime: "",
          customerServiceTime: "",
          userFirstName: "",
          branchOfficeName: "",
        },
      ],
      usersList: [{ id: 0, firstName: "" }],
      branchOfficesList: [{ id: 0, name: "" }],
      visitPurposesList: [{ id: 0, name: "" }],
      userId: "",
      visitPurposeId: "",
      branchOfficeId: "",
      searchExpression: "",
      dateFrom: new Date(),
      dateTo: new Date(),
      currentPage: 1,
      pageLimit: 5,
      paginationData: { currentPage: 0, totalPages: 0, totalRecords: 0 },
    };

    this.pagesCountLimmit = [5, 10, 15, 20, 50, 100];
    this.adapter = new TicketReportPageAdapter();
    this.headers = [
      "Полный номер билета",
      "Цель визита",
      "Номер телефона клиента",
      "Дата создания",
      "Время создания",
      "Время ожидания",
      "Время обслужвания",
      "Имя юзера",
      "Филиал",
    ];
  }

  componentDidMount(): void {
    this.setTicketsList();
    this.setUsersList();
    this.setBranchOfficesList();
    this.setVisitPurposesList();
  }

  async setTicketsList(currentPage: null | number = null): Promise<void> {
    const nextPage = currentPage ? currentPage : this.state.currentPage;
    const ticketsList = await this.adapter.getTicketsList(
      this.state.branchOfficeId,
      this.state.visitPurposeId,
      this.state.userId,
      this.state.searchExpression,
      this.state.pageLimit,
      this.state.dateFrom,
      this.state.dateTo,
      nextPage
    );
    const paginationData = this.adapter.getPaginationData();

    if (ticketsList) {
      await this.setState({
        ticketsList: ticketsList,
        paginationData: paginationData,
      });
    }
  }

  async setUsersList(): Promise<void> {
    const usersList = await this.adapter.getUsersList();
    this.setState({ usersList: usersList });
  }

  async setBranchOfficesList(): Promise<void> {
    const branchOfficesList = await this.adapter.getBranchOfficesList();
    this.setState({ branchOfficesList: branchOfficesList });
  }

  async setVisitPurposesList(): Promise<void> {
    const visitPurposesList = await this.adapter.getVisitPurposesList();
    this.setState({ visitPurposesList: visitPurposesList });
  }

  onPageChanged = async (page: number): Promise<void> => {
    await this.setState({ currentPage: page });
    await this.setTicketsList();
  };

  render(): React.ReactNode {
    return (
      <main className={cn(classes.container)}>
        <NotificationContainer />
        <div>
          <div className={cn(classes.filters)}>
            <Form.Group className={cn(classes.formGroup)}>
              <Form.Control
                as="select"
                value={this.state.pageLimit}
                onChange={this.setPage}
                custom
              >
                {this.getPageOptions()}
              </Form.Control>
            </Form.Group>
            <Form.Group className={cn(classes.formGroup)}>
              <Form.Control
                as="select"
                value={this.state.branchOfficeId}
                onChange={this.setBranchOfficeId}
                custom
              >
                <option value="0">Все филиалы</option>
                {this.getBranchOfficeOptions()}
              </Form.Control>
            </Form.Group>
            <Form.Group className={cn(classes.formGroup)}>
              <Form.Control
                as="select"
                value={this.state.userId}
                onChange={this.setUserId}
                custom
              >
                <option value="0">Все юзеры</option>
                {this.getUserOptions()}
              </Form.Control>
            </Form.Group>
            <Form.Group className={cn(classes.formGroup)}>
              <Form.Control
                as="select"
                value={this.state.visitPurposeId}
                onChange={this.setVisitPurposeId}
                custom
              >
                <option value="0">Все цели визита</option>
                {this.getVisitPurposeOptions()}
              </Form.Control>
            </Form.Group>
            <Form.Group className={cn(classes.formGroup)}>
              <Form.Control
                placeholder="Search"
                type="text"
                value={this.state.searchExpression}
                onChange={this.setSearchExpression}
              />
            </Form.Group>
            <Button variant="primary" onClick={this.filter}>
              <Typography color="white" variant="span">
                Фильтр
              </Typography>
            </Button>
            <Button variant="success" onClick={this.export}>
              <Typography color="white" variant="span">
                Экспорт
              </Typography>
            </Button>
          </div>
          <div className={cn(classes.dataFilters)}>
            <DatePicker
              dateFormat="yyyy/MM/dd"
              selected={this.state.dateFrom}
              onChange={this.setDateFrom}
              className={cn(classes.dataFilter)}
            />
            <DatePicker
              dateFormat="yyyy/MM/dd"
              selected={this.state.dateTo}
              onChange={this.setDateTo}
              className={cn(classes.toDataFilter, classes.dataFilter)}
            />
          </div>
          <div className={cn(classes.content)}>
            <Table className={cn(classes.table)} striped size="lg">
              <thead className={cn(classes.tableHeader)}>
                <tr>{this.getTableHeaders()}</tr>
              </thead>
              <tbody>{this.getTicketsList()}</tbody>
            </Table>
          </div>
        </div>
        <div>
          {this.state.paginationData.totalPages != 0 ? (
            <Pagination
              totalPages={this.state.paginationData.totalPages}
              totalRecords={this.state.paginationData.totalRecords}
              onPageChanged={this.onPageChanged}
              currentPage={this.state.paginationData.currentPage}
            />
          ) : null}
        </div>
      </main>
    );
  }

  getPageOptions(): JSX.Element[] {
    return this.pagesCountLimmit.map((pageCount: number, index: number) => {
      return (
        <option key={index} value={pageCount}>
          {pageCount}
        </option>
      );
    });
  }

  getBranchOfficeOptions(): JSX.Element[] {
    return this.state.branchOfficesList.map(
      (branchOffice: BranchOffice, index: number) => {
        return (
          <option key={index} value={branchOffice.id}>
            {branchOffice.name}
          </option>
        );
      }
    );
  }

  getVisitPurposeOptions(): JSX.Element[] {
    return this.state.visitPurposesList.map(
      (visitPurpose: VisitPurpose, index: number) => {
        return (
          <option key={index} value={visitPurpose.id}>
            {visitPurpose.name}
          </option>
        );
      }
    );
  }

  getUserOptions(): JSX.Element[] {
    return this.state.usersList.map((user: User, index: number) => {
      return (
        <option key={index} value={user.id}>
          {user.firstName}
        </option>
      );
    });
  }

  setBranchOfficeId = async (
    event: React.ChangeEvent<HTMLSelectElement>
  ): Promise<void> => {
    const branchOfficeId = event.target.value;
    this.setState({ branchOfficeId: branchOfficeId });
  };

  setUserId = async (
    event: React.ChangeEvent<HTMLSelectElement>
  ): Promise<void> => {
    const userId = event.target.value;
    this.setState({ userId: userId });
  };

  setVisitPurposeId = async (
    event: React.ChangeEvent<HTMLSelectElement>
  ): Promise<void> => {
    const visitPurposeId = event.target.value;
    this.setState({ visitPurposeId: visitPurposeId });
  };

  setSearchExpression = (event: React.ChangeEvent<HTMLInputElement>): void => {
    this.setState({ searchExpression: event.target.value });
  };

  setPage = (event: React.ChangeEvent<HTMLSelectElement>): void => {
    this.setState({ pageLimit: Number(event.target.value) });
  };

  filter = (): void => {
    this.setTicketsList(1);
  };

  export = (): void => {
    this.adapter.export(
      this.state.branchOfficeId,
      this.state.visitPurposeId,
      this.state.userId,
      this.state.searchExpression,
      this.state.dateFrom,
      this.state.dateTo
    );
  };

  setDateTo = (date: Date): void => {
    this.setState({ dateTo: date });
  };

  setDateFrom = (date: Date): void => {
    this.setState({ dateFrom: date });
  };

  getTableHeaders(): JSX.Element[] {
    return this.headers.map((header: string, index: number) => {
      return (
        <th key={index}>
          <Typography variant="span">{header}</Typography>
        </th>
      );
    });
  }

  getTicketsList(): JSX.Element[] {
    return this.state.ticketsList.map((ticket: Ticket) => {
      return (
        <tr key={ticket.id} className={cn(classes.tableRaw)}>
          <td>{ticket.ticketFullNumber}</td>
          <td>{ticket.visitPurposeName}</td>
          <td>{ticket.customerPhoneNumber}</td>
          <td>{ticket.date}</td>
          <td>{ticket.time}</td>
          <td>{ticket.customerPendingTime}</td>
          <td>{ticket.customerServiceTime}</td>
          <td>{ticket.userFirstName}</td>
          <td>{ticket.branchOfficeName}</td>
        </tr>
      );
    });
  }
}
export default TicketReportPage;
