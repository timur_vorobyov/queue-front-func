import { PureComponent } from "react";
import cn from "clsx";
import classes from "./UserReportPage.css";
import React from "react";
import Typography from "src/components/Typography";
import { Button, Form, Table } from "react-bootstrap";
// @ts-ignore
import Pagination, {
  PaginationData,
} from "src/components/Pagination/Pagination";
// @ts-ignore
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import UserReportPageAdapter, {
  Status,
  Statuses,
  UserActionTime,
  UserActionTimes,
  BranchOffice,
  BranchOffices,
  Users,
  User,
} from "src/services/SuperAdminPagesServices/ReportPageServices/UserTicketPageService/UserTicketPageAdapter";

interface UserReportPageStates {
  userActionTimesList: UserActionTimes;
  usersList: Users;
  branchOfficesList: BranchOffices;
  statusesList: Statuses;
  userId: string;
  statusId: string;
  branchOfficeId: string;
  searchExpression: string;
  currentPage: number;
  pageLimit: number;
  paginationData: PaginationData;
  dateFrom: Date;
  dateTo: Date;
}

class UserReportPage extends PureComponent<{}, UserReportPageStates> {
  private adapter: UserReportPageAdapter;
  private pagesCountLimmit: Array<number>;
  private headers: Array<string>;

  constructor(props: {}) {
    super(props);
    this.state = {
      usersList: [{ id: 0, firstName: "" }],
      branchOfficesList: [{ id: 0, name: "" }],
      statusesList: [{ id: 0, name: "" }],
      userActionTimesList: [
        {
          id: 0,
          userFullName: "",
          lunchTime: "",
          workTime: "",
          breakTime: "",
          offlineTime: "",
        },
      ],
      userId: "",
      statusId: "",
      branchOfficeId: "",
      searchExpression: "",
      dateFrom: new Date(),
      dateTo: new Date(),
      currentPage: 1,
      pageLimit: 5,
      paginationData: { currentPage: 0, totalPages: 0, totalRecords: 0 },
    };

    this.pagesCountLimmit = [5, 10, 15, 20, 50, 100];
    this.adapter = new UserReportPageAdapter();
    this.headers = [
      "Полное имя юзера",
      "Время обеда",
      "Время работы",
      "Время перерыва",
      "Время отсутствия",
    ];
  }

  componentDidMount(): void {
    this.setUserActionTimesList();
    this.setUsersList();
    this.setBranchOfficesList();
    this.setStatusesList();
  }

  async setUserActionTimesList(
    currentPage: null | number = null
  ): Promise<void> {
    const nextPage = currentPage ? currentPage : this.state.currentPage;
    const userActionTimesList = await this.adapter.getUserActionTimesList(
      this.state.branchOfficeId,
      this.state.statusId,
      this.state.userId,
      this.state.searchExpression,
      this.state.pageLimit,
      this.state.dateFrom,
      this.state.dateTo,
      nextPage
    );
    const paginationData = this.adapter.getPaginationData();
    await this.setState({
      userActionTimesList: userActionTimesList,
      paginationData: paginationData,
    });
  }

  async setUsersList(): Promise<void> {
    const usersList = await this.adapter.getUsersList();
    this.setState({ usersList: usersList });
  }

  async setBranchOfficesList(): Promise<void> {
    const branchOfficesList = await this.adapter.getBranchOfficesList();
    this.setState({ branchOfficesList: branchOfficesList });
  }

  async setStatusesList(): Promise<void> {
    const statusesList = await this.adapter.getStatusesList();
    await this.setState({ statusesList: statusesList });
  }

  onPageChanged = async (page: number): Promise<void> => {
    await this.setState({ currentPage: page });
    await this.setUserActionTimesList();
  };

  render(): React.ReactNode {
    return (
      <main className={cn(classes.container)}>
        <div>
          <div className={cn(classes.filters)}>
            <Form.Group className={cn(classes.formGroup)}>
              <Form.Control
                as="select"
                value={this.state.pageLimit}
                onChange={this.setPage}
                custom
              >
                {this.getPageOptions()}
              </Form.Control>
            </Form.Group>
            <Form.Group className={cn(classes.formGroup)}>
              <Form.Control
                as="select"
                value={this.state.branchOfficeId}
                onChange={this.setBranchOfficeId}
                custom
              >
                <option value="0">Все филиалы</option>
                {this.getBranchOfficeOptions()}
              </Form.Control>
            </Form.Group>
            <Form.Group className={cn(classes.formGroup)}>
              <Form.Control
                as="select"
                value={this.state.userId}
                onChange={this.setUserId}
                custom
              >
                <option value="0">Все юзеры</option>
                {this.getUserOptions()}
              </Form.Control>
            </Form.Group>
            <Form.Group className={cn(classes.formGroup)}>
              <Form.Control
                as="select"
                value={this.state.statusId}
                onChange={this.setStatusId}
                custom
              >
                <option value="0">Все статусы</option>
                {this.getStatusOptions()}
              </Form.Control>
            </Form.Group>
            <Form.Group className={cn(classes.formGroup)}>
              <Form.Control
                placeholder="Search"
                type="text"
                value={this.state.searchExpression}
                onChange={this.setSearchExpression}
              />
            </Form.Group>
            <Button variant="primary" onClick={this.filter}>
              <Typography color="white" variant="span">
                Фильтр
              </Typography>
            </Button>
            <Button variant="success" onClick={this.export}>
              <Typography color="white" variant="span">
                Экспорт
              </Typography>
            </Button>
          </div>
          <div className={cn(classes.dataFilters)}>
            <DatePicker
              dateFormat="yyyy/MM/dd"
              selected={this.state.dateFrom}
              onChange={this.setDateFrom}
              className={cn(classes.dataFilter)}
            />
            <DatePicker
              dateFormat="yyyy/MM/dd"
              selected={this.state.dateTo}
              onChange={this.setDateTo}
              className={cn(classes.toDataFilter, classes.dataFilter)}
            />
          </div>
          <div className={cn(classes.content)}>
            <Table className={cn(classes.table)} striped size="lg">
              <thead className={cn(classes.tableHeader)}>
                <tr>{this.getTableHeaders()}</tr>
              </thead>
              <tbody>{this.getUserActionTimesList()}</tbody>
            </Table>
          </div>
        </div>
        <div>
          {this.state.paginationData.totalPages != 0 ? (
            <Pagination
              totalPages={this.state.paginationData.totalPages}
              totalRecords={this.state.paginationData.totalRecords}
              onPageChanged={this.onPageChanged}
              currentPage={this.state.paginationData.currentPage}
            />
          ) : null}
        </div>
      </main>
    );
  }

  getPageOptions(): JSX.Element[] {
    return this.pagesCountLimmit.map((pageCount: number, index: number) => {
      return (
        <option key={index} value={pageCount}>
          {pageCount}
        </option>
      );
    });
  }

  getBranchOfficeOptions(): JSX.Element[] {
    return this.state.branchOfficesList.map(
      (branchOffice: BranchOffice, index: number) => {
        return (
          <option key={index} value={branchOffice.id}>
            {branchOffice.name}
          </option>
        );
      }
    );
  }

  getStatusOptions(): JSX.Element[] {
    return this.state.statusesList.map((status: Status, index: number) => {
      return (
        <option key={index} value={status.id}>
          {status.name}
        </option>
      );
    });
  }

  getUserOptions(): JSX.Element[] {
    return this.state.usersList.map((user: User, index: number) => {
      return (
        <option key={index} value={user.id}>
          {user.firstName}
        </option>
      );
    });
  }

  setBranchOfficeId = async (
    event: React.ChangeEvent<HTMLSelectElement>
  ): Promise<void> => {
    const branchOfficeId = event.target.value;
    this.setState({ branchOfficeId: branchOfficeId });
  };

  setUserId = async (
    event: React.ChangeEvent<HTMLSelectElement>
  ): Promise<void> => {
    const userId = event.target.value;
    this.setState({ userId: userId });
  };

  setStatusId = async (
    event: React.ChangeEvent<HTMLSelectElement>
  ): Promise<void> => {
    const statusId = event.target.value;
    this.setState({ statusId: statusId });
  };

  setSearchExpression = (event: React.ChangeEvent<HTMLInputElement>): void => {
    this.setState({ searchExpression: event.target.value });
  };

  setPage = (event: React.ChangeEvent<HTMLSelectElement>): void => {
    this.setState({ pageLimit: Number(event.target.value) });
  };

  filter = (): void => {
    this.setUserActionTimesList(1);
  };

  export = (): void => {
    this.adapter.export(
      this.state.branchOfficeId,
      this.state.statusId,
      this.state.userId,
      this.state.searchExpression,
      this.state.dateFrom,
      this.state.dateTo
    );
  };

  setDateTo = (date: Date): void => {
    this.setState({ dateTo: date });
  };

  setDateFrom = (date: Date): void => {
    this.setState({ dateFrom: date });
  };

  getTableHeaders(): JSX.Element[] {
    return this.headers.map((header: string, index: number) => {
      return (
        <th key={index}>
          <Typography variant="span">{header}</Typography>
        </th>
      );
    });
  }

  getUserActionTimesList(): JSX.Element[] {
    return this.state.userActionTimesList.map(
      (userActionTime: UserActionTime) => {
        return (
          <tr key={userActionTime.id} className={cn(classes.tableRaw)}>
            <td>{userActionTime.userFullName}</td>
            <td>{userActionTime.lunchTime}</td>
            <td>{userActionTime.workTime}</td>
            <td>{userActionTime.breakTime}</td>
            <td>{userActionTime.offlineTime}</td>
          </tr>
        );
      }
    );
  }
}
export default UserReportPage;
