import React from "react";
import { Button, Modal } from "react-bootstrap";
import commonClasses from "../../SuperAdminPages.css";
import cn from "clsx";
import Typography from "src/components/Typography";
import { createNotification } from "src/helpers/crud-notifications";
import { deleteRole } from "src/apis/AdminPages/SuperAdminPages/RolePage/role";
import { ModalDeletePropertiesState } from "src/pages/Admin/types";

interface RoleDeleteModalProps {
  modalDeletePropertiesState: ModalDeletePropertiesState;
  id: number;
  recallRoleApi: () => void;
}

const RoleDeleteModal = ({
  modalDeletePropertiesState,
  id,
  recallRoleApi,
}: RoleDeleteModalProps) => {
  const handleConfirmationDeleteRoleModalClick = async () => {
    const response = await deleteRole(id);

    if (response == "ROLE_IS_REMOVED") {
      createNotification("success", "Роль удалена");
      modalDeletePropertiesState.setIsDeleteModalShown(false);
      recallRoleApi();
    }
  };

  return (
    <Modal
      className={cn(commonClasses.modalDiolog)}
      show={modalDeletePropertiesState.isDeleteModalShown}
      onHide={() => modalDeletePropertiesState.setIsDeleteModalShown(false)}
    >
      <Modal.Header closeButton>
        <Modal.Title>
          <Typography variant="span">
            Вы уверены что хотите удалить выбранный пункт?
          </Typography>
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className={cn(commonClasses.modalDeleteActionButtons)}>
        <Button
          className={cn(commonClasses.modalDeleteActionButton)}
          variant="secondary"
          onClick={() => handleConfirmationDeleteRoleModalClick()}
        >
          <Typography color="white" variant="span">
            Да
          </Typography>
        </Button>
        <Button
          className={cn(commonClasses.modalDeleteActionButton)}
          variant="primary"
          onClick={() =>
            modalDeletePropertiesState.setIsDeleteModalShown(false)
          }
        >
          <Typography color="white" variant="span">
            Нет
          </Typography>
        </Button>
      </Modal.Body>
    </Modal>
  );
};

export default RoleDeleteModal;
