import React, { useEffect, useState } from "react";
import commonClasses from "../../SuperAdminPages.css";
import cn from "clsx";
import Typography from "src/components/Typography";
import { MODAL_TYPE } from "../../consts";
import { createNotification } from "src/helpers/crud-notifications";
import { Button, Form, Modal } from "react-bootstrap";
import { ModalPropertiesState } from "../../types";
import { RoleInModal } from "src/types/Admin/SuperAdmin/Role/role";
import {
  storeRole,
  updateRole,
} from "src/apis/AdminPages/SuperAdminPages/RolePage/role";
// @ts-ignore
import Select from "react-select";
import useApiCall from "src/utils/hooks/use-async-call";
import { getPermissionsList } from "src/apis/AdminPages/SuperAdminPages/RolePage/permission";
import { Permission } from "src/types/Admin/SuperAdmin/Role/permission";

interface RoleModalProps {
  modalPropertiesState: ModalPropertiesState;
  roleInModalState: {
    roleInModal: RoleInModal;
    setRoleInModal: (roleInModalState: RoleInModal) => void;
  };
  recallRoleApi: () => void;
}

const RoleModal = ({
  modalPropertiesState,
  roleInModalState,
  recallRoleApi,
}: RoleModalProps) => {
  const [permissions, callPermissionApi] = useApiCall(getPermissionsList);
  const [permissionsList, setPermissionsList] = useState<Permission[]>([]);
  useEffect(() => {
    callPermissionApi();
  }, []);

  useEffect(() => {
    if (permissions) {
      setPermissionsList(permissions);
    }
  }, [permissions]);

  const getPermissionOptions = () => {
    const options = permissionsList.map((permission: Permission) => {
      return { value: permission.id, label: permission.name };
    });
    return options;
  };

  const handlePermissionIdsChange = (
    selectedPermissions: [{ value: number }]
  ): void => {
    const options = selectedPermissions;
    const permissionIds = [];

    for (let i = 0; i < options.length; i++) {
      permissionIds.push(options[i].value);
    }

    roleInModalState.setRoleInModal({
      ...roleInModalState.roleInModal,
      permissionIds,
    });
  };

  const handleCreateRoleClick = async () => {
    const response = await storeRole(roleInModalState.roleInModal);

    if (response == "ROLE_IS_SAVED") {
      createNotification("success", "Роль сохранена");
      recallRoleApi();
      modalPropertiesState.setModalProperties({
        ...modalPropertiesState.modalProperties,
        isModalShown: false,
      });
    }
  };

  const handleUpdateRoleClick = async () => {
    const response = await updateRole(roleInModalState.roleInModal);

    if (response == "ROLE_IS_UPDATED") {
      createNotification("success", "Роль обновлена");
      recallRoleApi();
      modalPropertiesState.setModalProperties({
        ...modalPropertiesState.modalProperties,
        isModalShown: false,
      });
    }
  };

  return (
    <Modal
      className={cn(commonClasses.modalDiolog)}
      show={modalPropertiesState.modalProperties.isModalShown}
      onHide={() =>
        modalPropertiesState.setModalProperties({
          ...modalPropertiesState.modalProperties,
          isModalShown: false,
        })
      }
    >
      <Modal.Header closeButton>
        <Modal.Title>
          <Typography variant="span">
            {modalPropertiesState.modalProperties.action ==
            MODAL_TYPE.CREATE.NAME
              ? "Добавить роль"
              : "Изменить роль"}
          </Typography>
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div>
          <Form.Group>
            <Typography variant="label">Наименование</Typography>
            <Form.Control
              data-testid="role-name"
              type="text"
              value={roleInModalState.roleInModal.name}
              onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
                roleInModalState.setRoleInModal({
                  ...roleInModalState.roleInModal,
                  name: event.target.value,
                })
              }
            />
          </Form.Group>
          <Form.Group>
            <Typography variant="label">Пермишины</Typography>
            <Select
              aria-label="permissions-ids"
              defaultValue={permissionsList.map((permission: Permission) => {
                if (
                  roleInModalState.roleInModal.permissionIds.includes(
                    permission.id
                  )
                ) {
                  return { label: permission.name, value: permission.id };
                }
                return null;
              })}
              options={getPermissionOptions()}
              isMulti
              onChange={handlePermissionIdsChange}
            />
          </Form.Group>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <Button
          variant="secondary"
          onClick={
            modalPropertiesState.modalProperties.action ==
            MODAL_TYPE.CREATE.NAME
              ? () => handleCreateRoleClick()
              : () => handleUpdateRoleClick()
          }
        >
          <Typography color="white" variant="span">
            {modalPropertiesState.modalProperties.action ==
            MODAL_TYPE.CREATE.NAME
              ? MODAL_TYPE.CREATE.ACTION
              : MODAL_TYPE.UPDATE.ACTION}
          </Typography>
        </Button>
        <Button
          variant="primary"
          onClick={() =>
            modalPropertiesState.setModalProperties({
              ...modalPropertiesState.modalProperties,
              isModalShown: false,
            })
          }
        >
          <Typography color="white" variant="span">
            Закрыть
          </Typography>
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default RoleModal;
