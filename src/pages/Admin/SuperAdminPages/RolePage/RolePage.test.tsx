import {
  cleanup,
  waitForElementToBeRemoved,
  screen,
  fireEvent,
} from "@testing-library/react";
import React from "react";
import { server } from "src/__mocks__/automatic/mockHttp";
import RolePage from "./RolePage";
import "@testing-library/jest-dom";
import { renderWithRouter } from "src/__mocks__/utils";
import {
  DELETE_ROLE_RESPONSE,
  GET_PAGINATED_ROLES_LIST_RESPONSE,
  STORE_ROLE_RESPONSE,
  UPDATE_ROLE_RESPONSE,
} from "src/__mocks__/handlers/role";
import { GET_PERMISSIONS_LIST_RESPONSE } from "src/__mocks__/handlers/permission";
import { mockedRoles } from "src/__mocks__/domains/role";

afterEach(cleanup);

describe("RolePage component", () => {
  it("should show data in the table", async () => {
    const limit = 5;
    const page = 1;
    const searchExpression = "";
    server.use(...GET_PERMISSIONS_LIST_RESPONSE);
    server.use(
      ...GET_PAGINATED_ROLES_LIST_RESPONSE(searchExpression, limit, page)
    );
    renderWithRouter(<RolePage />);

    await waitForElementToBeRemoved(() => screen.getByLabelText("Loading"));
    expect(screen.getByText("admin")).toBeInTheDocument();
    expect(screen.getByText("admin access")).toBeInTheDocument();
  });

  it("should paginate to second page", async () => {
    const limit = 5;
    const page = 1;
    const searchExpression = "";
    server.use(...GET_PERMISSIONS_LIST_RESPONSE);
    server.use(
      ...GET_PAGINATED_ROLES_LIST_RESPONSE(searchExpression, limit, page)
    );
    renderWithRouter(<RolePage />);

    await waitForElementToBeRemoved(() => screen.getByLabelText("Loading"));
    expect(screen.getByText("admin")).toBeInTheDocument();
    expect(screen.getByText("admin access")).toBeInTheDocument();

    const paginationSecondPageButton = screen.getByText("2");
    server.use(
      ...GET_PAGINATED_ROLES_LIST_RESPONSE(searchExpression, limit, 2)
    );
    fireEvent.click(paginationSecondPageButton);
    await waitForElementToBeRemoved(() => screen.getByLabelText("Loading"));
    expect(screen.queryByText("admin")).not.toBeInTheDocument();
    expect(screen.queryByText("admin access")).not.toBeInTheDocument();
    expect(screen.queryByText("customer")).toBeInTheDocument();
    expect(screen.queryByText("crm partners")).toBeInTheDocument();
  });

  it("should show 10 items on the first page", async () => {
    const limit = 5;
    const page = 1;
    const searchExpression = "";

    server.use(...GET_PERMISSIONS_LIST_RESPONSE);
    server.use(
      ...GET_PAGINATED_ROLES_LIST_RESPONSE(searchExpression, limit, page)
    );
    renderWithRouter(<RolePage />);

    await waitForElementToBeRemoved(() => screen.getByLabelText("Loading"));
    expect(screen.getByText("admin")).toBeInTheDocument();
    expect(screen.getByText("admin access")).toBeInTheDocument();

    const pageLimitSelector = screen.getByTestId("page-limit-selector");
    fireEvent.change(pageLimitSelector, { target: { value: "10" } });
    const filterButton = screen.getByTestId("filter");
    server.use(
      ...GET_PAGINATED_ROLES_LIST_RESPONSE(searchExpression, 10, page)
    );
    fireEvent.click(filterButton);
    await waitForElementToBeRemoved(() => screen.getByLabelText("Loading"));
    expect(screen.getByText("admin")).toBeInTheDocument();
    expect(screen.getByText("admin access")).toBeInTheDocument();
    expect(screen.queryByText("customer")).toBeInTheDocument();
    expect(screen.queryByText("crm partners")).toBeInTheDocument();
  });

  it("should show an item with name 'customer'", async () => {
    const limit = 5;
    const page = 1;
    const searchExpression = "";

    server.use(...GET_PERMISSIONS_LIST_RESPONSE);
    server.use(
      ...GET_PAGINATED_ROLES_LIST_RESPONSE(searchExpression, limit, page)
    );
    renderWithRouter(<RolePage />);

    await waitForElementToBeRemoved(() => screen.getByLabelText("Loading"));
    expect(screen.getByText("admin")).toBeInTheDocument();
    expect(screen.getByText("admin access")).toBeInTheDocument();

    const roleName = "customer";
    const searchInput = screen.getByTestId("search-selector");
    fireEvent.change(searchInput, { target: { value: roleName } });
    const filterButton = screen.getByTestId("filter");
    server.use(...GET_PAGINATED_ROLES_LIST_RESPONSE(roleName, limit, page));
    fireEvent.click(filterButton);
    await waitForElementToBeRemoved(() => screen.getByLabelText("Loading"));
    expect(screen.queryByText("customer")).toBeInTheDocument();
    expect(screen.queryByText("admin")).not.toBeInTheDocument();
  });

  it("should open remove pop up when admin click on cross icon", async () => {
    const limit = 5;
    const page = 1;
    const searchExpression = "";

    server.use(...GET_PERMISSIONS_LIST_RESPONSE);
    server.use(
      ...GET_PAGINATED_ROLES_LIST_RESPONSE(searchExpression, limit, page)
    );
    renderWithRouter(<RolePage />);

    await waitForElementToBeRemoved(() => screen.getByLabelText("Loading"));
    expect(screen.getByText("admin")).toBeInTheDocument();
    expect(screen.getByText("admin access")).toBeInTheDocument();

    const removeButton = screen.getByTestId(`remove-icon-${mockedRoles[0].id}`);
    fireEvent.click(removeButton);
    expect(
      screen.queryByText("Вы уверены что хотите удалить выбранный пункт?")
    ).toBeInTheDocument();
  });

  it("should remove the chosen branch office when a 'admin' click on the cross icon", async () => {
    const limit = 5;
    const page = 1;
    const searchExpression = "";

    server.use(...GET_PERMISSIONS_LIST_RESPONSE);
    server.use(
      ...GET_PAGINATED_ROLES_LIST_RESPONSE(searchExpression, limit, page)
    );
    renderWithRouter(<RolePage />);

    await waitForElementToBeRemoved(() => screen.getByLabelText("Loading"));
    expect(screen.getByText("admin")).toBeInTheDocument();
    expect(screen.getByText("admin access")).toBeInTheDocument();

    const removeIconButton = screen.getByTestId(
      `remove-icon-${mockedRoles[0].id}`
    );
    fireEvent.click(removeIconButton);
    const removeButton = screen.getByText("Да");
    server.use(...DELETE_ROLE_RESPONSE);
    fireEvent.click(removeButton);
    await waitForElementToBeRemoved(() =>
      screen.getByText("Вы уверены что хотите удалить выбранный пункт?")
    );
    expect(screen.queryByText("Роль удалена")).toBeInTheDocument();
    /** the logic of removing, recalls the
      permissions list as a result data from the
      previous rendering get not up to date. So if you try to find permissionName from previous rendering
      jest will not find it
      */
  });

  it("should create a new role", async () => {
    const limit = 5;
    const page = 1;
    const searchExpression = "";

    server.use(...GET_PERMISSIONS_LIST_RESPONSE);
    server.use(
      ...GET_PAGINATED_ROLES_LIST_RESPONSE(searchExpression, limit, page)
    );
    renderWithRouter(<RolePage />);

    await waitForElementToBeRemoved(() => screen.getByLabelText("Loading"));
    expect(screen.getByText("admin")).toBeInTheDocument();
    expect(screen.getByText("admin access")).toBeInTheDocument();

    const addButton = screen.getByTestId("add-role");
    fireEvent.click(addButton);
    expect(screen.queryByText("Добавить роль")).toBeInTheDocument();

    const roleNameInput = screen.getByTestId("role-name");
    fireEvent.change(roleNameInput, {
      target: { value: "programmer" },
    });
    const permissionsIdsInput = screen.getByLabelText("permissions-ids");
    fireEvent.change(permissionsIdsInput, {
      target: { value: "admin access" },
    });
    const options = screen.getAllByText("admin access");
    fireEvent.click(options[2]);
    const submitButton = screen.getByText("Сохранить");
    server.use(...STORE_ROLE_RESPONSE);
    fireEvent.click(submitButton);
    await waitForElementToBeRemoved(() => screen.getByText("Добавить роль"));
    expect(screen.queryByText("Роль сохранена")).toBeInTheDocument();
  });

  it("should update a role", async () => {
    const limit = 5;
    const page = 1;
    const searchExpression = "";

    server.use(...GET_PERMISSIONS_LIST_RESPONSE);
    server.use(
      ...GET_PAGINATED_ROLES_LIST_RESPONSE(searchExpression, limit, page)
    );
    renderWithRouter(<RolePage />);

    await waitForElementToBeRemoved(() => screen.getByLabelText("Loading"));
    expect(screen.getByText("admin")).toBeInTheDocument();
    expect(screen.getByText("admin access")).toBeInTheDocument();

    const updateIconButton = screen.getByTestId(`update-icon-1`);
    fireEvent.click(updateIconButton);
    expect(screen.queryByText("Изменить роль")).toBeInTheDocument();
    const roleNameInput = screen.getByTestId("role-name");
    const visitPurposeIdsInputValue = screen.getAllByText("admin access")[1];
    expect(roleNameInput).toHaveValue("admin");
    expect(visitPurposeIdsInputValue).toBeInTheDocument();

    fireEvent.change(roleNameInput, {
      target: { value: "programmer" },
    });
    const visitPurposeIdsInput = screen.getByLabelText("permissions-ids");
    fireEvent.change(visitPurposeIdsInput, {
      target: { value: "crm developer" },
    });
    const options = screen.getAllByText("crm developer");
    fireEvent.click(options[2]);
    const submitButton = screen.getByText("Обновить");
    server.use(...UPDATE_ROLE_RESPONSE);
    fireEvent.click(submitButton);
    await waitForElementToBeRemoved(() => screen.getByText("Изменить роль"));
    expect(screen.queryByText("Роль обновлена")).toBeInTheDocument();
  });
});
