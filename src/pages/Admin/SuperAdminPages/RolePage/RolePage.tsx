import React, { useEffect, useState } from "react";
import classes from "./RolePage.css";
import cn from "clsx";
import Typography from "src/components/Typography";
import { Button, Form } from "react-bootstrap";
// @ts-ignore
import { NotificationContainer } from "react-notifications";
import { MODAL_TYPE } from "../consts";
import AdminPagesWrapper from "src/wrappers/AdminPagesWrapper";
import { AdminPagesWrapperProps } from "src/wrappers/AdminPagesWrapper/AdminPagesWrapper";
import useApiCall from "src/utils/hooks/use-async-call";
import { getPageOptions } from "../utils";
import Pagination from "src/components/Pagination/Pagination";
import RoleTable from "./RoleTable";
import RoleDeleteModal from "./RoleDeleteModal";
import RoleModal from "./RoleModal";
import { RoleInModal } from "src/types/Admin/SuperAdmin/Role/role";
import { getPaginatedRolesList } from "src/apis/AdminPages/SuperAdminPages/RolePage/role";

const RolePage = ({
  modalPropertiesState,
  pageLimitState,
  paginationDataState,
  searchExpressionState,
  modalDeletePropertiesState,
}: AdminPagesWrapperProps) => {
  const [rolesList, setRolesList] = useState([]);
  const [data, callRoleApi, isLoading] = useApiCall(getPaginatedRolesList);
  const [roleInModal, setRoleInModal] = useState<RoleInModal>({
    id: 0,
    name: "",
    permissionIds: [],
  });
  useEffect(() => {
    callRoleApi([
      searchExpressionState.searchExpression,
      pageLimitState.pageLimit,
    ]);
  }, []);

  useEffect(() => {
    const { roles, paginationData } = data;
    if (roles) {
      setRolesList(roles);
      paginationDataState.setPaginationData(paginationData);
    }
  }, [data]);

  const recallRoleApi = () => {
    callRoleApi([
      searchExpressionState.searchExpression,
      pageLimitState.pageLimit,
    ]);
  };

  const onPageChanged = (page: number): void => {
    callRoleApi([
      searchExpressionState.searchExpression,
      pageLimitState.pageLimit,
      page,
    ]);
  };

  return (
    <main className={cn(classes.container)}>
      <NotificationContainer />
      <div className={cn(classes.containerHeader)}>
        <div className={cn(classes.filters)}>
          <Form.Group>
            <Form.Control
              data-testid="page-limit-selector"
              as="select"
              value={pageLimitState.pageLimit}
              onChange={(event: React.ChangeEvent<HTMLSelectElement>) =>
                pageLimitState.setPageLimit(Number(event.target.value))
              }
              custom
            >
              {getPageOptions()}
            </Form.Control>
          </Form.Group>
          <Form.Group>
            <Form.Control
              data-testid="search-selector"
              type="text"
              placeholder="Поиск по имени"
              value={searchExpressionState.searchExpression}
              onChange={(event: React.ChangeEvent<HTMLSelectElement>) =>
                searchExpressionState.setSearchExpression(event.target.value)
              }
            />
          </Form.Group>
          <Button
            data-testid="filter"
            variant="primary"
            onClick={() => {
              callRoleApi([
                searchExpressionState.searchExpression,
                pageLimitState.pageLimit,
              ]);
            }}
          >
            <Typography color="white" variant="span">
              Фильтр
            </Typography>
          </Button>
          <Button
            data-testid="add-role"
            variant="primary"
            onClick={() => {
              modalPropertiesState.setModalProperties({
                action: MODAL_TYPE.CREATE.NAME,
                isModalShown: true,
              });
              setRoleInModal({
                name: "",
                id: 0,
                permissionIds: [],
              });
            }}
          >
            <Typography color="white" variant="span">
              Добавить
            </Typography>
          </Button>
        </div>
      </div>
      {isLoading ? (
        <span aria-label="Loading" />
      ) : (
        <RoleTable
          setModalProperties={modalPropertiesState.setModalProperties}
          setRoleInModal={setRoleInModal}
          setIsDeleteModalShown={
            modalDeletePropertiesState.setIsDeleteModalShown
          }
          rolesList={rolesList}
        />
      )}
      <RoleDeleteModal
        modalDeletePropertiesState={modalDeletePropertiesState}
        id={roleInModal.id}
        recallRoleApi={recallRoleApi}
      />
      <RoleModal
        modalPropertiesState={modalPropertiesState}
        roleInModalState={{
          roleInModal: roleInModal,
          setRoleInModal: setRoleInModal,
        }}
        recallRoleApi={recallRoleApi}
      />
      {paginationDataState.paginationData !== undefined &&
        paginationDataState.paginationData.totalPages != 0 && (
          <Pagination
            totalPages={paginationDataState.paginationData.totalPages}
            totalRecords={paginationDataState.paginationData.totalRecords}
            onPageChanged={onPageChanged}
            currentPage={paginationDataState.paginationData.currentPage}
          />
        )}
    </main>
  );
};

export default () => AdminPagesWrapper(RolePage);
