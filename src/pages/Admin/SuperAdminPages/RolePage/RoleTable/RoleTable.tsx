import React from "react";
import { Table } from "react-bootstrap";
import commonClasses from "../../SuperAdminPages.css";
import cn from "clsx";
import Typography from "src/components/Typography";
import { MODAL_TYPE } from "../../consts";
import Delete from "../../images/Delete";
import Edit from "../../images/Edit";
import classes from "./RoleTable.css";
import { ModalProperties } from "../../types";
import { Role, RoleInModal } from "src/types/Admin/SuperAdmin/Role/role";
import { Permission } from "src/types/Admin/SuperAdmin/Role/permission";

interface RoleTableProps {
  setModalProperties: (modalProperties: ModalProperties) => void;
  setRoleInModal: (roleInModal: RoleInModal) => void;
  setIsDeleteModalShown: (isDeleteModalShown: boolean) => void;
  rolesList: Role[];
}

const getPermissionIds = (selectedPermissions: Permission[]) => {
  const options = selectedPermissions;
  const selectedOptions = [];

  for (let i = 0; i < options.length; i++) {
    selectedOptions.push(options[i].id);
  }

  return selectedOptions;
};

const getPermissionRows = (permissions: { name: string }[]) => {
  const arrayLength = permissions.length;
  return permissions.map((permission: { name: string }, index: number) => {
    if (arrayLength == index + 1) {
      return permission.name;
    }

    return permission.name + ", ";
  });
};

const RoleTable = ({
  setModalProperties,
  setRoleInModal,
  setIsDeleteModalShown,
  rolesList,
}: RoleTableProps) => {
  const getRolesList = () => {
    return rolesList.map((role: Role) => {
      return (
        <tr key={role.id} className={cn(classes.tableRaw)}>
          <td>{role.name}</td>
          <td>{getPermissionRows(role.permissions)}</td>
          <td>
            <button
              data-testid={`remove-icon-${role.id}`}
              className={cn(commonClasses.buttonAction)}
              type="button"
              onClick={() => {
                setIsDeleteModalShown(true);
                setRoleInModal({
                  id: role.id,
                  name: "",
                  permissionIds: [],
                });
              }}
            >
              {Delete()}
            </button>
            <button
              data-testid={`update-icon-${role.id}`}
              className={cn(commonClasses.buttonAction)}
              type="button"
              onClick={() => {
                setModalProperties({
                  isModalShown: true,
                  action: MODAL_TYPE.UPDATE.NAME,
                });
                setRoleInModal({
                  name: role.name,
                  id: role.id,
                  permissionIds: getPermissionIds(role.permissions),
                });
              }}
            >
              {Edit()}
            </button>
          </td>
        </tr>
      );
    });
  };

  return (
    <Table className={cn(classes.table)} striped size="lg">
      <thead className={cn(classes.tableHeader)}>
        <tr>
          <th>
            <Typography variant="span">Название</Typography>
          </th>
          <th>
            <Typography variant="span">Разрешения</Typography>
          </th>
          <th>
            <Typography variant="span">Действие</Typography>
          </th>
        </tr>
      </thead>
      <tbody>{getRolesList()}</tbody>
    </Table>
  );
};

export default RoleTable;
