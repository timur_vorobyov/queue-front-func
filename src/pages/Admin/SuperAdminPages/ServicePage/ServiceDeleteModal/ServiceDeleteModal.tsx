import React from "react";
import { Button, Modal } from "react-bootstrap";
import commonClasses from "../../SuperAdminPages.css";
import cn from "clsx";
import Typography from "src/components/Typography";
import { createNotification } from "src/helpers/crud-notifications";
import { deleteService } from "src/apis/AdminPages/SuperAdminPages/ServicePagePage/service";
import { ModalDeletePropertiesState } from "src/pages/Admin/types";

interface ServiceDeleteModalProps {
  modalDeletePropertiesState: ModalDeletePropertiesState;
  id: number;
  recallServiceApi: () => void;
}

const ServiceDeleteModal = ({
  modalDeletePropertiesState,
  id,
  recallServiceApi,
}: ServiceDeleteModalProps) => {
  const handleConfirmationDeleteServiceModalClick = async () => {
    const response = await deleteService(id);

    if (response == "SERVICE_IS_REMOVED") {
      createNotification("success", "Сервис удален");
      modalDeletePropertiesState.setIsDeleteModalShown(false);
      recallServiceApi();
    }
  };

  return (
    <Modal
      className={cn(commonClasses.modalDiolog)}
      show={modalDeletePropertiesState.isDeleteModalShown}
      onHide={() => modalDeletePropertiesState.setIsDeleteModalShown(false)}
    >
      <Modal.Header closeButton>
        <Modal.Title>
          <Typography variant="span">
            Вы уверены что хотите удалить выбранный пункт?
          </Typography>
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className={cn(commonClasses.modalDeleteActionButtons)}>
        <Button
          className={cn(commonClasses.modalDeleteActionButton)}
          variant="secondary"
          onClick={() => handleConfirmationDeleteServiceModalClick()}
        >
          <Typography color="white" variant="span">
            Да
          </Typography>
        </Button>
        <Button
          className={cn(commonClasses.modalDeleteActionButton)}
          variant="primary"
          onClick={() =>
            modalDeletePropertiesState.setIsDeleteModalShown(false)
          }
        >
          <Typography color="white" variant="span">
            Нет
          </Typography>
        </Button>
      </Modal.Body>
    </Modal>
  );
};

export default ServiceDeleteModal;
