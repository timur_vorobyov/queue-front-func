import React from "react";
import commonClasses from "../../SuperAdminPages.css";
import cn from "clsx";
import Typography from "src/components/Typography";
import { MODAL_TYPE } from "../../consts";
import { createNotification } from "src/helpers/crud-notifications";
import { Button, Form, Modal } from "react-bootstrap";
import { ModalPropertiesState } from "../../types";
import { ServiceInModal } from "src/types/Admin/SuperAdmin/Service/service";
import {
  storeService,
  updateService,
} from "src/apis/AdminPages/SuperAdminPages/ServicePagePage/service";

interface ServiceModalProps {
  modalPropertiesState: ModalPropertiesState;
  serviceInModalState: {
    serviceInModal: ServiceInModal;
    setServiceInModal: (serviceInModalState: ServiceInModal) => void;
  };
  recallServiceApi: () => void;
}

const ServiceModal = ({
  modalPropertiesState,
  serviceInModalState,
  recallServiceApi,
}: ServiceModalProps) => {
  const handleCreateServiceClick = async () => {
    const response = await storeService(serviceInModalState.serviceInModal);

    if (response == "SERVICE_IS_SAVED") {
      createNotification("success", "Сервиc сохранен");
      recallServiceApi();
      modalPropertiesState.setModalProperties({
        ...modalPropertiesState.modalProperties,
        isModalShown: false,
      });
    }
  };

  const handleUpdateServiceClick = async () => {
    const response = await updateService(serviceInModalState.serviceInModal);

    if (response == "SERVICE_IS_UPDATED") {
      createNotification("success", "Сервис обновлен");
      recallServiceApi();
      modalPropertiesState.setModalProperties({
        ...modalPropertiesState.modalProperties,
        isModalShown: false,
      });
    }
  };

  return (
    <Modal
      className={cn(commonClasses.modalDiolog)}
      show={modalPropertiesState.modalProperties.isModalShown}
      onHide={() =>
        modalPropertiesState.setModalProperties({
          ...modalPropertiesState.modalProperties,
          isModalShown: false,
        })
      }
    >
      <Modal.Header closeButton>
        <Modal.Title>
          <Typography variant="span">
            {modalPropertiesState.modalProperties.action ==
            MODAL_TYPE.CREATE.NAME
              ? "Добавить сервис"
              : "Изменить сервис"}
          </Typography>
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div>
          <Form.Group>
            <Typography variant="label">Имя услуги</Typography>
            <Form.Control
              data-testid="service-name"
              type="text"
              value={serviceInModalState.serviceInModal.name}
              onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
                serviceInModalState.setServiceInModal({
                  ...serviceInModalState.serviceInModal,
                  name: event.target.value,
                })
              }
            />
          </Form.Group>
          <Form.Group>
            <Typography variant="label">Статус</Typography>
            <Form.Check
              data-testid="service-activity"
              type="switch"
              id="custom-switch"
              label="Активный?"
              checked={serviceInModalState.serviceInModal.isActive}
              onChange={() =>
                serviceInModalState.setServiceInModal({
                  ...serviceInModalState.serviceInModal,
                  isActive: !serviceInModalState.serviceInModal.isActive,
                })
              }
            />
          </Form.Group>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <Button
          variant="secondary"
          onClick={
            modalPropertiesState.modalProperties.action ==
            MODAL_TYPE.CREATE.NAME
              ? () => handleCreateServiceClick()
              : () => handleUpdateServiceClick()
          }
        >
          <Typography color="white" variant="span">
            {modalPropertiesState.modalProperties.action ==
            MODAL_TYPE.CREATE.NAME
              ? MODAL_TYPE.CREATE.ACTION
              : MODAL_TYPE.UPDATE.ACTION}
          </Typography>
        </Button>
        <Button
          variant="primary"
          onClick={() =>
            modalPropertiesState.setModalProperties({
              ...modalPropertiesState.modalProperties,
              isModalShown: false,
            })
          }
        >
          <Typography color="white" variant="span">
            Закрыть
          </Typography>
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default ServiceModal;
