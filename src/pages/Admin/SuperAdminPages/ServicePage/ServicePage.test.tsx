import {
  cleanup,
  waitForElementToBeRemoved,
  screen,
  fireEvent,
} from "@testing-library/react";
import React from "react";
import { server } from "src/__mocks__/automatic/mockHttp";
import ServicePage from "./ServicePage";
import "@testing-library/jest-dom";
import { renderWithRouter } from "src/__mocks__/utils";
import {
  DELETE_SERVICE_RESPONSE,
  GET_PAGINATED_SERVICES_LIST_RESPONSE,
  STORE_SERVICE_RESPONSE,
  UPDATE_SERVICE_RESPONSE,
} from "src/__mocks__/handlers/service";
import { mockedServices } from "src/__mocks__/domains/service";

afterEach(cleanup);

describe("ServicePage component", () => {
  it("should show data in the table", async () => {
    const limit = 5;
    const page = 1;
    const searchExpression = "";
    server.use(
      ...GET_PAGINATED_SERVICES_LIST_RESPONSE(searchExpression, limit, page)
    );
    renderWithRouter(<ServicePage />);

    await waitForElementToBeRemoved(() => screen.getByLabelText("Loading"));
    expect(screen.getByText("Заявка на карту")).toBeInTheDocument();
  });

  it("should paginate to second page", async () => {
    const limit = 5;
    const page = 1;
    const searchExpression = "";
    server.use(
      ...GET_PAGINATED_SERVICES_LIST_RESPONSE(searchExpression, limit, page)
    );
    renderWithRouter(<ServicePage />);

    await waitForElementToBeRemoved(() => screen.getByLabelText("Loading"));
    expect(screen.getByText("Заявка на карту")).toBeInTheDocument();

    const paginationSecondPageButton = screen.getByText("2");
    server.use(
      ...GET_PAGINATED_SERVICES_LIST_RESPONSE(searchExpression, limit, 2)
    );
    fireEvent.click(paginationSecondPageButton);
    await waitForElementToBeRemoved(() => screen.getByLabelText("Loading"));
    expect(screen.queryByText("Заявка на карту")).not.toBeInTheDocument();
    expect(screen.queryByText("Денежные переводы/SWIFT")).toBeInTheDocument();
  });

  it("should show 10 items on the first page", async () => {
    const limit = 5;
    const page = 1;
    const searchExpression = "";

    server.use(
      ...GET_PAGINATED_SERVICES_LIST_RESPONSE(searchExpression, limit, page)
    );
    renderWithRouter(<ServicePage />);

    await waitForElementToBeRemoved(() => screen.getByLabelText("Loading"));
    expect(screen.getByText("Заявка на карту")).toBeInTheDocument();

    const pageLimitSelector = screen.getByTestId("page-limit-selector");
    fireEvent.change(pageLimitSelector, { target: { value: "10" } });
    const filterButton = screen.getByTestId("filter");
    server.use(
      ...GET_PAGINATED_SERVICES_LIST_RESPONSE(searchExpression, 10, page)
    );
    fireEvent.click(filterButton);
    await waitForElementToBeRemoved(() => screen.getByLabelText("Loading"));
    expect(screen.queryByText("Заявка на карту")).toBeInTheDocument();
    expect(screen.queryByText("Денежные переводы/SWIFT")).toBeInTheDocument();
    expect(screen.getByText("2")).toBeInTheDocument();
  });

  it("should show 2 items which includes 'Конвертация' inside its name", async () => {
    const limit = 5;
    const page = 1;
    const searchExpression = "";

    server.use(
      ...GET_PAGINATED_SERVICES_LIST_RESPONSE(searchExpression, limit, page)
    );
    renderWithRouter(<ServicePage />);

    await waitForElementToBeRemoved(() => screen.getByLabelText("Loading"));
    expect(screen.getByText("Заявка на карту")).toBeInTheDocument();

    const serviceName = "Конвертация";
    const searchInput = screen.getByTestId("search-selector");
    fireEvent.change(searchInput, { target: { value: serviceName } });
    const filterButton = screen.getByTestId("filter");
    server.use(
      ...GET_PAGINATED_SERVICES_LIST_RESPONSE(serviceName, limit, page)
    );
    fireEvent.click(filterButton);
    await waitForElementToBeRemoved(() => screen.getByLabelText("Loading"));
    expect(screen.queryByText("Конвертация")).toBeInTheDocument();
    expect(screen.queryByText("Заявка на карту")).not.toBeInTheDocument();
  });

  it("should open remove pop up when 'Заявка на карту' click on cross icon", async () => {
    const limit = 5;
    const page = 1;
    const searchExpression = "";

    server.use(
      ...GET_PAGINATED_SERVICES_LIST_RESPONSE(searchExpression, limit, page)
    );
    renderWithRouter(<ServicePage />);

    await waitForElementToBeRemoved(() => screen.getByLabelText("Loading"));
    expect(screen.getByText("Заявка на карту")).toBeInTheDocument();

    const removeButton = screen.getByTestId(
      `remove-icon-${mockedServices[0].id}`
    );
    fireEvent.click(removeButton);
    expect(
      screen.queryByText("Вы уверены что хотите удалить выбранный пункт?")
    ).toBeInTheDocument();
  });

  it("should remove the chosen branch office when a 'Заявка на карту' click on the cross icon", async () => {
    const limit = 5;
    const page = 1;
    const searchExpression = "";

    server.use(
      ...GET_PAGINATED_SERVICES_LIST_RESPONSE(searchExpression, limit, page)
    );
    renderWithRouter(<ServicePage />);

    await waitForElementToBeRemoved(() => screen.getByLabelText("Loading"));
    expect(screen.getByText("Заявка на карту")).toBeInTheDocument();

    const removeIconButton = screen.getByTestId(
      `remove-icon-${mockedServices[0].id}`
    );
    fireEvent.click(removeIconButton);
    const removeButton = screen.getByText("Да");
    server.use(...DELETE_SERVICE_RESPONSE);
    fireEvent.click(removeButton);
    await waitForElementToBeRemoved(() =>
      screen.getByText("Вы уверены что хотите удалить выбранный пункт?")
    );
    expect(screen.queryByText("Сервис удален")).toBeInTheDocument();
    /** the logic of removing, recalls the
      permissions list as a result data from the
      previous rendering get not up to date. So if you try to find permissionName from previous rendering
      jest will not find it
      */
  });

  it("should create a new service", async () => {
    const limit = 5;
    const page = 1;
    const searchExpression = "";
    server.use(
      ...GET_PAGINATED_SERVICES_LIST_RESPONSE(searchExpression, limit, page)
    );
    renderWithRouter(<ServicePage />);

    await waitForElementToBeRemoved(() => screen.getByLabelText("Loading"));
    expect(screen.getByText("Заявка на карту")).toBeInTheDocument();

    const addButton = screen.getByTestId("add-service");
    fireEvent.click(addButton);
    expect(screen.queryByText("Добавить сервис")).toBeInTheDocument();

    const serviceNameInput = screen.getByTestId("service-name");
    const serviceActivitySwitch = screen.getByTestId("service-activity");
    fireEvent.change(serviceNameInput, {
      target: { value: "Пополнение кошелька" },
    });
    fireEvent.click(serviceActivitySwitch);
    const submitButton = screen.getByText("Сохранить");
    server.use(...STORE_SERVICE_RESPONSE);
    fireEvent.click(submitButton);
    await waitForElementToBeRemoved(() => screen.getByText("Добавить сервис"));
    expect(screen.queryByText("Сервиc сохранен")).toBeInTheDocument();
  });

  it("should update a service", async () => {
    const limit = 5;
    const page = 1;
    const searchExpression = "";

    server.use(
      ...GET_PAGINATED_SERVICES_LIST_RESPONSE(searchExpression, limit, page)
    );
    renderWithRouter(<ServicePage />);

    await waitForElementToBeRemoved(() => screen.getByLabelText("Loading"));
    expect(screen.getByText("Заявка на карту")).toBeInTheDocument();

    const updateIconButton = screen.getByTestId(`update-icon-3`);
    fireEvent.click(updateIconButton);
    expect(screen.queryByText("Изменить сервис")).toBeInTheDocument();
    const serviceNameInput = screen.getByTestId("service-name");
    const serviceActivitySwitch = screen.getByTestId("service-activity");
    expect(serviceNameInput).toHaveValue("Заявка на карту");
    expect(serviceActivitySwitch).not.toBeChecked();

    fireEvent.change(serviceNameInput, {
      target: { value: "Пополнение кошелька" },
    });
    const submitButton = screen.getByText("Обновить");
    server.use(...UPDATE_SERVICE_RESPONSE);
    fireEvent.click(submitButton);
    await waitForElementToBeRemoved(() => screen.getByText("Изменить сервис"));
    expect(screen.queryByText("Сервис обновлен")).toBeInTheDocument();
  });
});
