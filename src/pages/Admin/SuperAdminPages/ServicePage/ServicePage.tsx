import React, { useEffect, useState } from "react";
import classes from "./ServicePage.css";
import cn from "clsx";
import Typography from "src/components/Typography";
import { Button, Form } from "react-bootstrap";
// @ts-ignore
import { NotificationContainer } from "react-notifications";
import { MODAL_TYPE } from "../consts";
import AdminPagesWrapper from "src/wrappers/AdminPagesWrapper";
import { AdminPagesWrapperProps } from "src/wrappers/AdminPagesWrapper/AdminPagesWrapper";
import useApiCall from "src/utils/hooks/use-async-call";
import { getPageOptions } from "../utils";
import Pagination from "src/components/Pagination/Pagination";
import { ServiceInModal } from "src/types/Admin/SuperAdmin/Service/service";
import { getPaginatedServicesList } from "src/apis/AdminPages/SuperAdminPages/ServicePagePage/service";
import ServiceTable from "./ServiceTable";
import ServiceDeleteModal from "./ServiceDeleteModal";
import ServiceModal from "./ServiceModal";

const ServicePage = ({
  modalPropertiesState,
  pageLimitState,
  paginationDataState,
  searchExpressionState,
  modalDeletePropertiesState,
}: AdminPagesWrapperProps) => {
  const [servicesList, setServicesList] = useState([]);
  const [data, callServiceApi, isLoading] = useApiCall(
    getPaginatedServicesList
  );
  const [serviceInModal, setServiceInModal] = useState<ServiceInModal>({
    id: 0,
    name: "",
    isActive: true,
  });

  useEffect(() => {
    callServiceApi([
      searchExpressionState.searchExpression,
      pageLimitState.pageLimit,
    ]);
  }, []);

  useEffect(() => {
    const { services, paginationData } = data;
    if (services) {
      setServicesList(services);
      paginationDataState.setPaginationData(paginationData);
    }
  }, [data]);

  const recallServiceApi = () => {
    callServiceApi([
      searchExpressionState.searchExpression,
      pageLimitState.pageLimit,
    ]);
  };

  const onPageChanged = (page: number): void => {
    callServiceApi([
      searchExpressionState.searchExpression,
      pageLimitState.pageLimit,
      page,
    ]);
  };

  return (
    <main className={cn(classes.container)}>
      <NotificationContainer />
      <div className={cn(classes.containerHeader)}>
        <div className={cn(classes.filters)}>
          <Form.Group>
            <Form.Control
              data-testid="page-limit-selector"
              as="select"
              value={pageLimitState.pageLimit}
              onChange={(event: React.ChangeEvent<HTMLSelectElement>) =>
                pageLimitState.setPageLimit(Number(event.target.value))
              }
              custom
            >
              {getPageOptions()}
            </Form.Control>
          </Form.Group>
          <Form.Group>
            <Form.Control
              data-testid="search-selector"
              type="text"
              placeholder="Поиск по имени"
              value={searchExpressionState.searchExpression}
              onChange={(event: React.ChangeEvent<HTMLSelectElement>) =>
                searchExpressionState.setSearchExpression(event.target.value)
              }
            />
          </Form.Group>
          <Button
            data-testid="filter"
            variant="primary"
            onClick={() => {
              callServiceApi([
                searchExpressionState.searchExpression,
                pageLimitState.pageLimit,
              ]);
            }}
          >
            <Typography color="white" variant="span">
              Фильтр
            </Typography>
          </Button>
          <Button
            data-testid="add-service"
            variant="primary"
            onClick={() => {
              modalPropertiesState.setModalProperties({
                action: MODAL_TYPE.CREATE.NAME,
                isModalShown: true,
              });
              setServiceInModal({
                name: "",
                id: 0,
                isActive: true,
              });
            }}
          >
            <Typography color="white" variant="span">
              Добавить
            </Typography>
          </Button>
        </div>
      </div>
      {isLoading ? (
        <span aria-label="Loading" />
      ) : (
        <ServiceTable
          setModalProperties={modalPropertiesState.setModalProperties}
          setServiceInModal={setServiceInModal}
          setIsDeleteModalShown={
            modalDeletePropertiesState.setIsDeleteModalShown
          }
          servicesList={servicesList}
        />
      )}
      <ServiceDeleteModal
        modalDeletePropertiesState={modalDeletePropertiesState}
        id={serviceInModal.id}
        recallServiceApi={recallServiceApi}
      />
      <ServiceModal
        modalPropertiesState={modalPropertiesState}
        serviceInModalState={{
          serviceInModal: serviceInModal,
          setServiceInModal: setServiceInModal,
        }}
        recallServiceApi={recallServiceApi}
      />
      {paginationDataState.paginationData !== undefined &&
        paginationDataState.paginationData.totalPages != 0 && (
          <Pagination
            totalPages={paginationDataState.paginationData.totalPages}
            totalRecords={paginationDataState.paginationData.totalRecords}
            onPageChanged={onPageChanged}
            currentPage={paginationDataState.paginationData.currentPage}
          />
        )}
    </main>
  );
};

export default () => AdminPagesWrapper(ServicePage);
