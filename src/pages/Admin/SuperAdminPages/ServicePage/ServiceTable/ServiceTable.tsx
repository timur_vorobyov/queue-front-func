import React from "react";
import { Table } from "react-bootstrap";
import commonClasses from "../../SuperAdminPages.css";
import cn from "clsx";
import Typography from "src/components/Typography";
import { MODAL_TYPE } from "../../consts";
import Delete from "../../images/Delete";
import Edit from "../../images/Edit";
import classes from "./ServiceTable.css";
import { ModalProperties } from "../../types";
import {
  Service,
  ServiceInModal,
} from "src/types/Admin/SuperAdmin/Service/service";

interface ServiceTableProps {
  setModalProperties: (modalProperties: ModalProperties) => void;
  setServiceInModal: (serviceInModal: ServiceInModal) => void;
  setIsDeleteModalShown: (isDeleteModalShown: boolean) => void;
  servicesList: Service[];
}

const ServiceTable = ({
  setModalProperties,
  setServiceInModal,
  setIsDeleteModalShown,
  servicesList,
}: ServiceTableProps) => {
  const getServicesList = () => {
    return servicesList.map((service: Service) => {
      return (
        <tr key={service.id} className={cn(classes.tableRaw)}>
          <td>{service.name}</td>
          <td>{service.isActive ? "Активный" : "Не актвный"}</td>
          <td>
            <button
              data-testid={`remove-icon-${service.id}`}
              className={cn(commonClasses.buttonAction)}
              type="button"
              onClick={() => {
                setIsDeleteModalShown(true);
                setServiceInModal({
                  id: service.id,
                  name: "",
                  isActive: true,
                });
              }}
            >
              {Delete()}
            </button>
            <button
              data-testid={`update-icon-${service.id}`}
              className={cn(commonClasses.buttonAction)}
              type="button"
              onClick={() => {
                setModalProperties({
                  isModalShown: true,
                  action: MODAL_TYPE.UPDATE.NAME,
                });
                setServiceInModal({
                  name: service.name,
                  isActive: service.isActive,
                  id: service.id,
                });
              }}
            >
              {Edit()}
            </button>
          </td>
        </tr>
      );
    });
  };

  return (
    <Table className={cn(classes.table)} striped size="lg">
      <thead className={cn(classes.tableHeader)}>
        <tr>
          <th>
            <Typography variant="span">Название</Typography>
          </th>
          <th>
            <Typography variant="span">Статус</Typography>
          </th>
          <th>
            <Typography variant="span">Действие</Typography>
          </th>
        </tr>
      </thead>
      <tbody>{getServicesList()}</tbody>
    </Table>
  );
};

export default ServiceTable;
