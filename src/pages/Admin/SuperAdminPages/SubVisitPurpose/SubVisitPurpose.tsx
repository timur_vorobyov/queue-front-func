import React, { useEffect, useState } from "react";
import classes from "./SubVisitPurpose.css";
import cn from "clsx";
import Typography from "src/components/Typography";
import { Button, Form } from "react-bootstrap";
// @ts-ignore
import { NotificationContainer } from "react-notifications";
import SubVisitPurposeModal from "./SubVisitPurposeModal";
import { MODAL_TYPE } from "../consts";
import SubVisitPurposeTable from "./SubVisitPurposeTable";
import useApiCall from "src/utils/hooks/use-async-call";
import SubVisitPurposeDeleteModal from "./SubVisitPurposeDeleteModal";
import AdminPagesWrapper from "src/wrappers/AdminPagesWrapper";
import { AdminPagesWrapperProps } from "src/wrappers/AdminPagesWrapper/AdminPagesWrapper";
import { getPaginatedSubVisitPurposesList } from "src/apis/AdminPages/SuperAdminPages/SubVisitPurposePage/subVisitPurpose";
import { SubVisitPurposeInModal } from "src/types/Admin/SuperAdmin/SubVisitPurpose/subVisitPurpose";
import { getPageOptions } from "../utils";
import Pagination from "src/components/Pagination/Pagination";

const SubVisitPurpose = ({
  modalPropertiesState,
  pageLimitState,
  paginationDataState,
  searchExpressionState,
  modalDeletePropertiesState,
}: AdminPagesWrapperProps) => {
  const [subVisitPurposeInModal, setSubVisitPurposeInModal] = useState<
    SubVisitPurposeInModal
  >({
    name: "",
    visitPurposeId: 0,
    id: 0,
  });

  const [subVisitPurposesList, setSubVisitPurposesList] = useState([]);
  const [data, callSubVisitPurposeApi] = useApiCall(
    getPaginatedSubVisitPurposesList
  );

  useEffect(() => {
    callSubVisitPurposeApi([
      searchExpressionState.searchExpression,
      pageLimitState.pageLimit,
    ]);
  }, []);

  useEffect(() => {
    const { subVisitPurposes, paginationData } = data;
    if (subVisitPurposes) {
      setSubVisitPurposesList(subVisitPurposes);
      paginationDataState.setPaginationData(paginationData);
    }
  }, [data]);

  const recallSubVisitPurposeApi = () => {
    callSubVisitPurposeApi([
      searchExpressionState.searchExpression,
      pageLimitState.pageLimit,
    ]);
  };

  const onPageChanged = (page: number): void => {
    callSubVisitPurposeApi([
      searchExpressionState.searchExpression,
      pageLimitState.pageLimit,
      page,
    ]);
  };

  return (
    <main className={cn(classes.container)}>
      <NotificationContainer />
      <div className={cn(classes.containerHeader)}>
        <div className={cn(classes.filters)}>
          <Form.Group>
            <Form.Control
              as="select"
              value={pageLimitState.pageLimit}
              onChange={(event: React.ChangeEvent<HTMLSelectElement>) =>
                pageLimitState.setPageLimit(Number(event.target.value))
              }
              custom
            >
              {getPageOptions()}
            </Form.Control>
          </Form.Group>
          <Form.Group>
            <Form.Control
              type="text"
              placeholder="Поиск по имени"
              value={searchExpressionState.searchExpression}
              onChange={(event: React.ChangeEvent<HTMLSelectElement>) =>
                searchExpressionState.setSearchExpression(event.target.value)
              }
            />
          </Form.Group>
          <Button
            variant="primary"
            onClick={() => {
              callSubVisitPurposeApi([
                searchExpressionState.searchExpression,
                pageLimitState.pageLimit,
              ]);
            }}
          >
            <Typography color="white" variant="span">
              Фильтр
            </Typography>
          </Button>
          <Button
            variant="primary"
            onClick={() => {
              modalPropertiesState.setModalProperties({
                ...modalPropertiesState.modalProperties,
                action: MODAL_TYPE.CREATE.NAME,
                isModalShown: true,
              });
              setSubVisitPurposeInModal({
                name: "",
                visitPurposeId: 0,
                id: 0,
              });
            }}
          >
            <Typography color="white" variant="span">
              Добавить
            </Typography>
          </Button>
        </div>
      </div>
      <SubVisitPurposeTable
        setModalProperties={modalPropertiesState.setModalProperties}
        setSubVisitPurposeInModal={setSubVisitPurposeInModal}
        setIsDeleteModalShown={modalDeletePropertiesState.setIsDeleteModalShown}
        subVisitPurposesList={subVisitPurposesList}
      />
      <SubVisitPurposeModal
        modalPropertiesState={modalPropertiesState}
        subVisitPurposeInModalState={{
          subVisitPurposeInModal: subVisitPurposeInModal,
          setSubVisitPurposeInModal: setSubVisitPurposeInModal,
        }}
        recallSubVisitPurposeApi={recallSubVisitPurposeApi}
      />
      <SubVisitPurposeDeleteModal
        modalDeletePropertiesState={modalDeletePropertiesState}
        id={subVisitPurposeInModal.id}
        recallSubVisitPurposeApi={recallSubVisitPurposeApi}
      />
      {paginationDataState.paginationData !== undefined &&
        paginationDataState.paginationData.totalPages != 0 && (
          <Pagination
            totalPages={paginationDataState.paginationData.totalPages}
            totalRecords={paginationDataState.paginationData.totalRecords}
            onPageChanged={onPageChanged}
            currentPage={paginationDataState.paginationData.currentPage}
          />
        )}
    </main>
  );
};

export default () => AdminPagesWrapper(SubVisitPurpose);
