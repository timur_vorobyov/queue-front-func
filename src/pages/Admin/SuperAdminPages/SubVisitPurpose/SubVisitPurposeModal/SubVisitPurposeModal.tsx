import React, { useEffect, useState } from "react";
import { Button, Form, Modal } from "react-bootstrap";
import commonClasses from "../../SuperAdminPages.css";
import cn from "clsx";
import Typography from "src/components/Typography";
import { MODAL_TYPE } from "../../consts";
import {
  storeSubVisitPurpose,
  updateSubVisitPurpose,
} from "src/apis/AdminPages/SuperAdminPages/SubVisitPurposePage/subVisitPurpose";
import { createNotification } from "src/helpers/crud-notifications";
import { ModalPropertiesState } from "../../types";
import { SubVisitPurposeInModal } from "src/types/Admin/SuperAdmin/SubVisitPurpose/subVisitPurpose";
import useApiCall from "src/utils/hooks/use-async-call";
import { getVisitPurposesList } from "src/apis/AdminPages/SuperAdminPages/visitPurpose";
import { VisitPurpose } from "src/types/Admin/visitPurpose";

interface SubVisitPurposeModalProps {
  modalPropertiesState: ModalPropertiesState;
  subVisitPurposeInModalState: {
    subVisitPurposeInModal: SubVisitPurposeInModal;
    setSubVisitPurposeInModal: (
      visitPurposeInModal: SubVisitPurposeInModal
    ) => void;
  };
  recallSubVisitPurposeApi: () => void;
}

const SubVisitPurposeModal = ({
  modalPropertiesState,
  subVisitPurposeInModalState,
  recallSubVisitPurposeApi,
}: SubVisitPurposeModalProps) => {
  const [visitPurposes, callVisitPurposeApi] = useApiCall(getVisitPurposesList);
  const [visitPurposesList, setVisitPurposesList] = useState([]);

  useEffect(() => {
    callVisitPurposeApi();
  }, []);

  useEffect(() => {
    if (visitPurposes) {
      setVisitPurposesList(visitPurposes);
    }
  }, [visitPurposes]);

  const handleCreateSubVisitPurposeClick = async () => {
    const response = await storeSubVisitPurpose(
      subVisitPurposeInModalState.subVisitPurposeInModal
    );

    if (response == "SUB_VISIT_PURPOSE_IS_SAVED") {
      createNotification("success", "Подцель визита сохранена");
      recallSubVisitPurposeApi();
      modalPropertiesState.setModalProperties({
        ...modalPropertiesState.modalProperties,
        isModalShown: false,
      });
    }
  };

  const handleUpdateSubVisitPurposeClick = async () => {
    const response = await updateSubVisitPurpose(
      subVisitPurposeInModalState.subVisitPurposeInModal
    );

    if (response == "SUB_VISIT_PURPOSE_IS_UPDATED") {
      createNotification("success", "Подцель визита обновлена");
      recallSubVisitPurposeApi();
      modalPropertiesState.setModalProperties({
        ...modalPropertiesState.modalProperties,
        isModalShown: false,
      });
    }
  };

  const getVisitPurposeOptions = () => {
    return visitPurposesList.map((visitPurpose: VisitPurpose) => {
      return (
        <option key={visitPurpose.id} value={visitPurpose.id}>
          {visitPurpose.name}
        </option>
      );
    });
  };

  return (
    <Modal
      className={cn(commonClasses.modalDiolog)}
      show={modalPropertiesState.modalProperties.isModalShown}
      onHide={() =>
        modalPropertiesState.setModalProperties({
          ...modalPropertiesState.modalProperties,
          isModalShown: false,
        })
      }
    >
      <Modal.Header closeButton>
        <Modal.Title>
          <Typography variant="span">
            {modalPropertiesState.modalProperties.action ==
            MODAL_TYPE.CREATE.NAME
              ? "Добавить подцель"
              : "Изменить подцель"}
          </Typography>
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div>
          <Form.Group>
            <Typography variant="label">Цели визита</Typography>
            <Form.Control
              as="select"
              value={
                subVisitPurposeInModalState.subVisitPurposeInModal
                  .visitPurposeId
              }
              onChange={(event: React.ChangeEvent<HTMLSelectElement>) =>
                subVisitPurposeInModalState.setSubVisitPurposeInModal({
                  ...subVisitPurposeInModalState.subVisitPurposeInModal,
                  visitPurposeId: Number(event.target.value),
                })
              }
              custom
            >
              <option value="0">Все цели визита</option>
              {getVisitPurposeOptions()}
            </Form.Control>
          </Form.Group>
          <Form.Group>
            <Typography variant="label">Имя подцели</Typography>
            <Form.Control
              type="text"
              value={subVisitPurposeInModalState.subVisitPurposeInModal.name}
              onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
                subVisitPurposeInModalState.setSubVisitPurposeInModal({
                  ...subVisitPurposeInModalState.subVisitPurposeInModal,
                  name: event.target.value,
                })
              }
            />
          </Form.Group>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <Button
          variant="secondary"
          onClick={
            modalPropertiesState.modalProperties.action ==
            MODAL_TYPE.CREATE.NAME
              ? () => handleCreateSubVisitPurposeClick()
              : () => handleUpdateSubVisitPurposeClick()
          }
        >
          <Typography color="white" variant="span">
            {modalPropertiesState.modalProperties.action ==
            MODAL_TYPE.CREATE.NAME
              ? MODAL_TYPE.CREATE.ACTION
              : MODAL_TYPE.UPDATE.ACTION}
          </Typography>
        </Button>
        <Button
          variant="primary"
          onClick={() =>
            modalPropertiesState.setModalProperties({
              ...modalPropertiesState.modalProperties,
              isModalShown: false,
            })
          }
        >
          <Typography color="white" variant="span">
            Закрыть
          </Typography>
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default SubVisitPurposeModal;
