import React from "react";
import { Table } from "react-bootstrap";
import commonClasses from "../../SuperAdminPages.css";
import cn from "clsx";
import Typography from "src/components/Typography";
import { MODAL_TYPE } from "../../consts";
import Delete from "../../images/Delete";
import Edit from "../../images/Edit";
import classes from "./SubVisitPurposeTable.css";
import {
  SubVisitPurpose,
  SubVisitPurposeInModal,
} from "src/types/Admin/SuperAdmin/SubVisitPurpose/subVisitPurpose";
import { ModalProperties } from "../../types";

interface SubVisitPurposeTableProps {
  setModalProperties: (modalProperties: ModalProperties) => void;
  setSubVisitPurposeInModal: (
    subVisitPurposeInModal: SubVisitPurposeInModal
  ) => void;
  setIsDeleteModalShown: (isDeleteModalShown: boolean) => void;
  subVisitPurposesList: SubVisitPurpose[];
}

const SubVisitPurposeTable = ({
  setModalProperties,
  setSubVisitPurposeInModal,
  setIsDeleteModalShown,
  subVisitPurposesList,
}: SubVisitPurposeTableProps) => {
  const getSubVisitPurposesList = () => {
    return subVisitPurposesList.map((subVisitPurpose: SubVisitPurpose) => {
      return (
        <tr key={subVisitPurpose.id} className={cn(classes.tableRaw)}>
          <td>{subVisitPurpose.name}</td>
          <td>{subVisitPurpose.visitPurpose.name}</td>
          <td>
            <button
              className={cn(commonClasses.buttonAction)}
              type="button"
              onClick={() => {
                setIsDeleteModalShown(true);
                setSubVisitPurposeInModal({
                  id: subVisitPurpose.id,
                  name: "",
                  visitPurposeId: 0,
                });
              }}
            >
              {Delete()}
            </button>
            <button
              className={cn(commonClasses.buttonAction)}
              type="button"
              onClick={() => {
                setModalProperties({
                  isModalShown: true,
                  action: MODAL_TYPE.UPDATE.NAME,
                });
                setSubVisitPurposeInModal({
                  name: subVisitPurpose.name,
                  visitPurposeId: subVisitPurpose.visitPurpose.id,
                  id: subVisitPurpose.id,
                });
              }}
            >
              {Edit()}
            </button>
          </td>
        </tr>
      );
    });
  };

  return (
    <Table className={cn(classes.table)} striped size="lg">
      <thead className={cn(classes.tableHeader)}>
        <tr>
          <th>
            <Typography variant="span">Название</Typography>
          </th>
          <th>
            <Typography variant="span">Название цели визита</Typography>
          </th>
          <th>
            <Typography variant="span">Действие</Typography>
          </th>
        </tr>
      </thead>
      <tbody>{getSubVisitPurposesList()}</tbody>
    </Table>
  );
};

export default SubVisitPurposeTable;
