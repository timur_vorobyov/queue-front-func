import React from "react";
import { Button, Modal } from "react-bootstrap";
import commonClasses from "../../SuperAdminPages.css";
import cn from "clsx";
import Typography from "src/components/Typography";
import { createNotification } from "src/helpers/crud-notifications";
import { deleteTown } from "src/apis/AdminPages/SuperAdminPages/TownPage/town";
import { ModalDeletePropertiesState } from "src/pages/Admin/types";

interface TownDeleteModalProps {
  modalDeletePropertiesState: ModalDeletePropertiesState;
  id: number;
  recallTownApi: () => void;
}

const TownDeleteModal = ({
  modalDeletePropertiesState,
  id,
  recallTownApi,
}: TownDeleteModalProps) => {
  const handleConfirmationDeleteVisitPurposeModalClick = async () => {
    const response = await deleteTown(id);

    if (response == "TOWN_IS_REMOVED") {
      createNotification("success", "Город удален");
      modalDeletePropertiesState.setIsDeleteModalShown(false);
      recallTownApi();
    }
  };
  return (
    <Modal
      className={cn(commonClasses.modalDiolog)}
      show={modalDeletePropertiesState.isDeleteModalShown}
      onHide={() => modalDeletePropertiesState.setIsDeleteModalShown(false)}
    >
      <Modal.Header closeButton>
        <Modal.Title>
          <Typography variant="span">
            Вы уверены что хотите удалить выбранный пункт?
          </Typography>
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className={cn(commonClasses.modalDeleteActionButtons)}>
        <Button
          className={cn(commonClasses.modalDeleteActionButton)}
          variant="secondary"
          onClick={() => handleConfirmationDeleteVisitPurposeModalClick()}
        >
          <Typography color="white" variant="span">
            Да
          </Typography>
        </Button>
        <Button
          className={cn(commonClasses.modalDeleteActionButton)}
          variant="primary"
          onClick={() =>
            modalDeletePropertiesState.setIsDeleteModalShown(false)
          }
        >
          <Typography color="white" variant="span">
            Нет
          </Typography>
        </Button>
      </Modal.Body>
    </Modal>
  );
};

export default TownDeleteModal;
