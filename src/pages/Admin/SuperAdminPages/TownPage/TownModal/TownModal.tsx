import React from "react";
import commonClasses from "../../SuperAdminPages.css";
import cn from "clsx";
import Typography from "src/components/Typography";
import { MODAL_TYPE } from "../../consts";
import { createNotification } from "src/helpers/crud-notifications";
import { Button, Form, Modal } from "react-bootstrap";
import { ModalPropertiesState } from "../../types";
import { TownInModal } from "src/types/Admin/SuperAdmin/Town/town";
import {
  storeTown,
  updateTown,
} from "src/apis/AdminPages/SuperAdminPages/TownPage/town";

interface TownModalProps {
  modalPropertiesState: ModalPropertiesState;
  townInModalState: {
    townInModal: TownInModal;
    setTownInModal: (townInModal: TownInModal) => void;
  };
  recallTownApi: () => void;
}

const TownModal = ({
  modalPropertiesState,
  townInModalState,
  recallTownApi,
}: TownModalProps) => {
  const handleCreateBranchOfficeClick = async () => {
    const response = await storeTown(townInModalState.townInModal);

    if (response == "TOWN_IS_SAVED") {
      createNotification("success", "Город сохранен");
      recallTownApi();
      modalPropertiesState.setModalProperties({
        ...modalPropertiesState.modalProperties,
        isModalShown: false,
      });
    }
  };

  const handleUpdateBranchOfficeClick = async () => {
    const response = await updateTown(townInModalState.townInModal);

    if (response == "TOWN_IS_UPDATED") {
      createNotification("success", "Город обновлен");
      recallTownApi();
      modalPropertiesState.setModalProperties({
        ...modalPropertiesState.modalProperties,
        isModalShown: false,
      });
    }
  };

  return (
    <Modal
      className={cn(commonClasses.modalDiolog)}
      show={modalPropertiesState.modalProperties.isModalShown}
      onHide={() =>
        modalPropertiesState.setModalProperties({
          ...modalPropertiesState.modalProperties,
          isModalShown: false,
        })
      }
    >
      <Modal.Header closeButton>
        <Modal.Title>
          <Typography variant="span">
            {modalPropertiesState.modalProperties.action ==
            MODAL_TYPE.CREATE.NAME
              ? "Добавить цель"
              : "Изменить цель"}
          </Typography>
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div>
          <Form.Group>
            <Typography variant="label">Название</Typography>
            <Form.Control
              type="text"
              value={townInModalState.townInModal.name}
              onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
                townInModalState.setTownInModal({
                  ...townInModalState.townInModal,
                  name: event.target.value,
                })
              }
            />
          </Form.Group>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <Button
          variant="secondary"
          onClick={
            modalPropertiesState.modalProperties.action ==
            MODAL_TYPE.CREATE.NAME
              ? () => handleCreateBranchOfficeClick()
              : () => handleUpdateBranchOfficeClick()
          }
        >
          <Typography color="white" variant="span">
            {modalPropertiesState.modalProperties.action ==
            MODAL_TYPE.CREATE.NAME
              ? MODAL_TYPE.CREATE.ACTION
              : MODAL_TYPE.UPDATE.ACTION}
          </Typography>
        </Button>
        <Button
          variant="primary"
          onClick={() =>
            modalPropertiesState.setModalProperties({
              ...modalPropertiesState.modalProperties,
              isModalShown: false,
            })
          }
        >
          <Typography color="white" variant="span">
            Закрыть
          </Typography>
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default TownModal;
