import React, { useEffect, useState } from "react";
import classes from "./TownPage.css";
import cn from "clsx";
// @ts-ignore
import { NotificationContainer } from "react-notifications";
import { MODAL_TYPE } from "../consts";
import useApiCall from "src/utils/hooks/use-async-call";
import Pagination from "src/components/Pagination/Pagination";
import Typography from "src/components/Typography";
import AdminPagesWrapper from "src/wrappers/AdminPagesWrapper";
import { AdminPagesWrapperProps } from "src/wrappers/AdminPagesWrapper/AdminPagesWrapper";
import { getPageOptions } from "../utils";
import { Town } from "src/types/Admin/SuperAdmin/BranchOffice/town";
import { Button, Form } from "react-bootstrap";
import { TownInModal } from "src/types/Admin/SuperAdmin/Town/town";
import { getPaginatedTownsList } from "src/apis/AdminPages/SuperAdminPages/TownPage/town";
import TownTable from "./TownTable";
import TownModal from "./TownModal";
import TownDeleteModal from "./TownDeleteModal";

const TownPage = ({
  modalPropertiesState,
  pageLimitState,
  paginationDataState,
  searchExpressionState,
  modalDeletePropertiesState,
}: AdminPagesWrapperProps) => {
  const [data, callTownApi] = useApiCall(getPaginatedTownsList);
  const [townsList, setTownsList] = useState([]);
  const [townInModal, setTownInModal] = useState<TownInModal>({
    id: 0,
    name: "",
  });

  useEffect(() => {
    callTownApi([
      searchExpressionState.searchExpression,
      pageLimitState.pageLimit,
    ]);
  }, []);

  useEffect(() => {
    const { towns, paginationData } = data;
    if (towns) {
      setTownsList(towns);
      paginationDataState.setPaginationData(paginationData);
    }
  }, [data]);

  const onPageChanged = (page: number): void => {
    callTownApi([
      searchExpressionState.searchExpression,
      pageLimitState.pageLimit,
      page,
    ]);
  };

  const recallTownApi = () => {
    callTownApi([
      searchExpressionState.searchExpression,
      pageLimitState.pageLimit,
    ]);
  };

  return (
    <main className={cn(classes.container)}>
      <NotificationContainer />
      <div className={cn(classes.containerHeader)}>
        <div className={cn(classes.filters)}>
          <Form.Group>
            <Form.Control
              as="select"
              value={pageLimitState.pageLimit}
              onChange={(event: React.ChangeEvent<HTMLSelectElement>) =>
                pageLimitState.setPageLimit(Number(event.target.value))
              }
              custom
            >
              {getPageOptions()}
            </Form.Control>
          </Form.Group>
          <Form.Group>
            <Form.Control
              type="text"
              placeholder="Поиск по имени"
              value={searchExpressionState.searchExpression}
              onChange={(event: React.ChangeEvent<HTMLSelectElement>) =>
                searchExpressionState.setSearchExpression(event.target.value)
              }
            />
          </Form.Group>
          <Button
            variant="primary"
            onClick={() =>
              callTownApi([
                searchExpressionState.searchExpression,
                pageLimitState.pageLimit,
              ])
            }
          >
            <Typography color="white" variant="span">
              Фильтр
            </Typography>
          </Button>
          <Button
            variant="primary"
            onClick={() => {
              modalPropertiesState.setModalProperties({
                action: MODAL_TYPE.CREATE.NAME,
                isModalShown: true,
              });
              setTownInModal({
                id: 0,
                name: "",
              });
            }}
          >
            <Typography color="white" variant="span">
              Добавить
            </Typography>
          </Button>
        </div>
      </div>
      <TownTable
        setModalProperties={modalPropertiesState.setModalProperties}
        setTownInModal={setTownInModal}
        setIsDeleteModalShown={modalDeletePropertiesState.setIsDeleteModalShown}
        townsList={townsList}
      />
      <TownModal
        modalPropertiesState={modalPropertiesState}
        townInModalState={{
          townInModal: townInModal,
          setTownInModal: setTownInModal,
        }}
        recallTownApi={recallTownApi}
      />
      <TownDeleteModal
        modalDeletePropertiesState={modalDeletePropertiesState}
        id={townInModal.id}
        recallTownApi={recallTownApi}
      />
      {paginationDataState.paginationData !== undefined &&
        paginationDataState.paginationData.totalPages != 0 && (
          <Pagination
            totalPages={paginationDataState.paginationData.totalPages}
            totalRecords={paginationDataState.paginationData.totalRecords}
            onPageChanged={onPageChanged}
            currentPage={paginationDataState.paginationData.currentPage}
          />
        )}
    </main>
  );
};

export default () => AdminPagesWrapper(TownPage);
