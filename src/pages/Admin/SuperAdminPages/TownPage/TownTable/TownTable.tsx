import React from "react";
import {
  BranchOffice,
  BranchOfficeInModal,
} from "src/types/Admin/SuperAdmin/BranchOffice/branchOffice";
import { ModalProperties } from "../../types";
import { Table } from "react-bootstrap";
import commonClasses from "../../SuperAdminPages.css";
import cn from "clsx";
import Typography from "src/components/Typography";
import { MODAL_TYPE } from "../../consts";
import Delete from "../../images/Delete";
import Edit from "../../images/Edit";
import classes from "./TownTable.css";
import { Town, TownInModal } from "src/types/Admin/SuperAdmin/Town/town";

interface TownTableProps {
  setModalProperties: (modalProperties: ModalProperties) => void;
  setTownInModal: (townInModal: TownInModal) => void;
  setIsDeleteModalShown: (isDeleteModalShown: boolean) => void;
  townsList: Town[];
}

const TownTable = ({
  setModalProperties,
  setTownInModal,
  setIsDeleteModalShown,
  townsList,
}: TownTableProps) => {
  const getTownsList = () => {
    return townsList.map((town: Town) => {
      return (
        <tr key={town.id} className={cn(classes.tableRaw)}>
          <td>{town.name}</td>
          <td>
            <button
              className={cn(commonClasses.buttonAction)}
              type="button"
              onClick={() => {
                setIsDeleteModalShown(true);
                setTownInModal({
                  id: town.id,
                  name: "",
                });
              }}
            >
              {Delete()}
            </button>
            <button
              className={cn(commonClasses.buttonAction)}
              type="button"
              onClick={() => {
                setModalProperties({
                  isModalShown: true,
                  action: MODAL_TYPE.UPDATE.NAME,
                });
                setTownInModal({
                  id: town.id,
                  name: town.name,
                });
              }}
            >
              {Edit()}
            </button>
          </td>
        </tr>
      );
    });
  };

  return (
    <Table className={cn(classes.table)}>
      <thead className={cn(classes.tableHeader)}>
        <tr>
          <th>
            <Typography variant="span">Название</Typography>
          </th>
          <th>
            <Typography variant="span">Действие</Typography>
          </th>
        </tr>
      </thead>
      <tbody>{getTownsList()}</tbody>
    </Table>
  );
};

export default TownTable;
