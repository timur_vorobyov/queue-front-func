import React, { useEffect, useState } from "react";
import commonClasses from "../../SuperAdminPages.css";
import cn from "clsx";
import Typography from "src/components/Typography";
import { MODAL_TYPE } from "../../consts";
import { createNotification } from "src/helpers/crud-notifications";
import { Button, Form, Modal } from "react-bootstrap";
import { ModalPropertiesState } from "../../types";
import {
  storeUser,
  updateUser,
} from "src/apis/AdminPages/SuperAdminPages/UserPage/user";
// @ts-ignore
import Select from "react-select";
import { UserInModal } from "src/types/Admin/SuperAdmin/User/user";
import classes from "./UserModal.css";
import { VisitPurpose } from "src/types/Admin/visitPurpose";
import useApiCall from "src/utils/hooks/use-async-call";
import { getBranchOfficesList } from "src/apis/AdminPages/SuperAdminPages/branchOffice";
import { Town } from "src/types/Admin/SuperAdmin/town";
import { BranchOffice } from "src/types/Admin/SuperAdmin/branchOffice";
import { getVisitPurposesList } from "src/apis/AdminPages/SuperAdminPages/visitPurpose";
import { getRolesList } from "src/apis/AdminPages/SuperAdminPages/role";
import { Role } from "src/types/Admin/SuperAdmin/User/role";

interface UserModalProps {
  modalPropertiesState: ModalPropertiesState;
  userInModalState: {
    userInModal: UserInModal;
    setUserInModal: (userInModalState: UserInModal) => void;
  };
  recallUserApi: () => void;
  townsList: Town[];
}

const UserModal = ({
  modalPropertiesState,
  userInModalState,
  recallUserApi,
  townsList,
}: UserModalProps) => {
  const [branchOfficesList, setBranchOfficesList] = useState([]);
  const [branchOffices, callBranchOfficeApi] = useApiCall(getBranchOfficesList);
  const [visitPurposes, callVisitPurposeApi] = useApiCall(getVisitPurposesList);
  const [visitPurposesList, setVisitPurposesList] = useState([]);
  const [roles, callRoleApi] = useApiCall(getRolesList);
  const [rolesList, setRolesList] = useState([]);

  useEffect(() => {
    callVisitPurposeApi();
    callRoleApi();
    callBranchOfficeApi([userInModalState.userInModal.townId]);
  }, []);

  useEffect(() => {
    if (visitPurposes) {
      setVisitPurposesList(visitPurposes);
    }
  }, [visitPurposes]);

  useEffect(() => {
    if (branchOffices) {
      setBranchOfficesList(branchOffices);
    }
  }, [branchOffices]);

  useEffect(() => {
    if (roles) {
      setRolesList(roles);
    }
  }, [roles]);

  const getBranchOfficeOptions = (): JSX.Element[] =>
    branchOfficesList.map((branchOffice: BranchOffice) => {
      return (
        <option key={branchOffice.id} value={branchOffice.id}>
          {branchOffice.name}
        </option>
      );
    });

  const getRoleOptions = (): JSX.Element[] =>
    rolesList.map((role: Role) => {
      return (
        <option key={role.id} value={role.id}>
          {role.name}
        </option>
      );
    });

  const getTownOptions = (): JSX.Element[] =>
    townsList.map((town: Town) => {
      return (
        <option key={town.id} value={town.id}>
          {town.name}
        </option>
      );
    });

  const handleCreateUserClick = async () => {
    const response = await storeUser(userInModalState.userInModal);

    if (response == "EMAIL_ERROR") {
      createNotification("error", "Пользователь с таким email уже существует");
    }

    if (response == "USER_IS_SAVED") {
      createNotification("success", "Юзер сохранен");
      recallUserApi();
      modalPropertiesState.setModalProperties({
        ...modalPropertiesState.modalProperties,
        isModalShown: false,
      });
    }
  };

  const handleUpdateUserClick = async () => {
    const response = await updateUser(userInModalState.userInModal);

    if (response == "EMAIL_ERROR") {
      createNotification("error", "Пользователь с таким email уже существует");
    }
    if (response == "USER_IS_UPDATED") {
      createNotification("success", "Юзер обновлен");
      recallUserApi();
      modalPropertiesState.setModalProperties({
        ...modalPropertiesState.modalProperties,
        isModalShown: false,
      });
    }
  };

  const getVisitPurposeOptions = () => {
    const options = visitPurposesList.map((visitPurpose: VisitPurpose) => {
      return { value: visitPurpose.id, label: visitPurpose.name };
    });
    return options;
  };

  const setTownState = async (id: number) => {
    userInModalState.setUserInModal({
      ...userInModalState.userInModal,
      townId: id,
    });
    callBranchOfficeApi([id]);
  };

  const handleVistiPurposesIdsChange = (
    selectedVisitPurposes: [{ value: number }]
  ): void => {
    const options = selectedVisitPurposes;
    const selectedOptions = [];

    for (let i = 0; i < options.length; i++) {
      selectedOptions.push(options[i].value);
    }

    userInModalState.setUserInModal({
      ...userInModalState.userInModal,
      visitPurposesIds: selectedOptions,
    });
  };

  return (
    <Modal
      className={cn(commonClasses.modalDiolog)}
      show={modalPropertiesState.modalProperties.isModalShown}
      onHide={() =>
        modalPropertiesState.setModalProperties({
          ...modalPropertiesState.modalProperties,
          isModalShown: false,
        })
      }
    >
      <Modal.Header closeButton>
        <Modal.Title>
          <Typography variant="span">
            {modalPropertiesState.modalProperties.action ==
            MODAL_TYPE.CREATE.NAME
              ? "Добавить разрешение"
              : "Изменить разрешение"}
          </Typography>
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div>
          <Form.Group>
            <Typography variant="label">Город</Typography>
            <Form.Control
              as="select"
              value={userInModalState.userInModal.townId}
              onChange={(event: React.ChangeEvent<HTMLSelectElement>) =>
                setTownState(Number(event.target.value))
              }
              custom
            >
              <option value="0">Все города</option>
              {getTownOptions()}
            </Form.Control>
          </Form.Group>
          <Form.Group>
            <Typography variant="label">Филиал</Typography>
            <Form.Control
              as="select"
              value={userInModalState.userInModal.branchOfficeId}
              onChange={(event: React.ChangeEvent<HTMLSelectElement>) =>
                userInModalState.setUserInModal({
                  ...userInModalState.userInModal,
                  branchOfficeId: Number(event.target.value),
                })
              }
              custom
            >
              <option value="0">Все филиалы</option>
              {getBranchOfficeOptions()}
            </Form.Control>
          </Form.Group>
          <Form.Group>
            <Typography variant="label">Имя пользователя</Typography>
            <Form.Control
              type="text"
              value={userInModalState.userInModal.firstName}
              onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
                userInModalState.setUserInModal({
                  ...userInModalState.userInModal,
                  firstName: event.target.value,
                })
              }
            />
          </Form.Group>
          <Form.Group>
            <Typography variant="label">Фамилия пользователя</Typography>
            <Form.Control
              type="text"
              value={userInModalState.userInModal.lastName}
              onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
                userInModalState.setUserInModal({
                  ...userInModalState.userInModal,
                  lastName: event.target.value,
                })
              }
            />
          </Form.Group>
          <Form.Group>
            <Typography variant="label">Email</Typography>
            <Form.Control
              type="email"
              value={userInModalState.userInModal.email}
              onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
                userInModalState.setUserInModal({
                  ...userInModalState.userInModal,
                  email: event.target.value,
                })
              }
            />
          </Form.Group>
          <Form.Group>
            <Typography variant="label">Пароль</Typography>
            <Form.Control
              type="text"
              value={userInModalState.userInModal.password}
              onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
                userInModalState.setUserInModal({
                  ...userInModalState.userInModal,
                  password: event.target.value,
                })
              }
            />
          </Form.Group>
          <Form.Group>
            <Typography variant="label">Цели визита</Typography>
            <Select
              className={cn(classes.select)}
              defaultValue={visitPurposesList.map(
                (visitPurpose: VisitPurpose) => {
                  if (
                    userInModalState.userInModal.visitPurposesIds.includes(
                      visitPurpose.id
                    )
                  ) {
                    return { label: visitPurpose.name, value: visitPurpose.id };
                  }
                }
              )}
              options={getVisitPurposeOptions()}
              isMulti
              onChange={handleVistiPurposesIdsChange}
            />
          </Form.Group>
          <Form.Group>
            <Typography variant="label">Роль</Typography>
            <Form.Control
              as="select"
              value={userInModalState.userInModal.roleId}
              onChange={(event: React.ChangeEvent<HTMLSelectElement>) =>
                userInModalState.setUserInModal({
                  ...userInModalState.userInModal,
                  roleId: Number(event.target.value),
                })
              }
              custom
            >
              <option value="0">Пожалуйста выберите роль юзера</option>
              {getRoleOptions()}
            </Form.Control>
          </Form.Group>
          <Form.Group>
            <Form.Check
              type="switch"
              id="custom-switch"
              label="Активный?"
              checked={userInModalState.userInModal.isActive}
              onChange={() =>
                userInModalState.setUserInModal({
                  ...userInModalState.userInModal,
                  isActive: !userInModalState.userInModal.isActive,
                })
              }
            />
          </Form.Group>
          <Form.Group>
            <Form.Check
              type="switch"
              id="is-allowed-have-same-desk-number"
              label="Позволить создавать одинаковые столы?"
              checked={userInModalState.userInModal.isAllowedHaveSameDeskNumber}
              onChange={() =>
                userInModalState.setUserInModal({
                  ...userInModalState.userInModal,
                  isAllowedHaveSameDeskNumber: !userInModalState.userInModal
                    .isAllowedHaveSameDeskNumber,
                })
              }
            />
          </Form.Group>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <Button
          variant="secondary"
          onClick={
            modalPropertiesState.modalProperties.action ==
            MODAL_TYPE.CREATE.NAME
              ? () => handleCreateUserClick()
              : () => handleUpdateUserClick()
          }
        >
          <Typography color="white" variant="span">
            {modalPropertiesState.modalProperties.action ==
            MODAL_TYPE.CREATE.NAME
              ? MODAL_TYPE.CREATE.ACTION
              : MODAL_TYPE.UPDATE.ACTION}
          </Typography>
        </Button>
        <Button
          variant="primary"
          onClick={() =>
            modalPropertiesState.setModalProperties({
              ...modalPropertiesState.modalProperties,
              isModalShown: false,
            })
          }
        >
          <Typography color="white" variant="span">
            Закрыть
          </Typography>
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default UserModal;
