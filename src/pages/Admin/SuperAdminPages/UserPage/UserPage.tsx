import React, { useEffect, useState } from "react";
import classes from "./UserPage.css";
import cn from "clsx";
// @ts-ignore
import { NotificationContainer } from "react-notifications";
import { MODAL_TYPE } from "../consts";
import useApiCall from "src/utils/hooks/use-async-call";
import Pagination from "src/components/Pagination/Pagination";
import { getPaginatedUsersList } from "src/apis/AdminPages/SuperAdminPages/UserPage/user";
import { Button, Form } from "react-bootstrap";
import Typography from "src/components/Typography";
import AdminPagesWrapper from "src/wrappers/AdminPagesWrapper";
import { AdminPagesWrapperProps } from "src/wrappers/AdminPagesWrapper/AdminPagesWrapper";
import { getPageOptions } from "../utils";
import { UserInModal } from "src/types/Admin/SuperAdmin/User/user";
import { getTownsList } from "src/apis/AdminPages/SuperAdminPages/town";
import { Town } from "src/types/Admin/SuperAdmin/town";
import { BranchOffice } from "src/types/Admin/SuperAdmin/branchOffice";
import { getBranchOfficesList } from "src/apis/AdminPages/SuperAdminPages/branchOffice";
import UserTable from "./UserTable";
import UserDeleteModal from "./UserDeleteModal";
import UserModal from "./UserModal";

const UserPage = ({
  modalPropertiesState,
  pageLimitState,
  paginationDataState,
  searchExpressionState,
  modalDeletePropertiesState,
}: AdminPagesWrapperProps) => {
  const [branchOfficeId, setBranchOfficeId] = useState<number>(0);
  const [townId, setTownId] = useState<number>(0);
  const [usersList, setUsersList] = useState([]);
  const [data, callUserApi] = useApiCall(getPaginatedUsersList);
  const [townsList, setTownsList] = useState([]);
  const [towns, callTownApi] = useApiCall(getTownsList);
  const [branchOfficesList, setBranchOfficesList] = useState([]);
  const [branchOffices, callBranchOfficeApi] = useApiCall(getBranchOfficesList);
  const [userInModal, setUserInModal] = useState<UserInModal>({
    id: 0,
    firstName: "",
    lastName: "",
    branchOfficeId: 0,
    visitPurposesIds: [],
    password: "",
    email: "",
    isActive: true,
    isAllowedHaveSameDeskNumber: true,
    roleId: 0,
    townId: 0,
  });

  useEffect(() => {
    callUserApi([
      branchOfficeId,
      searchExpressionState.searchExpression,
      pageLimitState.pageLimit,
    ]);
    callTownApi();
  }, []);

  useEffect(() => {
    if (towns) {
      setTownsList(towns);
    }
  }, [towns]);

  useEffect(() => {
    if (branchOffices) {
      setBranchOfficesList(branchOffices);
    }
  }, [branchOffices]);

  useEffect(() => {
    const { users, paginationData } = data;
    if (users) {
      setUsersList(users);
      paginationDataState.setPaginationData(paginationData);
    }
  }, [data]);

  const onPageChanged = (page: number): void => {
    callUserApi([
      branchOfficeId,
      searchExpressionState.searchExpression,
      pageLimitState.pageLimit,
      page,
    ]);
  };

  const getBranchOfficeOptions = (): JSX.Element[] =>
    branchOfficesList.map((branchOffice: BranchOffice, index: number) => {
      return (
        <option key={index} value={branchOffice.id}>
          {branchOffice.name}
        </option>
      );
    });

  const getTownOptions = (): JSX.Element[] =>
    townsList.map((town: Town, index: number) => {
      return (
        <option key={index} value={town.id}>
          {town.name}
        </option>
      );
    });

  const recallUserApi = () => {
    callUserApi([
      branchOfficeId,
      searchExpressionState.searchExpression,
      pageLimitState.pageLimit,
    ]);
  };

  const setTownState = async (id: number) => {
    setTownId(id);
    callBranchOfficeApi([id]);
  };

  return (
    <main className={cn(classes.container)}>
      <NotificationContainer />
      <div className={cn(classes.containerHeader)}>
        <div className={cn(classes.filters)}>
          <Form.Group>
            <Form.Control
              as="select"
              value={pageLimitState.pageLimit}
              onChange={(event: React.ChangeEvent<HTMLSelectElement>) =>
                pageLimitState.setPageLimit(Number(event.target.value))
              }
              custom
            >
              {getPageOptions()}
            </Form.Control>
          </Form.Group>
          <Form.Group>
            <Form.Control
              as="select"
              value={townId}
              onChange={(event: React.ChangeEvent<HTMLSelectElement>) =>
                setTownState(Number(event.target.value))
              }
              custom
            >
              <option value="0">Все города</option>
              {getTownOptions()}
            </Form.Control>
          </Form.Group>
          <Form.Group>
            <Form.Control
              as="select"
              value={branchOfficeId}
              onChange={(event: React.ChangeEvent<HTMLSelectElement>) =>
                setBranchOfficeId(Number(event.target.value))
              }
              custom
            >
              <option value="0">Все филиалы</option>
              {getBranchOfficeOptions()}
            </Form.Control>
          </Form.Group>
          <Form.Group>
            <Form.Control
              type="text"
              placeholder="Поиск по имени"
              value={searchExpressionState.searchExpression}
              onChange={(event: React.ChangeEvent<HTMLSelectElement>) =>
                searchExpressionState.setSearchExpression(event.target.value)
              }
            />
          </Form.Group>
          <Button
            variant="primary"
            onClick={() =>
              callUserApi([
                branchOfficeId,
                searchExpressionState.searchExpression,
                pageLimitState.pageLimit,
              ])
            }
          >
            <Typography color="white" variant="span">
              Фильтр
            </Typography>
          </Button>
          <Button
            variant="primary"
            onClick={() => {
              modalPropertiesState.setModalProperties({
                action: MODAL_TYPE.CREATE.NAME,
                isModalShown: true,
              });
              setUserInModal({
                id: 0,
                firstName: "",
                lastName: "",
                branchOfficeId: 0,
                visitPurposesIds: [],
                password: "",
                email: "",
                isActive: true,
                isAllowedHaveSameDeskNumber: true,
                roleId: 0,
                townId: 0,
              });
            }}
          >
            <Typography color="white" variant="span">
              Добавить
            </Typography>
          </Button>
        </div>
      </div>
      <UserTable
        setModalProperties={modalPropertiesState.setModalProperties}
        setUserInModal={setUserInModal}
        setIsDeleteModalShown={modalDeletePropertiesState.setIsDeleteModalShown}
        usersList={usersList}
      />
      <UserModal
        modalPropertiesState={modalPropertiesState}
        userInModalState={{
          userInModal: userInModal,
          setUserInModal: setUserInModal,
        }}
        recallUserApi={recallUserApi}
        townsList={townsList}
      />
      <UserDeleteModal
        modalDeletePropertiesState={modalDeletePropertiesState}
        id={userInModal.id}
        recallUserApi={recallUserApi}
      />
      {paginationDataState.paginationData !== undefined &&
        paginationDataState.paginationData.totalPages != 0 && (
          <Pagination
            totalPages={paginationDataState.paginationData.totalPages}
            totalRecords={paginationDataState.paginationData.totalRecords}
            onPageChanged={onPageChanged}
            currentPage={paginationDataState.paginationData.currentPage}
          />
        )}
    </main>
  );
};

export default () => AdminPagesWrapper(UserPage);
