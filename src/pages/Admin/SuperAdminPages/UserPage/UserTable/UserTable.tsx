import React from "react";
import { Table } from "react-bootstrap";
import commonClasses from "../../SuperAdminPages.css";
import cn from "clsx";
import Typography from "src/components/Typography";
import { MODAL_TYPE } from "../../consts";
import Delete from "../../images/Delete";
import Edit from "../../images/Edit";
import classes from "./UserTable.css";
import { ModalProperties } from "../../types";
import { User, UserInModal } from "src/types/Admin/SuperAdmin/User/user";

interface UserTableProps {
  setModalProperties: (modalProperties: ModalProperties) => void;
  setUserInModal: (userInModal: UserInModal) => void;
  setIsDeleteModalShown: (isDeleteModalShown: boolean) => void;
  usersList: User[];
}

const UserTable = ({
  setModalProperties,
  setUserInModal,
  setIsDeleteModalShown,
  usersList,
}: UserTableProps) => {
  const getUsersList = () => {
    return usersList.map((user: User) => {
      return (
        <tr key={user.id} className={cn(classes.tableRaw)}>
          <td>{user.firstName + " " + user.lastName}</td>
          <td>{user.role.name}</td>
          <td>{user.branchOffice.name}</td>
          <td>{user.deskNumber}</td>
          <td>{user.createdAt}</td>
          <td>
            <div>{user.isActive ? "Активен" : "Не активен"}</div>
          </td>
          <td>
            <button
              className={cn(commonClasses.buttonAction)}
              type="button"
              onClick={() => {
                setIsDeleteModalShown(true);
                setUserInModal({
                  id: 0,
                  firstName: "",
                  lastName: "",
                  branchOfficeId: 0,
                  visitPurposesIds: [],
                  password: "",
                  email: "",
                  isActive: true,
                  isAllowedHaveSameDeskNumber: true,
                  roleId: 0,
                  townId: 0,
                });
              }}
            >
              {Delete()}
            </button>
            <button
              className={cn(commonClasses.buttonAction)}
              type="button"
              onClick={() => {
                setModalProperties({
                  isModalShown: true,
                  action: MODAL_TYPE.UPDATE.NAME,
                });
                setUserInModal({
                  id: user.id,
                  firstName: user.firstName,
                  lastName: user.lastName,
                  branchOfficeId: user.branchOffice.id,
                  visitPurposesIds: user.visitPurposesIds,
                  password: "",
                  email: user.email,
                  isActive: user.isActive,
                  isAllowedHaveSameDeskNumber: user.isAllowedHaveSameDeskNumber,
                  roleId: user.role.id,
                  townId: user.branchOffice.townId,
                });
              }}
            >
              {Edit()}
            </button>
          </td>
        </tr>
      );
    });
  };

  return (
    <Table className={cn(classes.table)} striped size="lg">
      <thead className={cn(classes.tableHeader)}>
        <tr>
          <th>
            <Typography variant="span">Имя</Typography>
          </th>
          <th>
            <Typography variant="span">Роль</Typography>
          </th>
          <th>
            <Typography variant="span">Филиал</Typography>
          </th>
          <th>
            <Typography variant="span">Окно</Typography>
          </th>
          <th>
            <Typography variant="span">Дата создания</Typography>
          </th>
          <th>
            <Typography variant="span">Статус</Typography>
          </th>
          <th>
            <Typography variant="span">Действия</Typography>
          </th>
        </tr>
      </thead>
      <tbody>{getUsersList()}</tbody>
    </Table>
  );
};

export default UserTable;
