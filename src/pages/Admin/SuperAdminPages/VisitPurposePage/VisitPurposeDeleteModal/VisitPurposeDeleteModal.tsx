import React from "react";
import { Button, Modal } from "react-bootstrap";
import commonClasses from "../../SuperAdminPages.css";
import cn from "clsx";
import Typography from "src/components/Typography";
import { createNotification } from "src/helpers/crud-notifications";
import { deleteVisitPurpose } from "src/apis/AdminPages/SuperAdminPages/VisitPurposePage/visitPurpose";
import { ModalDeletePropertiesState } from "src/pages/Admin/types";

interface VisitPurposeDeleteModalProps {
  modalDeletePropertiesState: ModalDeletePropertiesState;
  id: number;
  recallVisitPurposeApi: () => void;
}

const VisitPurposeDeleteModal = ({
  modalDeletePropertiesState,
  id,
  recallVisitPurposeApi,
}: VisitPurposeDeleteModalProps) => {
  const handleConfirmationDeleteVisitPurposeModalClick = async () => {
    const response = await deleteVisitPurpose(id);

    if (response == "VISIT_PURPOSE_IS_REMOVED") {
      createNotification("success", "Цель визита удалена");
      modalDeletePropertiesState.setIsDeleteModalShown(false);
      recallVisitPurposeApi();
    }
  };

  return (
    <Modal
      className={cn(commonClasses.modalDiolog)}
      show={modalDeletePropertiesState.isDeleteModalShown}
      onHide={() => modalDeletePropertiesState.setIsDeleteModalShown(false)}
    >
      <Modal.Header closeButton>
        <Modal.Title>
          <Typography variant="span">
            Вы уверены что хотите удалить выбранный пункт?
          </Typography>
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className={cn(commonClasses.modalDeleteActionButtons)}>
        <Button
          className={cn(commonClasses.modalDeleteActionButton)}
          variant="secondary"
          onClick={() => handleConfirmationDeleteVisitPurposeModalClick()}
        >
          <Typography color="white" variant="span">
            Да
          </Typography>
        </Button>
        <Button
          className={cn(commonClasses.modalDeleteActionButton)}
          variant="primary"
          onClick={() =>
            modalDeletePropertiesState.setIsDeleteModalShown(false)
          }
        >
          <Typography color="white" variant="span">
            Нет
          </Typography>
        </Button>
      </Modal.Body>
    </Modal>
  );
};

export default VisitPurposeDeleteModal;
