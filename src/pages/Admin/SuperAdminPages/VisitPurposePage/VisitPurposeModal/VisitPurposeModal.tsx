import React, { useEffect, useState } from "react";
import commonClasses from "../../SuperAdminPages.css";
import cn from "clsx";
import Typography from "src/components/Typography";
import { MODAL_TYPE } from "../../consts";
import { createNotification } from "src/helpers/crud-notifications";
import {
  getServicesList,
  getVisitPurposeTypesList,
  storeVisitPurpose,
  updateVisitPurpose,
} from "src/apis/AdminPages/SuperAdminPages/VisitPurposePage/visitPurpose";
import { VisitPurposeInModal } from "src/types/Admin/SuperAdmin/VisitPurpose/visitPurpose";
import { Button, Form, Modal, OverlayTrigger, Tooltip } from "react-bootstrap";
// @ts-ignore
import Select from "react-select";
import classes from "./VisitPurposeModal.css";
import exclamationPoint from "src/pages/Admin/SuperAdminPages/VisitPurposePage/images/exclamation-point.svg";
import { Service } from "src/types/Admin/SuperAdmin/VisitPurpose/service";
import useApiCall from "src/utils/hooks/use-async-call";
import { ModalPropertiesState } from "../../types";
import { VisitPurposeType } from "src/types/Admin/SuperAdmin/VisitPurpose/visitPurposeType";

interface VisitPurposeModalProps {
  modalPropertiesState: ModalPropertiesState;
  visitPurposeInModalState: {
    visitPurposeInModal: VisitPurposeInModal;
    setVisitPurposeInModal: (visitPurposeInModal: VisitPurposeInModal) => void;
  };
  recallVisitPurposeApi: () => void;
}

const VisitPurposeModal = ({
  modalPropertiesState,
  visitPurposeInModalState,
  recallVisitPurposeApi,
}: VisitPurposeModalProps) => {
  const [services, callServiceApi] = useApiCall(getServicesList);
  const [servicesList, setServicesList] = useState<Service[]>([]);
  const [visitPurposeTypes, callVisitPurposeTypeApi] = useApiCall(
    getVisitPurposeTypesList
  );
  const [visitPurposeTypesList, setVisitPurposesList] = useState<
    VisitPurposeType[]
  >([]);
  useEffect(() => {
    callServiceApi();
    callVisitPurposeTypeApi();
  }, []);

  useEffect(() => {
    if (services) {
      setServicesList(services);
    }
  }, [services]);

  useEffect(() => {
    if (visitPurposeTypes) {
      setVisitPurposesList(visitPurposeTypes);
    }
  }, [visitPurposeTypes]);

  const handleCreateVisitPurposeClick = async () => {
    const response = await storeVisitPurpose(
      visitPurposeInModalState.visitPurposeInModal
    );

    if (response == "VISIT_PURPOSE_IS_SAVED") {
      createNotification("success", "Цель визита сохранена");
      recallVisitPurposeApi();
      modalPropertiesState.setModalProperties({
        ...modalPropertiesState.modalProperties,
        isModalShown: false,
      });
    }
  };

  const handleUpdateVisitPurposeClick = async () => {
    const response = await updateVisitPurpose(
      visitPurposeInModalState.visitPurposeInModal
    );

    if (response == "VISIT_PURPOSE_IS_UPDATED") {
      createNotification("success", "Цель визита обновлена");
      recallVisitPurposeApi();
      modalPropertiesState.setModalProperties({
        ...modalPropertiesState.modalProperties,
        isModalShown: false,
      });
    }
  };

  const instructionTooltip = () => {
    return (
      <OverlayTrigger
        placement="top"
        onEntering={entering}
        overlay={
          <Tooltip id="tooltip-top">
            {`desk_number - номерстолика \n ticket_number - номер очереди \n expert_name - имя экперта`}
          </Tooltip>
        }
      >
        {({ ref, ...triggerHandler }) => (
          <Button
            variant="transparent"
            {...triggerHandler}
            className={cn(
              classes.buttunExclamationPoint,
              "d-inline-flex align-items-center"
            )}
          >
            <img
              ref={ref}
              className={cn(classes.checkMark)}
              src={exclamationPoint}
            />
          </Button>
        )}
      </OverlayTrigger>
    );
  };

  const handleServicesIdsChange = (
    selectedServices: [{ value: number }]
  ): void => {
    const options = selectedServices;
    const selectedOptions = [];

    for (let i = 0; i < options.length; i++) {
      selectedOptions.push(options[i].value);
    }

    visitPurposeInModalState.setVisitPurposeInModal({
      ...visitPurposeInModalState.visitPurposeInModal,
      servicesIds: selectedOptions,
    });
  };

  const getVisitPurposeTypeOptions = () => {
    const options = visitPurposeTypesList.map(
      (visitPutposeType: VisitPurposeType) => {
        return { value: visitPutposeType.id, label: visitPutposeType.name };
      }
    );
    return options;
  };

  const handleVisitPurposeTypeChange = (selectedVisitPurposeType: {
    value: number;
  }) => {
    visitPurposeInModalState.setVisitPurposeInModal({
      ...visitPurposeInModalState.visitPurposeInModal,
      visitPurposeTypeId: selectedVisitPurposeType.value,
    });
  };

  const entering = (e: any) => {
    e.children[1].style.padding = "0.25rem 1.5rem";
    e.children[1].style.maxWidth = "250px";
    e.children[1].style.textAlign = "left";
    e.children[1].style.borderRadius = "1.25rem";
    e.children[1].style.backgroundColor = "gray";
  };

  const getDefaultVisitPurposeType = () => {
    const visitPurposeType = visitPurposeTypesList.find(
      (visitPurposeType: VisitPurposeType) =>
        visitPurposeInModalState.visitPurposeInModal.visitPurposeTypeId ===
        visitPurposeType.id
    );

    if (visitPurposeType) {
      return {
        label: visitPurposeType.name,
        value: visitPurposeType.id,
      };
    }
  };

  const getServiceOptions = () => {
    const options = servicesList.map((service: Service) => {
      return { value: service.id, label: service.name };
    });
    return options;
  };

  return (
    <Modal
      className={cn(commonClasses.modalDiolog)}
      show={modalPropertiesState.modalProperties.isModalShown}
      onHide={() =>
        modalPropertiesState.setModalProperties({
          ...modalPropertiesState.modalProperties,
          isModalShown: false,
        })
      }
    >
      <Modal.Header closeButton>
        <Modal.Title>
          <Typography variant="span">
            {modalPropertiesState.modalProperties.action ==
            MODAL_TYPE.CREATE.NAME
              ? "Добавить цель"
              : "Изменить цель"}
          </Typography>
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div>
          <Form.Group>
            <Typography variant="label">Имя цели визита</Typography>
            <Form.Control
              type="text"
              value={visitPurposeInModalState.visitPurposeInModal.name}
              onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
                visitPurposeInModalState.setVisitPurposeInModal({
                  ...visitPurposeInModalState.visitPurposeInModal,
                  name: event.target.value,
                })
              }
            />
          </Form.Group>
          <Form.Group>
            <div className={cn(classes.labelRow)}>
              <Typography variant="label">Сообщение на таджикском</Typography>
              {instructionTooltip()}
            </div>
            <Form.Control
              type="text"
              value={visitPurposeInModalState.visitPurposeInModal.tjMessage}
              onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
                visitPurposeInModalState.setVisitPurposeInModal({
                  ...visitPurposeInModalState.visitPurposeInModal,
                  tjMessage: event.target.value,
                })
              }
            />
          </Form.Group>
          <Form.Group>
            <div className={cn(classes.labelRow)}>
              <Typography variant="label">Сообщение на русском</Typography>
              {instructionTooltip()}
            </div>
            <Form.Control
              type="text"
              value={visitPurposeInModalState.visitPurposeInModal.ruMessage}
              onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
                visitPurposeInModalState.setVisitPurposeInModal({
                  ...visitPurposeInModalState.visitPurposeInModal,
                  ruMessage: event.target.value,
                })
              }
            />
          </Form.Group>
          <Form.Group>
            <Typography variant="label">Сервисы</Typography>
            <Select
              defaultValue={servicesList.map((service: Service) => {
                if (
                  visitPurposeInModalState.visitPurposeInModal.servicesIds.includes(
                    service.id
                  )
                ) {
                  return { label: service.name, value: service.id };
                }
              })}
              options={getServiceOptions()}
              isMulti
              onChange={handleServicesIdsChange}
            />
          </Form.Group>
          <Form.Group>
            <Typography variant="label">Типы</Typography>
            <Select
              defaultValue={getDefaultVisitPurposeType()}
              options={getVisitPurposeTypeOptions()}
              onChange={handleVisitPurposeTypeChange}
            />
          </Form.Group>
          <Form.Group>
            <Typography variant="label">Серия</Typography>
            <Form.Control
              type="text"
              value={visitPurposeInModalState.visitPurposeInModal.series}
              onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
                visitPurposeInModalState.setVisitPurposeInModal({
                  ...visitPurposeInModalState.visitPurposeInModal,
                  series: event.target.value,
                })
              }
            />
          </Form.Group>
          <Form.Group className={cn(classes.switch)}>
            <Form.Check
              className={cn(classes.customControl)}
              type="switch"
              id="is-prioratised-switch"
              label="В приоритете?"
              checked={
                visitPurposeInModalState.visitPurposeInModal.isPrioratised
              }
              onChange={() =>
                visitPurposeInModalState.setVisitPurposeInModal({
                  ...visitPurposeInModalState.visitPurposeInModal,
                  isPrioratised: !visitPurposeInModalState.visitPurposeInModal
                    .isPrioratised,
                })
              }
            />
          </Form.Group>
          <Form.Group className={cn(classes.switch)}>
            <Form.Check
              className={cn(classes.customControl)}
              type="switch"
              id="is-cash-register-switch"
              label="Связана с кассой?"
              checked={
                visitPurposeInModalState.visitPurposeInModal.isCashRegister
              }
              onChange={() =>
                visitPurposeInModalState.setVisitPurposeInModal({
                  ...visitPurposeInModalState.visitPurposeInModal,
                  isCashRegister: !visitPurposeInModalState.visitPurposeInModal
                    .isCashRegister,
                })
              }
            />
          </Form.Group>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <Button
          variant="secondary"
          onClick={
            modalPropertiesState.modalProperties.action ==
            MODAL_TYPE.CREATE.NAME
              ? () => handleCreateVisitPurposeClick()
              : () => handleUpdateVisitPurposeClick()
          }
        >
          <Typography color="white" variant="span">
            {modalPropertiesState.modalProperties.action ==
            MODAL_TYPE.CREATE.NAME
              ? MODAL_TYPE.CREATE.ACTION
              : MODAL_TYPE.UPDATE.ACTION}
          </Typography>
        </Button>
        <Button
          variant="primary"
          onClick={() =>
            modalPropertiesState.setModalProperties({
              ...modalPropertiesState.modalProperties,
              isModalShown: false,
            })
          }
        >
          <Typography color="white" variant="span">
            Закрыть
          </Typography>
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default VisitPurposeModal;
