import React, { useEffect, useState } from "react";
import classes from "./VisitPurposePage.css";
import cn from "clsx";
// @ts-ignore
import { NotificationContainer } from "react-notifications";
import { MODAL_TYPE } from "../consts";
import useApiCall from "src/utils/hooks/use-async-call";
import Pagination from "src/components/Pagination/Pagination";
import { getPaginatedVisitPurposesList } from "src/apis/AdminPages/SuperAdminPages/VisitPurposePage/visitPurpose";
import VisitPurposeTable from "./VisitPurposeTable";
import VisitPurposeDeleteModal from "./VisitPurposeDeleteModal";
import VisitPurposeModal from "./VisitPurposeModal";
import { VisitPurposeInModal } from "src/types/Admin/SuperAdmin/VisitPurpose/visitPurpose";
import { BranchOffice } from "src/types/Admin/SuperAdmin/VisitPurpose/branchOffice";
import { Button, Form } from "react-bootstrap";
import Typography from "src/components/Typography";
import AdminPagesWrapper from "src/wrappers/AdminPagesWrapper";
import { AdminPagesWrapperProps } from "src/wrappers/AdminPagesWrapper/AdminPagesWrapper";
import { getPageOptions } from "../utils";
import { getBranchOfficesList } from "src/apis/AdminPages/SuperAdminPages/branchOffice";

const VisitPurposePage = ({
  modalPropertiesState,
  pageLimitState,
  paginationDataState,
  searchExpressionState,
  modalDeletePropertiesState,
}: AdminPagesWrapperProps) => {
  const [branchOfficeId, setBranchOfficeId] = useState<number>(0);
  const [visitPurposesList, setVisitPurposesList] = useState([]);
  const [data, callVisitPurposeApi] = useApiCall(getPaginatedVisitPurposesList);
  const [branchOfficesList, setBranchOfficesList] = useState<BranchOffice[]>(
    []
  );
  const [branchOffices, callBranchOfficeApi] = useApiCall(getBranchOfficesList);
  const [visitPurposeInModal, setVisitPurposeInModal] = useState<
    VisitPurposeInModal
  >({
    id: 0,
    name: "",
    series: "",
    servicesIds: [],
    visitPurposeTypeId: 0,
    tjMessage: "",
    ruMessage: "",
    isPrioratised: false,
    isCashRegister: false,
  });

  useEffect(() => {
    callVisitPurposeApi([
      branchOfficeId,
      searchExpressionState.searchExpression,
      pageLimitState.pageLimit,
    ]);
    callBranchOfficeApi();
  }, []);

  useEffect(() => {
    if (branchOffices) {
      setBranchOfficesList(branchOffices);
    }
  }, [branchOffices]);

  useEffect(() => {
    const { visitPurposes, paginationData } = data;
    if (visitPurposes) {
      setVisitPurposesList(visitPurposes);
      paginationDataState.setPaginationData(paginationData);
    }
  }, [data]);

  const onPageChanged = (page: number): void => {
    callVisitPurposeApi([
      branchOfficeId,
      searchExpressionState.searchExpression,
      pageLimitState.pageLimit,
      page,
    ]);
  };

  const getBranchOfficeOptions = (): JSX.Element[] =>
    branchOfficesList.map((branchOffice: BranchOffice, index: number) => {
      return (
        <option key={index} value={branchOffice.id}>
          {branchOffice.name}
        </option>
      );
    });

  const recallVisitPurposeApi = () => {
    callVisitPurposeApi([
      branchOfficeId,
      searchExpressionState.searchExpression,
      pageLimitState.pageLimit,
    ]);
  };
  return (
    <main className={cn(classes.container)}>
      <NotificationContainer />
      <div className={cn(classes.containerHeader)}>
        <div className={cn(classes.filters)}>
          <Form.Group>
            <Form.Control
              as="select"
              value={pageLimitState.pageLimit}
              onChange={(event: React.ChangeEvent<HTMLSelectElement>) =>
                pageLimitState.setPageLimit(Number(event.target.value))
              }
              custom
            >
              {getPageOptions()}
            </Form.Control>
          </Form.Group>
          <Form.Group>
            <Form.Control
              as="select"
              value={branchOfficeId}
              onChange={(event: React.ChangeEvent<HTMLSelectElement>) =>
                setBranchOfficeId(Number(event.target.value))
              }
              custom
            >
              <option value="0">Все филиалы</option>
              {getBranchOfficeOptions()}
            </Form.Control>
          </Form.Group>
          <Form.Group>
            <Form.Control
              type="text"
              placeholder="Поиск по имени"
              value={searchExpressionState.searchExpression}
              onChange={(event: React.ChangeEvent<HTMLSelectElement>) =>
                searchExpressionState.setSearchExpression(event.target.value)
              }
            />
          </Form.Group>
          <Button
            variant="primary"
            onClick={() =>
              callVisitPurposeApi([
                branchOfficeId,
                searchExpressionState.searchExpression,
                pageLimitState.pageLimit,
              ])
            }
          >
            <Typography color="white" variant="span">
              Фильтр
            </Typography>
          </Button>
          <Button
            variant="primary"
            onClick={() => {
              modalPropertiesState.setModalProperties({
                action: MODAL_TYPE.CREATE.NAME,
                isModalShown: true,
              });
              setVisitPurposeInModal({
                id: 0,
                name: "",
                series: "",
                servicesIds: [],
                tjMessage: "",
                ruMessage: "",
                visitPurposeTypeId: 0,
                isPrioratised: false,
                isCashRegister: false,
              });
            }}
          >
            <Typography color="white" variant="span">
              Добавить
            </Typography>
          </Button>
        </div>
      </div>
      <VisitPurposeTable
        setModalProperties={modalPropertiesState.setModalProperties}
        setVisitPurposeInModal={setVisitPurposeInModal}
        setIsDeleteModalShown={modalDeletePropertiesState.setIsDeleteModalShown}
        visitPurposesList={visitPurposesList}
      />
      <VisitPurposeModal
        modalPropertiesState={modalPropertiesState}
        visitPurposeInModalState={{
          visitPurposeInModal: visitPurposeInModal,
          setVisitPurposeInModal: setVisitPurposeInModal,
        }}
        recallVisitPurposeApi={recallVisitPurposeApi}
      />
      <VisitPurposeDeleteModal
        modalDeletePropertiesState={modalDeletePropertiesState}
        id={visitPurposeInModal.id}
        recallVisitPurposeApi={recallVisitPurposeApi}
      />
      {paginationDataState.paginationData !== undefined &&
        paginationDataState.paginationData.totalPages != 0 && (
          <Pagination
            totalPages={paginationDataState.paginationData.totalPages}
            totalRecords={paginationDataState.paginationData.totalRecords}
            onPageChanged={onPageChanged}
            currentPage={paginationDataState.paginationData.currentPage}
          />
        )}
    </main>
  );
};

export default () => AdminPagesWrapper(VisitPurposePage);
