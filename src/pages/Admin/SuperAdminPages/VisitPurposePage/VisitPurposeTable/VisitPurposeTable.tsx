import React from "react";
import { Table } from "react-bootstrap";
import commonClasses from "../../SuperAdminPages.css";
import cn from "clsx";
import Typography from "src/components/Typography";
import { MODAL_TYPE } from "../../consts";
import Delete from "../../images/Delete";
import Edit from "../../images/Edit";
import classes from "./VisitPurposeTable.css";
import {
  VisitPurpose,
  VisitPurposeInModal,
} from "src/types/Admin/SuperAdmin/VisitPurpose/visitPurpose";
import { ModalProperties } from "../../types";
import { Service } from "src/types/Admin/SuperAdmin/VisitPurpose/service";

const getServicesIds = (selectedService: Service[]) => {
  const options = selectedService;
  const selectedOptions = [];

  for (let i = 0; i < options.length; i++) {
    selectedOptions.push(options[i].id);
  }

  return selectedOptions;
};

const getServicesRows = (services: { name: string }[]) => {
  const arrayLength = services.length;
  return services.map((service: { name: string }, index: number) => {
    if (arrayLength == index + 1) {
      return service.name;
    }

    return service.name + ", ";
  });
};
interface VisitPurposeTableProps {
  setModalProperties: (modalProperties: ModalProperties) => void;
  setVisitPurposeInModal: (visitPurposeInModal: VisitPurposeInModal) => void;
  setIsDeleteModalShown: (isDeleteModalShown: boolean) => void;
  visitPurposesList: VisitPurpose[];
}

const VisitPurposeTable = ({
  setModalProperties,
  setVisitPurposeInModal,
  setIsDeleteModalShown,
  visitPurposesList,
}: VisitPurposeTableProps) => {
  const getVisitPurposesList = () => {
    return visitPurposesList.map((visitPurpose: VisitPurpose) => {
      return (
        <tr key={visitPurpose.id} className={cn(classes.tableRaw)}>
          <td>{visitPurpose.name}</td>
          <td>{getServicesRows(visitPurpose.services)}</td>
          <td>{visitPurpose.series}</td>
          <td>{visitPurpose.isPrioratised ? "Да" : "Нет"}</td>
          <td>
            <button
              className={cn(commonClasses.buttonAction)}
              type="button"
              onClick={() => {
                setIsDeleteModalShown(true);
                setVisitPurposeInModal({
                  id: visitPurpose.id,
                  name: "",
                  series: "",
                  visitPurposeTypeId: 0,
                  servicesIds: [],
                  tjMessage: "",
                  ruMessage: "",
                  isPrioratised: false,
                  isCashRegister: false,
                });
              }}
            >
              {Delete()}
            </button>
            <button
              className={cn(commonClasses.buttonAction)}
              type="button"
              onClick={() => {
                setModalProperties({
                  isModalShown: true,
                  action: MODAL_TYPE.UPDATE.NAME,
                });
                setVisitPurposeInModal({
                  id: visitPurpose.id,
                  name: visitPurpose.name,
                  series: visitPurpose.series,
                  visitPurposeTypeId: visitPurpose.visitPurposeType
                    ? visitPurpose.visitPurposeType.id
                    : 0,
                  servicesIds: getServicesIds(visitPurpose.services),
                  tjMessage: visitPurpose.tjMessage,
                  ruMessage: visitPurpose.ruMessage,
                  isPrioratised: visitPurpose.isPrioratised,
                  isCashRegister: visitPurpose.isCashRegister,
                });
              }}
            >
              {Edit()}
            </button>
          </td>
        </tr>
      );
    });
  };

  return (
    <Table className={cn(classes.table)} striped size="lg">
      <thead className={cn(classes.tableHeader)}>
        <tr>
          <th>
            <Typography variant="span">Название</Typography>
          </th>
          <th>
            <Typography variant="span">Сервисы</Typography>
          </th>
          <th>
            <Typography variant="span">Серия</Typography>
          </th>
          <th>
            <Typography variant="span">В приоритете</Typography>
          </th>
          <th>
            <Typography variant="span">Действие</Typography>
          </th>
        </tr>
      </thead>
      <tbody>{getVisitPurposesList()}</tbody>
    </Table>
  );
};

export default VisitPurposeTable;
