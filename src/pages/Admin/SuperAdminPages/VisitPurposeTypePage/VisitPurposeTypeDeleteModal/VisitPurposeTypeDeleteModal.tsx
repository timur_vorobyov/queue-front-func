import React from "react";
import { Button, Modal } from "react-bootstrap";
import commonClasses from "../../SuperAdminPages.css";
import cn from "clsx";
import Typography from "src/components/Typography";
import { createNotification } from "src/helpers/crud-notifications";
import { deleteVisitPurposeType } from "src/apis/AdminPages/SuperAdminPages/VisitPurposeTypePage/visitPurposeType";
import { ModalDeletePropertiesState } from "src/pages/Admin/types";

interface VisitPurposeTypeDeleteModalProps {
  modalDeletePropertiesState: ModalDeletePropertiesState;
  id: number;
  recallVisitPurposeTypeApi: () => void;
}

const VisitPurposeTypeDeleteModal = ({
  modalDeletePropertiesState,
  recallVisitPurposeTypeApi,
  id,
}: VisitPurposeTypeDeleteModalProps) => {
  const handleConfirmationDeleteVisitPurposeModalClick = async () => {
    const response = await deleteVisitPurposeType(id);

    if (response == "VISIT_PURPOSE_TYPE_IS_REMOVED") {
      createNotification("success", "Тип подцели визита удален");
      recallVisitPurposeTypeApi();
      modalDeletePropertiesState.setIsDeleteModalShown(false);
    }
  };

  return (
    <Modal
      className={cn(commonClasses.modalDiolog)}
      show={modalDeletePropertiesState.isDeleteModalShown}
      onHide={() => modalDeletePropertiesState.setIsDeleteModalShown(false)}
    >
      <Modal.Header closeButton>
        <Modal.Title>
          <Typography variant="span">
            Вы уверены что хотите удалить выбранный пункт?
          </Typography>
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className={cn(commonClasses.modalDeleteActionButtons)}>
        <Button
          className={cn(commonClasses.modalDeleteActionButton)}
          variant="secondary"
          onClick={() => handleConfirmationDeleteVisitPurposeModalClick()}
        >
          <Typography color="white" variant="span">
            Да
          </Typography>
        </Button>
        <Button
          className={cn(commonClasses.modalDeleteActionButton)}
          variant="primary"
          onClick={() =>
            modalDeletePropertiesState.setIsDeleteModalShown(false)
          }
        >
          <Typography color="white" variant="span">
            Нет
          </Typography>
        </Button>
      </Modal.Body>
    </Modal>
  );
};

export default VisitPurposeTypeDeleteModal;
