import React from "react";
import { Button, Form, Modal } from "react-bootstrap";
import commonClasses from "../../SuperAdminPages.css";
import cn from "clsx";
import Typography from "src/components/Typography";
import { MODAL_TYPE } from "../../consts";
import { setStateAction } from "src/types/generics";
import { createNotification } from "src/helpers/crud-notifications";
import {
  VisitPurposeType,
  VisitPurposeTypeInModal,
} from "src/types/Admin/SuperAdmin/ViisitPurposeType/visitPurposeType";
import {
  storeVisitPurposeType,
  updateVisitPurposeType,
} from "src/apis/AdminPages/SuperAdminPages/VisitPurposeTypePage/visitPurposeType";
import { ModalProperties } from "../../types";

interface VisitPurposeModalProps {
  modalPropertiesState: {
    modalProperties: ModalProperties;
    setModalProperties: (modalProperties: ModalProperties) => void;
  };
  visitPurposeTypeInModalState: {
    visitPurposeTypeInModal: VisitPurposeTypeInModal;
    setVisitPurposeTypeInModal: (
      visitPurposeTypeInModal: VisitPurposeTypeInModal
    ) => void;
  };
  recallVisitPurposeTypeApi: () => void;
}

const VisitPurposeTypeModal = ({
  modalPropertiesState,
  visitPurposeTypeInModalState,
  recallVisitPurposeTypeApi,
}: VisitPurposeModalProps) => {
  const handleCreateVisitPurposeClick = async () => {
    const response = await storeVisitPurposeType(
      visitPurposeTypeInModalState.visitPurposeTypeInModal
    );

    if (response == "VISIT_PURPOSE_TYPE_IS_SAVED") {
      createNotification("success", "Тип цели визита сохранен");
      recallVisitPurposeTypeApi();
      modalPropertiesState.setModalProperties({
        ...modalPropertiesState.modalProperties,
        isModalShown: false,
      });
    }
  };

  const handleUpdateVisitPurposeClick = async () => {
    const response = await updateVisitPurposeType(
      visitPurposeTypeInModalState.visitPurposeTypeInModal
    );

    if (response == "VISIT_PURPOSE_TYPE_IS_UPDATED") {
      createNotification("success", "Тип цели визита обновлен");
      recallVisitPurposeTypeApi();
      modalPropertiesState.setModalProperties({
        ...modalPropertiesState.modalProperties,
        isModalShown: false,
      });
    }
  };

  return (
    <Modal
      className={cn(commonClasses.modalDiolog)}
      show={modalPropertiesState.modalProperties.isModalShown}
      onHide={() =>
        modalPropertiesState.setModalProperties({
          ...modalPropertiesState.modalProperties,
          isModalShown: false,
        })
      }
    >
      <Modal.Header closeButton>
        <Modal.Title>
          <Typography variant="span">
            {modalPropertiesState.modalProperties.action ==
            MODAL_TYPE.CREATE.NAME
              ? "Добавить подцель"
              : "Изменить подцель"}
          </Typography>
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div>
          <Form.Group>
            <Typography variant="label">Имя подцели</Typography>
            <Form.Control
              type="text"
              value={visitPurposeTypeInModalState.visitPurposeTypeInModal.name}
              onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
                visitPurposeTypeInModalState.setVisitPurposeTypeInModal({
                  ...visitPurposeTypeInModalState.visitPurposeTypeInModal,
                  name: event.target.value,
                })
              }
            />
          </Form.Group>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <Button
          variant="secondary"
          onClick={
            modalPropertiesState.modalProperties.action ==
            MODAL_TYPE.CREATE.NAME
              ? () => handleCreateVisitPurposeClick()
              : () => handleUpdateVisitPurposeClick()
          }
        >
          <Typography color="white" variant="span">
            {modalPropertiesState.modalProperties.action ==
            MODAL_TYPE.CREATE.NAME
              ? MODAL_TYPE.CREATE.ACTION
              : MODAL_TYPE.UPDATE.ACTION}
          </Typography>
        </Button>
        <Button
          variant="primary"
          onClick={() =>
            modalPropertiesState.setModalProperties({
              ...modalPropertiesState.modalProperties,
              isModalShown: false,
            })
          }
        >
          <Typography color="white" variant="span">
            Закрыть
          </Typography>
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default VisitPurposeTypeModal;
