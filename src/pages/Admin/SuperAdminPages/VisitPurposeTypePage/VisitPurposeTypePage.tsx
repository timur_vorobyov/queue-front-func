import React, { useEffect, useState } from "react";
import classes from "./VisitPurposeTypePage.css";
import cn from "clsx";
import Typography from "src/components/Typography";
import { Button, Form } from "react-bootstrap";
// @ts-ignore
import { NotificationContainer } from "react-notifications";
import { MODAL_TYPE } from "../consts";
import VisitPurposeTypeTable from "./VisitPurposeTypeTable";
import VisitPurposeTypeModal from "./VisitPurposeTypeModal";
import VisitPurposeTypeDeleteModal from "./VisitPurposeTypeDeleteModal";
import AdminPagesWrapper from "src/wrappers/AdminPagesWrapper";
import { AdminPagesWrapperProps } from "src/wrappers/AdminPagesWrapper/AdminPagesWrapper";
import useApiCall from "src/utils/hooks/use-async-call";
import { getPaginatedVisitPurposeTypesList } from "src/apis/AdminPages/SuperAdminPages/VisitPurposeTypePage/visitPurposeType";
import { getPageOptions } from "../utils";
import { VisitPurposeTypeInModal } from "src/types/Admin/SuperAdmin/ViisitPurposeType/visitPurposeType";
import Pagination from "src/components/Pagination/Pagination";

const VisitPurposeTypePage = ({
  modalPropertiesState,
  pageLimitState,
  paginationDataState,
  searchExpressionState,
  modalDeletePropertiesState,
}: AdminPagesWrapperProps) => {
  const [visitPurposeTypesList, setVisitPurposeTypesList] = useState([]);
  const [data, callVisitPurposeTypesApi] = useApiCall(
    getPaginatedVisitPurposeTypesList
  );
  const [visitPurposeTypeInModal, setVisitPurposeTypeInModal] = useState<
    VisitPurposeTypeInModal
  >({
    id: 0,
    name: "",
  });

  useEffect(() => {
    callVisitPurposeTypesApi([
      searchExpressionState.searchExpression,
      pageLimitState.pageLimit,
    ]);
  }, []);

  useEffect(() => {
    const { visitPurposeTypes, paginationData } = data;
    if (visitPurposeTypes) {
      setVisitPurposeTypesList(visitPurposeTypes);
      paginationDataState.setPaginationData(paginationData);
    }
  }, [data]);

  const recallVisitPurposeTypeApi = () => {
    callVisitPurposeTypesApi([
      searchExpressionState.searchExpression,
      pageLimitState.pageLimit,
    ]);
  };

  const onPageChanged = (page: number): void => {
    callVisitPurposeTypesApi([
      searchExpressionState.searchExpression,
      pageLimitState.pageLimit,
      page,
    ]);
  };

  return (
    <main className={cn(classes.container)}>
      <NotificationContainer />
      <div className={cn(classes.containerHeader)}>
        <div className={cn(classes.filters)}>
          <Form.Group>
            <Form.Control
              as="select"
              value={pageLimitState.pageLimit}
              onChange={(event: React.ChangeEvent<HTMLSelectElement>) =>
                pageLimitState.setPageLimit(Number(event.target.value))
              }
              custom
            >
              {getPageOptions()}
            </Form.Control>
          </Form.Group>
          <Form.Group>
            <Form.Control
              type="text"
              placeholder="Поиск по имени"
              value={searchExpressionState.searchExpression}
              onChange={(event: React.ChangeEvent<HTMLSelectElement>) =>
                searchExpressionState.setSearchExpression(event.target.value)
              }
            />
          </Form.Group>
          <Button
            variant="primary"
            onClick={() => {
              callVisitPurposeTypesApi([
                searchExpressionState.searchExpression,
                pageLimitState.pageLimit,
              ]);
            }}
          >
            <Typography color="white" variant="span">
              Фильтр
            </Typography>
          </Button>
          <Button
            variant="primary"
            onClick={() => {
              modalPropertiesState.setModalProperties({
                action: MODAL_TYPE.CREATE.NAME,
                isModalShown: true,
              });
              setVisitPurposeTypeInModal({
                name: "",
                id: 0,
              });
            }}
          >
            <Typography color="white" variant="span">
              Добавить
            </Typography>
          </Button>
        </div>
      </div>

      <VisitPurposeTypeTable
        setModalProperties={modalPropertiesState.setModalProperties}
        setVisitPurposeTypeInModal={setVisitPurposeTypeInModal}
        setIsDeleteModalShown={modalDeletePropertiesState.setIsDeleteModalShown}
        visitPurposeTypesList={visitPurposeTypesList}
      />
      <VisitPurposeTypeModal
        modalPropertiesState={modalPropertiesState}
        visitPurposeTypeInModalState={{
          visitPurposeTypeInModal: visitPurposeTypeInModal,
          setVisitPurposeTypeInModal: setVisitPurposeTypeInModal,
        }}
        recallVisitPurposeTypeApi={recallVisitPurposeTypeApi}
      />
      <VisitPurposeTypeDeleteModal
        modalDeletePropertiesState={modalDeletePropertiesState}
        id={visitPurposeTypeInModal.id}
        recallVisitPurposeTypeApi={recallVisitPurposeTypeApi}
      />
      {paginationDataState.paginationData !== undefined &&
        paginationDataState.paginationData.totalPages != 0 && (
          <Pagination
            totalPages={paginationDataState.paginationData.totalPages}
            totalRecords={paginationDataState.paginationData.totalRecords}
            onPageChanged={onPageChanged}
            currentPage={paginationDataState.paginationData.currentPage}
          />
        )}
    </main>
  );
};

export default () => AdminPagesWrapper(VisitPurposeTypePage);
