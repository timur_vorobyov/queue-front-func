import React from "react";
import { Table } from "react-bootstrap";
import commonClasses from "../../SuperAdminPages.css";
import cn from "clsx";
import Typography from "src/components/Typography";
import { MODAL_TYPE } from "../../consts";
import Delete from "../../images/Delete";
import Edit from "../../images/Edit";
import classes from "./VisitPurposeTypeTable.css";
import {
  VisitPurposeType,
  VisitPurposeTypeInModal,
} from "src/types/Admin/SuperAdmin/ViisitPurposeType/visitPurposeType";
import { ModalProperties } from "../../types";

interface VisitPurposeTypeTableProps {
  setModalProperties: (modalProperties: ModalProperties) => void;
  setVisitPurposeTypeInModal: (
    visitPurposeTypeInModal: VisitPurposeTypeInModal
  ) => void;
  setIsDeleteModalShown: (isDeleteModalShown: boolean) => void;
  visitPurposeTypesList: VisitPurposeTypeInModal[];
}

const VisitPurposeTypeTable = ({
  setModalProperties,
  setVisitPurposeTypeInModal,
  setIsDeleteModalShown,
  visitPurposeTypesList,
}: VisitPurposeTypeTableProps) => {
  const getSubVisitPurposesList = () => {
    return visitPurposeTypesList.map((visitPurposeType: VisitPurposeType) => {
      return (
        <tr key={visitPurposeType.id} className={cn(classes.tableRaw)}>
          <td>{visitPurposeType.name}</td>
          <td>
            <button
              className={cn(commonClasses.buttonAction)}
              type="button"
              onClick={() => {
                setIsDeleteModalShown(true);
                setVisitPurposeTypeInModal({
                  id: visitPurposeType.id,
                  name: "",
                });
              }}
            >
              {Delete()}
            </button>
            <button
              className={cn(commonClasses.buttonAction)}
              type="button"
              onClick={() => {
                setModalProperties({
                  isModalShown: true,
                  action: MODAL_TYPE.UPDATE.NAME,
                });
                setVisitPurposeTypeInModal({
                  name: visitPurposeType.name,
                  id: visitPurposeType.id,
                });
              }}
            >
              {Edit()}
            </button>
          </td>
        </tr>
      );
    });
  };

  return (
    <Table className={cn(classes.table)} striped size="lg">
      <thead className={cn(classes.tableHeader)}>
        <tr>
          <th>
            <Typography variant="span">Название</Typography>
          </th>
          <th>
            <Typography variant="span">Действие</Typography>
          </th>
        </tr>
      </thead>
      <tbody>{getSubVisitPurposesList()}</tbody>
    </Table>
  );
};

export default VisitPurposeTypeTable;
