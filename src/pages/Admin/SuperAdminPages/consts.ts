export const MODAL_TYPE = {
  CREATE: {
    NAME: "create",
    ACTION: "Сохранить",
  },
  UPDATE: {
    NAME: "update",
    ACTION: "Обновить",
  },
};
