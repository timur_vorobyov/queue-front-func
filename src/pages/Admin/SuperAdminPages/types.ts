import { PaginationData } from "src/components/Pagination/Pagination";

export type ModalProperties = {
  isModalShown: boolean;
  action: string;
};

export type ModalPropertiesState = {
  modalProperties: ModalProperties;
  setModalProperties: (modalProperties: ModalProperties) => void;
};

export type PageLimitState = {
  pageLimit: number;
  setPageLimit: (pageLimit: number) => void;
};

export type PaginationDataState = {
  paginationData: PaginationData;
  setPaginationData: (paginationData: PaginationData) => void;
};

export type SearchExpressionState = {
  searchExpression: string;
  setSearchExpression: (searchExpression: string) => void;
};
