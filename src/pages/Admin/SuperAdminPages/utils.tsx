import React from "react";

export const getPageOptions = () => {
  const pagesCountLimmit = [5, 10, 15, 20, 50, 100];
  return pagesCountLimmit.map((pageCount: number, index: number) => {
    return (
      <option key={index} value={pageCount}>
        {pageCount}
      </option>
    );
  });
};
