import React from "react";
import { PureComponent } from "react";
import classes from "./TicketBasketPage.css";
import cn from "clsx";
// @ts-ignore
import { NotificationContainer } from "react-notifications";
import { createNotification } from "src/helpers/crud-notifications";
import TicketBasketPageAdapter, {
  BranchOffice,
  BranchOffices,
  DeclinedTicket,
  DeclinedTickets,
} from "src/services/TicketBasketPageService/TicketBasketPageAdapter";
import Typography from "src/components/Typography";
import { Button, Col, Form, Row } from "react-bootstrap";
import { getBranchOfficeId, roleName } from "src/helpers/auth-data";

interface TicketBasketPageStates {
  declinedTicketsCards: DeclinedTickets;
  branchOfficesList: BranchOffices;
  branchOfficeId: number;
}

class TicketBasketPage extends PureComponent<{}, TicketBasketPageStates> {
  private adapter: TicketBasketPageAdapter;

  constructor(props: {}) {
    super(props);
    this.state = {
      declinedTicketsCards: [],
      branchOfficesList: [{ id: 0, name: "" }],
      branchOfficeId: roleName() == "super admin" ? 0 : getBranchOfficeId(),
    };

    this.adapter = new TicketBasketPageAdapter();
  }

  componentDidMount(): void {
    this.setDeclinedTicketsList();
    this.setBranchOfficesList();
  }

  async setBranchOfficesList(): Promise<void> {
    const branchOfficesList = await this.adapter.getBranchOfficesList();
    await this.setState({ branchOfficesList: branchOfficesList });
  }

  async setDeclinedTicketsList(): Promise<void> {
    const declinedTicketsCards = await this.adapter.getDeclinedTicketsList(
      this.state.branchOfficeId
    );

    if (declinedTicketsCards) {
      await this.setState({
        declinedTicketsCards: declinedTicketsCards,
      });
    }
  }

  render(): React.ReactNode {
    return (
      <main className={cn(classes.container)}>
        {roleName() == "super admin" ? (
          <div className={cn(classes.filters)}>
            <Form.Group className={cn(classes.formGroup)}>
              <Form.Control
                as="select"
                value={this.state.branchOfficeId}
                onChange={this.setBranchOfficeId}
                custom
              >
                <option value="0">Все филиалы</option>
                {this.getBranchOfficeOptions()}
              </Form.Control>
            </Form.Group>
            <Button
              variant="primary"
              className={cn(classes.filterButton)}
              onClick={this.filter}
            >
              <Typography color="white" variant="span">
                Фильтр
              </Typography>
            </Button>
          </div>
        ) : null}
        <NotificationContainer />
        <Row>{this.getTicketsPendingCards()}</Row>
      </main>
    );
  }

  setBranchOfficeId = async (
    event: React.ChangeEvent<HTMLSelectElement>
  ): Promise<void> => {
    const branchOfficeId = event.target.value;
    this.setState({ branchOfficeId: Number(branchOfficeId) });
  };

  getBranchOfficeOptions(): JSX.Element[] {
    return this.state.branchOfficesList.map(
      (branchOffice: BranchOffice, index: number) => {
        return (
          <option key={index} value={branchOffice.id}>
            {branchOffice.name}
          </option>
        );
      }
    );
  }

  filter = (): void => {
    this.setDeclinedTicketsList();
  };

  getTicketsPendingCards(): JSX.Element[] | JSX.Element {
    if (this.state.declinedTicketsCards.length == 0) {
      return (
        <Col lg={12} className={cn(classes.epmtyBasket)}>
          <Typography color="muted" variant="h3">
            На данный момент корзина пуста
          </Typography>
        </Col>
      );
    }
    return this.state.declinedTicketsCards.map(
      (declinedTicket: DeclinedTicket, index: number) => {
        return (
          <Col key={index} lg={6}>
            <div className={cn(classes.declinedTicketCard)}>
              <div className={cn(classes.ticketInfo)}>
                <Typography color="muted" variant="h4">
                  {declinedTicket.visitPurposeName}
                </Typography>
                <div className={cn(classes.ticketNumber)}>
                  <Typography color="muted" variant="span">
                    {declinedTicket.ticketFullNumber}
                  </Typography>
                </div>
              </div>
              <div className={cn(classes.mainInfo)}>
                <div>
                  <Typography color="muted" variant="h4">
                    Выдали тикет:
                  </Typography>
                  <Typography color="muted" variant="span">
                    {declinedTicket.createdAt}
                  </Typography>
                </div>
                <div className={cn(classes.externlaIntegrationCards)}>
                  <Button
                    onClick={() => {
                      this.handleRecoverTicketClick(declinedTicket.id);
                    }}
                  >
                    Восстановить
                  </Button>
                </div>
              </div>
            </div>
          </Col>
        );
      }
    );
  }

  handleRecoverTicketClick = async (ticketId: number): Promise<void> => {
    const response = await this.adapter.recoverTicket(ticketId);

    if (response == "Ticket is recovered") {
      createNotification("success", "Билет восстановлен");
      this.setDeclinedTicketsList();
    } else {
      createNotification("error", "Билет не восстановлен");
    }
  };
}

export default TicketBasketPage;
