export type ModalDeletePropertiesState = {
  isDeleteModalShown: boolean;
  setIsDeleteModalShown: (isDeleteModalShown: boolean) => void;
};
