import { useState } from "react";
import cn from "clsx";
import classes from "./LoginPage.css";
import React from "react";
import { Button, Form } from "react-bootstrap";
import Typography from "src/components/Typography";
import { Redirect } from "react-router-dom";
import { routes } from "src/config/routes";
import apiClient from "src/helpers/httpInterceptor";
// @ts-ignore
import { NotificationContainer } from "react-notifications";
import { createNotification } from "src/helpers/crud-notifications";
import { userIsLoggedIn } from "src/helpers/auth-data";

function LoginPage() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isLoggedIn, setIsLoggedIn] = useState(userIsLoggedIn());

  if (isLoggedIn) {
    return <Redirect to={routes.queueService} />;
  }

  const setUserDataState = async (id: number): Promise<void> => {
    if (id != null) {
      const userData = await apiClient.get(
        routes.api.getUserData + encodeURIComponent(id)
      );

      if (userData.data == "User not found") {
        localStorage.removeItem("queue-user-data");
        localStorage.removeItem("queue-access-token");
        return window.location.replace(routes.login);
      }

      await localStorage.setItem(
        "queue-user-data",
        JSON.stringify(userData.data)
      );
      setIsLoggedIn(true);
    }
  };

  const setToken = (accessToken: string): void => {
    if (accessToken == null) {
      createNotification("error", "Неверно введён пароль или логин");
    }
    localStorage.setItem("queue-access-token", accessToken);
  };

  const fetchUserData = async (
    email: string,
    password: string
  ): Promise<void> => {
    const response = await apiClient.post(routes.login, {
      email: email,
      password: password,
    });

    setToken(response.data.accessToken);
    await setUserDataState(response.data.id);
  };

  const handleSubmitClick = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    if (email == "") {
      createNotification("warning", "Пожалуйста введите email");
      return;
    }
    if (password == "") {
      createNotification("warning", "Пожалуйста введите пароль");
      return;
    }
    fetchUserData(email, password);
  };

  const setEmailState = (event: React.ChangeEvent<HTMLInputElement>) => {
    setEmail(event.target.value);
  };

  const setPasswordState = (event: React.ChangeEvent<HTMLInputElement>) => {
    setPassword(event.target.value);
  };

  return (
    <main className={cn(classes.container)}>
      <NotificationContainer />
      <div className={cn(classes.loginFormContainer)}>
        <Typography variant="h4">Вход</Typography>
        <Typography variant="p">Пожалуйста войдите в свой аккаунт.</Typography>
        <Form onSubmit={handleSubmitClick}>
          <Form.Group controlId="formBasicEmail">
            <Typography variant="label">Email</Typography>
            <Form.Control type="email" value={email} onChange={setEmailState} />
          </Form.Group>

          <Form.Group controlId="formBasicPassword">
            <Typography variant="label">Password</Typography>
            <Form.Control
              type="password"
              value={password}
              onChange={setPasswordState}
            />
          </Form.Group>
          <Button variant="primary" type="submit">
            Войти
          </Button>
        </Form>
      </div>
    </main>
  );
}

export default LoginPage;
