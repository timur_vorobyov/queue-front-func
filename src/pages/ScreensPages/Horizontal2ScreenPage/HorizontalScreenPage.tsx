import cn from "clsx";
import classes from "./HorizontalScreenPage.css";
import React from "react";
import Typography from "src/components/Typography";
import { Col, Row } from "react-bootstrap";
import ChevronRight from "../images/ChevronRight";
import { useTypedSelector } from "src/utils/hooks/useTypedSelector";
import { Ticket } from "src/store/types/screenPageTicket";

const HorizontalScreenPage = () => {
  const { acceptedTickets } = useTypedSelector(
    (state) => state.screenPageTicket
  );

  const getEvenAcceptedTicketsCards = () => {
    return acceptedTickets
      .slice(0, 6)
      .map((acceptedTicket: Ticket, index: number) => {
        if (!acceptedTicket.isCashRegister) {
          if (index % 2 == 0) {
            return (
              <div key={index} className={cn(classes.ticketInfo)}>
                <Typography variant="span">
                  {acceptedTicket.ticketFullNumber}
                </Typography>
                <div>
                  <ChevronRight />
                </div>
                <Typography variant="span">
                  {acceptedTicket.deskNumber}
                </Typography>
              </div>
            );
          }
        }
      });
  };

  const getOddAcceptedTicketsCards = () => {
    return acceptedTickets
      .slice(0, 6)
      .map((acceptedTicket: Ticket, index: number) => {
        if (!acceptedTicket.isCashRegister) {
          if (index % 2 != 0) {
            return (
              <div key={index} className={cn(classes.ticketInfo)}>
                <Typography variant="span">
                  {acceptedTicket.ticketFullNumber}
                </Typography>
                <div>
                  <ChevronRight />
                </div>
                <Typography variant="span">
                  {acceptedTicket.deskNumber}
                </Typography>
              </div>
            );
          }
        }
      });
  };

  const getCashAcceptedTicketsCards = () => {
    return acceptedTickets
      .slice(0, 6)
      .map((acceptedTicket: Ticket, index: number) => {
        if (acceptedTicket.isCashRegister) {
          return (
            <div key={index} className={cn(classes.ticketInfo)}>
              <Typography variant="span">
                {acceptedTicket.ticketFullNumber}
              </Typography>
              <div>
                <ChevronRight />
              </div>
              <Typography variant="span">
                {acceptedTicket.deskNumber}
              </Typography>
            </div>
          );
        } else {
        }
      });
  };

  return (
    <main className={cn(classes.container)}>
      <Row className={cn(classes.containerRow)}>
        <Col xl={8} lg={6} className={cn(classes.leftSide)}>
          <h2 className={cn(classes.pageTitle)}>Навбати толор</h2>

          <Row className={cn(classes.ticketsListRow)}>
            <Col className="pr-xl-3">
              {acceptedTickets.length > 0
                ? getEvenAcceptedTicketsCards()
                : null}
            </Col>
            <Col className="pl-xl-3">
              {acceptedTickets.length > 0 ? getOddAcceptedTicketsCards() : null}
            </Col>
          </Row>
        </Col>
        <Col xl={4} lg={6} className={cn(classes.rightSide)}>
          <h2 className={cn(classes.pageTitle)}>Навбати хазина</h2>

          <Row className={cn(classes.ticketsListRow)}>
            <Col lg={12}>{getCashAcceptedTicketsCards()}</Col>
          </Row>
        </Col>
      </Row>
    </main>
  );
};

export default HorizontalScreenPage;
