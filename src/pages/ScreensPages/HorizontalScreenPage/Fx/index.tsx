import cn from "clsx";
import classes from "./Fx.css";
import React from "react";
import Typography from "src/components/Typography";
import { Col, Row, Table } from "react-bootstrap";
import ChevronRight from "../../images/ChevronRight";
import { useTypedSelector } from "src/utils/hooks/useTypedSelector";
import { Ticket } from "src/store/types/screenPageTicket";

const Fx = () => {
  const { acceptedTickets } = useTypedSelector(
    (state) => state.screenPageTicket
  );

  const getCashAcceptedTicketsCards = () => {
    return acceptedTickets
      .slice(0, 6)
      .map((acceptedTicket: Ticket, index: number) => {
        // TODO: if type of ticket == "cash"
        if (true) {
          return (
            <div key={index} className={cn(classes.ticketInfo)}>
              <Typography variant="span">
                {acceptedTicket.ticketFullNumber}
              </Typography>
              <div>
                <ChevronRight />
              </div>
              <Typography variant="span">
                {acceptedTicket.deskNumber}
              </Typography>
            </div>
          );
        } else {
        }
      });
  };

  return (
    <main className={cn(classes.container)}>
      <Row className={cn(classes.containerRow)}>
        <Col xl={8} lg={6} className={cn(classes.leftSide)}>
          <Row style={{ marginBottom: 68 }}>
            <Col>
              <h2 className={cn(classes.pageTitle)}>Асъор</h2>
            </Col>
            <Col>
              <h2 className={cn(classes.pageTitle)}>Харид</h2>
            </Col>
            <Col>
              <h2 className={cn(classes.pageTitle)}>Фуруш</h2>
            </Col>
          </Row>
          <Row style={{ marginBottom: 68 }}>
            <Col>
              <div className={classes.fxValue}>
                <svg
                  width="88"
                  height="60"
                  viewBox="0 0 88 60"
                  className="mr-4"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <g clipPath="url(#clip0_735_748)">
                    <path
                      d="M0 6.00049C0 2.68678 2.68629 0.000488281 6 0.000488281H48V36.0005H0V6.00049Z"
                      fill="#2E52B2"
                    />
                    <path
                      d="M24.0035 20.241L22.8293 23.8533H19.0312L22.1045 26.0847L20.9303 29.6967L24.0035 27.4654L27.0747 29.6967L25.9008 26.0847L28.974 23.8533H25.1757L24.0035 20.241Z"
                      fill="white"
                    />
                    <path
                      d="M11.4176 20.241L10.2433 23.8533H6.44531L9.51854 26.0847L8.34433 29.6967L11.4176 27.4654L14.4891 29.6967L13.3151 26.0847L16.3878 23.8533H12.5901L11.4176 20.241Z"
                      fill="white"
                    />
                    <path
                      d="M24.0035 6.30334L22.8293 9.91596H19.0312L22.1045 12.1476L20.9303 15.7596L24.0035 13.528L27.0747 15.7596L25.9008 12.1476L28.974 9.91596H25.1757L24.0035 6.30334Z"
                      fill="white"
                    />
                    <path
                      d="M11.4176 6.30334L10.2433 9.91596H6.44531L9.51854 12.1476L8.34433 15.7596L11.4176 13.528L14.4891 15.7596L13.3151 12.1476L16.3878 9.91596H12.5901L11.4176 6.30334Z"
                      fill="white"
                    />
                    <path
                      d="M36.5891 20.241L35.4152 23.8533H31.6172L34.6901 26.0847L33.5165 29.6967L36.5891 27.4654L39.6609 29.6967L38.4867 26.0847L41.56 23.8533H37.7619L36.5891 20.241Z"
                      fill="white"
                    />
                    <path
                      d="M36.5891 6.30334L35.4152 9.91596H31.6172L34.6901 12.1476L33.5165 15.7596L36.5891 13.528L39.6609 15.7596L38.4867 12.1476L41.56 9.91596H37.7619L36.5891 6.30334Z"
                      fill="white"
                    />
                  </g>
                  <rect
                    x="48"
                    y="24.0005"
                    width="40"
                    height="8"
                    fill="#F0F2F4"
                  />
                  <rect y="36.0005" width="88" height="12" fill="#F0F2F4" />
                  <path
                    d="M0 48H88V54C88 57.3137 85.3137 60 82 60H6C2.68629 60 0 57.3137 0 54V48Z"
                    fill="#D80027"
                  />
                  <rect
                    x="48"
                    y="24.0005"
                    width="40"
                    height="12"
                    fill="#D80027"
                  />
                  <rect
                    x="48"
                    y="12.0005"
                    width="40"
                    height="12"
                    fill="#F0F2F4"
                  />
                  <path
                    d="M48 0.000488281H82C85.3137 0.000488281 88 2.68678 88 6.00049V12.0005H48V0.000488281Z"
                    fill="#D80027"
                  />
                  <defs>
                    <clipPath id="clip0_735_748">
                      <rect width="48" height="36" fill="white" />
                    </clipPath>
                  </defs>
                </svg>
                USD
              </div>
            </Col>
            <Col>
              <div className={classes.fxValue}>942.15</div>
            </Col>
            <Col>
              <div className={classes.fxValue}>948.2</div>
            </Col>
          </Row>
          <Row style={{ marginBottom: 68 }}>
            <Col>
              <div className={classes.fxValue}>
                <svg
                  width="88"
                  height="60"
                  viewBox="0 0 88 60"
                  className="mr-4"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <g clipPath="url(#clip0_735_748)">
                    <path
                      d="M0 6.00049C0 2.68678 2.68629 0.000488281 6 0.000488281H48V36.0005H0V6.00049Z"
                      fill="#2E52B2"
                    />
                    <path
                      d="M24.0035 20.241L22.8293 23.8533H19.0312L22.1045 26.0847L20.9303 29.6967L24.0035 27.4654L27.0747 29.6967L25.9008 26.0847L28.974 23.8533H25.1757L24.0035 20.241Z"
                      fill="white"
                    />
                    <path
                      d="M11.4176 20.241L10.2433 23.8533H6.44531L9.51854 26.0847L8.34433 29.6967L11.4176 27.4654L14.4891 29.6967L13.3151 26.0847L16.3878 23.8533H12.5901L11.4176 20.241Z"
                      fill="white"
                    />
                    <path
                      d="M24.0035 6.30334L22.8293 9.91596H19.0312L22.1045 12.1476L20.9303 15.7596L24.0035 13.528L27.0747 15.7596L25.9008 12.1476L28.974 9.91596H25.1757L24.0035 6.30334Z"
                      fill="white"
                    />
                    <path
                      d="M11.4176 6.30334L10.2433 9.91596H6.44531L9.51854 12.1476L8.34433 15.7596L11.4176 13.528L14.4891 15.7596L13.3151 12.1476L16.3878 9.91596H12.5901L11.4176 6.30334Z"
                      fill="white"
                    />
                    <path
                      d="M36.5891 20.241L35.4152 23.8533H31.6172L34.6901 26.0847L33.5165 29.6967L36.5891 27.4654L39.6609 29.6967L38.4867 26.0847L41.56 23.8533H37.7619L36.5891 20.241Z"
                      fill="white"
                    />
                    <path
                      d="M36.5891 6.30334L35.4152 9.91596H31.6172L34.6901 12.1476L33.5165 15.7596L36.5891 13.528L39.6609 15.7596L38.4867 12.1476L41.56 9.91596H37.7619L36.5891 6.30334Z"
                      fill="white"
                    />
                  </g>
                  <rect
                    x="48"
                    y="24.0005"
                    width="40"
                    height="8"
                    fill="#F0F2F4"
                  />
                  <rect y="36.0005" width="88" height="12" fill="#F0F2F4" />
                  <path
                    d="M0 48H88V54C88 57.3137 85.3137 60 82 60H6C2.68629 60 0 57.3137 0 54V48Z"
                    fill="#D80027"
                  />
                  <rect
                    x="48"
                    y="24.0005"
                    width="40"
                    height="12"
                    fill="#D80027"
                  />
                  <rect
                    x="48"
                    y="12.0005"
                    width="40"
                    height="12"
                    fill="#F0F2F4"
                  />
                  <path
                    d="M48 0.000488281H82C85.3137 0.000488281 88 2.68678 88 6.00049V12.0005H48V0.000488281Z"
                    fill="#D80027"
                  />
                  <defs>
                    <clipPath id="clip0_735_748">
                      <rect width="48" height="36" fill="white" />
                    </clipPath>
                  </defs>
                </svg>
                EUR
              </div>
            </Col>
            <Col>
              <div className={classes.fxValue}>942.15</div>
            </Col>
            <Col>
              <div className={classes.fxValue}>948.2</div>
            </Col>
          </Row>
          <Row style={{ marginBottom: 68 }}>
            <Col>
              <div className={classes.fxValue}>
                <svg
                  width="88"
                  height="60"
                  viewBox="0 0 88 60"
                  className="mr-4"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <g clipPath="url(#clip0_735_748)">
                    <path
                      d="M0 6.00049C0 2.68678 2.68629 0.000488281 6 0.000488281H48V36.0005H0V6.00049Z"
                      fill="#2E52B2"
                    />
                    <path
                      d="M24.0035 20.241L22.8293 23.8533H19.0312L22.1045 26.0847L20.9303 29.6967L24.0035 27.4654L27.0747 29.6967L25.9008 26.0847L28.974 23.8533H25.1757L24.0035 20.241Z"
                      fill="white"
                    />
                    <path
                      d="M11.4176 20.241L10.2433 23.8533H6.44531L9.51854 26.0847L8.34433 29.6967L11.4176 27.4654L14.4891 29.6967L13.3151 26.0847L16.3878 23.8533H12.5901L11.4176 20.241Z"
                      fill="white"
                    />
                    <path
                      d="M24.0035 6.30334L22.8293 9.91596H19.0312L22.1045 12.1476L20.9303 15.7596L24.0035 13.528L27.0747 15.7596L25.9008 12.1476L28.974 9.91596H25.1757L24.0035 6.30334Z"
                      fill="white"
                    />
                    <path
                      d="M11.4176 6.30334L10.2433 9.91596H6.44531L9.51854 12.1476L8.34433 15.7596L11.4176 13.528L14.4891 15.7596L13.3151 12.1476L16.3878 9.91596H12.5901L11.4176 6.30334Z"
                      fill="white"
                    />
                    <path
                      d="M36.5891 20.241L35.4152 23.8533H31.6172L34.6901 26.0847L33.5165 29.6967L36.5891 27.4654L39.6609 29.6967L38.4867 26.0847L41.56 23.8533H37.7619L36.5891 20.241Z"
                      fill="white"
                    />
                    <path
                      d="M36.5891 6.30334L35.4152 9.91596H31.6172L34.6901 12.1476L33.5165 15.7596L36.5891 13.528L39.6609 15.7596L38.4867 12.1476L41.56 9.91596H37.7619L36.5891 6.30334Z"
                      fill="white"
                    />
                  </g>
                  <rect
                    x="48"
                    y="24.0005"
                    width="40"
                    height="8"
                    fill="#F0F2F4"
                  />
                  <rect y="36.0005" width="88" height="12" fill="#F0F2F4" />
                  <path
                    d="M0 48H88V54C88 57.3137 85.3137 60 82 60H6C2.68629 60 0 57.3137 0 54V48Z"
                    fill="#D80027"
                  />
                  <rect
                    x="48"
                    y="24.0005"
                    width="40"
                    height="12"
                    fill="#D80027"
                  />
                  <rect
                    x="48"
                    y="12.0005"
                    width="40"
                    height="12"
                    fill="#F0F2F4"
                  />
                  <path
                    d="M48 0.000488281H82C85.3137 0.000488281 88 2.68678 88 6.00049V12.0005H48V0.000488281Z"
                    fill="#D80027"
                  />
                  <defs>
                    <clipPath id="clip0_735_748">
                      <rect width="48" height="36" fill="white" />
                    </clipPath>
                  </defs>
                </svg>
                RUB
              </div>
            </Col>
            <Col>
              <div className={classes.fxValue}>942.15</div>
            </Col>
            <Col>
              <div className={classes.fxValue}>948.2</div>
            </Col>
          </Row>

          <Row>
            <Col>
              <h2
                className={cn(classes.pageTitle)}
                style={{ marginTop: 26, marginBottom: 68 }}
              >
                Интиқоли маблағ
              </h2>
            </Col>
          </Row>
          <Row>
            <Col>
              <div className={`d-flex align-items-center ${classes.fxValue}`}>
                <svg
                  width="88"
                  height="60"
                  viewBox="0 0 88 60"
                  className="mr-4"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    d="M0 6C0 2.68629 2.68629 0 6 0H82C85.3137 0 88 2.68629 88 6V20H0V6Z"
                    fill="#F0F2F4"
                  />
                  <rect y="20" width="88" height="20" fill="#4A89DC" />
                  <path
                    d="M0 40H88V54C88 57.3137 85.3137 60 82 60H6C2.68629 60 0 57.3137 0 54V40Z"
                    fill="#ED5565"
                  />
                </svg>
                RUB
              </div>
            </Col>
            <Col>
              <div className={classes.fxValue}>942.15</div>
            </Col>
            <Col>
              <div className={classes.fxValue}>948.2</div>
            </Col>
          </Row>
        </Col>
        <Col xl={4} lg={6} className={cn(classes.rightSide)}>
          <h2 className={`mb-0 ${cn(classes.pageTitle)}`}>Навбати хазина</h2>

          <Row className={cn(classes.ticketsListRow)}>
            <Col lg={12}>{getCashAcceptedTicketsCards()}</Col>
          </Row>
        </Col>
      </Row>
    </main>
  );
};

export default Fx;
