import { useEffect, useState } from "react";
import cn from "clsx";
import classes from "./HorizontalScreenPage.css";
import React from "react";
import Typography from "src/components/Typography";
import { Col, Row } from "react-bootstrap";
import RightArrow from "../images/RightArrow";
import { useTypedSelector } from "src/utils/hooks/useTypedSelector";
import { Ticket } from "src/store/types/screenPageTicket";

const HorizontalScreenPage = () => {
  const today = new Date();
  const [currentTime, setCurrentTime] = useState(
    today.getHours() + ":" + ("0" + today.getMinutes()).slice(-2)
  );
  const { incomingTickets, acceptedTickets } = useTypedSelector(
    (state) => state.screenPageTicket
  );
  useEffect(() => {
    const interval = setInterval(() => {
      run();
    }, 1000);

    return () => clearInterval(interval);
  }, []);

  const getIncomingTicketsCards = (): JSX.Element[] => {
    return incomingTickets
      .slice(0, 9)
      .map((incomingTicket: Ticket, index: number) => {
        return (
          <div key={index} className={cn(classes.ticketCard)}>
            <Typography variant="span">
              {incomingTicket.ticketFullNumber}
            </Typography>
          </div>
        );
      });
  };

  const getEvenAcceptedTicketsCards = () => {
    return acceptedTickets
      .slice(0, 6)
      .map((acceptedTicket: Ticket, index: number) => {
        if (index % 2 == 0) {
          return (
            <div key={index} className={cn(classes.ticketInfo)}>
              <Typography variant="span">
                {acceptedTicket.ticketFullNumber}
              </Typography>
              <div className={cn(classes.arrow)}>
                <RightArrow />
              </div>
              <Typography variant="span">
                {acceptedTicket.deskNumber}
              </Typography>
            </div>
          );
        } else {
          return null;
        }
      });
  };

  const getOddAcceptedTicketsCards = () => {
    return acceptedTickets
      .slice(0, 6)
      .map((acceptedTicket: Ticket, index: number) => {
        if (index % 2 != 0) {
          return (
            <div key={index} className={cn(classes.ticketInfo)}>
              <Typography variant="span">
                {acceptedTicket.ticketFullNumber}
              </Typography>
              <div className={cn(classes.arrow)}>
                <RightArrow />
              </div>
              <Typography variant="span">
                {acceptedTicket.deskNumber}
              </Typography>
            </div>
          );
        } else {
          return null;
        }
      });
  };

  const run = () => {
    const today = new Date();
    const time = today.getHours() + ":" + ("0" + today.getMinutes()).slice(-2);
    setCurrentTime(time);
  };

  return (
    <main className={cn(classes.container)}>
      <Row className={cn(classes.containerRow)}>
        <Col lg={10} className={cn(classes.leftSide)}>
          <div className={cn(classes.leftSideHeader)}>
            <Typography fontWeight="bold" variant="h1">
              Навбати электронӣ
            </Typography>
            <Typography fontWeight="bold" variant="h1">
              {currentTime}
            </Typography>
          </div>
          <Row className={cn(classes.ticketsListRow)}>
            <Col lg={6}>
              <div className={cn(classes.ticketsListHeader)}>
                <span className={cn(classes.ticketListHeader)}>Мизоҷ</span>
                <span className={cn(classes.ticketListHeader)}>Миз</span>
              </div>
              {acceptedTickets.length > 0
                ? getEvenAcceptedTicketsCards()
                : null}
            </Col>
            <Col lg={6}>
              <div className={cn(classes.ticketsListHeader)}>
                <span className={cn(classes.ticketListHeader)}>Мизоҷ</span>
                <span className={cn(classes.ticketListHeader)}>Миз</span>
              </div>
              {acceptedTickets.length > 0 ? getOddAcceptedTicketsCards() : null}
            </Col>
          </Row>
        </Col>
        <Col lg={2} className={cn(classes.rightSide)}>
          <div className={cn(classes.rightSideHeader)}>
            <Typography fontWeight="bold" color="black" variant="h3">
              Дар навбат
            </Typography>
          </div>
          {getIncomingTicketsCards()}
        </Col>
      </Row>
    </main>
  );
};

export default HorizontalScreenPage;
