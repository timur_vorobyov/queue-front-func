import { PureComponent } from "react";
import classes from "./ScreensPages.css";
import cn from "clsx";
import React from "react";
import Typography from "src/components/Typography";
import { Button, Form } from "react-bootstrap";
import ScreensPagesService, {
  BranchOffice,
  BranchOffices,
  Town,
  Towns,
  VisitPurpose,
  VisitPurposes,
} from "src/services/ScreensPagesService/ScreensPagesService";
import { Redirect } from "react-router-dom";
// @ts-ignore
import Select from "react-select";
interface ScreensPageStates {
  townId: string;
  branchOfficeId: string;
  screenName: string;
  branchOfficeOptions: BranchOffices;
  townOptions: Towns;
  visitPurposeOptions: VisitPurposes;
  redirect: string;
  visitPurposesIds: number[];
}

class ScreensPages extends PureComponent<{}, ScreensPageStates> {
  private service: ScreensPagesService;

  constructor(props: {}) {
    super(props);
    this.state = {
      townId: "",
      branchOfficeId: "",
      screenName: "",
      branchOfficeOptions: [{ id: 1, branchOfficeName: "" }],
      townOptions: [{ id: 1, name: "" }],
      redirect: "",
      visitPurposeOptions: [{ id: 0, name: "" }],
      visitPurposesIds: [],
    };
    this.service = new ScreensPagesService();
  }

  componentDidMount(): void {
    this.setTownList();
  }

  private async setTownList() {
    const townList = await this.service.getTownList();
    await this.setState({ townOptions: townList });
  }

  private getTownList() {
    return this.state.townOptions.map((town: Town, index: number) => {
      return (
        <option key={index} value={town.id}>
          {town.name}
        </option>
      );
    });
  }

  private async setBranchOfficeList(townId: string) {
    const branchOfficeList = await this.service.getBranchOfficeList(townId);
    await this.setState({ branchOfficeOptions: branchOfficeList });
  }

  private getBranchOfficeList() {
    return this.state.branchOfficeOptions.map(
      (office: BranchOffice, index: number) => {
        return (
          <option key={index} value={office.id}>
            {office.branchOfficeName}
          </option>
        );
      }
    );
  }

  handleButtonClick = async (
    event: React.MouseEvent<HTMLElement, MouseEvent>
  ) => {
    event.preventDefault();

    if (this.state.screenName == "tv") {
      this.setState({
        redirect: `/screen/tv?branchOfficeId=${this.state.branchOfficeId}&visitPurposesIds=${this.state.visitPurposesIds}`,
      });
      return;
    }

    if (this.state.screenName == "led") {
      this.setState({
        redirect: `/screen/led?branchOfficeId=${this.state.branchOfficeId}&visitPurposesIds=${this.state.visitPurposesIds}`,
      });
      return;
    }

    if (this.state.screenName == "led white") {
      this.setState({
        redirect: `/screen/led-white?branchOfficeId=${this.state.branchOfficeId}&visitPurposesIds=${this.state.visitPurposesIds}`,
      });
      return;
    }

    if (this.state.screenName == "new tv") {
      this.setState({
        redirect: `/screen/tv-new?branchOfficeId=${this.state.branchOfficeId}&visitPurposesIds=${this.state.visitPurposesIds}`,
      });
      return;
    }
  };

  setTownId = (event: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({ townId: event.target.value });
    this.setBranchOfficeList(event.target.value);
    const element = document.getElementById("town-option-clue");
    if (element != null) {
      element.remove();
    }
  };

  setBranchOfficeId = (event: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({ branchOfficeId: event.target.value });
    this.setVisitPurposeList(event.target.value);
    const element = document.getElementById("office-option-clue");
    if (this.getBranchOfficeList.length > 2 && element != null) {
      element.remove();
    }
  };

  async setVisitPurposeList(branchOfficeId: string): Promise<void> {
    const visitPurposesList = await this.service.getVisitPurposeList(
      Number(branchOfficeId)
    );
    await this.setState({ visitPurposeOptions: visitPurposesList });
  }

  setScreenName = (event: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({ screenName: event.target.value });
    const element = document.getElementById("screen-option-clue");
    if (element != null) {
      element.remove();
    }
  };

  setVisitPurposesIds = async (
    selectedVisitPurpose: [{ value: number }]
  ): Promise<void> => {
    const options = selectedVisitPurpose;
    const selectedOptions = [];

    for (let i = 0; i < options.length; i++) {
      selectedOptions.push(options[i].value);
    }

    await this.setState({ visitPurposesIds: selectedOptions });
  };

  getVisitPurposesOptions(): any {
    const options = this.state.visitPurposeOptions.map(
      (visitPurpose: VisitPurpose) => {
        return { value: visitPurpose.id, label: visitPurpose.name };
      }
    );
    return options;
  }

  render(): React.ReactNode {
    if (this.state.redirect) {
      return <Redirect to={this.state.redirect} />;
    }
    return (
      <main className={cn(classes.container)}>
        <div className={cn(classes.branchOfficeOptions)}>
          <div className={cn(classes.item)}>
            <Typography fontWeight="bold" variant="h5">
              Шаг 1. Выберите город
            </Typography>
            <Form.Control
              className={cn(classes.itemsList)}
              as="select"
              value={this.state.townId}
              onChange={this.setTownId}
              custom
            >
              <option id="town-option-clue">Пожалуйста выберите город</option>
              {this.getTownList()}
            </Form.Control>
          </div>
          <div className={cn(classes.item)}>
            <Typography fontWeight="bold" variant="h5">
              Шаг 2. Выберите филиал
            </Typography>
            <Form.Control
              className={cn(classes.itemsList)}
              as="select"
              value={this.state.branchOfficeId}
              onChange={this.setBranchOfficeId}
              custom
            >
              <option id="office-option-clue">
                Пожалуйста выберите филиал
              </option>
              {this.getBranchOfficeList()}
            </Form.Control>
          </div>
          <div className={cn(classes.item)}>
            <Typography fontWeight="bold" variant="h5">
              Шаг 3. Выберите экран
            </Typography>
            <Form.Control
              className={cn(classes.itemsList)}
              as="select"
              value={this.state.screenName}
              onChange={this.setScreenName}
              custom
            >
              <option id="screen-option-clue">Пожалуйста выберите экран</option>
              <option>tv</option>
              <option>led</option>
              <option>led white</option>
              <option>new tv</option>
            </Form.Control>
          </div>
          <div className={cn(classes.item)}>
            <Typography fontWeight="bold" variant="h5">
              Шаг 4. Выберите цель виизита
            </Typography>
            <Form.Group className={cn(classes.itemsList)}>
              <Select
                options={this.getVisitPurposesOptions()}
                isMulti
                onChange={this.setVisitPurposesIds}
              />
            </Form.Group>
          </div>
          <div>
            <Button
              className={cn(classes.button)}
              variant="primary"
              type="button"
              onClick={this.handleButtonClick}
            >
              Перейти к экрану
            </Button>
          </div>
        </div>
      </main>
    );
  }
}

export default ScreensPages;
