import { useEffect } from "react";

import { useActions } from "src/utils/hooks/useActions";
import echo from "laravel-echo";
import { createEchoInstance } from "src/helpers/echo";
import useMultipleApiCalls from "src/utils/hooks/use-async-multuple-call";
import {
  getAcceptedTickets,
  getIncomingTickets,
} from "src/apis/ScreenPage/ticket";
import tune from "./audio/queueSound.mp3";
import { Ticket } from "src/store/types/screenPageTicket";
import { useLocation } from "react-router-dom";

type SubscriptionsManagerProps = {
  visitPurposesIds: string[];
  branchOfficeId: string;
};

const SubscriptionsManager = ({
  visitPurposesIds,
  branchOfficeId,
}: SubscriptionsManagerProps) => {
  const {
    setScreenPageIncomingTickets,
    setScreenPageAcceptedTickets,
    setScreenPageRecoveredTicket,
    setScreenPageInitialData,
  } = useActions();
  const [
    [incomingTicketsList, acceptedTicketsList],
    setApiCallData,
  ] = useMultipleApiCalls(
    [getIncomingTickets, getAcceptedTickets],
    [
      [branchOfficeId, visitPurposesIds],
      [branchOfficeId, visitPurposesIds],
    ]
  );
  let echo: echo;
  const location = useLocation();

  useEffect(() => {
    setApiCallData();
  }, [location]);

  useEffect(() => {
    if (incomingTicketsList && acceptedTicketsList) {
      setScreenPageInitialData({
        incomingTickets: incomingTicketsList,
        acceptedTickets: acceptedTicketsList,
      });
    }
  }, [incomingTicketsList, acceptedTicketsList]);

  useEffect(() => {
    connectEcho();
    listenTicketsEvent();
    return () => disconnectEcho();
  }, []);

  const connectEcho = (): void => {
    echo = createEchoInstance();
  };

  const disconnectEcho = (): void => {
    echo.disconnect();
  };

  const listenTicketsEvent = (): void => {
    visitPurposesIds.map((visitPurposeId: string) => {
      echo
        .channel(
          "ticket." + branchOfficeId + "." + visitPurposeId + ".pending.recover"
        )
        .listen(
          ".ticket-pending",
          (data: { shortTicketPending: Ticket }): void => {
            setScreenPageRecoveredTicket(data.shortTicketPending);
          }
        );

      echo
        .channel(
          "ticket." + branchOfficeId + "." + visitPurposeId + ".pending.add"
        )
        .listen(
          ".ticket-pending",
          (data: { shortTicketPending: Ticket }): void => {
            setScreenPageIncomingTickets(data.shortTicketPending);
          }
        );
      echo
        .channel(
          "ticket.all." +
            branchOfficeId +
            "." +
            visitPurposeId +
            ".accepted.add"
        )
        .listen(
          ".ticket-accepted",
          (data: { acceptedTicket: Ticket }): void => {
            const audio = new Audio(tune);
            audio.play();
            setScreenPageAcceptedTickets(data.acceptedTicket);
          }
        );
      echo
        .channel(
          "ticket." + branchOfficeId + "." + visitPurposeId + ".pending.delete"
        )
        .listen(
          ".ticket-pending",
          (data: { shortTicketPending: Ticket }): void => {
            setScreenPageIncomingTickets(data.shortTicketPending);
          }
        );
      echo
        .channel(
          "ticket.all." +
            branchOfficeId +
            "." +
            visitPurposeId +
            ".accepted.delete"
        )
        .listen(
          ".ticket-accepted",
          (data: { acceptedTicket: Ticket }): void => {
            setScreenPageAcceptedTickets(data.acceptedTicket);
          }
        );
    });
  };

  return null;
};

export default SubscriptionsManager;
