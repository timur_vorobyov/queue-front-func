import cn from "clsx";
import classes from "./VerticalScreenPage.css";
import React from "react";
import Typography from "src/components/Typography";
import RightArrow from "../images/RightArrow";
import { Ticket } from "src/store/types/screenPageTicket";
import { useTypedSelector } from "src/utils/hooks/useTypedSelector";

const VerticalScreenPage = () => {
  const { incomingTickets, acceptedTickets } = useTypedSelector(
    (state) => state.screenPageTicket
  );
  const location = window.location.pathname;

  const getIncomingTicketsCards = (): JSX.Element[] => {
    return incomingTickets
      .slice(0, 9)
      .map((incomingTicket: Ticket, index: number) => {
        return (
          <div key={index} className={cn(classes.ticketCard)}>
            <span
              className={cn([
                location == "/screen/led-white" ? classes.black : classes.white,
                classes.tvTicketsPendingData,
              ])}
            >
              {index + 1 == incomingTickets.length
                ? incomingTicket.ticketFullNumber
                : incomingTicket.ticketFullNumber + ", "}
            </span>
          </div>
        );
      });
  };

  const getAcceptedTicketsCards = () => {
    return acceptedTickets.map((acceptedTicket: Ticket, index: number) => {
      return (
        <div
          key={index}
          className={cn(
            location == "/screen/led-white"
              ? classes.whiteThemeTicketInfo
              : classes.ticketInfo
          )}
        >
          <span
            className={cn([
              location == "/screen/led-white" ? classes.black : classes.white,
              classes.tvAcceptedTicketsData,
            ])}
          >
            {acceptedTicket.ticketFullNumber}
          </span>
          <div className={cn(classes.arrow)}>
            <RightArrow />
          </div>
          <span
            className={cn([
              location == "/screen/led-white" ? classes.black : classes.white,
              classes.tvAcceptedTicketsData,
            ])}
          >
            {acceptedTicket.deskNumber}
          </span>
        </div>
      );
    });
  };

  return (
    <main
      className={cn(
        location == "/screen/led-white"
          ? classes.whiteThemeContainer
          : classes.container
      )}
    >
      <div className={cn(classes.content)}>
        <div>
          <div className={cn(classes.ticketsListRow)}>
            <div className={cn(classes.ticketsListHeader)}>
              <Typography
                variant="span"
                color={location == "/screen/led-white" ? "black" : "white"}
                className="white"
              >
                Мизоҷ
              </Typography>
              <Typography
                variant="span"
                color={location == "/screen/led-white" ? "black" : "white"}
                className="white"
              >
                Миз
              </Typography>
            </div>
            {acceptedTickets.length > 0 ? getAcceptedTicketsCards() : null}
          </div>
        </div>
        <div>
          <div
            className={cn(
              location == "/screen/led-white"
                ? classes.whiteThemeBottomSide
                : classes.bottomSide
            )}
          >
            <span
              className={cn([
                location == "/screen/led-white" ? classes.black : classes.white,
                classes.tvTicketsPendingDataHeader,
              ])}
            >
              ОЯНДА &nbsp;
            </span>
            {incomingTickets.length > 0 ? getIncomingTicketsCards() : null}
          </div>
        </div>
      </div>
    </main>
  );
};

export default VerticalScreenPage;
