import React from "react";

const ChevronRight = () => {
  return (
    <svg
      width="64"
      height="64"
      fill="none"
      viewBox="0 0 64 64"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M21 53L42 32L21 11"
        stroke="#39B980"
        strokeWidth="8"
        strokeLinejoin="round"
      />
    </svg>
  );
};

export default ChevronRight;
