import React, { useContext, useEffect } from "react";
import cn from "clsx";
import classes from "./CreateTicketPage.css";
import { Button } from "react-bootstrap";
import logImg from "src/pages/UserPage/CreateTicketPages/images/logo-alif.svg";
import tjFlag from "src/pages/UserPage/CreateTicketPages/images/tj-flag.svg";
import ruFlag from "src/pages/UserPage/CreateTicketPages/images/ru-flag.svg";
// @ts-ignore
import { NotificationContainer } from "react-notifications";
import translate from "src/i18n/translate";
import { LOCALES } from "src/i18n";
import { I18nContext } from "src/contexts/i18nContextProvider/I18nContextProvider";
import { useHistory } from "react-router-dom";
import { routes } from "src/config/routes";
import { CreateTicketPagesContext } from "src/contexts/CreateTicketPagesContext/CreateTicketPagesContextProvider";

const CreateTicketPage = () => {
  const { locale, setLocale } = useContext(I18nContext);
  const language = {
    ru: "Русский",
    tj: "Тоҷикӣ",
  };
  const history = useHistory();
  const {
    setCustomerPhoneNumber,
    setIsWithoutCustomerPhoneNumber,
    setVisitPurposeId,
    setIsRedirectedToHomePage,
  } = useContext(CreateTicketPagesContext);

  useEffect(() => {
    setCustomerPhoneNumber("");
    setIsWithoutCustomerPhoneNumber(false);
    setVisitPurposeId(0);
    setIsRedirectedToHomePage(false);
  }, []);

  const getDesiredLocaleButton = () => {
    if (locale == LOCALES.TAJIK) {
      return (
        <button
          className={cn("btn", classes.btnTranslator)}
          onClick={() => setLocale(LOCALES.RUSSIAN)}
        >
          <div className={cn(classes.buttonContent)}>
            <img className={cn(classes.ruFlag)} src={ruFlag} />
            <span className={cn(classes.btnTranslatorText)}>{language.ru}</span>
          </div>
        </button>
      );
    } else {
      return (
        <button
          className={cn("btn", classes.btnTranslator)}
          onClick={() => setLocale(LOCALES.TAJIK)}
        >
          <div className={cn(classes.buttonContent)}>
            <img className={cn(classes.tjFlag)} src={tjFlag} />
            <span className={cn(classes.btnTranslatorText)}>{language.tj}</span>
          </div>
        </button>
      );
    }
  };

  return (
    <main className={cn(classes.container)}>
      <NotificationContainer />
      <div className={cn(classes.description)}>
        <img className={cn(classes.logo)} src={logImg} />
        <span className={cn(classes.header)}>{translate("welcome")}</span>
        <span className={cn(classes.header)}>
          {translate("pushToGetOrder")}
        </span>
      </div>
      <div className={cn(classes.actions)}>
        <Button
          variant="secondary"
          type="button"
          onClick={() => history.push(routes.ticketVisitPurpose)}
          className={cn(classes.getTicketButton)}
        >
          <span className={cn(classes.btnText)}>{translate("getOrder")}</span>
        </Button>
        {getDesiredLocaleButton()}
      </div>
    </main>
  );
};

export default CreateTicketPage;
