import React, { useContext, useEffect, useState } from "react";
import { Alert, Button } from "react-bootstrap";
import { Redirect, useHistory } from "react-router-dom";
import Typography from "src/components/Typography";
import cn from "clsx";
import classes from "./CustomerPhoneNumberPage.css";
import { routes } from "src/config/routes";
import BackButton from "../images/BackButton";
import SentButton from "../images/SentButton";
import translate from "src/i18n/translate";
import { CreateTicketPagesContext } from "src/contexts/CreateTicketPagesContext/CreateTicketPagesContextProvider";
import Keyboard from "./Keyboard";
import KeyInput from "./KeyInput";

const CustomerPhoneNumberPage = () => {
  const {
    customerPhoneNumber,
    setCustomerPhoneNumber,
    visitPurposeId,
    setVisitPurposeId,
    setIsWithoutCustomerPhoneNumber,
    isWithoutCustomerPhoneNumber,
    isRedirectedToHomePage,
  } = useContext(CreateTicketPagesContext);
  const history = useHistory();
  const backUrl = routes.ticketVisitPurpose;
  const [isAlertShown, setIsAlertShown] = useState(false);
  const [isDisabled, setIsDisabled] = useState(true);
  const [customerPhoneNumberState, setCustomerPhoneNumberState] = useState("");
  const customerPhoneNumberWithoutSpace = () => {
    return customerPhoneNumberState.replace(/\s+/g, "");
  };

  useEffect(() => {
    if (!visitPurposeId) {
      history.push(backUrl);
    } else {
      const timer = setTimeout(() => {
        history.push(backUrl);
      }, 60000);

      return () => clearTimeout(timer);
    }
  }, [visitPurposeId]);

  useEffect(() => {
    customerPhoneNumberWithoutSpace().length === 9
      ? setIsDisabled(false)
      : setIsDisabled(true);
  }, [customerPhoneNumberState]);

  const handleAddNumberClick = (number: string) => {
    if (customerPhoneNumberWithoutSpace().length < 9) {
      /**it is impossible to set the first number the same as the non-editable characters in the mask, without space ' ' before the first number*/
      const updatedCustomerPhoneNumber =
        customerPhoneNumberState === ""
          ? " " + number
          : customerPhoneNumberState + number;
      setCustomerPhoneNumberState(updatedCustomerPhoneNumber);
    }
  };

  const handleRemoveNumberClick = () => {
    const updatedCustomerPhoneNumber = customerPhoneNumberState.slice(0, -1);
    setCustomerPhoneNumberState(updatedCustomerPhoneNumber);
  };

  const handleGetTicketButtonClick = () => {
    if (
      customerPhoneNumberWithoutSpace().length !== 9 &&
      customerPhoneNumberWithoutSpace().length !== 0
    ) {
      setIsAlertShown(true);
    } else {
      customerPhoneNumberWithoutSpace().length === 0
        ? setIsWithoutCustomerPhoneNumber(true)
        : setCustomerPhoneNumber(customerPhoneNumberWithoutSpace());
    }
  };

  if (isRedirectedToHomePage) {
    return <Redirect to={routes.registerTicket} />;
  } else {
    if (customerPhoneNumber || isWithoutCustomerPhoneNumber) {
      return <Redirect to={routes.showTicket} />;
    } else {
      return (
        <main className={cn(classes.container)}>
          <div className={cn(classes.headersContainer)}>
            <span className={cn(classes.header)}>
              {translate("inputPhoneNumber")}
            </span>
            <span className={cn(classes.header, classes.subheader)}>
              {translate("youWillReceiveTheMessage")}
            </span>
          </div>
          <Alert show={isAlertShown} variant="danger">
            <Typography variant="p">
              {translate("enterYourNumberInFull")}
            </Typography>
          </Alert>
          <KeyInput
            customerPhoneNumberState={customerPhoneNumberState}
            setCustomerPhoneNumberState={setCustomerPhoneNumberState}
            handleRemoveNumberClick={handleRemoveNumberClick}
          />
          <Keyboard handleAddNumberClick={handleAddNumberClick} />
          <div className={cn(classes.redirectButtons)}>
            <button
              onClick={() => setVisitPurposeId(0)}
              className={cn(classes.redirectButton, "btn")}
            >
              {BackButton()}
              <span className={cn(classes.backRedirectButtonText)}>
                {translate("back")}
              </span>
            </button>
            <button
              onClick={handleGetTicketButtonClick}
              className={cn(classes.redirectButton, "btn")}
              disabled={isDisabled}
            >
              {SentButton()}
              <span className={cn(classes.sentButtonText)}>
                {translate("send")}
              </span>
            </button>
          </div>
          <Button
            type="button"
            variant="link"
            onClick={handleGetTicketButtonClick}
            className={cn(classes.redirectButton)}
          >
            <span>{translate("phoneNumberNotExist")}</span>
          </Button>
        </main>
      );
    }
  }
};

export default CustomerPhoneNumberPage;
