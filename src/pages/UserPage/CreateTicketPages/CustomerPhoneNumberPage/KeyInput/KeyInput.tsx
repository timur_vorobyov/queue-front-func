import React from "react";
// @ts-ignore
import InputMask from "react-input-mask";
import cn from "clsx";
import classes from "./KeyInput.css";
import removeNumberImg from "src/pages/UserPage/CreateTicketPages/images/remove-number.svg";

interface KeyInputProps {
  customerPhoneNumberState: string;
  setCustomerPhoneNumberState: (value: string) => void;
  handleRemoveNumberClick: () => void;
}

const KeyInput = ({
  customerPhoneNumberState,
  setCustomerPhoneNumberState,
  handleRemoveNumberClick,
}: KeyInputProps) => {
  return (
    <div className={cn(classes.container)}>
      <InputMask
        mask="(+\9\92) 999 999 999"
        maskChar=""
        alwaysShowMask={true}
        value={customerPhoneNumberState}
        className={cn(classes.keyInput)}
        onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
          setCustomerPhoneNumberState(event.target.value.slice(6))
        }
      />
      <button
        type={"button"}
        className={cn(classes.removeNumberButton)}
        onClick={handleRemoveNumberClick}
      >
        <img src={removeNumberImg} className={cn(classes.removeNumberImage)} />
      </button>
    </div>
  );
};

export default KeyInput;
