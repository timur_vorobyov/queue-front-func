import React from "react";
import cn from "clsx";
import classes from "./KeyButton.css";
import { Button } from "react-bootstrap";
import Typography from "src/components/Typography";

interface KeyButtonProps {
  handleAddNumberClick: (number: string) => void;
  number: string;
}

const KeyButton = ({ handleAddNumberClick, number }: KeyButtonProps) => {
  return (
    <Button
      className={cn(classes.customerPhoneNumberButton)}
      type="button"
      variant="info"
      onClick={() => handleAddNumberClick(number)}
    >
      <Typography variant="span">{number}</Typography>
    </Button>
  );
};

export default KeyButton;
