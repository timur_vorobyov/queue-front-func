import React from "react";
import cn from "clsx";
import classes from "./Keyboard.css";
import KeyButton from "./KeyButton";

interface KeyboardProps {
  handleAddNumberClick: (number: string) => void;
}

const Keyboard = ({ handleAddNumberClick }: KeyboardProps) => {
  const getKeyNumbersRaw = (generatedKeysAmount: number) => {
    const numberKeys: string[] = [];

    if (!generatedKeysAmount) {
      numberKeys.push("0");
    } else {
      for (let i = generatedKeysAmount - 2; i <= generatedKeysAmount; i++) {
        numberKeys.push(i.toString());
      }
    }

    return numberKeys.map((numberKey, index) => {
      return (
        <KeyButton
          key={index}
          handleAddNumberClick={handleAddNumberClick}
          number={numberKey}
        />
      );
    });
  };

  return (
    <div className={cn(classes.container)}>
      <div>{getKeyNumbersRaw(3)}</div>
      <div>{getKeyNumbersRaw(6)}</div>
      <div>{getKeyNumbersRaw(9)}</div>
      <div className="text-center">{getKeyNumbersRaw(0)}</div>
    </div>
  );
};

export default Keyboard;
