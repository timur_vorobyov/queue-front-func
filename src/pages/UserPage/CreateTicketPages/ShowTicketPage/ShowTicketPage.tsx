import React, { useContext, useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import cn from "clsx";
import classes from "./ShowTicketPage.css";
import { routes } from "src/config/routes";
import ticketSsavedImg from "src/pages/UserPage/CreateTicketPages/images/ticket-saved.svg";
import translate from "src/i18n/translate";
import { createTicket } from "src/apis/CreateTicketPage/ticket";
import { CreateTicketPagesContext } from "src/contexts/CreateTicketPagesContext/CreateTicketPagesContextProvider";

const ShowTicketPage = () => {
  const [ticketFullNumber, setTicketFullNumber] = useState("");
  const history = useHistory();
  const {
    visitPurposeId,
    customerPhoneNumber,
    subVisitPurposeId,
    isWithoutCustomerPhoneNumber,
    setIsRedirectedToHomePage,
  } = useContext(CreateTicketPagesContext);

  useEffect(() => {
    (async () => {
      setIsRedirectedToHomePage(true);
      if (
        !visitPurposeId ||
        (!customerPhoneNumber && !isWithoutCustomerPhoneNumber)
      ) {
        history.push(routes.registerTicket);
        return;
      }

      const ticketFullNumber = await createTicket(
        visitPurposeId,
        customerPhoneNumber,
        subVisitPurposeId
      );

      setTicketFullNumber(ticketFullNumber);
    })();
  }, []);

  useEffect(() => {
    if (ticketFullNumber) {
      const timer = setTimeout(() => {
        history.push(routes.registerTicket);
      }, 3000);

      return () => clearTimeout(timer);
    }
  }, [ticketFullNumber]);

  return ticketFullNumber ? (
    <main className={cn(classes.container)}>
      <div className={cn(classes.ticket)}>
        <div className={cn(classes.ticketContent)}>
          <img className={cn(classes.checkMark)} src={ticketSsavedImg} />
          <span className={cn(classes.header)}>{translate("thank")}</span>
          <div className={cn(classes.ticketInfoHeader)}>
            <span className={cn(classes.subheader)}>
              {translate("messageWasSentOnYourPhone")}
            </span>
            <span className={cn(classes.subheader)}>
              {translate("checkYourPhone")}
            </span>
          </div>
          <div className={cn(classes.ticketInfoContent)}>
            <span className={cn(classes.ticketInfo)}>
              {translate("yourPhoneNumberIs")}
            </span>
            <div className={cn(classes.ticketInfo)}>
              <span className={cn(classes.ticketNumber)}>
                {ticketFullNumber}
              </span>
            </div>
            <span className={cn(classes.ticketInfo)}>
              {translate("notForget")}
            </span>
          </div>
        </div>
        <div className={cn(classes.ticketFooter)}></div>
      </div>
    </main>
  ) : null;
};

export default ShowTicketPage;
