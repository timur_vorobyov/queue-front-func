import React, { useContext, useEffect, useState } from "react";
import cn from "clsx";
import classes from "./TicketVisitPurposePage.css";
import { Button } from "react-bootstrap";
import { routes } from "src/config/routes";
import translate from "src/i18n/translate";
import { CreateTicketPagesContext } from "src/contexts/CreateTicketPagesContext/CreateTicketPagesContextProvider";
import { useHistory } from "react-router-dom";
import { branchOfficeFullName } from "src/helpers/auth-data";
import useApiCall from "src/utils/hooks/use-async-call";
import { getVisitPurposeList } from "src/apis/CreateTicketPage/visitPurpose";
import {
  SubVisitPurpose,
  VisitPurpose,
} from "src/types/UserPageTypes/CreateTicketPageTypes/visitPurpose";

const TicketVisitPurposePage = () => {
  const userJsonData = localStorage.getItem("queue-user-data");
  const userData = JSON.parse(userJsonData ? userJsonData : " ");
  const branchOfficeId = userData.branchOfficeId;
  const [visitPurposesList, setVisitPurposesList] = useState([]);
  const [visitPurposes, setApiCallVisitPurposes] = useApiCall(
    getVisitPurposeList,
    [Number(branchOfficeId)]
  );
  const { setVisitPurposeId, visitPurposeId, setSubVisitPurposeId } =
    useContext(CreateTicketPagesContext);
  const [subVisitPurposes, setSubVisitPurposes] = useState<SubVisitPurpose[]>(
    []
  );
  const history = useHistory();
  const backUrl = routes.registerTicket;

  useEffect(() => {
    if (visitPurposeId) {
      history.push(routes.ticketCustomerPhoneNumber);
    } else {
      const timer = setTimeout(() => {
        history.push(backUrl);
      }, 60000);

      return () => clearTimeout(timer);
    }
  }, [visitPurposeId]);

  useEffect(() => {
    setApiCallVisitPurposes();
  }, [branchOfficeId]);

  useEffect(() => {
    if (visitPurposes.length) {
      setVisitPurposesList(visitPurposes);
    }
  }, [visitPurposes]);

  const handleButtonClick = (visitPurposeId: number) => {
    const visitPurpose: VisitPurpose = visitPurposes.find(
      (visitPurpose: VisitPurpose) => visitPurpose.id === visitPurposeId
    );
    visitPurpose.subVisitPurposes.length
      ? setSubVisitPurposes(visitPurpose.subVisitPurposes)
      : setVisitPurposeId(visitPurposeId);
  };

  const getButtonsList = () => {
    if (subVisitPurposes.length) {
      return subVisitPurposes.map((subVisitPurpose: SubVisitPurpose) => {
        return (
          <Button
            variant="secondary"
            key={subVisitPurpose.id}
            className={cn(classes.visitPurposeButton)}
            type="button"
            onClick={() => {
              setVisitPurposeId(subVisitPurpose.visitPurposeId);
              setSubVisitPurposeId(subVisitPurpose.id);
            }}
          >
            {subVisitPurpose.name}
          </Button>
        );
      });
    } else {
      return visitPurposesList.map(
        (visitPurpose: VisitPurpose, index: number) => {
          return (
            <Button
              variant="secondary"
              key={index}
              className={cn(classes.visitPurposeButton)}
              type="button"
              onClick={() => handleButtonClick(visitPurpose.id)}
            >
              {visitPurpose.name}
            </Button>
          );
        }
      );
    }
  };

  return (
    <main className={cn(classes.container)}>
      <div className={cn(classes.visitPurposesContainer)}>
        <span className={cn(classes.header)}>
          {subVisitPurposes.length
            ? translate("yourSubVisitPurpose")
            : translate("yourVisitPurpose")}
        </span>
        <span className={cn(classes.header, classes.branchOffice)}>
          {branchOfficeFullName()}
        </span>
        {getButtonsList()}
      </div>
      <div className={cn(classes.redirectButtonContainer)}>
        <Button
          type="button"
          variant="link"
          onClick={() => history.push(routes.registerTicket)}
          className={cn(classes.redirectButton)}
        >
          <span className={cn(classes.redirectButtonText)}>
            {translate("backToHome")}
          </span>
        </Button>
      </div>
    </main>
  );
};

export default TicketVisitPurposePage;
