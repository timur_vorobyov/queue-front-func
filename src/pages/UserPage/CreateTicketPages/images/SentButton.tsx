import React from "react";
import cn from "clsx";
import classes from "../CustomerPhoneNumberPage/CustomerPhoneNumberPage.css";

const SentButton = () => {
  return (
    <svg
      width="207"
      height="96"
      viewBox="0 0 207 96"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        className={cn(classes.sentButtonImg)}
        d="M0 10C0 4.47715 4.47715 0 10 0H160.743C163.47 0 166.079 1.1139 167.966 3.08366L203.516 40.2079C207.163 44.016 207.226 50.0011 203.661 53.8857L167.985 92.7614C166.091 94.8252 163.418 96 160.617 96H10C4.47715 96 0 91.5229 0 86V10Z"
        fill="#39B980"
      />
    </svg>
  );
};

export default SentButton;
