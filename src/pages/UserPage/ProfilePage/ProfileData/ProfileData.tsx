import React from "react";
import cn from "clsx";
import classes from "./ProfileData.css";
import Typography from "src/components/Typography";
import {
  branchOfficeFullName,
  getDeskNumber,
  userFullName,
  visitPurposesNames,
} from "src/helpers/auth-data";
import { Button } from "react-bootstrap";
import { setStateAction } from "src/types/generics";

type ProfileDataProps = {
  setIsProfileDataShown: setStateAction;
};

const ProfileData = ({ setIsProfileDataShown }: ProfileDataProps) => {
  return (
    <div>
      <div className={cn(classes.userSettings)}>
        <div className={cn(classes.userSettingsCard)}>
          <span className={cn(classes.settingCardHeader)}>
            Общие сведения пользователя
          </span>
          <div>
            <Typography variant="span">Полное имя: </Typography>
            <Typography variant="span">{userFullName()}</Typography>
          </div>
        </div>
        <div className={cn(classes.userSettingsCard)}>
          <span className={cn(classes.settingCardHeader)}>Текущий филиал</span>
          <div>
            <Typography variant="span">Филиал: </Typography>
            <Typography variant="span">{branchOfficeFullName()}</Typography>
          </div>
        </div>
        <div className={cn(classes.userSettingsCard)}>
          <span className={cn(classes.settingCardHeader)}>
            Текущий номер столика
          </span>
          <div>
            <Typography variant="span">Номер столика: </Typography>
            <Typography variant="span">{getDeskNumber()}</Typography>
          </div>
        </div>
        <div className={cn(classes.userSettingsCard)}>
          <span className={cn(classes.settingCardHeader)}>
            Текущие цели визита
          </span>
          <div>
            <Typography variant="span">Цель визита: </Typography>
            <Typography variant="span">
              {visitPurposesNames().join(", ")}
            </Typography>
          </div>
        </div>
      </div>
      <div className={cn(classes.buttonsContainer)}>
        <Button
          variant="primary"
          type="submit"
          onClick={() => setIsProfileDataShown(false)}
        >
          Редактировать настройки
        </Button>
      </div>
    </div>
  );
};

export default ProfileData;
