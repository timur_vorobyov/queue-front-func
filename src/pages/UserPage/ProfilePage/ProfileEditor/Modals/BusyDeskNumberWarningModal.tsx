import React from "react";
import cn from "clsx";
import classes from "./Modals.css";
import Typography from "src/components/Typography";
import { Modal } from "react-bootstrap";
import { setStateAction } from "src/types/generics";

type BusyDeskNumberWarningModalProps = {
  isUserHasClient: boolean;
  setIsUserHasClient: setStateAction;
};

const BusyDeskNumberWarningModal = ({
  isUserHasClient,
  setIsUserHasClient,
}: BusyDeskNumberWarningModalProps) => {
  return (
    <Modal
      className={cn(classes.modalDiolog)}
      show={isUserHasClient}
      onHide={() => setIsUserHasClient(false)}
    >
      <Modal.Header closeButton>
        <Modal.Title>
          <Typography variant="span">Предупреждение</Typography>
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Typography variant="span">
          На данный момент столик занят и не может быть изменён. Так как
          пользователь этого столика обслуживает клиента
        </Typography>
      </Modal.Body>
    </Modal>
  );
};

export default BusyDeskNumberWarningModal;
