import React from "react";
import cn from "clsx";
import classes from "./Modals.css";
import Typography from "src/components/Typography";
import { Button, Modal } from "react-bootstrap";
// @ts-ignore
import { createNotification } from "src/helpers/crud-notifications";
import { setStateAction } from "src/types/generics";
import { updateUserSettings } from "src/apis/ProfilePage/user";

type DeskNumberUpdateConfirmationModalProps = {
  isBusyDeskNumberShown: boolean;
  setIsBusyDeskNumberShown: setStateAction;
  userIdWithSameDeskNumber: number | null;
  deskNumber: string;
  branchOfficeId: string;
  visitPurposesIds: number[];
  servingUserFirstName: string;
};

const DeskNumberUpdateConfirmationModal = ({
  isBusyDeskNumberShown,
  setIsBusyDeskNumberShown,
  userIdWithSameDeskNumber,
  deskNumber,
  branchOfficeId,
  visitPurposesIds,
  servingUserFirstName,
}: DeskNumberUpdateConfirmationModalProps) => {
  const handleUpdateUserSettingClick = async (): Promise<void> => {
    await updateUserSettings(
      userIdWithSameDeskNumber,
      deskNumber,
      branchOfficeId,
      visitPurposesIds
    );

    createNotification("success", "Пользователь обновлён");
    setIsBusyDeskNumberShown(false);
  };

  return (
    <Modal
      className={cn(classes.modalDiolog)}
      show={isBusyDeskNumberShown}
      onHide={() => setIsBusyDeskNumberShown(false)}
    >
      <Modal.Header closeButton>
        <Modal.Title>
          <Typography variant="span">Изменить настройки</Typography>
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Typography variant="span">
          На данный момент столик занят {servingUserFirstName}. Хотите обновить
          настройки?
        </Typography>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={handleUpdateUserSettingClick}>
          <Typography color="white" variant="span">
            Да
          </Typography>
        </Button>
        <Button
          variant="primary"
          onClick={() => setIsBusyDeskNumberShown(false)}
        >
          <Typography color="white" variant="span">
            Нет
          </Typography>
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default DeskNumberUpdateConfirmationModal;
