import React, { useEffect, useState } from "react";
import cn from "clsx";
import classes from "./ProfileEditor.css";
import {
  getDeskNumber,
  getBranchOfficeId,
  getVisitPurposesIds,
} from "src/helpers/auth-data";
import { Button, Form } from "react-bootstrap";
// @ts-ignore
import { createNotification } from "src/helpers/crud-notifications";
// @ts-ignore
import Select from "react-select";
import { setStateAction } from "src/types/generics";
import {
  checkDeskNumberExistance,
  updateUserSettings,
} from "src/apis/ProfilePage/user";

import { getVisitPurposeList } from "src/apis/ProfilePage/visitPurpose";
import useApiCall from "src/utils/hooks/use-async-call";
import { userSettingsChecker } from "../../utils";
import DeskNumberUpdateConfirmationModal from "./Modals/DeskNumberUpdateConfirmationModal";
import BusyDeskNumberWarningModal from "./Modals/BusyDeskNumberWarningModal";
import { BranchOffice } from "src/types/UserPageTypes/ProfilePageTypes/branchOffice";
import { VisitPurpose } from "src/types/UserPageTypes/ProfilePageTypes/visitPurpose";
import { getBranchOfficeList } from "src/apis/ProfilePage/branchOffice";

type ProfileEditorProps = {
  setIsProfileDataShown: setStateAction;
};

const ProfileEditor = ({ setIsProfileDataShown }: ProfileEditorProps) => {
  const [deskNumber, setDeskNumber] = useState(getDeskNumber());
  const [branchOfficeId, setBranchOfficeId] = useState(getBranchOfficeId());
  const [visitPurposesIds, setVisitPurposesIds] = useState(
    getVisitPurposesIds()
  );
  const [servingUserFirstName, setServingUserFirstName] = useState("");
  const [isUserHasClient, setIsUserHasClient] = useState(false);
  const [isBusyDeskNumberShown, setIsBusyDeskNumberShown] = useState(false);
  const [userIdWithSameDeskNumber, setUserIdWithTheSameDeskNumber] = useState<
    null | number
  >(null);
  const [
    isSaveSettingsButtonDisabled,
    setIsSaveSettingsButtonDisabled,
  ] = useState(true);
  const [
    visitPurposes,
    setApiCallVisitPurposes,
  ] = useApiCall(getVisitPurposeList, [Number(branchOfficeId)]);
  const [branchOffices, setApiCallBranchOffices] = useApiCall(
    getBranchOfficeList
  );

  useEffect(() => {
    setDisabledFalse();
    userSettingsChecker();
  }, [branchOfficeId, deskNumber, visitPurposesIds.length]);

  useEffect(() => {
    setApiCallBranchOffices();
    setApiCallVisitPurposes();
  }, [branchOfficeId]);

  const setDisabledFalse = () => {
    if (
      branchOfficeId != "" &&
      branchOfficeId != "0" &&
      deskNumber != "" &&
      visitPurposesIds.length
    ) {
      setIsSaveSettingsButtonDisabled(false);
    } else {
      setIsSaveSettingsButtonDisabled(true);
    }
  };

  const setVisitPurposesIdsState = async (
    selectedVisitPurpose: [{ value: number }]
  ): Promise<void> => {
    const options = selectedVisitPurpose;
    const selectedOptions = [];

    for (let i = 0; i < options.length; i++) {
      selectedOptions.push(options[i].value);
    }

    setVisitPurposesIds(selectedOptions);
  };

  const setDeskNumberState = async (
    event: React.ChangeEvent<HTMLInputElement>
  ): Promise<void> => {
    if (Number(event.target.value) > 99) {
      createNotification(
        "warning",
        "Максимальный номер столика, который вы можете выбрать равен 99"
      );
      return;
    }
    if (event.target.value == "" || Number(event.target.value) === 0) {
      createNotification(
        "warning",
        "Минимальный номер столика, который вы можете выбрать равен 1"
      );
      return;
    }
    setDeskNumber(Number(event.target.value));
  };

  const setBranchOfficeIdState = (id: number): void => {
    setBranchOfficeId(id);
    setVisitPurposesIds([]);
  };

  const getVisitPurposesOptions = () => {
    return visitPurposes.map((visitPurpose: VisitPurpose) => {
      return { value: visitPurpose.id, label: visitPurpose.name };
    });
  };

  const getBranchOfficeOptions = () => {
    return branchOffices.map((branchOffice: BranchOffice, index: number) => {
      return (
        <option key={index} value={branchOffice.id}>
          {branchOffice.name}
        </option>
      );
    });
  };

  const getSelectedVisitPurposeOptions = () => {
    const selected = visitPurposes.map((visitPurpose: VisitPurpose) => {
      if (visitPurposesIds.includes(visitPurpose.id)) {
        return {
          label: visitPurpose.name,
          value: visitPurpose.id,
        };
      }
    });

    return selected;
  };

  const handleSubmitClick = async (
    event: React.MouseEvent<HTMLElement, MouseEvent>
  ): Promise<void> => {
    event.preventDefault();

    const response = await checkDeskNumberExistance(
      deskNumber,
      branchOfficeId,
      visitPurposesIds
    );

    if (response.message == "USER_WITH_THE_SAME_DESK_NUMBER_HAS_CLIENT") {
      setIsUserHasClient(true);
      return;
    }

    if (response.message == "UPDATE_IS_ALLOWED") {
      await updateUserSettings(
        userIdWithSameDeskNumber,
        deskNumber,
        branchOfficeId,
        visitPurposesIds
      );

      createNotification("success", "Пользователь обновлён");
    } else {
      setUserIdWithTheSameDeskNumber(response.userIdWithSameDeskNumber);
      setServingUserFirstName(response.userFirstName);
      setIsBusyDeskNumberShown(true);
    }
  };

  return (
    <>
      <BusyDeskNumberWarningModal
        isUserHasClient={isUserHasClient}
        setIsUserHasClient={setIsUserHasClient}
      />
      <DeskNumberUpdateConfirmationModal
        isBusyDeskNumberShown={isBusyDeskNumberShown}
        setIsBusyDeskNumberShown={setIsBusyDeskNumberShown}
        userIdWithSameDeskNumber={userIdWithSameDeskNumber}
        deskNumber={deskNumber}
        branchOfficeId={branchOfficeId}
        visitPurposesIds={visitPurposesIds}
        servingUserFirstName={servingUserFirstName}
      />
      <Form className={cn(classes.formContainer)}>
        <div className={cn(classes.userSettings)}>
          <div className={cn(classes.userSettingsCard)}>
            <span className={cn(classes.settingCardHeader)}>
              Шаг 1. Выберите филиал
            </span>
            <div>
              <Form.Control
                as="select"
                value={branchOfficeId}
                onChange={(event) =>
                  setBranchOfficeIdState(Number(event.target.value))
                }
                className={cn(classes.branchOfficeSelector)}
                custom
              >
                {getBranchOfficeOptions()}
              </Form.Control>
            </div>
          </div>
          <div className={cn(classes.userSettingsCard)}>
            <span className={cn(classes.settingCardHeader)}>
              Шаг 2. Выберите номер столика
            </span>
            <Form.Control
              type="number"
              value={deskNumber}
              onChange={setDeskNumberState}
              min="1"
              max="99"
            />
          </div>
          <div className={cn(classes.userSettingsCard)}>
            <span className={cn(classes.settingCardHeader)}>
              Шаг 3. Выберите цель визита клиента
            </span>
            <Form.Group className={cn(classes.formGroup)}>
              <Select
                value={getSelectedVisitPurposeOptions()}
                options={getVisitPurposesOptions()}
                isMulti
                onChange={setVisitPurposesIdsState}
              />
            </Form.Group>
          </div>
        </div>
        <div className={cn(classes.buttonsContainer)}>
          <Button
            className={cn(classes.setButton)}
            variant="secondary"
            type="submit"
            onClick={handleSubmitClick}
            disabled={isSaveSettingsButtonDisabled}
          >
            Установить настройки
          </Button>
          <Button
            variant="primary"
            type="submit"
            onClick={() => setIsProfileDataShown(true)}
          >
            Отменить
          </Button>
        </div>
      </Form>
    </>
  );
};

export default ProfileEditor;
