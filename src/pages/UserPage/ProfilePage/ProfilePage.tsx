import React, { useState } from "react";
import cn from "clsx";
import classes from "./ProfilePage.css";
// @ts-ignore
import { NotificationContainer } from "react-notifications";
import ProfileData from "./ProfileData";
import ProfileEditor from "./ProfileEditor";

const ProfilePage = () => {
  const [isProfileDataShown, setIsProfileDataShown] = useState(true);

  return (
    <main className={cn(classes.container)}>
      <NotificationContainer />
      <div className={cn(classes.userSettingsContainer)}>
        {isProfileDataShown ? (
          <ProfileData setIsProfileDataShown={setIsProfileDataShown} />
        ) : (
          <ProfileEditor setIsProfileDataShown={setIsProfileDataShown} />
        )}
      </div>
    </main>
  );
};

export default ProfilePage;
