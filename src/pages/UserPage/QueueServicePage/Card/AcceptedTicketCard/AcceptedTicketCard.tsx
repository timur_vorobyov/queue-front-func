import cn from "clsx";
import cardClasses from "../Card.css";
import React from "react";
import Typography from "src/components/Typography";
import { Button } from "react-bootstrap";
import { declineTicket, serveTicket } from "src/apis/QueueServicePage/ticket";
import { userStatusName } from "src/helpers/auth-data";
import { useActions } from "src/utils/hooks/useActions";
import { AcceptedTicket } from "src/store/types/ticket";

type AcceptedTicketCardProps = {
  acceptedTicket: AcceptedTicket;
  handleAcceptTicketClick: Function;
};

const AcceptedTicketCard = ({
  acceptedTicket,
  handleAcceptTicketClick,
}: AcceptedTicketCardProps) => {
  const { setLoadingStatus } = useActions();

  const handleServeTicketClick = (ticketId: number): void => {
    setLoadingStatus(true);
    serveTicket(ticketId);
  };

  const handleDeclineTicketClick = (ticketId: number): void => {
    declineTicket(ticketId);
    if (userStatusName() == "work time") {
      handleAcceptTicketClick();
    }
  };

  return (
    <div className={cn(cardClasses.ticketServiceCard)}>
      <div className={cn(cardClasses.ticketInfo)}>
        <div className={cn(cardClasses.visitPurposeContaner)}>
          <Typography fontWeight="bold" color="muted" variant="span">
            {acceptedTicket.visitPurposeName}
          </Typography>
        </div>
        <div className={cn(cardClasses.ticketNumber)}>
          <Typography color="muted" variant="span">
            {acceptedTicket.ticketFullNumber}
          </Typography>
        </div>
      </div>
      {acceptedTicket.subVisitPurpose ? (
        <div className={cn(cardClasses.customerPhoneNumber)}>
          <Typography fontWeight="bold" color="muted" variant="span">
            Подцель визита
          </Typography>
          <Typography color="muted" variant="span">
            {acceptedTicket.subVisitPurpose.name}
          </Typography>
        </div>
      ) : null}
      <div className={cn(cardClasses.customerPhoneNumber)}>
        <Typography fontWeight="bold" color="muted" variant="span">
          Номер телефона:
        </Typography>
        <Typography color="muted" variant="span">
          {acceptedTicket.customerPhoneNumber}
        </Typography>
      </div>
      <div className={cn(cardClasses.pendingTime)}>
        <Typography fontWeight="bold" color="muted" variant="span">
          Сколько клиент ждал:
        </Typography>
        <Typography color="muted" variant="span">
          {acceptedTicket.pendingTime}
        </Typography>
      </div>
      <div className={cn(cardClasses.mainInfo)}>
        <div>
          <div>
            <Typography fontWeight="bold" color="muted" variant="span">
              Выдали тикет:
            </Typography>
          </div>
          <div>
            <Typography color="muted" variant="span">
              {acceptedTicket.createdAt}
            </Typography>
          </div>
        </div>
        {acceptedTicket.crmCustomerId ? (
          <div className={cn(cardClasses.externalIntegrationCards)}>
            <a
              className={cn(cardClasses.crmLink)}
              href={`https://crm3.alif.tj/clients/${acceptedTicket.crmCustomerId}`}
              target="_blank"
              rel="noreferrer"
            >
              CRM
            </a>
            <a
              className={cn(cardClasses.abcLink)}
              href={`https://core2.alif.tj/common/contractors?crmClientId=${acceptedTicket.crmCustomerId}`}
            >
              ABC
            </a>
          </div>
        ) : (
          <div>
            <div className={cn(cardClasses.newCustomer)}>
              <Typography color="white" variant="span">
                Новый клиент
              </Typography>
            </div>
          </div>
        )}
      </div>
      <div className={cn(cardClasses.acceptedTicketButtons)}>
        <Button
          variant="primary"
          type="button"
          onClick={() => {
            handleServeTicketClick(acceptedTicket.id);
          }}
        >
          Обслужить клиента
        </Button>
        <Button
          variant="primary"
          type="button"
          onClick={() => {
            handleDeclineTicketClick(acceptedTicket.id);
          }}
        >
          Клиент не подошел
        </Button>
      </div>
    </div>
  );
};

export default AcceptedTicketCard;
