import cn from "clsx";
import classes from "./IncomingTicketCard.css";
import React from "react";
import Typography from "src/components/Typography";
import cardClasses from "../Card.css";
import { Ticket } from "src/store/types/ticket";

type IncomingTicketCardProps = {
  incomingTickets: Ticket[];
};
const IncomingTicketCard = ({ incomingTickets }: IncomingTicketCardProps) => {
  return (
    <>
      {incomingTickets.map((incomingTicket: Ticket) => {
        return (
          <div
            key={incomingTicket.id}
            className={cn(classes.incomingTicketCard)}
          >
            <div className={cn(cardClasses.ticketInfo)}>
              <div className={cn(cardClasses.visitPurposeContaner)}>
                <Typography fontWeight="bold" color="muted" variant="span">
                  {incomingTicket.visitPurposeName}
                </Typography>
              </div>
              <div className={cn(cardClasses.ticketNumber)}>
                <Typography color="muted" variant="span">
                  {incomingTicket.ticketFullNumber}
                </Typography>
              </div>
            </div>
            <div className={cn(cardClasses.mainInfo)}>
              <div>
                <div>
                  <Typography fontWeight="bold" color="muted" variant="span">
                    Выдали тикет:
                  </Typography>
                </div>
                <div>
                  <Typography color="muted" variant="span">
                    {incomingTicket.createdAt}
                  </Typography>
                </div>
              </div>
              {incomingTicket.crmCustomerId ? (
                <div className={cn(cardClasses.externalIntegrationCards)}>
                  <a
                    className={cn(cardClasses.crmLink)}
                    href={`https://crm3.alif.tj/clients/${incomingTicket.crmCustomerId}`}
                    target="_blank"
                    rel="noreferrer"
                  >
                    CRM
                  </a>
                  <a
                    className={cn(cardClasses.abcLink)}
                    href={`https://core2.alif.tj/common/contractors?crmClientId=${incomingTicket.crmCustomerId}`}
                  >
                    ABC
                  </a>
                </div>
              ) : (
                <div>
                  <div className={cn(cardClasses.newCustomer)}>
                    <Typography color="white" variant="span">
                      Новый клиент
                    </Typography>
                  </div>
                </div>
              )}
            </div>
          </div>
        );
      })}
    </>
  );
};

export default IncomingTicketCard;
