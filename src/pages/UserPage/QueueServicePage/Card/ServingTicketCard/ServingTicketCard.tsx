import cn from "clsx";
import React, { useEffect, useState } from "react";
import Typography from "src/components/Typography";
import { Button, Form, Modal } from "react-bootstrap";
import { timer, userStatusName } from "src/helpers/auth-data";
import { VisitPurpose } from "src/types/UserPageTypes/QueuePageTypes/visitPurpose";
import { Service } from "src/types/UserPageTypes/QueuePageTypes/service";
// @ts-ignore
import ReactNotifications from "react-browser-notifications";
import { setStateAction } from "src/types/generics";
import { completeService } from "src/apis/QueueServicePage/ticket";
import useMultipleApiCalls from "src/utils/hooks/use-async-multuple-call";
import { getServicesList } from "src/apis/QueueServicePage/service";
import { getVisitPurposeList } from "src/apis/QueueServicePage/visitPurppose";
import cardClasses from "../Card.css";
import { ServingTicket } from "src/store/types/ticket";
import { getSubVisitPurposesList } from "src/apis/QueueServicePage/subVisitPurpose";
import { SubVisitPurpose } from "src/types/UserPageTypes/QueuePageTypes/subVisitPurpose";

type TicketServingCardProps = {
  servingTicket: ServingTicket;
  notification: ReactNotifications;
  setBrowserNotificationMessaage: setStateAction;
  handleAcceptTicketClick: Function;
  incomingTicketsAmount: number;
};
const TicketServingCard = ({
  servingTicket,
  notification,
  setBrowserNotificationMessaage,
  handleAcceptTicketClick,
  incomingTicketsAmount,
}: TicketServingCardProps) => {
  const [isDisabled, setIsDisabled] = useState(true);
  const [isModalShown, setIsModalShown] = useState(false);
  const [serviceOptions, setServiceOptions] = useState<Service[]>([]);
  const [subVisitPurposeOptions, setSubVisitPurposeOptions] = useState<
    SubVisitPurpose[]
  >([]);
  const [visitPurposeOptions, setVisitPurposeOptions] = useState([]);
  const [selectedServiceId, setSelectedServiceId] = useState<number>(0);
  const [selectedVisitPurposeId, setSelectedVisitPurposeId] =
    useState<number>(0);
  const [selectedSubVisitPurposeId, setSelectedSubVisitPurposeId] =
    useState<number>(0);
  const [[visitPurposesList], setApiCallVisitPurposes] = useMultipleApiCalls([
    getVisitPurposeList,
  ]);
  useEffect(() => {
    setApiCallVisitPurposes();
  }, []);

  useEffect(() => {
    if (visitPurposesList !== undefined) {
      setVisitPurposeOptions(visitPurposesList);
    }
  }, [visitPurposesList]);

  const handleCompleteServiceClick = (): void => {
    completeService(
      servingTicket.id,
      selectedServiceId,
      selectedVisitPurposeId,
      selectedSubVisitPurposeId
    );

    handleCloseModalClick();
    if (incomingTicketsAmount > 0 && userStatusName() == "work time") {
      handleAcceptTicketClick();
    }
  };

  const handleCloseModalClick = (): void => {
    setIsModalShown(false);
  };

  const handleShowModalClick = (): void => {
    setIsModalShown(true);
  };

  const getServiceOptions = (): JSX.Element[] => {
    return serviceOptions.map((service: Service, index: number) => {
      return (
        <option key={index} value={service.id}>
          {service.name}
        </option>
      );
    });
  };

  const getVisitPurposeOptions = (): JSX.Element[] => {
    return visitPurposeOptions.map(
      (visitPurpose: VisitPurpose, index: number) => {
        return (
          <option key={index} value={visitPurpose.id}>
            {visitPurpose.name}
          </option>
        );
      }
    );
  };

  const getSubVisitPurposeOptions = (): JSX.Element[] => {
    return subVisitPurposeOptions.map(
      (subVisitPurpose: SubVisitPurpose, index: number) => {
        return (
          <option key={index} value={subVisitPurpose.id}>
            {subVisitPurpose.name}
          </option>
        );
      }
    );
  };

  const setSelectedServiceIdState = async (
    event: React.ChangeEvent<HTMLSelectElement>
  ): Promise<void> => {
    setSelectedServiceId(Number(event.target.value));

    if (event.target.value) {
      setIsDisabled(false);
    }
  };
  const setSelectedVisitPurposeIdState = async (
    event: React.ChangeEvent<HTMLSelectElement>
  ): Promise<void> => {
    const visitPurposeId = Number(event.target.value);
    const servicesList = await getServicesList(visitPurposeId);
    const subVisitPurposesList = await getSubVisitPurposesList(visitPurposeId);
    setServiceOptions(servicesList);
    setSubVisitPurposeOptions(subVisitPurposesList);
    setSelectedVisitPurposeId(visitPurposeId);
  };

  return (
    <div className={cn(cardClasses.ticketServiceCard)}>
      <div className={cn(cardClasses.ticketInfo)}>
        <div className={cn(cardClasses.visitPurposeContaner)}>
          <Typography fontWeight="bold" color="muted" variant="span">
            {servingTicket.visitPurposeName}
          </Typography>
        </div>
        <div className={cn(cardClasses.ticketNumber)}>
          <Typography color="muted" variant="span">
            {servingTicket.ticketFullNumber}
          </Typography>
        </div>
      </div>
      {servingTicket?.subVisitPurpose?.id ? (
        <div className={cn(cardClasses.customerPhoneNumber)}>
          <Typography fontWeight="bold" color="muted" variant="span">
            Подцель визита
          </Typography>
          <Typography color="muted" variant="span">
            {servingTicket.subVisitPurpose.name}
          </Typography>
        </div>
      ) : null}
      <div className={cn(cardClasses.customerPhoneNumber)}>
        <Typography fontWeight="bold" color="muted" variant="span">
          Номер телефона
        </Typography>
        <Typography color="muted" variant="span">
          {servingTicket.customerPhoneNumber}
        </Typography>
      </div>
      <div className={cn(cardClasses.pendingTime)}>
        <Typography fontWeight="bold" color="muted" variant="span">
          Сколько клиент ждал:
        </Typography>
        <Typography color="muted" variant="span">
          {servingTicket.pendingTime}
        </Typography>
      </div>
      <div className={cn(cardClasses.mainInfo)}>
        <div>
          <div>
            <Typography fontWeight="bold" color="muted" variant="span">
              Выдали тикет:
            </Typography>
          </div>
          <div>
            <Typography color="muted" variant="span">
              {servingTicket.createdAt}
            </Typography>
          </div>
        </div>
        {servingTicket.crmCustomerId ? (
          <div className={cn(cardClasses.externalIntegrationCards)}>
            <a
              className={cn(cardClasses.crmLink)}
              href={`https://crm3.alif.tj/clients/${servingTicket.crmCustomerId}`}
              target="_blank"
              rel="noreferrer"
            >
              CRM
            </a>
            <a
              className={cn(cardClasses.abcLink)}
              href={`https://core2.alif.tj/common/contractors?crmClientId=${servingTicket.crmCustomerId}`}
            >
              ABC
            </a>
          </div>
        ) : (
          <div>
            <div className={cn(cardClasses.newCustomer)}>
              <Typography color="white" variant="span">
                Новый клиент
              </Typography>
            </div>
          </div>
        )}
      </div>
      <div className={cn(cardClasses.acceptedTicketButtons)}>
        <Button
          type="button"
          onClick={() => {
            handleShowModalClick();
          }}
        >
          Сделано
        </Button>
      </div>
      <Modal show={isModalShown} onHide={handleCloseModalClick}>
        <Modal.Header closeButton>
          <Modal.Title>Обслуживание</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div>
            <Form.Group>
              <Form.Control
                as="select"
                value={selectedVisitPurposeId}
                onChange={setSelectedVisitPurposeIdState}
                custom
              >
                <option>Пожалуйста укажите цель визита</option>
                {getVisitPurposeOptions()}
              </Form.Control>
            </Form.Group>
            {subVisitPurposeOptions.length ? (
              <Form.Group>
                <Form.Control
                  as="select"
                  value={selectedSubVisitPurposeId}
                  onChange={(event: React.ChangeEvent<HTMLSelectElement>) =>
                    setSelectedSubVisitPurposeId(Number(event.target.value))
                  }
                  custom
                >
                  <option>Пожалуйста укажите подцель визита</option>
                  {getSubVisitPurposeOptions()}
                </Form.Control>
              </Form.Group>
            ) : null}
            <Form.Group>
              <Form.Control
                as="select"
                value={selectedServiceId}
                onChange={setSelectedServiceIdState}
                custom
              >
                <option>Пожалуйста укажите услугу</option>
                {getServiceOptions()}
              </Form.Control>
            </Form.Group>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleCloseModalClick}>
            Отмена
          </Button>
          <Button
            variant="primary"
            type="button"
            onClick={() => handleCompleteServiceClick()}
            disabled={isDisabled}
          >
            Сохранить
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
};

export default TicketServingCard;
