import React, { useState } from "react";
import { useTypedSelector } from "src/utils/hooks/useTypedSelector";
import cn from "clsx";
import classes from "./QueueServicePage.css";
import { Button, Spinner } from "react-bootstrap";
import { acceptTicket } from "src/apis/QueueServicePage/ticket";
// @ts-ignore
import { NotificationContainer } from "react-notifications";
// @ts-ignore
import ReactNotifications from "react-browser-notifications";
import Statistics from "./Statistics";
import ServingTicketCard from "./Card/ServingTicketCard";
import AcceptedTicketCard from "./Card/AcceptedTicketCard";
import IncomingTicketCard from "./Card/IncomingTicketCard";
import { useActions } from "src/utils/hooks/useActions";
import { userSettingsChecker } from "../utils";

type QueueServicePageProps = {
  isAcceptTicketButtonDisabled: boolean;
};
const QueueServicePage = ({
  isAcceptTicketButtonDisabled,
}: QueueServicePageProps) => {
  const [
    browserNotificationMessaage,
    setBrowserNotificationMessaage,
  ] = useState("");
  const {
    incomingTickets,
    acceptedTicket,
    servingTicket,
    loadingStatus,
  } = useTypedSelector((state) => state.ticket);
  let notification: ReactNotifications;
  const { setLoadingStatus } = useActions();

  const handleAcceptTicketClick = async (): Promise<void> => {
    userSettingsChecker();
    if (incomingTickets.length == 0) {
      return;
    }
    setLoadingStatus(true);
    await acceptTicket(incomingTickets[0].id);
  };

  const getCurrentCard = (): JSX.Element | null => {
    if (acceptedTicket.id) {
      return (
        <AcceptedTicketCard
          acceptedTicket={acceptedTicket}
          handleAcceptTicketClick={handleAcceptTicketClick}
        />
      );
    }
    if (servingTicket.id) {
      return (
        <ServingTicketCard
          servingTicket={servingTicket}
          handleAcceptTicketClick={handleAcceptTicketClick}
          setBrowserNotificationMessaage={setBrowserNotificationMessaage}
          notification={notification}
          incomingTicketsAmount={incomingTickets.length}
        />
      );
    }

    if (incomingTickets.length == 0) {
      return null;
    }

    return (
      <Button
        disabled={isAcceptTicketButtonDisabled}
        variant="primary"
        type="button"
        onClick={() => handleAcceptTicketClick()}
      >
        Принять клиента
      </Button>
    );
  };

  return (
    <main className={cn(classes.container)}>
      <ReactNotifications
        onRef={(ref: ReactNotifications) => {
          notification = ref;
        }}
        title="Q.ALIF"
        body={browserNotificationMessaage}
        icon="r"
        timeout="2000"
      />
      <NotificationContainer />
      <Statistics />
      <div className={cn(classes.tickets)}>
        <div className={cn(classes.ticket)}>
          <div className={cn(classes.ticketListHeader)}>
            <span className={cn(classes.ticketHeader)}>В ожидании</span>
            <div
              className={
                incomingTickets.length != 0 ? classes.ticketsPendingNumber : ""
              }
            >
              <span className={cn(classes.ticketsAmount)}>
                {incomingTickets.length != 0 ? incomingTickets.length : ""}
              </span>
            </div>
          </div>
          {<IncomingTicketCard incomingTickets={incomingTickets.slice(0, 2)} />}
        </div>
        <div className={cn(classes.ticket)}>
          <div className={cn(classes.ticketListHeader)}>
            <span className={cn(classes.ticketHeader)}>Обслуживается</span>
          </div>
          {loadingStatus ? (
            <Spinner animation="border" variant="primary" />
          ) : (
            getCurrentCard()
          )}
        </div>
      </div>
    </main>
  );
};

export default QueueServicePage;
