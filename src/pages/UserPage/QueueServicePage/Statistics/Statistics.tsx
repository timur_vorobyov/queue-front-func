import cn from "clsx";
import classes from "./Statistics.css";
import React, { useEffect, useState } from "react";
import Typography from "src/components/Typography";
import statisticsImg from "src/pages/UserPage/QueueServicePage/images/statistics.svg";
import { userId } from "src/helpers/auth-data";
import echo from "laravel-echo";
import { createEchoInstance } from "src/helpers/echo";
import useMultipleApiCalls from "src/utils/hooks/use-async-multuple-call";
import {
  getCustomersAmountUserServed,
  getUserAverageServiceTime,
  getUsersAverageServiceTime,
  getUsersMaxServiceTime,
} from "src/apis/QueueServicePage/userData";

const Statistics = () => {
  let echo: echo;
  const [statistics, setStatistics] = useState({
    userAverageServiceTime: "",
    usersAverageServiceTime: "",
    customersAmountUserServed: "",
    usersMaxServiceTime: "",
  });

  const [
    [
      customersAmountUserServed,
      userAverageServiceTime,
      usersAverageServiceTime,
      usersMaxServiceTime,
    ],
    setApiStatisticsData,
  ] = useMultipleApiCalls([
    getCustomersAmountUserServed,
    getUserAverageServiceTime,
    getUsersAverageServiceTime,
    getUsersMaxServiceTime,
  ]);

  useEffect(() => {
    setApiStatisticsData();
  }, []);

  useEffect(() => {
    if (
      customersAmountUserServed ||
      userAverageServiceTime ||
      usersAverageServiceTime ||
      usersMaxServiceTime
    ) {
      setStatistics({
        userAverageServiceTime,
        usersAverageServiceTime,
        customersAmountUserServed,
        usersMaxServiceTime,
      });
    }
  }, [
    customersAmountUserServed,
    userAverageServiceTime,
    usersAverageServiceTime,
    usersMaxServiceTime,
  ]);

  useEffect(() => {
    connectEcho();
    listenTicketsEvent();
    return () => disconnectEcho();
  }, []);

  const connectEcho = (): void => {
    echo = createEchoInstance();
  };

  const disconnectEcho = (): void => {
    echo.disconnect();
  };

  const listenTicketsEvent = () => {
    echo.channel("users." + userId() + ".statistics").listen(
      ".statistics-data",
      (data: {
        userStatisticsData: {
          usersAverageServiceTime: string;
          userAverageServiceTime: string;
          customersAmountUserServed: string;
          usersMaxServiceTime: string;
        };
      }): void => {
        setStatistics({ ...data.userStatisticsData });
      }
    );
  };
  return (
    <div className={cn(classes.generalStatistics)}>
      <div className={cn(classes.cardStatistics)}>
        <div className={cn(classes.cardStatisticsInfoContainer)}>
          <div className={cn(classes.cardStatisticsInfo)}>
            <span className={cn(classes.usersAverageServiceTime)}>
              {statistics.usersAverageServiceTime} мин.
            </span>
          </div>
          <img src={statisticsImg} className={cn(classes.statisticsImage)} />
        </div>
        <div className={cn(classes.avarageTimeContainer)}>
          <Typography className="statisticTitle" color="white" variant="span">
            Сред. время обслуживания
          </Typography>
        </div>
      </div>
      <div className={cn(classes.cardStatistics)}>
        <div className={cn(classes.cardStatisticsInfoContainer)}>
          <div className={cn(classes.cardStatisticsInfo)}>
            <span className={cn(classes.usersMaxServiceTime)}>
              {statistics.usersMaxServiceTime} мин.
            </span>
          </div>
          <img src={statisticsImg} className={cn(classes.statisticsImage)} />
        </div>
        <div className={cn(classes.maxTimeContainer)}>
          <Typography className="statisticTitle" color="white" variant="span">
            Макс. время обслуживания
          </Typography>
        </div>
      </div>
      <div className={cn(classes.cardStatistics)}>
        <div className={cn(classes.cardStatisticsInfoContainer)}>
          <div className={cn(classes.cardStatisticsInfo)}>
            <span className={cn(classes.userAverageServiceTime)}>
              {statistics.userAverageServiceTime} мин.
            </span>
          </div>
          <img src={statisticsImg} className={cn(classes.statisticsImage)} />
        </div>
        <div className={cn(classes.userAvarageTimeContainer)}>
          <Typography className="statisticTitle" color="white" variant="span">
            Моё сред. время обслуживания
          </Typography>
        </div>
      </div>
      <div className={cn(classes.cardStatistics)}>
        <div className={cn(classes.cardStatisticsInfoContainer)}>
          <div className={cn(classes.cardStatisticsInfo)}>
            <span className={cn(classes.customersAmountUserServed)}>
              {statistics.customersAmountUserServed} клиентов
            </span>
            <Typography color="muted" variant="span">
              Сегодня
            </Typography>
          </div>
          <img src={statisticsImg} className={cn(classes.statisticsImage)} />
        </div>
        <div className={cn(classes.servedCustomersNumber)}>
          <Typography className="statisticTitle" color="white" variant="span">
            Обслужил
          </Typography>
        </div>
      </div>
    </div>
  );
};

export default Statistics;
