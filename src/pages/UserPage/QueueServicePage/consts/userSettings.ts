export const USER_SETTINGS_WARNINGS = {
  deskeNumberIsEmpty: "У вас не выбран номер столика, пожалуйста выберите",
  visitPurposeIsEmpty: "У вас не выбрана цель визита, пожалуйста выберите",
  branchOfficeIsEmpty: "У вас не выбран филиал, пожалуйста выберите",
};
