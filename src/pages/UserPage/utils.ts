import {
  getBranchOfficeId,
  getDeskNumber,
  getVisitPurposesIds,
} from "src/helpers/auth-data";
import { createNotification } from "src/helpers/crud-notifications";
import { USER_SETTINGS_WARNINGS } from "./QueueServicePage/consts/userSettings";

export const userSettingsChecker = () => {
  if (getDeskNumber() == "" || getDeskNumber() == null) {
    createNotification("warning", USER_SETTINGS_WARNINGS.deskeNumberIsEmpty);
  }
  if (!getVisitPurposesIds().length) {
    createNotification("warning", USER_SETTINGS_WARNINGS.visitPurposeIsEmpty);
  }
  if (getBranchOfficeId() == "" || getBranchOfficeId() == null) {
    createNotification("warning", USER_SETTINGS_WARNINGS.branchOfficeIsEmpty);
  }
};
