import { Redirect, Switch } from "react-router-dom";
import QueueServicePage from "src/pages/UserPage/QueueServicePage";
import ProfilePage from "src/pages/UserPage/ProfilePage";
import BranchOfficePage from "src/pages/Admin/SuperAdminPages/BranchOfficePage";
import PermissionPage from "src/pages/Admin/SuperAdminPages/PermissionPage";
import RolePage from "src/pages/Admin/SuperAdminPages/RolePage";
import ServicePage from "src/pages/Admin/SuperAdminPages/ServicePage";
import HallAdminUserPage from "src/pages/Admin/HallAdminPages/UserPage/UserPage";
import AdminTicketBasketPage from "src/pages/Admin/TicketBasketPage/TicketBasketPage";
import TicketReportPage from "src/pages/Admin/SuperAdminPages/Reports/TicketReportPage";
import UserReportPage from "src/pages/Admin/SuperAdminPages/Reports/UserReportPage";
import React from "react";
import SuperAdminUserPage from "src/pages/Admin/SuperAdminPages/UserPage/UserPage";

import { routes } from "src/config/routes";
import { AdminProtectedRoute } from "src/components/AdminProtectedRoute";
import SubVisitPurpose from "src/pages/Admin/SuperAdminPages/SubVisitPurpose";
import VisitPurposeTypePage from "src/pages/Admin/SuperAdminPages/VisitPurposeTypePage/VisitPurposeTypePage";
import VisitPurposePage from "src/pages/Admin/SuperAdminPages/VisitPurposePage";
import TownPage from "src/pages/Admin/SuperAdminPages/TownPage";
import { AuthProtectedRoute } from "src/components/AuthProtectedRoute";
import AdminIncomingTicketPage from "src/pages/Admin/IncomingTicketPage";

type RouterComponentProps = {
  isAcceptTicketButtonDisabled: boolean;
};

const PagesWithtNavAndSideBars = ({
  isAcceptTicketButtonDisabled,
}: RouterComponentProps) => {
  return (
    <Switch>
      <AuthProtectedRoute
        path={routes.queueService}
        exact
        component={() => (
          <QueueServicePage
            isAcceptTicketButtonDisabled={isAcceptTicketButtonDisabled}
          />
        )}
      />
      <AuthProtectedRoute path={routes.profile} exact component={ProfilePage} />
      <AdminProtectedRoute
        path={routes.superAdmin.branchOffices}
        exact
        component={BranchOfficePage}
      />
      <AdminProtectedRoute
        path={routes.superAdmin.towns}
        exact
        component={TownPage}
      />
      <AdminProtectedRoute
        path={routes.superAdmin.users}
        exact
        component={SuperAdminUserPage}
      />
      <AdminProtectedRoute
        path={routes.superAdmin.permissions}
        exact
        component={PermissionPage}
      />
      <AdminProtectedRoute
        path={routes.superAdmin.roles}
        exact
        component={RolePage}
      />
      <AdminProtectedRoute
        path={routes.superAdmin.visitPurposes}
        exact
        component={VisitPurposePage}
      />
      <AdminProtectedRoute
        path={routes.superAdmin.subVisitPurposes}
        exact
        component={SubVisitPurpose}
      />
      <AdminProtectedRoute
        path={routes.superAdmin.visitPurposesTypes}
        exact
        component={VisitPurposeTypePage}
      />
      <AdminProtectedRoute
        path={routes.superAdmin.services}
        exact
        component={ServicePage}
      />
      <AdminProtectedRoute
        path={routes.superAdmin.reports.tickets}
        exact
        component={TicketReportPage}
      />
      <AdminProtectedRoute
        path={routes.superAdmin.reports.users}
        exact
        component={UserReportPage}
      />

      <AdminProtectedRoute
        path={routes.hallAdmin.users}
        exact
        component={HallAdminUserPage}
      />
      <AdminProtectedRoute
        path={routes.admin.declinedTickets}
        exact
        component={AdminTicketBasketPage}
      />

      <AdminProtectedRoute
        path={routes.admin.incomingTickets}
        exact
        component={AdminIncomingTicketPage}
      />

      <AdminProtectedRoute path="*" exact>
        <Redirect to={routes.notFound} />
      </AdminProtectedRoute>
    </Switch>
  );
};

export default PagesWithtNavAndSideBars;
