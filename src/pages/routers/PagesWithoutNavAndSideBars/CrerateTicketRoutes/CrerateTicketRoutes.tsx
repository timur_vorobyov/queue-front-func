import React from "react";
import { Route, Switch } from "react-router-dom";
import { routes } from "src/config/routes";
import CreateTicketPagesContextProvider from "src/contexts/CreateTicketPagesContext";
import CreateTicketPages from "src/pages/UserPage/CreateTicketPages";
import CustomerPhoneNumberPage from "src/pages/UserPage/CreateTicketPages/CustomerPhoneNumberPage";
import ShowTicketPage from "src/pages/UserPage/CreateTicketPages/ShowTicketPage";
import TicketVisitPurposePage from "src/pages/UserPage/CreateTicketPages/TicketVisitPurposePage";

const CrerateTicketRoutes = () => {
  return (
    <CreateTicketPagesContextProvider>
      <Switch>
        <Route
          path={routes.registerTicket}
          exact
          component={CreateTicketPages}
        />
        <Route
          path={routes.ticketVisitPurpose}
          component={TicketVisitPurposePage}
        />
        <Route
          path={routes.ticketCustomerPhoneNumber}
          component={CustomerPhoneNumberPage}
        />
        <Route path={routes.showTicket} component={ShowTicketPage} />
      </Switch>
    </CreateTicketPagesContextProvider>
  );
};

export default CrerateTicketRoutes;
