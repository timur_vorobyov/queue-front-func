import React, { PureComponent } from "react";
import { Switch, Route } from "react-router-dom";
import { routes } from "src/config/routes";
import LoginPage from "../../LoginPage";
import NotFound from "../../ErrorsPages/NotFound";
import ScreensPages from "../../ScreensPages";
import ScreensRoutes from "./ScreensRoutes/ScreensRoutes";
import CrerateTicketRoutes from "./CrerateTicketRoutes/CrerateTicketRoutes";
import { AuthProtectedRoute } from "src/components/AuthProtectedRoute";
interface PagesWithoutNavAndSideBarsProps {
  children: React.ReactNode;
}

class PagesWithoutNavAndSideBars extends PureComponent<
  PagesWithoutNavAndSideBarsProps
> {
  render(): React.ReactNode {
    return (
      <Switch>
        <Route path={routes.notFound} exact component={NotFound} />
        <Route path={routes.login} exact component={LoginPage} />
        <AuthProtectedRoute
          path={routes.screens}
          exact
          component={ScreensPages}
        />
        <AuthProtectedRoute path={routes.screen} component={ScreensRoutes} />
        <Route path={routes.registerTicket} component={CrerateTicketRoutes} />
        <Route>{this.props.children}</Route>
        <Route path={"*"} component={NotFound} />
      </Switch>
    );
  }
}

export default PagesWithoutNavAndSideBars;
