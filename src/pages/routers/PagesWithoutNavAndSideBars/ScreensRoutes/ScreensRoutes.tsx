import React from "react";
import { Switch, useHistory } from "react-router-dom";
import { routes } from "src/config/routes";
import HorizontalScreenPage from "src/pages/ScreensPages/HorizontalScreenPage";
import Fx from "src/pages/ScreensPages/HorizontalScreenPage/Fx";
import VerticalScreenPage from "src/pages/ScreensPages/VerticalScreenPage";
import queryString from "query-string";
import SubscriptionsManager from "src/pages/ScreensPages/SubscriptionsManager";
import { AuthProtectedRoute } from "src/components/AuthProtectedRoute";
import Horizontal2ScreenPage from "src/pages/ScreensPages/Horizontal2ScreenPage";

const ScreensRoutes = () => {
  const parsed = queryString.parse(location.search);
  const history = useHistory();
  let branchOfficeId = "";
  let visitPurposesIds = [] as string[];
  if (
    parsed.branchOfficeId &&
    parsed.visitPurposesIds &&
    !Array.isArray(parsed.branchOfficeId) &&
    !Array.isArray(parsed.visitPurposesIds)
  ) {
    branchOfficeId = parsed.branchOfficeId;
    visitPurposesIds = parsed.visitPurposesIds.split(",");
  } else {
    return history.push(routes.screens);
  }

  return (
    <>
      <SubscriptionsManager
        branchOfficeId={branchOfficeId}
        visitPurposesIds={visitPurposesIds}
      />
      <Switch>
        <AuthProtectedRoute
          path={routes.tv}
          exact
          component={HorizontalScreenPage}
        />
        <AuthProtectedRoute
          path={routes.tvNew}
          exact
          component={Horizontal2ScreenPage}
        />
        <AuthProtectedRoute path={routes.tvFx} exact component={Fx} />
        <AuthProtectedRoute
          path={routes.led}
          exact
          component={VerticalScreenPage}
        />
        <AuthProtectedRoute
          path={routes.ledWhite}
          exact
          component={VerticalScreenPage}
        />
      </Switch>
    </>
  );
};

export default ScreensRoutes;
