import { routes } from "src/config/routes";
import apiClient from "src/helpers/httpInterceptor";
import { PaginationData } from "src/components/Pagination/Pagination";
import { getBranchOfficeId } from "src/helpers/auth-data";

export type Users = {
  id: number;
  branchOfficeName: string;
  branchOfficeId: string;
  visitPurposeId: string;
  email: string;
  deskNumber: string;
  isActive: boolean;
  fullName: string;
  firstName: string;
  lastName: string;
  createdAt: string;
  role: { name: string; id: number };
}[];
export type User = {
  id: number;
  branchOfficeName: string;
  branchOfficeId: string;
  visitPurposeId: string;
  email: string;
  deskNumber: string;
  isActive: boolean;
  fullName: string;
  firstName: string;
  lastName: string;
  role: { name: string; id: number };
  createdAt: string;
};

class UserPageAdapter {
  private pagination: PaginationData;

  constructor() {
    this.pagination = { currentPage: 0, totalPages: 0, totalRecords: 0 };
  }

  public async updateUser(
    userId: string | number,
    deskNumber: number | undefined
  ): Promise<string> {
    const response = await apiClient.put(
      routes.api.hallAdmin.updateUser + encodeURIComponent(userId),
      {
        deskNumber: deskNumber,
      }
    );
    return response.data.message;
  }

  public async getPaginatedUsersList(
    searchExpression: string,
    limit: number,
    nextPage: number
  ): Promise<Users> {
    const response = await apiClient.get(
      routes.api.hallAdmin.getPaginatedUsersList +
        "?branchOfficeId=" +
        getBranchOfficeId() +
        "&searchExpression=" +
        searchExpression +
        "&limit=" +
        limit +
        "&page=" +
        nextPage
    );
    this.pagination = response.data.pagination;
    return this.toUserFrontFormat(response.data.usersPureList);
  }

  private toUserFrontFormat(usersList: []): Users {
    const formattedUsers = usersList.map((user: User) => ({
      id: user.id,
      branchOfficeId: user.branchOfficeId,
      branchOfficeName: user.branchOfficeName,
      visitPurposeId: user.visitPurposeId,
      email: user.email,
      deskNumber: user.deskNumber,
      createdAt: user.createdAt,
      fullName: user.fullName,
      isActive: user.isActive,
      firstName: user.firstName,
      lastName: user.lastName,
      role: {
        id: user.role ? user.role.id : 0,
        name: user.role ? user.role.name : "",
      },
    }));

    return formattedUsers;
  }

  public getPaginationData(): PaginationData {
    return this.pagination;
  }
}

export default UserPageAdapter;
