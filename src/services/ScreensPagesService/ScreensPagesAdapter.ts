import { routes } from "src/config/routes";
import apiClient from "src/helpers/httpInterceptor";
import {
  AcceptedTickets,
  BranchOffice,
  BranchOffices,
  TicketsPending,
  Town,
  Towns,
  VisitPurpose,
  VisitPurposes,
} from "./ScreensPagesService";

class ScreensPagesAdapter {
  public async getTownList(): Promise<Towns> {
    const response = await apiClient.get(routes.api.townsList);
    return this.toTownArray(response.data);
  }

  private toTownArray(townList: []): Towns {
    const formatedTowns = townList.map((town: Town) => ({
      id: town.id,
      name: town.name,
    }));

    return formatedTowns;
  }

  public async getBranchOfficeList(townId: string): Promise<BranchOffices> {
    const response = await apiClient.get(
      routes.api.branchOfficesList + encodeURIComponent(townId)
    );
    return this.toOfficeArray(response.data);
  }

  private toOfficeArray(branchOfficeList: []): BranchOffices {
    const formatedBranchOffices = branchOfficeList.map(
      (branchOffice: { id: number; name: string }) => ({
        id: branchOffice.id,
        branchOfficeName: branchOffice.name,
      })
    );

    return formatedBranchOffices;
  }

  public async getTicketsPendingList(
    branchOfficeId: string,
    visitPurposeIds: Array<string>
  ): Promise<TicketsPending> {
    const response = await apiClient.get(routes.api.shortTicketsList, {
      params: {
        branchOfficeId: branchOfficeId,
        statusName: "new",
        visitPurposeIds: visitPurposeIds,
      },
    });
    return response.data;
  }

  public async getAcceptedTicketsList(
    branchOfficeId: string,
    visitPurposeIds: Array<string>
  ): Promise<AcceptedTickets> {
    const response = await apiClient.get(routes.api.shortTicketsList, {
      params: {
        branchOfficeId: branchOfficeId,
        statusName: "invited",
        visitPurposeIds: visitPurposeIds,
      },
    });
    return response.data;
  }

  public async getVisitPurposeList(
    branchOfficeId: number
  ): Promise<VisitPurposes> {
    const response = await apiClient.get(
      routes.api.visitPurposesList + encodeURIComponent(branchOfficeId)
    );
    return this.toVisitPurposeFrontFormat(response.data);
  }

  private toVisitPurposeFrontFormat(visitPurposeList: []): VisitPurposes {
    const formatedVisitPurposes = visitPurposeList.map(
      (visitPurpose: VisitPurpose) => ({
        id: visitPurpose.id,
        name: visitPurpose.name,
      })
    );

    return formatedVisitPurposes;
  }
}

export default ScreensPagesAdapter;
