import ScreensPagesAdapter from "./ScreensPagesAdapter";

export type AcceptedTickets = AcceptedTicket[];

export type AcceptedTicket = {
  id: string;
  ticketFullNumber: string;
  deskNumber: string;
};

export type TicketsPending = TicketPending[];

export type TicketPending = {
  id: string;
  ticketFullNumber: string;
  isSalomCache: boolean;
  isVisitPurposePrioratised: boolean;
};

export type Towns = { id: number; name: string }[];
export type Town = { id: number; name: string };
export type BranchOffices = { id: number; branchOfficeName: string }[];
export type BranchOffice = { id: number; branchOfficeName: string };
export type VisitPurposes = { id: number; name: string }[];
export type VisitPurpose = { id: number; name: string };
class ScreensPagesService {
  private serviceAdapter: ScreensPagesAdapter;

  constructor() {
    this.serviceAdapter = new ScreensPagesAdapter();
  }

  public getTownList(): Promise<Towns> {
    return this.serviceAdapter.getTownList();
  }

  public getBranchOfficeList(townId: string): Promise<BranchOffices> {
    return this.serviceAdapter.getBranchOfficeList(townId);
  }

  public getTicketsPendingList(
    branchOfficeId: string,
    visitPurposeIds: Array<string>
  ): Promise<TicketsPending> {
    return this.serviceAdapter.getTicketsPendingList(
      branchOfficeId,
      visitPurposeIds
    );
  }

  public getAcceptedTicketsList(
    branchOfficeId: string,
    visitPurposeIds: Array<string>
  ): Promise<AcceptedTickets> {
    return this.serviceAdapter.getAcceptedTicketsList(
      branchOfficeId,
      visitPurposeIds
    );
  }

  public getVisitPurposeList(branchOfficeId: number): Promise<VisitPurposes> {
    return this.serviceAdapter.getVisitPurposeList(branchOfficeId);
  }
}

export default ScreensPagesService;
