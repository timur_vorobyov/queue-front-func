import { routes } from "src/config/routes";
import apiClient from "src/helpers/httpInterceptor";

export type DeclinedTickets = {
  id: number;
  ticketFullNumber: string;
  customerPhoneNumber: string;
  createdAt: string;
  visitPurposeName: string;
}[];
export type DeclinedTicket = {
  id: number;
  ticketFullNumber: string;
  customerPhoneNumber: string;
  createdAt: string;
  visitPurposeName: string;
};
export type BranchOffices = { id: number; name: string }[];
export type BranchOffice = { id: number; name: string };

class TicketBasketPageAdapter {
  public async getDeclinedTicketsList(
    branchOfficeId: number
  ): Promise<DeclinedTickets | null> {
    if (branchOfficeId != null) {
      const response = await apiClient.get(
        routes.api.admin.getDeclinedTicketsList +
          encodeURIComponent(branchOfficeId)
      );

      return this.toUserFrontFormat(response.data);
    }

    return null;
  }

  private toUserFrontFormat(declinedTicketsList: []): DeclinedTickets {
    const formattedDeclinedTickets = declinedTicketsList.map(
      (declinedTicket: DeclinedTicket) => ({
        id: declinedTicket.id,
        ticketFullNumber: declinedTicket.ticketFullNumber,
        customerPhoneNumber: declinedTicket.customerPhoneNumber,
        createdAt: declinedTicket.createdAt,
        visitPurposeName: declinedTicket.visitPurposeName,
      })
    );

    return formattedDeclinedTickets;
  }

  public async getBranchOfficesList(): Promise<BranchOffices> {
    const response = await apiClient.get(routes.api.getBranchOfficesList);
    return this.toBranchOfficeFrontFormat(response.data);
  }

  private toBranchOfficeFrontFormat(branchOfficesList: []): BranchOffices {
    const formattedBranchOffices = branchOfficesList.map(
      (bracnhOffice: BranchOffice) => ({
        id: bracnhOffice.id,
        name: bracnhOffice.name,
      })
    );

    return formattedBranchOffices;
  }

  public async recoverTicket(ticketId: number): Promise<string> {
    const response = await apiClient.put(
      routes.api.admin.recoverTicket + encodeURIComponent(ticketId)
    );

    return response.data.message;
  }
}

export default TicketBasketPageAdapter;
