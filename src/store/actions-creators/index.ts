import * as LoaderActionCreators from "../actions-creators/loader";
import * as TicketActionCreators from "../actions-creators/ticket";
import * as ScreenPageTicketActionCreators from "../actions-creators/screenPageTicket";

export const ActionCreators = {
  ...LoaderActionCreators,
  ...TicketActionCreators,
  ...ScreenPageTicketActionCreators,
};
