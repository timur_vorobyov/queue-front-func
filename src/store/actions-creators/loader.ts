import { LoaderActionTypes } from "../types/loader";

export const increaseLoaderNumberValue = () => {
  return {
    type: LoaderActionTypes.INCREASE_LOADER_NUMBER,
  };
};

export const decreaseLoaderNumberValue = () => {
  return {
    type: LoaderActionTypes.DECREASE_LOADER_NUMBER,
  };
};
