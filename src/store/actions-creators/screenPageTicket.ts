import { Ticket, ScreenPageTicketActionTypes } from "../types/screenPageTicket";

export const setScreenPageIncomingTickets = (incomingTicket: Ticket) => {
  return {
    type: ScreenPageTicketActionTypes.SCREEN_PAGE_SET_INCOMING_TICKETS,
    payload: {
      incomingTicket: incomingTicket,
    },
  };
};

export const setScreenPageAcceptedTickets = (acceptedTicket: Ticket) => {
  return {
    type: ScreenPageTicketActionTypes.SCREEN_PAGE_SET_ACCEPTED_TICKETS,
    payload: {
      acceptedTicket: acceptedTicket,
    },
  };
};

export const setScreenPageRecoveredTicket = (recoveredTicket: Ticket) => {
  return {
    type: ScreenPageTicketActionTypes.SCREEN_PAGE_SET_RECOVERED_TICKET,
    payload: {
      recoveredTicket: recoveredTicket,
    },
  };
};

export const setScreenPageInitialData = (initialData: {
  incomingTickets: Ticket[];
  acceptedTickets: Ticket[];
}) => {
  return {
    type: ScreenPageTicketActionTypes.SCREEN_PAGE_SET_INITIAL_DATA,
    payload: initialData,
  };
};
