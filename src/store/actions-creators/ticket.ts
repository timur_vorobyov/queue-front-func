import { Ticket, TicketActionTypes } from "../types/ticket";

export const setIncomingTickets = (incomingTicket: Ticket) => {
  return {
    type: TicketActionTypes.SET_INCOMING_TICKETS,
    payload: {
      incomingTicket: incomingTicket,
    },
  };
};

export const setAcceptedTicket = (acceptedTicket: Ticket) => {
  return {
    type: TicketActionTypes.SET_ACCEPTED_TICKET,
    payload: {
      acceptedTicket: acceptedTicket,
    },
  };
};

export const setServingTicket = (servingTicket: Ticket) => {
  return {
    type: TicketActionTypes.SET_SERVING_TICKET,
    payload: {
      servingTicket: servingTicket,
    },
  };
};

export const setRecoveredTicket = (recoveredTicket: Ticket) => {
  return {
    type: TicketActionTypes.SET_RECOVERED_TICKET,
    payload: {
      recoveredTicket: recoveredTicket,
    },
  };
};

export const setInitialData = (initialData: {
  incomingTickets: Ticket[];
  acceptedTicket: Ticket;
  servingTicket: Ticket;
}) => {
  return {
    type: TicketActionTypes.SET_INITIAL_DATA,
    payload: initialData,
  };
};

export const setLoadingStatus = (loadingStatus: boolean) => {
  return {
    type: TicketActionTypes.SET_LOADING_STATUS,
    payload: {
      loadingStatus: loadingStatus,
    },
  };
};
