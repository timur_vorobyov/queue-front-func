import { combineReducers } from "redux";
import loaderReducer from "./loaderReducer";
import ticketReducer from "./ticketReducer";
import screenPageTicketReducer from "./screenPageTicketReducer";

export const rootReducer = combineReducers({
  ticket: ticketReducer,
  screenPageTicket: screenPageTicketReducer,
  loader: loaderReducer,
});

export type RootState = ReturnType<typeof rootReducer>;
