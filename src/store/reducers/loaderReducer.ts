import { LoaderAction, LoaderActionTypes, LoaderState } from "../types/loader";

const initialState: LoaderState = {
  loaderNumber: 0,
};

export default function (state = initialState, action: LoaderAction) {
  switch (action.type) {
    case LoaderActionTypes.INCREASE_LOADER_NUMBER:
      const increasedLoaderNumber = state.loaderNumber + 1;
      return { ...state, loaderNumber: increasedLoaderNumber };
    case LoaderActionTypes.DECREASE_LOADER_NUMBER:
      const decreasedLoaderNumber = state.loaderNumber - 1;
      return { ...state, loaderNumber: decreasedLoaderNumber };
    default:
      return state;
  }
}
