import {
  Ticket,
  ScreenPageTicketAction,
  ScreenPageTicketActionTypes,
  ScreenPageTicketState,
} from "../types/screenPageTicket";

const initialState: ScreenPageTicketState = {
  incomingTickets: [] as Ticket[],
  acceptedTickets: [] as Ticket[],
};

export default function (state = initialState, action: ScreenPageTicketAction) {
  switch (action.type) {
    case ScreenPageTicketActionTypes.SCREEN_PAGE_SET_INCOMING_TICKETS:
      let updatedIncomingTickets = [...state.incomingTickets];
      const isIncomingTicketExist = updatedIncomingTickets.find(
        (incomingTicket) =>
          incomingTicket.id === action.payload.incomingTicket.id
      );

      if (isIncomingTicketExist) {
        updatedIncomingTickets = state.incomingTickets.filter(
          (incomingTicket) =>
            incomingTicket.id !== action.payload.incomingTicket.id
        );
      } else {
        updatedIncomingTickets = [
          ...state.incomingTickets,
          action.payload.incomingTicket,
        ];
      }

      const sortedBySalomCacheIncomingTicketsCards = sortBySalomCache(
        updatedIncomingTickets
      );
      const sortedByVisitPurposeIncomingTicketsCards = sortByVisitPurpose(
        sortedBySalomCacheIncomingTicketsCards
      );

      return {
        ...state,
        incomingTickets: sortedByVisitPurposeIncomingTicketsCards,
      };
    case ScreenPageTicketActionTypes.SCREEN_PAGE_SET_RECOVERED_TICKET:
      const recoveredTickets = [...state.incomingTickets];
      recoveredTickets.unshift(action.payload.recoveredTicket);
      return { ...state, incomingTickets: recoveredTickets };
    case ScreenPageTicketActionTypes.SCREEN_PAGE_SET_ACCEPTED_TICKETS:
      let updatedAcceptedTickets = [...state.acceptedTickets];

      const isAcceptedTicketExist = updatedAcceptedTickets.find(
        (acceptedTicket) =>
          acceptedTicket.id === action.payload.acceptedTicket.id
      );

      if (isAcceptedTicketExist) {
        updatedAcceptedTickets = state.acceptedTickets.filter(
          (acceptedTicket) =>
            acceptedTicket.id !== action.payload.acceptedTicket.id
        );
      } else {
        updatedAcceptedTickets = [
          ...state.acceptedTickets,
          action.payload.acceptedTicket,
        ];
      }

      return { ...state, acceptedTickets: updatedAcceptedTickets };
    case ScreenPageTicketActionTypes.SCREEN_PAGE_SET_INITIAL_DATA:
      return {
        ...state,
        acceptedTickets: action.payload.acceptedTickets,
        incomingTickets: action.payload.incomingTickets,
      };
    default:
      return state;
  }
}

const sortBySalomCache = (tickets: Ticket[]) => {
  const sortedBySalomCacheTickets = tickets.sort((x, y) => {
    if (x.isSalomCache === y.isSalomCache) return 0;
    if (x.isSalomCache) return -1;
    return 1;
  });
  return sortedBySalomCacheTickets;
};

const sortByVisitPurpose = (tickets: Ticket[]) => {
  const sortedByVisitPurposeTickets = tickets.sort((x, y) => {
    if (x.isVisitPurposePrioratised === y.isVisitPurposePrioratised) return 0;
    if (x.isVisitPurposePrioratised) return -1;
    return 1;
  });

  return sortedByVisitPurposeTickets;
};
