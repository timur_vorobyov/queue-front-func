import {
  TicketAction,
  TicketState,
  TicketActionTypes,
  Ticket,
} from "../types/ticket";

const initialState: TicketState = {
  loadingStatus: false,
  incomingTickets: [] as Ticket[],
  acceptedTicket: {} as Ticket,
  servingTicket: {} as Ticket,
};

export default function (state = initialState, action: TicketAction) {
  switch (action.type) {
    case TicketActionTypes.SET_INCOMING_TICKETS:
      let updatedIncomingTickets = [...state.incomingTickets];

      const isIncomingTicketExist = updatedIncomingTickets.find(
        (incomingTicket) =>
          incomingTicket.id === action.payload.incomingTicket.id
      );

      if (isIncomingTicketExist) {
        updatedIncomingTickets = state.incomingTickets.filter(
          (incomingTicket) =>
            incomingTicket.id !== action.payload.incomingTicket.id
        );
      } else {
        updatedIncomingTickets = [
          ...state.incomingTickets,
          action.payload.incomingTicket,
        ];
      }

      const sortedBySalomCacheIncomingTicketsCards = sortBySalomCache(
        updatedIncomingTickets
      );
      const sortedByVisitPurposeIncomingTicketsCards = sortByVisitPurpose(
        sortedBySalomCacheIncomingTicketsCards
      );

      return {
        ...state,
        incomingTickets: sortedByVisitPurposeIncomingTicketsCards,
      };
    case TicketActionTypes.SET_RECOVERED_TICKET:
      const recoveredTickets = [...state.incomingTickets];
      recoveredTickets.unshift(action.payload.recoveredTicket);
      return {
        ...state,
        incomingTickets: recoveredTickets,
        loadingStatus: false,
      };
    case TicketActionTypes.SET_ACCEPTED_TICKET:
      if (Object.keys(action.payload.acceptedTicket).length) {
        return {
          ...state,
          acceptedTicket: action.payload.acceptedTicket,
          loadingStatus: false,
        };
      } else {
        return {
          ...state,
          acceptedTicket: action.payload.acceptedTicket,
        };
      }
    case TicketActionTypes.SET_SERVING_TICKET:
      if (Object.keys(action.payload.servingTicket).length) {
        return {
          ...state,
          servingTicket: action.payload.servingTicket,
          loadingStatus: false,
        };
      } else {
        return {
          ...state,
          servingTicket: action.payload.servingTicket,
        };
      }
    case TicketActionTypes.SET_INITIAL_DATA:
      return {
        ...state,
        servingTicket: action.payload.servingTicket,
        acceptedTicket: action.payload.acceptedTicket,
        incomingTickets: action.payload.incomingTickets,
      };

    case TicketActionTypes.SET_LOADING_STATUS:
      return {
        ...state,
        loadingStatus: action.payload.loadingStatus,
      };
    default:
      return state;
  }
}

const sortBySalomCache = (tickets: Ticket[]) => {
  const sortedBySalomCacheTickets = tickets.sort((x, y) => {
    if (x.isSalomCache === y.isSalomCache) return 0;
    if (x.isSalomCache) return -1;
    return 1;
  });
  return sortedBySalomCacheTickets;
};

const sortByVisitPurpose = (tickets: Ticket[]) => {
  const sortedByVisitPurposeTickets = tickets.sort((x, y) => {
    if (x.isVisitPurposePrioratised === y.isVisitPurposePrioratised) return 0;
    if (x.isVisitPurposePrioratised) return -1;
    return 1;
  });

  return sortedByVisitPurposeTickets;
};
