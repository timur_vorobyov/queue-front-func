export enum LoaderActionTypes {
  INCREASE_LOADER_NUMBER = "INCREASE_LOADER_NUMBER",
  DECREASE_LOADER_NUMBER = "DECREASE_LOADER_NUMBER",
}

type IncreaseLoaderNumberAction = {
  type: LoaderActionTypes.INCREASE_LOADER_NUMBER;
};

type DecreaseLoaderNumberAction = {
  type: LoaderActionTypes.DECREASE_LOADER_NUMBER;
};

export type LoaderState = {
  loaderNumber: number;
};

export type LoaderAction =
  | IncreaseLoaderNumberAction
  | DecreaseLoaderNumberAction;
