export enum ScreenPageTicketActionTypes {
  SCREEN_PAGE_SET_INCOMING_TICKETS = "SCREEN_PAGE_SET_INCOMING_TICKETS",
  SCREEN_PAGE_SET_ACCEPTED_TICKETS = "SCREEN_PAGE_SET_ACCEPTED_TICKETS",
  SCREEN_PAGE_SET_RECOVERED_TICKET = "SCREEN_PAGE_SET_RECOVERED_TICKET",
  SCREEN_PAGE_SET_INITIAL_DATA = "SCREEN_PAGE_SET_INITIAL_DATA",
}

type ScreenPageSetIncomingTickets = {
  type: ScreenPageTicketActionTypes.SCREEN_PAGE_SET_INCOMING_TICKETS;
  payload: {
    incomingTicket: Ticket;
  };
};

type ScreenPageSetAcceptedTickets = {
  type: ScreenPageTicketActionTypes.SCREEN_PAGE_SET_ACCEPTED_TICKETS;
  payload: {
    acceptedTicket: Ticket;
  };
};

type ScreenPageSetRecoveredTicket = {
  type: ScreenPageTicketActionTypes.SCREEN_PAGE_SET_RECOVERED_TICKET;
  payload: {
    recoveredTicket: Ticket;
  };
};

type ScreenPageSetInitialData = {
  type: ScreenPageTicketActionTypes.SCREEN_PAGE_SET_INITIAL_DATA;
  payload: {
    incomingTickets: Ticket[];
    acceptedTickets: Ticket[];
  };
};

export type ScreenPageTicketState = {
  incomingTickets: Ticket[];
  acceptedTickets: Ticket[];
};

export type ScreenPageTicketAction =
  | ScreenPageSetIncomingTickets
  | ScreenPageSetAcceptedTickets
  | ScreenPageSetRecoveredTicket
  | ScreenPageSetInitialData;

export type Ticket = {
  id: string;
  ticketFullNumber: string;
  deskNumber: string;
  isSalomCache: boolean;
  isVisitPurposePrioratised: boolean;
  isCashRegister: boolean;
};
