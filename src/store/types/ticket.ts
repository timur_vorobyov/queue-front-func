export enum TicketActionTypes {
  SET_INCOMING_TICKETS = "SET_INCOMING_TICKETS",
  SET_ACCEPTED_TICKET = "SET_ACCEPTED_TICKET",
  SET_SERVING_TICKET = "SET_SERVING_TICKET",
  SET_RECOVERED_TICKET = "SET_RECOVERED_TICKET",
  SET_INITIAL_DATA = "SET_INITIAL_DATA",
  SET_LOADING_STATUS = "SET_LOADING_STATUS",
}

type SetIncomingTickets = {
  type: TicketActionTypes.SET_INCOMING_TICKETS;
  payload: {
    incomingTicket: Ticket;
  };
};

type SetAcceptedTicket = {
  type: TicketActionTypes.SET_ACCEPTED_TICKET;
  payload: {
    acceptedTicket: AcceptedTicket;
  };
};

type SetServingTicket = {
  type: TicketActionTypes.SET_SERVING_TICKET;
  payload: {
    servingTicket: ServingTicket;
  };
};

type SetRecoveredTicket = {
  type: TicketActionTypes.SET_RECOVERED_TICKET;
  payload: {
    recoveredTicket: Ticket;
  };
};

type SetInitialData = {
  type: TicketActionTypes.SET_INITIAL_DATA;
  payload: {
    incomingTickets: Ticket[];
    acceptedTicket: AcceptedTicket;
    servingTicket: ServingTicket;
  };
};

type SetLoadingStatus = {
  type: TicketActionTypes.SET_LOADING_STATUS;
  payload: {
    loadingStatus: boolean;
  };
};

export type TicketState = {
  loadingStatus: boolean;
  incomingTickets: Ticket[];
  acceptedTicket: AcceptedTicket;
  servingTicket: ServingTicket;
};

export type TicketAction =
  | SetIncomingTickets
  | SetAcceptedTicket
  | SetServingTicket
  | SetRecoveredTicket
  | SetInitialData
  | SetLoadingStatus;

export type Ticket = {
  id: number;
  crmCustomerId: number;
  ticketFullNumber: string;
  customerFullName: string;
  customerPhoneNumber: string;
  createdAt: string;
  pendingTime: string;
  visitPurposeName: string;
  visitPurposeId: number;
  isSalomCache: boolean;
  isVisitPurposePrioratised: boolean;
  deskNumber: string;
};

export interface ServingTicket extends Ticket {
  subVisitPurpose: {
    id: number;
    name: string;
  };
}

export type AcceptedTicket = ServingTicket;
