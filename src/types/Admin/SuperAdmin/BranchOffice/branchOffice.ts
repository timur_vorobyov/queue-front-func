import { Town } from "../town";
import { VisitPurpose } from "../../visitPurpose";

export type BranchOffice = {
  id: number;
  name: string;
  town: Town;
  initial: string;
  timer: number;
  visitPurposes: VisitPurpose[];
};

export type BranchOfficeInModal = {
  townId: number;
  name: string;
  initial: string;
  timer: number;
  visitPurposesIds: number[];
  id: number;
};
