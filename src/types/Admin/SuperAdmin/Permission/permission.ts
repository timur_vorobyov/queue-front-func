export type Permission = {
  id: number;
  name: string;
};

export type PermissionInModal = Permission;
