import { Permission } from "./permission";

export type Role = {
  id: number;
  name: string;
  permissions: Permission[];
};

export type RoleInModal = {
  id: number;
  name: string;
  permissionIds: number[];
};
