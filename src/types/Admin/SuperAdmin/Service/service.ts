export type Service = {
  id: number;
  name: string;
  isActive: boolean;
};

export type ServiceInModal = Service;
