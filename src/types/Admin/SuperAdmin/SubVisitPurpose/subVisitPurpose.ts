export type SubVisitPurpose = {
  id: number;
  name: string;
  visitPurpose: {
    id: number;
    name: string;
  };
};

export type SubVisitPurposeInModal = {
  id: number;
  name: string;
  visitPurposeId: number;
};
