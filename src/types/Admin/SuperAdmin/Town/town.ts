export type Town = {
  id: number;
  name: string;
};

export type TownInModal = Town;
