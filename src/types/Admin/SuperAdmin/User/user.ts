export type User = {
  id: number;
  branchOffice: {
    id: number;
    name: string;
    townId: number;
  };
  visitPurposesIds: number[];
  email: string;
  deskNumber: string;
  isActive: boolean;
  isAllowedHaveSameDeskNumber: boolean;
  firstName: string;
  lastName: string;
  createdAt: string;
  role: { name: string; id: number };
};

export type UserInModal = {
  id: number;
  firstName: string;
  lastName: string;
  branchOfficeId: number;
  visitPurposesIds: number[];
  password: string;
  email: string;
  isActive: boolean;
  isAllowedHaveSameDeskNumber: boolean;
  roleId: number;
  townId: number;
};
