export type VisitPurposeType = {
  id: number;
  name: string;
};

export type VisitPurposeTypeInModal = {
  id: number;
  name: string;
};
