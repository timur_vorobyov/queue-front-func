export type BranchOffice = {
  id: number;
  name: string;
};
