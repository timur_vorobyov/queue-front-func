import { Service } from "./service";
import { VisitPurposeType } from "./visitPurposeType";

export type VisitPurpose = {
  id: number;
  name: string;
  series: string;
  services: Service[];
  visitPurposeType: VisitPurposeType;
  tjMessage: string;
  ruMessage: string;
  isPrioratised: boolean;
  isCashRegister: boolean;
};

export type VisitPurposeInModal = {
  id: number;
  name: string;
  series: string;
  servicesIds: number[];
  visitPurposeTypeId: number;
  tjMessage: string;
  ruMessage: string;
  isPrioratised: boolean;
  isCashRegister: boolean;
};
