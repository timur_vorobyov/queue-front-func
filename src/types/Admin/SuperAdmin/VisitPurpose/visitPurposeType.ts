export type VisitPurposeType = {
  id: number;
  name: string;
};
