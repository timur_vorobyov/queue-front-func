export type SubVisitPurposeDbType = {
  id: number;
  name: string;
  visit_purpose_id: number;
};

export type SubVisitPurpose = {
  id: number;
  name: string;
  visitPurposeId: number;
};

export type VisitPurpose = {
  id: number;
  name: string;
  subVisitPurposes: SubVisitPurpose[];
};
export type VisitPurposeDbType = {
  id: number;
  name: string;
  subVisitPurposes: SubVisitPurposeDbType[];
};
