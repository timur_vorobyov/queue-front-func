export interface ApiCallOptions {
  args?: any[];
  notifyOnSuccess?: string | false;
  notifyOnFail?: string | false;
  onSuccess?: (response: any) => void;
  onFail?: (error: unknown) => void;
}
