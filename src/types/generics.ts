import React from "react";

export type DateString = string;

export type Anything = any;
export type GenericFunction = (...args: any[]) => any;
export type GenericAsyncFunction = (...args: any[]) => Promise<any>;
export type GenericAsyncFunctions = (...args: any[]) => Promise<any>[];
export type Dictionary<T = any> = {
  [key: string]: T;
};
export type setStateAction = React.Dispatch<React.SetStateAction<any>>;
