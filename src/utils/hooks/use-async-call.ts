import { useState } from "react";
import { Anything, GenericAsyncFunction } from "src/types/generics";

const useApiCall = (
  asyncApiFunction: GenericAsyncFunction,
  args: any[] = []
) => {
  const [data, setData] = useState<Anything>([]);
  const [error, setError] = useState<unknown>(null);
  const [isLoading, setIsLoading] = useState(true);

  const fetchMethod = async (methodArgs: any[] = []) => {
    setIsLoading(true);

    try {
      const response = args.length
        ? await asyncApiFunction(...args)
        : await asyncApiFunction(...methodArgs);
      setData(response);
      setIsLoading(false);
    } catch (error) {
      console.error("useApiCall error", error);
      setError(error);
    }
  };

  return [data, fetchMethod, isLoading, error];
};
export default useApiCall;
