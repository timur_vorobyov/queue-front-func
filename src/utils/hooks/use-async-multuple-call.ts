import { useState } from "react";

const useMultipleApiCalls = (
  asyncApiFunctions: Array<any>,
  args: Array<any> = []
) => {
  const [data, setData] = useState<any>([]);
  const [error, setError] = useState<unknown>(null);

  const fetchMethod = async () => {
    try {
      const response = await Promise.all(
        asyncApiFunctions.map((asyncApiFunction) => {
          const cutrrentFunIndex = asyncApiFunctions.indexOf(asyncApiFunction);
          return args.length
            ? asyncApiFunction(...args[cutrrentFunIndex])
            : asyncApiFunction();
        })
      );
      setData(response);
    } catch (error) {
      console.error("useeMultipleApiCall error", error);
      setError(error);
    }
  };

  return [data, fetchMethod, error];
};
export default useMultipleApiCalls;
