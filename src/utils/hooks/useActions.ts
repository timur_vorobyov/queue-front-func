import { useDispatch } from "react-redux";
import { bindActionCreators } from "redux";
import { ActionCreators } from "src/store/actions-creators";

export const useActions = () => {
  const dispatch = useDispatch();
  return bindActionCreators(ActionCreators, dispatch);
};
