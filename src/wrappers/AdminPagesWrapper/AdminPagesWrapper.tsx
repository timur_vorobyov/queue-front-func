import React from "react";
import { useState } from "react";
import { PaginationData } from "src/components/Pagination/Pagination";
import { MODAL_TYPE } from "src/pages/Admin/SuperAdminPages/consts";
import {
  ModalProperties,
  ModalPropertiesState,
  PageLimitState,
  PaginationDataState,
  SearchExpressionState,
} from "src/pages/Admin/SuperAdminPages/types";
import { ModalDeletePropertiesState } from "src/pages/Admin/types";

export interface AdminPagesWrapperProps {
  modalPropertiesState: ModalPropertiesState;
  pageLimitState: PageLimitState;
  paginationDataState: PaginationDataState;
  searchExpressionState: SearchExpressionState;
  modalDeletePropertiesState: ModalDeletePropertiesState;
}

const AdminPagesWrapper = (
  Component: (props: AdminPagesWrapperProps) => JSX.Element
) => {
  const [pageLimit, setPageLimit] = useState<number>(5);
  const [searchExpression, setSearchExpression] = useState<string>("");
  const [paginationData, setPaginationData] = useState<PaginationData>({
    currentPage: 0,
    totalPages: 0,
    totalRecords: 0,
  });
  const [modalProperties, setModalProperties] = useState<ModalProperties>({
    isModalShown: false,
    action: MODAL_TYPE.CREATE.NAME,
  });
  const [isDeleteModalShown, setIsDeleteModalShown] = useState<boolean>(false);

  return (
    <Component
      modalPropertiesState={{
        modalProperties: modalProperties,
        setModalProperties: setModalProperties,
      }}
      pageLimitState={{ pageLimit: pageLimit, setPageLimit: setPageLimit }}
      paginationDataState={{
        paginationData: paginationData,
        setPaginationData: setPaginationData,
      }}
      searchExpressionState={{
        searchExpression: searchExpression,
        setSearchExpression: setSearchExpression,
      }}
      modalDeletePropertiesState={{
        isDeleteModalShown: isDeleteModalShown,
        setIsDeleteModalShown: setIsDeleteModalShown,
      }}
    />
  );
};

export default AdminPagesWrapper;
