import React, { PureComponent } from "react";
import cn from "clsx";
import { BrowserRouter } from "react-router-dom";
import "src/styles/global.css";
interface AppWrapperProps {
  children: React.ReactChild;
}
class AppWrapper extends PureComponent<AppWrapperProps> {
  render(): React.ReactNode {
    return (
      /* @ts-ignore */
      <BrowserRouter>
        <div className={cn(`theme`)}>{this.props.children}</div>
      </BrowserRouter>
    );
  }
}

export default AppWrapper;
